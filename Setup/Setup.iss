; -- Based on Example3.iss --
; Same as Example1.iss, but creates some registry entries too.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=SMAC_Salesman_GUI
AppVersion=0.26

DefaultDirName={pf}\SMAC\Salesman GUI
DefaultGroupName=Salesman GUI
UninstallDisplayIcon={app}\SMAC\Salesman GUI.exe
OutputDir=.\Output

[Files]
Source: "Files\Salesman GUI.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\System macros V2.mlm"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Program file.lcc"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Auto Sequence.lcc"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Actuators\*.*"; DestDir: "{app}\Actuators"; Flags: ignoreversion

; Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme

[Icons]
;Name: "{group}\My Program"; Filename: "{app}\MyProg.exe"
Name: "{commonprograms}\LCC Control Center"; Filename: "{app}\LCC Control Center.exe"
Name: "{commondesktop}\LCC Control Center"; Filename: "{app}\LCC Control Center.exe"

[Run]
Filename: "{app}\Salesman GUI.exe"; Description: "Launch Salesman GUI"; Flags: postinstall nowait skipifsilent 
