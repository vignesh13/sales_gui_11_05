﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using System.Threading;

namespace Salesman_GUI
{
    class Serial
    {
        private SerialPort port = new SerialPort();
        Utils utils = new Utils();  
        private SerialDataReceivedEventHandler theHandler;
        private string buffer = "";

        //Connects to Serial Port based on Baud Rate and Port Name.  All other settings are static:
        //No handshaking, no parity, one stop bit, 500ms write timeout, 250ms read timeout, 8 data bits

        public void Open(string strPortname, int iBaudrate, SerialDataReceivedEventHandler handler)
        {
            try
            {
                port.PortName = strPortname;
                port.ReceivedBytesThreshold = 1;    
                port.BaudRate = iBaudrate;
                port.DataBits = 8;
                port.WriteTimeout = 500;
                port.ReadTimeout = 500;
                port.Handshake = System.IO.Ports.Handshake.None;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.NewLine = "\r";            // Must be forced to be a single CR character
                if (!port.IsOpen)
                {
                    port.Open();
                    Thread.Sleep(100);  // Some delay

                    while (port.BytesToRead > 0)
                    {
                        port.ReadByte();
                    }
                    theHandler = new SerialDataReceivedEventHandler(handler);
                    port.DataReceived += theHandler;       // Add the received handler
                    buffer = "";
                }
                else
                {
                    MessageBox.Show("Port Already Opened");
                }
            }
            catch
            {
                MessageBox.Show("Port already opened by another program or device.  Please close all other Serial Connections before continuing.");
            }
        }

 
        public bool Overflow()
        {
            return (buffer.Length > 500);
        }

        public bool CompleteLineReceived()
        {
            //
            // First copy all available bytes to the buffer
            //

            if (buffer.IndexOf('\r') > 0)
            {
                return true;
            }

            char c = ' ';

            while (port.IsOpen && (port.BytesToRead > 0) && (c != '\r'))
            {
                c = (char)port.ReadChar();
                buffer += c;
            }
//            Globals.logSerial.Add(buffer+"\n");
            return (buffer.IndexOf('\r') > 0);
        }



        public string ReadLine()
        {
            string strTemp = "";
            if (port.IsOpen)
            {
                if (CompleteLineReceived())
                {
                    int i = buffer.IndexOf('\r');
                    strTemp = buffer.Substring(0, i);
                    buffer = buffer.Substring(i + 1);
                }
                else
                {
                    strTemp = buffer + port.ReadLine();
                    buffer = "";
                }
                strTemp = strTemp.Trim();       // TODO: check if this is necessary, seems that ReadLine() also returns the Newline Character
                Globals.logSerial.Add("< " + strTemp + "\n");
            }
            return strTemp;
        }

        public void WriteString(string strStringToWrite)
        {
            if (port.IsOpen)
            {
                Globals.logSerial.Add("> " + strStringToWrite + "\n");
                port.WriteLine(strStringToWrite);
            }
        }

        public void RequestObject(int node, int index)
        {
            WriteString(node.ToString() + " R 0X" + index.ToString("X6"));
        }

        public void RequestObject(int node, int index, int subindex)
        {
            RequestObject(node, (subindex << 16) + index);
        }


        //
        // GetObjectValue:
        // Reads the requested object from the controller
        //
        // Example: GetObjectValue(10, 0x6040, iValue)
        //
        // Writes 10 R 0x6400
        // Waits until command ss W 0x6400 dddd is received ss and dddd may be in decimal or hex format
        // returns dddd in iObjectValue
        //
        
        public bool GetObjectValue(int node, int iObject, ref Int64 iObjectValue)
        {
            bool bDone = false;
            string strResponse;
            int tryCount = 0;

            try  // This try/catch construction is necessary to prevent error messages from ReadLine()
            {
                WriteString(node.ToString() + " R 0x" + iObject.ToString("X"));
                do
                {
                    strResponse = ReadLine().ToUpper();
                    if (strResponse.Split()[1] == "W")
                    {
                        int receivedNode = (int)utils.ConvertStringToInt64(strResponse.Split()[0]);
                        int receivedObject = (int)utils.ConvertStringToInt64(strResponse.Split()[2]);
                        if ((receivedNode == node) && (receivedObject == iObject))
                        {
                            iObjectValue = utils.ConvertStringToInt64(strResponse.Split()[3],
                                                                      Globals.dictionary.ObjectSize(iObject),
                                                                      Globals.dictionary.ObjecIsSignedvalue(iObject));
                            bDone = true;
                        }
                        else
                        {
                            Globals.logSerial.Add("Skipped" + strResponse);
                            tryCount++;
                            if (tryCount == 100)     // Then the serial command is not answered
                            {
                                switch (MessageBox.Show("The controller keeps sending data and does not respond on requests\n\nSelect Yes to stop macro execution\nSelect No to cancel the connection", "Controller not responding", MessageBoxButtons.YesNo, MessageBoxIcon.Error))
                                {
                                    case DialogResult.Yes:
                                        WriteString(node.ToString() + " W 0x32c02 0");      // End program
                                        Thread.Sleep(500);                                  // Some time to stop the program
                                        port.DiscardInBuffer();
                                        break;
                                    case DialogResult.No:
                                        if (Globals.mainform.bConnected)
                                        {
                                            Globals.mainform.btnConnect_Click(null, null);
                                        }
                                        break;
                                }
                                return false;
                            }
                        }
                    }
                }
                while (!bDone);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool GetObjectValue(int node, int index, int subindex, ref Int64 value)
        {
            return GetObjectValue(node, (subindex << 16) + index, ref value);
        }

        public void WriteObject(int node, int index, Int64 value)
        {
            WriteString(node.ToString() + " W 0x" + index.ToString("X6") + " " + value.ToString());
            if ((index == 0x12503) || (index == 0x22503))   // If Torque control parameter set - Proportional or Integral constant
            {                                               // Then Write same value to Flux control parameter set (index is 1 lower)
                Thread.Sleep(Globals.mainform.NetworkDelay());           // This deley is necessary in daisy chain mode to avoid buffer overflows
                WriteString(node.ToString() + " W 0x" + (index - 1).ToString("X6") + " " + value.ToString());
            }
        }

        public void WriteObject(int node, int index, int subindex, Int64 value)
        {
             WriteObject(node, (subindex << 16) + index, value);
        }
 
        public bool IsOpen()
        {
            return port.IsOpen;
        }

        public void Close()
        {
            if (port.IsOpen)
            {

                port.DataReceived -= theHandler;    // Get rid of the handler
                theHandler = null;
                port.Close();
                Thread.Sleep(100);  // Wait some time
            }
        }

    }
}
