﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class IfSequence : Sequence
    {


        private string parms;   // The parameter string

        private string ifType = "";
        private string comparison = "";
        private string activeState = "";
        private string thenAction = "";
        private string elseAction = "";
        private string argument = "";
        private string variable = "";
        private string variable2 = "";



        private const string STR_PREFIX_VAR = "Var";
        private const string STR_PREFIX_VAR2 = "Var2";
        private const string STR_PREFIX_CONSTANT = "Const";

        private const string STR_VARIABLE = "Variable";
        private const string STR_VARIABLE_2 = "Variable 2";


        private Int32 constant = 0;
        private Int32 threshold;
        private Int32 thenMacronumber;
        private Int32 elseMacronumber;
       

        public string[] ifTypes = {
                                    Globals.STR_TARGET_REACHED,
                                    Globals.STR_TRAJECTORY_GENERATOR_READY,
                                    Globals.STR_ACTUAL_POSITION,
                                    Globals.STR_ACTUAL_VELOCITY,
                                    Globals.STR_ACTUAL_FORCE,
                                    Globals.STR_DIGITAL_INPUT_0,
                                    Globals.STR_DIGITAL_INPUT_1,
                                    Globals.STR_DIGITAL_INPUT_2,
                                    Globals.STR_DIGITAL_INPUT_3,
                                    Globals.STR_ANALOG_INPUT_1,
                                    Globals.STR_ANALOG_INPUT_2,
                                    Globals.STR_VARIABLE
                                  };

        private string[] comparisons1 = {
                                           Globals.STR_ABOVE,
                                           Globals.STR_BELOW
                                       };

        private string[] comparisons2 = {
                                           Globals.STR_ABOVE,
                                           Globals.STR_BELOW,
                                           Globals.STR_EQUAL,
                                           Globals.STR_UNEQUAL
                                       };

        private string[] activeStates = {
                                            Globals.STR_ON,
                                            Globals.STR_OFF
                                        };

        private string[]   ifActions = {
                                           Globals.STR_MACRO_CALL,
                                           Globals.STR_MACRO_JUMP,
                                           Globals.STR_MACRO_RETURN,
                                           Globals.STR_MACRO_CONTINUE,
                                           Globals.STR_END_PROGRAM
                                       };

        private string[] arguments = {
                                         Globals.STR_CONSTANT,
                                         Globals.STR_VARIABLE
                                     };

        private UserCombobox UserComboboxIftype;
        private UserCombobox UserComboboxcomparisons1;
        private UserCombobox UserComboboxcomparisons2;
        private UserCombobox UserComboboxActivestates;
        private UserCombobox UserComboboxThenactions;
        private UserCombobox UserComboboxElseactions;
        private UserCombobox UserComboboxArguments;

        private UserTextbox UserTextboxPositionthreshold;
        private UserTextbox UserTextboxVelocitythreshold;
        private UserTextbox UserTextboxForcethreshold;
        private UserTextbox UserTextboxAnalogvaluethreshold;
        private UserTextbox UserTextboxThenMacronumber;
        private UserTextbox UserTextboxElseMacronumber;
        private UserTextbox UserTextboxConstant;

        private UserObjectselector UserObjectselectorVariable1;
        private UserObjectselector UserObjectselectorVariable2;

        private bool bCheck = true;






        public IfSequence()    // The constructor
        {
            name = "If";
            mnemonic = "If";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, ifTypes);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, comparisons1);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox3, activeStates);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox4, ifActions);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox7, ifActions);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox5, comparisons2);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox6, arguments);

            UserComboboxIftype = new UserCombobox(Globals.mainform.lblName1, Globals.STR_CONDITION, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxcomparisons1 = new UserCombobox(Globals.mainform.lblName2, "", Globals.mainform.ComboBox2, Globals.mainform.lblUnits2, "");
            UserComboboxActivestates = new UserCombobox(Globals.mainform.lblName3, "", Globals.mainform.ComboBox3, Globals.mainform.lblUnits3, "");
            UserComboboxThenactions = new UserCombobox(Globals.mainform.lblName4, Globals.STR_THEN, Globals.mainform.ComboBox4, Globals.mainform.lblUnits4, "");
            UserComboboxElseactions = new UserCombobox(Globals.mainform.lblName15, Globals.STR_ELSE, Globals.mainform.ComboBox7, Globals.mainform.lblUnits15, "");
            UserComboboxcomparisons2 = new UserCombobox(Globals.mainform.lblName10, "", Globals.mainform.ComboBox5, Globals.mainform.lblUnits10, "");
            UserComboboxArguments = new UserCombobox(Globals.mainform.lblName11, "", Globals.mainform.ComboBox6, Globals.mainform.lblUnits11, "");

            UserTextboxPositionthreshold = new UserTextbox(Globals.mainform.lblName5, Globals.STR_THRESHOLD, Globals.mainform.txtParm1, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits5, Globals.STR_COUNTS, true);
            UserTextboxVelocitythreshold = new UserTextbox(Globals.mainform.lblName6, Globals.STR_THRESHOLD, Globals.mainform.txtParm2, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits6, Globals.STR_COUNTS_PER_SEC, true);
            UserTextboxForcethreshold = new UserTextbox(Globals.mainform.lblName7, Globals.STR_THRESHOLD, Globals.mainform.txtParm3, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits7, Globals.STR_PROMILLE_OF_RATED_FORCE, true);
            UserTextboxAnalogvaluethreshold = new UserTextbox(Globals.mainform.lblName8, Globals.STR_THRESHOLD, Globals.mainform.txtParm4, 0, 0, 1023, Globals.mainform.lblUnits8, "", true);
            UserTextboxThenMacronumber = new UserTextbox(Globals.mainform.lblName9, Globals.STR_MACRO_NUMBER, Globals.mainform.txtParm5, 0, 0, 59, Globals.mainform.lblUnits9, "", true);
            UserTextboxElseMacronumber = new UserTextbox(Globals.mainform.lblName16, Globals.STR_MACRO_NUMBER, Globals.mainform.txtParm7, 0, 0, 59, Globals.mainform.lblUnits16, "", true);
            UserTextboxConstant = new UserTextbox(Globals.mainform.lblName12, Globals.STR_CONSTANT, Globals.mainform.txtParm6, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits12, "", true);

            UserObjectselectorVariable1 = new UserObjectselector(Globals.mainform.lblName13, STR_VARIABLE, Globals.mainform.btnSelect1, Globals.mainform.lblUnits13, "", true, false);
            UserObjectselectorVariable2 = new UserObjectselector(Globals.mainform.lblName14, STR_VARIABLE_2, Globals.mainform.btnSelect2, Globals.mainform.lblUnits14, "", true, false);

            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxIftype = null;
            UserComboboxcomparisons1 = null;
            UserComboboxActivestates = null;
            UserComboboxThenactions = null;
            UserComboboxElseactions = null;
            UserComboboxcomparisons2 = null;
            UserComboboxArguments = null;


            UserTextboxPositionthreshold = null;
            UserTextboxVelocitythreshold = null;
            UserTextboxForcethreshold = null;
            UserTextboxAnalogvaluethreshold = null;
            UserTextboxConstant = null;
            UserTextboxThenMacronumber = null;
            UserTextboxElseMacronumber = null;

            UserObjectselectorVariable1 = null;
            UserObjectselectorVariable2 = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            //
            // Set the deafults
            //

            threshold = 0;
            thenMacronumber = 0;
            elseMacronumber = 0;
            constant = 0;
            variable = "";
            variable2 = "";
            ifType = Globals.STR_TARGET_REACHED;
            thenAction = Globals.STR_MACRO_CALL;
            elseAction = Globals.STR_MACRO_CONTINUE;

            if (parms != "")       // Then parse the parameter string
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_TARGET_REACHED:
                        case Globals.STR_TRAJECTORY_GENERATOR_READY:
                        case Globals.STR_ACTUAL_POSITION:
                        case Globals.STR_ACTUAL_VELOCITY:
                        case Globals.STR_ACTUAL_FORCE:
                        case Globals.STR_DIGITAL_INPUT_0:
                        case Globals.STR_DIGITAL_INPUT_1:
                        case Globals.STR_DIGITAL_INPUT_2:
                        case Globals.STR_DIGITAL_INPUT_3:
                        case Globals.STR_ANALOG_INPUT_1:
                        case Globals.STR_ANALOG_INPUT_2:
                        case Globals.STR_VARIABLE:
                            ifType = parameterName;
                            break;
                        case Globals.STR_MACRO_CALL:            // Keep these in place to support older versions of LCC Control Center
                        case Globals.STR_MACRO_JUMP:
                        case Globals.STR_MACRO_RETURN:
                        case Globals.STR_END_PROGRAM:
                            thenAction = parameterName;
                            break;

                        case Globals.STR_ABOVE:
                        case Globals.STR_BELOW:
                        case Globals.STR_EQUAL:
                        case Globals.STR_UNEQUAL:
                            comparison = parameterName;
                            break;

                        case Globals.STR_ON:
                        case Globals.STR_OFF:
                            activeState = parameterName;
                            break;

                        case Globals.STR_PREFIX_THRESHOLD:      // This is a mandatory parameter
                            threshold = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_MACRO_NUMBER:   // Keep this in place to stay compatible with older versions of LCC Control Center
                            thenMacronumber = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case STR_PREFIX_VAR:
                            variable = parameterValue;
                            break;

                        case STR_PREFIX_VAR2:
                            variable2 = parameterValue;
                            argument = Globals.STR_VARIABLE;
                            break;

                        case STR_PREFIX_CONSTANT:
                            argument = Globals.STR_CONSTANT;
                            constant = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_THEN_MACRO_CALL:
                            thenAction = Globals.STR_MACRO_CALL;
                            thenMacronumber = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_THEN_MACRO_JUMP:
                            thenMacronumber = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            thenAction = Globals.STR_MACRO_JUMP;
                            break;

                        case Globals.STR_PREFIX_THEN_MACRO_RETURN:
                            thenAction = Globals.STR_MACRO_RETURN;
                            break;

                        case Globals.STR_PREFIX_THEN_MACRO_CONTINUE:
                            thenAction = Globals.STR_MACRO_CONTINUE;
                            break;

                        case Globals.STR_PREFIX_THEN_END_PROGRAM:
                            thenAction = Globals.STR_END_PROGRAM;
                            break;

                        case Globals.STR_PREFIX_ELSE_MACRO_CALL:
                            elseAction = Globals.STR_MACRO_CALL;
                            elseMacronumber = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_ELSE_MACRO_JUMP:
                            elseAction = Globals.STR_MACRO_JUMP;
                            elseMacronumber = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_ELSE_MACRO_RETURN:
                            elseAction = Globals.STR_MACRO_RETURN;
                            break;

                        case Globals.STR_PREFIX_ELSE_MACRO_CONTINUE:
                            elseAction = Globals.STR_MACRO_CONTINUE;
                            break;

                        case Globals.STR_PREFIX_ELSE_END_PROGRAM:
                            elseAction = Globals.STR_END_PROGRAM;
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }

        private void ShowMacronumber(int line)
        {
            if ((thenAction == Globals.STR_MACRO_CALL) || (thenAction == Globals.STR_MACRO_JUMP))
            {
                UserTextboxThenMacronumber.Show(line++);
            }
            UserComboboxElseactions.Show(line++);
            {
                if ((elseAction == Globals.STR_MACRO_CALL) || (elseAction == Globals.STR_MACRO_JUMP))
                {
                    UserTextboxElseMacronumber.Show(line);
                }
            }
        }

        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserComboboxIftype.Hide();
            UserComboboxcomparisons1.Hide();
            UserComboboxActivestates.Hide();
            UserComboboxThenactions.Hide();
            UserComboboxElseactions.Hide();
            UserComboboxcomparisons2.Hide();
            UserComboboxArguments.Hide();

            UserTextboxPositionthreshold.Hide();
            UserTextboxVelocitythreshold.Hide();
            UserTextboxForcethreshold.Hide();
            UserTextboxAnalogvaluethreshold.Hide();
            UserTextboxThenMacronumber.Hide();
            UserTextboxElseMacronumber.Hide();
            UserTextboxConstant.Hide();

            UserObjectselectorVariable1.Hide();
            UserObjectselectorVariable2.Hide();

            Globals.mainform.ComboBox1.SelectedItem = ifType;
            Globals.mainform.ComboBox2.SelectedItem = comparison;
            Globals.mainform.ComboBox3.SelectedItem = activeState;
            Globals.mainform.ComboBox4.SelectedItem = thenAction;
            Globals.mainform.ComboBox5.SelectedItem = comparison;
            Globals.mainform.ComboBox6.SelectedItem = argument;
            Globals.mainform.ComboBox7.SelectedItem = elseAction;

            UserTextboxPositionthreshold.SetValue(threshold.ToString());
            UserTextboxVelocitythreshold.SetValue(threshold.ToString());
            UserTextboxForcethreshold.SetValue(threshold.ToString());
            UserTextboxAnalogvaluethreshold.SetValue(threshold.ToString());
            UserTextboxThenMacronumber.SetValue(thenMacronumber.ToString());
            UserTextboxElseMacronumber.SetValue(elseMacronumber.ToString());
            UserTextboxConstant.SetValue(constant.ToString());
            UserObjectselectorVariable1.SetSelection(variable);
            UserObjectselectorVariable2.SetSelection(variable2);

            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //
            UserComboboxIftype.Show(0);       // Allways show the selected wait type
            switch (ifType)
            {
                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                            // Nothing to show
                    UserComboboxThenactions.Show(1);
                    ShowMacronumber(2);
                    break;

                case Globals.STR_ACTUAL_POSITION:
                    UserComboboxcomparisons1.Show(1);
                    UserTextboxPositionthreshold.Show(2);
                    UserComboboxThenactions.Show(3);
                    ShowMacronumber(4);
                    break;

                case Globals.STR_ACTUAL_VELOCITY:
                    UserComboboxcomparisons1.Show(1);
                    UserTextboxVelocitythreshold.Show(2);
                    UserComboboxThenactions.Show(3);
                    ShowMacronumber(4);
                    break;

                case Globals.STR_ACTUAL_FORCE:
                    UserComboboxcomparisons1.Show(1);
                    UserTextboxForcethreshold.Show(2);
                    UserComboboxThenactions.Show(3);
                    ShowMacronumber(4);
                    break;

                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    UserComboboxActivestates.Show(1);
                    UserComboboxThenactions.Show(2);
                    ShowMacronumber(3);
                    break;

                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    UserComboboxcomparisons1.Show(1);
                    UserTextboxAnalogvaluethreshold.Show(2);
                    UserComboboxThenactions.Show(3);
                    ShowMacronumber(4);
                    break;

                case Globals.STR_VARIABLE:
                    UserObjectselectorVariable1.Show(1);
                    UserComboboxcomparisons2.Show(2);
                    UserComboboxArguments.Show(3);
//                    MessageBox.Show(UserComboboxArguments.Text());
                    if (UserComboboxArguments.Text() == Globals.STR_CONSTANT)
                    {
                        UserTextboxConstant.Show(4);
                    }
                    else
                    {
                        UserObjectselectorVariable2.Show(4);
                    }
                    UserComboboxThenactions.Show(5);
                    ShowMacronumber(6);

                    break;
 
                default:
                    break;
            }


            Globals.mainform.txtComment.Text = strComment;

        }

        public override void ButtonClick(Object sender)
        {
            if (sender == Globals.mainform.btnSelect1)
            {
                UserObjectselectorVariable1.SelectObject();
            }
            if (sender == Globals.mainform.btnSelect2)
            {
                UserObjectselectorVariable2.SelectObject();
            }
        }


        public override void UserInputHasChanged(Object sender)
        {
            ifType = Globals.mainform.ComboBox1.Text;
            bCheck = false;
            UpdateParametersFromGui();
            bCheck = true;
            if (sender == Globals.mainform.ComboBox1 ||
                sender == Globals.mainform.ComboBox4 ||
                sender == Globals.mainform.ComboBox6 ||
                sender == Globals.mainform.ComboBox7)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {


            ifType = UserComboboxIftype.Text();
            activeState = UserComboboxActivestates.Text();
            argument = UserComboboxArguments.Text();
            if (ifType == Globals.STR_VARIABLE)
            {
                comparison = UserComboboxcomparisons2.Text();
            }
            else
            {
                comparison = UserComboboxcomparisons1.Text();
            }
            thenAction = UserComboboxThenactions.Text();
            elseAction = UserComboboxElseactions.Text();

            thenMacronumber = (Int32)UserTextboxThenMacronumber.Value();
            elseMacronumber = (Int32)UserTextboxElseMacronumber.Value();


            switch (ifType)
            {

                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                    break;

                case Globals.STR_ACTUAL_POSITION:
                    threshold = (Int32)UserTextboxPositionthreshold.Value();
                    break;

                case Globals.STR_ACTUAL_VELOCITY:
                    threshold = (Int32)UserTextboxVelocitythreshold.Value();
                    break;

                case Globals.STR_ACTUAL_FORCE:
                    threshold = (Int32)UserTextboxForcethreshold.Value();
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    break;

                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    threshold = (Int32)UserTextboxAnalogvaluethreshold.Value();
                    break;

                case Globals.STR_VARIABLE:
                    variable = UserObjectselectorVariable1.selectedIndexString;
                    UserObjectselectorVariable1.ShowMessageIfNotSelected(bCheck);
                    if (argument == Globals.STR_CONSTANT)
                    {
                        constant = (Int32)UserTextboxConstant.Value();
                    }
                    else
                    {
                        variable2 = UserObjectselectorVariable2.selectedIndexString;
                        UserObjectselectorVariable2.ShowMessageIfNotSelected(bCheck);
                    }
                    break;

            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {
            string temp = ifType.Replace(' ', '_');

            switch (ifType)
            {
                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                    break;

                case Globals.STR_ACTUAL_POSITION:
                case Globals.STR_ACTUAL_VELOCITY:
                case Globals.STR_ACTUAL_FORCE:
                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    temp += Globals.STR_COMMA + comparison;
                    temp += Globals.STR_COMMA + Globals.STR_PREFIX_THRESHOLD + "=" + threshold.ToString();
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    {
                        temp += Globals.STR_COMMA + activeState;
                    }
                    break;
                case Globals.STR_VARIABLE:
                    temp += Globals.STR_COMMA + STR_PREFIX_VAR + "=" + variable;
                    temp += Globals.STR_COMMA + comparison;
                    if (argument == Globals.STR_CONSTANT)
                    {
                        temp += Globals.STR_COMMA + STR_PREFIX_CONSTANT + "=";
                        temp += constant.ToString();
                    }
                    else  // It must be variable
                    {
                        temp += Globals.STR_COMMA + STR_PREFIX_VAR2 + "=" + variable2;
                    }


                    break;
            }
            temp += Globals.STR_COMMA;

            switch (thenAction)
            {
                case Globals.STR_MACRO_CALL:
                    temp += Globals.STR_PREFIX_THEN_MACRO_CALL.Replace(' ', '_') + "=" + thenMacronumber.ToString();
                    break;
                case Globals.STR_MACRO_JUMP:
                    temp += Globals.STR_PREFIX_THEN_MACRO_JUMP.Replace(' ', '_') + "=" + thenMacronumber.ToString();
                    break;
                case Globals.STR_MACRO_RETURN:
                    temp += Globals.STR_PREFIX_THEN_MACRO_RETURN.Replace(' ', '_');
                    break;
                case Globals.STR_END_PROGRAM:
                    temp += Globals.STR_PREFIX_THEN_END_PROGRAM.Replace(' ', '_');
                    break;
                case Globals.STR_MACRO_CONTINUE:
                    temp += Globals.STR_PREFIX_THEN_MACRO_CONTINUE.Replace(' ', '_');
                    break;
            }

            temp += Globals.STR_COMMA;

            switch (elseAction)
            {
                case Globals.STR_MACRO_CALL:
                    temp += Globals.STR_PREFIX_ELSE_MACRO_CALL.Replace(' ', '_') + "=" + elseMacronumber.ToString();
                    break;
                case Globals.STR_MACRO_JUMP:
                    temp += Globals.STR_PREFIX_ELSE_MACRO_JUMP.Replace(' ', '_') + "=" + elseMacronumber.ToString();
                    break;
                case Globals.STR_MACRO_RETURN:
                    temp += Globals.STR_PREFIX_ELSE_MACRO_RETURN.Replace(' ', '_');
                    break;
                case Globals.STR_MACRO_CONTINUE:
                    temp += Globals.STR_PREFIX_ELSE_MACRO_CONTINUE.Replace(' ', '_');
                    break;
                case Globals.STR_END_PROGRAM:
                    temp += Globals.STR_PREFIX_ELSE_END_PROGRAM.Replace(' ', '_');
                    break;
            }

            return temp;            
        }

        private void CreateCode()
        {

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            int registerToRead = 0;
            int inputBit = 0;
            bool Is32Bits = true;    // assume that this is a 32 bits value
            bool extendSign = false; // assume that sign extension is not necessary

            int CALL = 1;
            int RETURN = 2;
            int JUMP = 3;
            int JUMP_RELATIVE = 6;


            instructions.Clear();

            // First case is to do some preparations

            switch (ifType)
            {
                case Globals.STR_ACTUAL_POSITION:
                    registerToRead = 0x6064;
                    break;
                case Globals.STR_ACTUAL_VELOCITY:
                    registerToRead = 0x606c;
                    break;
                case Globals.STR_ACTUAL_FORCE:
                    registerToRead = 0x6077;
                    Is32Bits = false;
                    extendSign = true;
                    break;
                case Globals.STR_ANALOG_INPUT_1:
                    registerToRead = 0x12a03;
                    break;
                case Globals.STR_ANALOG_INPUT_2:
                    registerToRead = 0x22a03;
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                    inputBit = 4;
                    break;
                case Globals.STR_DIGITAL_INPUT_1:
                    inputBit = 5;
                    break;
                case Globals.STR_DIGITAL_INPUT_2:
                    inputBit = 6;
                    break;
                case Globals.STR_DIGITAL_INPUT_3:
                    inputBit = 7;
                    break;
            }

            switch (ifType)
            {
                case Globals.STR_TARGET_REACHED:
                    instructions.Add(new WriteInstruction(0x2c01, 23, 0x6041, strtemp + "Get status in ACCUM"));
                    instructions.Add(new WriteInstruction(0x2c02, 10, 10, "If Target reached bit is set"));
                    break;

                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                    instructions.Add(new WriteInstruction(0x2c01, 22, 0x6062, strtemp + "Get position demand value in ACCUM"));
                    instructions.Add(new WriteInstruction(0x2c02, 21, 0x607a, "Compare with target position"));
                    break;

                case Globals.STR_ACTUAL_POSITION:
                case Globals.STR_ACTUAL_VELOCITY:
                case Globals.STR_ACTUAL_FORCE:
                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    instructions.Add(new WriteInstruction(0x2c01, Is32Bits ? 22 : 23, registerToRead, strtemp + "Get value in ACCUM"));
                    if (extendSign)
                    {
                        instructions.Add(new WriteInstruction(0x2c02, 10, 15, "If 16 bits value is negative"));
                        instructions.Add(new WriteInstruction(0x2c01, 6, -65536, "Sign extend"));       // -65536 equals 0xFFFF0000
                    }
                    instructions.Add(new WriteInstruction(0x2c02, comparison == Globals.STR_ABOVE ? 5 : 4, threshold, "Compare against threshold"));
                    break;

                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    instructions.Add(new WriteInstruction(0x2c02, activeState == Globals.STR_ON ? 9 : 8, inputBit, strtemp));
                    break;

                case Globals.STR_VARIABLE:
                    int variableIndex = UserObjectselectorVariable1.GetIndex(variable);
                    instructions.Add(new WriteInstruction(0x2c01, 0x16, variableIndex, strtemp + "Get variable in ACCUM"));
                    int subindex = 0;
                    switch (argument)
                    {
                        case Globals.STR_VARIABLE:
                            int variable2Index = UserObjectselectorVariable2.GetIndex(variable2);
                            switch (comparison)
                            {
                                case Globals.STR_BELOW:
                                    subindex = 0x13;
                                    break;
                                case Globals.STR_ABOVE:
                                    subindex = 0x14;
                                    break;
                                case Globals.STR_EQUAL:
                                    subindex = 0x15;
                                    break;
                                case Globals.STR_UNEQUAL:
                                    subindex = 0x16;
                                    break;
                            }
                            instructions.Add(new WriteInstruction(0x2c02, subindex, variable2Index, "Do comparison with variable"));
                            break;
                        case Globals.STR_CONSTANT:
                            switch (comparison)
                            {
                                case Globals.STR_BELOW:
                                    subindex = 0x04;
                                    break;
                                case Globals.STR_ABOVE:
                                    subindex = 0x05;
                                    break;
                                case Globals.STR_EQUAL:
                                    subindex = 0x06;
                                    break;
                                case Globals.STR_UNEQUAL:
                                    subindex = 0x07;
                                    break;
                            }
                            instructions.Add(new WriteInstruction(0x2c02, subindex, constant, "Do comparison with constant"));
                            break;
                    }
                    break;
            }

            switch (thenAction)
            {
                case Globals.STR_MACRO_CALL:
                    switch (elseAction)
                    {
                        case Globals.STR_MACRO_CALL:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 3, "Skip the else part"));
                            instructions.Add(new WriteInstruction(0x2c04, CALL, elseMacronumber, "Else call macro " + elseMacronumber.ToString()));
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Skip the then part"));
                            break;
                        case Globals.STR_MACRO_JUMP:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Skip the else part"));
                            instructions.Add(new WriteInstruction(0x2c04, JUMP, elseMacronumber, "Else jump to macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_RETURN:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Skip the else part"));
                            instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Else return from macro"));
                            break;
                        case Globals.STR_MACRO_CONTINUE:
                            break;
                        case Globals.STR_END_PROGRAM:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Skip the else part"));
                            instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Else end program"));
                            break;
                    }
                    instructions.Add(new WriteInstruction(0x2c04, CALL, thenMacronumber, "Then call macro " + thenMacronumber.ToString()));
                    break;

                case Globals.STR_MACRO_JUMP:
                    instructions.Add(new WriteInstruction(0x2c04, JUMP, thenMacronumber, "Then jump to macro" + thenMacronumber.ToString()));
                    switch (elseAction)
                    {
                        case Globals.STR_MACRO_CALL:
                            instructions.Add(new WriteInstruction(0x2c04, CALL, elseMacronumber, "Else call macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_JUMP:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP, elseMacronumber, "Else jump to macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_RETURN:
                            instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Else return from macro"));
                            break;
                        case Globals.STR_MACRO_CONTINUE:
                            break;
                        case Globals.STR_END_PROGRAM:
                            instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Else end program"));
                            break;
                    }
                    break;
                    

                case Globals.STR_MACRO_RETURN:
                    instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Then return from macro"));
                    switch (elseAction)
                    {
                        case Globals.STR_MACRO_CALL:
                            instructions.Add(new WriteInstruction(0x2c04, CALL, elseMacronumber, "Else call macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_JUMP:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP, elseMacronumber, "Else jump to macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_RETURN:
                            instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Else return from macro"));
                            break;
                        case Globals.STR_MACRO_CONTINUE:
                            break;
                        case Globals.STR_END_PROGRAM:
                            instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Else end program"));
                            break;
                    }
                    break;

                case Globals.STR_MACRO_CONTINUE:
                    switch (elseAction)
                    {
                        case Globals.STR_MACRO_CALL:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Then macro continue"));
                            instructions.Add(new WriteInstruction(0x2c04, CALL, elseMacronumber, "Else call macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_JUMP:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Then macro continue"));
                            instructions.Add(new WriteInstruction(0x2c04, JUMP, elseMacronumber, "Else jump to macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_RETURN:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Then macro continue"));
                            instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Else return from macro"));
                            break;
                        case Globals.STR_MACRO_CONTINUE:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 1, "Then macro continue"));
                            break;
                        case Globals.STR_END_PROGRAM:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP_RELATIVE, 2, "Then macro continue"));
                            instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Else end program"));
                            break;
                    }
                    break;

                case Globals.STR_END_PROGRAM:
                    instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Then end program"));
                    switch (elseAction)
                    {
                        case Globals.STR_MACRO_CALL:
                            instructions.Add(new WriteInstruction(0x2c04, CALL, elseMacronumber, "Else call macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_JUMP:
                            instructions.Add(new WriteInstruction(0x2c04, JUMP, elseMacronumber, "Else jump to macro " + elseMacronumber.ToString()));
                            break;
                        case Globals.STR_MACRO_RETURN:
                            instructions.Add(new WriteInstruction(0x2c04, RETURN, 0, "Else return from macro"));
                            break;
                        case Globals.STR_MACRO_CONTINUE:
                            break;
                        case Globals.STR_END_PROGRAM:
                            instructions.Add(new WriteInstruction(0x2c02, 3, 0, "Else end program"));
                            break;
                    }
                    break;
            }
        }
    }
}
