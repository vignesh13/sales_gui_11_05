﻿namespace Salesman_GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabMeasure = new System.Windows.Forms.TabControl();
            this.tabBuildandRun = new System.Windows.Forms.TabPage();
            this.btnEnable = new System.Windows.Forms.Button();
            this.btnContactInformation1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbDownloadConfig = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblUnits16 = new System.Windows.Forms.Label();
            this.lblName16 = new System.Windows.Forms.Label();
            this.ComboBox7 = new System.Windows.Forms.ComboBox();
            this.lblName15 = new System.Windows.Forms.Label();
            this.lblUnits15 = new System.Windows.Forms.Label();
            this.lblUnits14 = new System.Windows.Forms.Label();
            this.lblUnits13 = new System.Windows.Forms.Label();
            this.lblName14 = new System.Windows.Forms.Label();
            this.lblName13 = new System.Windows.Forms.Label();
            this.lblUnits12 = new System.Windows.Forms.Label();
            this.lblName12 = new System.Windows.Forms.Label();
            this.lblUnits11 = new System.Windows.Forms.Label();
            this.lblName11 = new System.Windows.Forms.Label();
            this.ComboBox6 = new System.Windows.Forms.ComboBox();
            this.lblUnits10 = new System.Windows.Forms.Label();
            this.lblName10 = new System.Windows.Forms.Label();
            this.ComboBox5 = new System.Windows.Forms.ComboBox();
            this.btnSelect4 = new System.Windows.Forms.Button();
            this.btnSelect3 = new System.Windows.Forms.Button();
            this.btnSelect2 = new System.Windows.Forms.Button();
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.lblUnits9 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.lblComment = new System.Windows.Forms.Label();
            this.lblUnits8 = new System.Windows.Forms.Label();
            this.lblUnits7 = new System.Windows.Forms.Label();
            this.txtParm8 = new System.Windows.Forms.TextBox();
            this.txtParm7 = new System.Windows.Forms.TextBox();
            this.ComboBox4 = new System.Windows.Forms.ComboBox();
            this.ComboBox3 = new System.Windows.Forms.ComboBox();
            this.ComboBox2 = new System.Windows.Forms.ComboBox();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.lblUnits6 = new System.Windows.Forms.Label();
            this.txtParm6 = new System.Windows.Forms.TextBox();
            this.lblUnits5 = new System.Windows.Forms.Label();
            this.lblUnits4 = new System.Windows.Forms.Label();
            this.lblUnits3 = new System.Windows.Forms.Label();
            this.lblUnits2 = new System.Windows.Forms.Label();
            this.lblUnits1 = new System.Windows.Forms.Label();
            this.txtParm2 = new System.Windows.Forms.TextBox();
            this.txtParm3 = new System.Windows.Forms.TextBox();
            this.txtParm4 = new System.Windows.Forms.TextBox();
            this.txtParm5 = new System.Windows.Forms.TextBox();
            this.txtParm1 = new System.Windows.Forms.TextBox();
            this.cmbFunction = new System.Windows.Forms.ComboBox();
            this.lblFunction = new System.Windows.Forms.Label();
            this.lblName9 = new System.Windows.Forms.Label();
            this.lblName8 = new System.Windows.Forms.Label();
            this.lblName7 = new System.Windows.Forms.Label();
            this.lblName6 = new System.Windows.Forms.Label();
            this.lblName5 = new System.Windows.Forms.Label();
            this.lblName4 = new System.Windows.Forms.Label();
            this.lblName3 = new System.Windows.Forms.Label();
            this.lblName2 = new System.Windows.Forms.Label();
            this.lblName1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAutoBackupInfo = new System.Windows.Forms.Button();
            this.chkAutoBackup = new System.Windows.Forms.CheckBox();
            this.btnNewFile = new System.Windows.Forms.Button();
            this.btnSaveProgram = new System.Windows.Forms.Button();
            this.btnOpenProgram = new System.Windows.Forms.Button();
            this.groupBoxMacro = new System.Windows.Forms.GroupBox();
            this.chkApplyToAllNodes = new System.Windows.Forms.CheckBox();
            this.chkSetProtectedAccess = new System.Windows.Forms.CheckBox();
            this.btnEraseAllMacros = new System.Windows.Forms.Button();
            this.cmbMacroDownload = new System.Windows.Forms.ComboBox();
            this.btnDownloadAllMacros = new System.Windows.Forms.Button();
            this.btnDownloadMacro = new System.Windows.Forms.Button();
            this.btnMoveDown = new System.Windows.Forms.Button();
            this.btnMoveUp = new System.Windows.Forms.Button();
            this.btnInsertBelow = new System.Windows.Forms.Button();
            this.btnInsertAbove = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.dgvProgram = new System.Windows.Forms.DataGridView();
            this.Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Line = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Command = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommentedOut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumberSave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabContLogging = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnAutoSeq = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnStartDemo = new System.Windows.Forms.Button();
            this.btnMotoroff = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnStartTraceLive = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.btnAutoscaleSecondary = new System.Windows.Forms.Button();
            this.txtMaxSecondaryRun = new System.Windows.Forms.TextBox();
            this.txtMinSecondaryRun = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.btnSaveLogs = new System.Windows.Forms.Button();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.btnAutoscalePrimary = new System.Windows.Forms.Button();
            this.txtMaxPrimaryRun = new System.Windows.Forms.TextBox();
            this.txtMinPrimaryRun = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkLogitem2secondary = new System.Windows.Forms.CheckBox();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.cmbSamples = new System.Windows.Forms.ComboBox();
            this.chkLogitem1primary = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtObjectindex1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbIntervalLive = new System.Windows.Forms.ComboBox();
            this.cmbBackgroundobject2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.cmbBackgroundobject1 = new System.Windows.Forms.ComboBox();
            this.txtObjectindex2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnContactInfo2 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtForceSlopeRange = new System.Windows.Forms.TextBox();
            this.txtForceRange = new System.Windows.Forms.TextBox();
            this.txtSoftlandRange = new System.Windows.Forms.TextBox();
            this.txtSpeedRange = new System.Windows.Forms.TextBox();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.lblFS1 = new System.Windows.Forms.Label();
            this.lblForce1 = new System.Windows.Forms.Label();
            this.lblSS1 = new System.Windows.Forms.Label();
            this.lblSpeed1 = new System.Windows.Forms.Label();
            this.lbltargetPosition1 = new System.Windows.Forms.Label();
            this.txtTargetPositionDemo = new System.Windows.Forms.TextBox();
            this.lblTargetPositionDemo = new System.Windows.Forms.Label();
            this.txtProfileVelocityDemo = new System.Windows.Forms.TextBox();
            this.lblSpeedDemo = new System.Windows.Forms.Label();
            this.txtSoftlandErrorDemo = new System.Windows.Forms.TextBox();
            this.lblSoftlandSensitivityDemo = new System.Windows.Forms.Label();
            this.lblForceDemo = new System.Windows.Forms.Label();
            this.txtTargetForceDemo = new System.Windows.Forms.TextBox();
            this.txtForceSlopeDemo = new System.Windows.Forms.TextBox();
            this.lblForceSlopeDemo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbActuatorDemo = new System.Windows.Forms.ComboBox();
            this.lblActuator = new System.Windows.Forms.Label();
            this.cmbProgramTypeDemo = new System.Windows.Forms.ComboBox();
            this.lblProgramType = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.cmbController = new System.Windows.Forms.ComboBox();
            this.cmbNode2 = new System.Windows.Forms.ComboBox();
            this.cmbNode1 = new System.Windows.Forms.ComboBox();
            this.chkLogitem2primary = new System.Windows.Forms.CheckBox();
            this.chkLogitem1secondary = new System.Windows.Forms.CheckBox();
            this.labelctr2 = new System.Windows.Forms.Label();
            this.labelctr1 = new System.Windows.Forms.Label();
            this.chartRun = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtMeasureResult = new System.Windows.Forms.TextBox();
            this.panel25 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.btnASP1 = new System.Windows.Forms.Button();
            this.txtMinSecondaryRun1 = new System.Windows.Forms.TextBox();
            this.txtMinPrimaryRun1 = new System.Windows.Forms.TextBox();
            this.txtMaxSecondaryRun1 = new System.Windows.Forms.TextBox();
            this.txtMaxPrimaryRun1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnTeachDatum = new System.Windows.Forms.Button();
            this.btnMeasureStop = new System.Windows.Forms.Button();
            this.btnASMeasure = new System.Windows.Forms.Button();
            this.btnMeasureHome = new System.Windows.Forms.Button();
            this.cmbNode2Measure = new System.Windows.Forms.ComboBox();
            this.cmbNode1Measure = new System.Windows.Forms.ComboBox();
            this.chkLP2Measure = new System.Windows.Forms.CheckBox();
            this.chkLS1Measure = new System.Windows.Forms.CheckBox();
            this.chkLP1Measure = new System.Windows.Forms.CheckBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnSavelogMeasure = new System.Windows.Forms.Button();
            this.cmbSamMeasure = new System.Windows.Forms.ComboBox();
            this.cmbILMeasure = new System.Windows.Forms.ComboBox();
            this.chkLS2Measure = new System.Windows.Forms.CheckBox();
            this.txtMeasureValue2 = new System.Windows.Forms.TextBox();
            this.txtMeasureValue1 = new System.Windows.Forms.TextBox();
            this.cmbBGobject2 = new System.Windows.Forms.ComboBox();
            this.cmbBGobject1 = new System.Windows.Forms.ComboBox();
            this.txtObIn1Measure = new System.Windows.Forms.TextBox();
            this.txtObIn2Measure = new System.Windows.Forms.TextBox();
            this.chartMeasure = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.txtMeasureDT = new System.Windows.Forms.TextBox();
            this.txtMsRangeDT = new System.Windows.Forms.TextBox();
            this.lblMeasureDT = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMsRangeF = new System.Windows.Forms.TextBox();
            this.txtMsRangeSen = new System.Windows.Forms.TextBox();
            this.txtMsRangeSpeed = new System.Windows.Forms.TextBox();
            this.txtMsRangePos = new System.Windows.Forms.TextBox();
            this.txtMeasureForce = new System.Windows.Forms.TextBox();
            this.lblMeasureForce = new System.Windows.Forms.Label();
            this.txtMeasureSensitivity = new System.Windows.Forms.TextBox();
            this.lblMeasureSensitivity = new System.Windows.Forms.Label();
            this.txtMeasureSpeed = new System.Windows.Forms.TextBox();
            this.lblMeasureSpeed = new System.Windows.Forms.Label();
            this.txtPosCloseToPart = new System.Windows.Forms.TextBox();
            this.lblPosCloseToPart = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnClearChart = new System.Windows.Forms.Button();
            this.cmbMeasureType = new System.Windows.Forms.ComboBox();
            this.lblMeasureType = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtPosSwitchRange = new System.Windows.Forms.TextBox();
            this.txtErrorSwitchRange = new System.Windows.Forms.TextBox();
            this.txtSwitchSpeedRange = new System.Windows.Forms.TextBox();
            this.txtSSSRange = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtPosCloseSwitch = new System.Windows.Forms.TextBox();
            this.txtErrorSwitch = new System.Windows.Forms.TextBox();
            this.txtSwitchSpeed = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.lblPositionSwitch = new System.Windows.Forms.Label();
            this.txtSoftlandSpeedSwitch = new System.Windows.Forms.TextBox();
            this.panel26 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label111 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.txtIniPosResult = new System.Windows.Forms.TextBox();
            this.txtLastPosResult = new System.Windows.Forms.TextBox();
            this.txtForceResult = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnClearSwitch = new System.Windows.Forms.Button();
            this.btnStopSwitch = new System.Windows.Forms.Button();
            this.btnStartSwitch = new System.Windows.Forms.Button();
            this.btnHomeSwitch = new System.Windows.Forms.Button();
            this.chkLP2Switch = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnSavelogSwitch = new System.Windows.Forms.Button();
            this.chkLP1Switch = new System.Windows.Forms.CheckBox();
            this.txtSwitchValue2 = new System.Windows.Forms.TextBox();
            this.txtSwitchValue1 = new System.Windows.Forms.TextBox();
            this.cmbSwitchObject2 = new System.Windows.Forms.ComboBox();
            this.cmbSwitchObject1 = new System.Windows.Forms.ComboBox();
            this.txtObject1Switch = new System.Windows.Forms.TextBox();
            this.txtObject2Switch = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.cmbSwitchType = new System.Windows.Forms.ComboBox();
            this.chartSwitch = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label96 = new System.Windows.Forms.Label();
            this.panel27 = new System.Windows.Forms.Panel();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.txtFinalLeak = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.txtSoftlandLeak = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.txtLeakResult = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.btnActuatorStopLeak = new System.Windows.Forms.Button();
            this.btnActuatorStartLeak = new System.Windows.Forms.Button();
            this.btnHomeLeak = new System.Windows.Forms.Button();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtDwellLeakRange = new System.Windows.Forms.TextBox();
            this.txtForceLeakRange = new System.Windows.Forms.TextBox();
            this.txtSpeedLeakRange = new System.Windows.Forms.TextBox();
            this.txtSensitivityLeakRange = new System.Windows.Forms.TextBox();
            this.txtPositionLeakRange = new System.Windows.Forms.TextBox();
            this.lblDwellLeak = new System.Windows.Forms.Label();
            this.lblForceLeak = new System.Windows.Forms.Label();
            this.lblSensitivityLeak = new System.Windows.Forms.Label();
            this.lblSpeedLeak = new System.Windows.Forms.Label();
            this.lblPostionLeak = new System.Windows.Forms.Label();
            this.txtDwellLeak = new System.Windows.Forms.TextBox();
            this.txtForceLeak = new System.Windows.Forms.TextBox();
            this.txtSpeedLeak = new System.Windows.Forms.TextBox();
            this.txtSensitivityLeak = new System.Windows.Forms.TextBox();
            this.txtPositionLeak = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.cmbLeakTestType = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label74 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label54 = new System.Windows.Forms.Label();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.txtHighSpeedCycleFreq = new System.Windows.Forms.TextBox();
            this.txtHighSpeedCycleCount = new System.Windows.Forms.TextBox();
            this.txtHighSpeedCycleTime = new System.Windows.Forms.TextBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnHighSpeedClear = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.cmbHighSpeed = new System.Windows.Forms.ComboBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txtHighSpeedVelocity = new System.Windows.Forms.TextBox();
            this.txtHighSpeedTarget2 = new System.Windows.Forms.TextBox();
            this.txtHighSpeedTarget1 = new System.Windows.Forms.TextBox();
            this.btnHighSpeedHome = new System.Windows.Forms.Button();
            this.btnHighSpeedStop = new System.Windows.Forms.Button();
            this.btnHighSpeedStart = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel32 = new System.Windows.Forms.Panel();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.txtPosition3StepForce = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.txtPosition2StepForce = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.txtPosition1StepForce = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.txtSoftlandStepForce = new System.Windows.Forms.TextBox();
            this.btnHomeStepForce = new System.Windows.Forms.Button();
            this.btnStopStepForce = new System.Windows.Forms.Button();
            this.btnStartStepForce = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel36 = new System.Windows.Forms.Panel();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.txtPreCycleRes = new System.Windows.Forms.TextBox();
            this.txtPreStroke = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label129 = new System.Windows.Forms.Label();
            this.txtSettleTime = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.txtPreDecel = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.btnPreUpdateRes = new System.Windows.Forms.Button();
            this.btnPreHome = new System.Windows.Forms.Button();
            this.txtPreForce = new System.Windows.Forms.TextBox();
            this.btnPreStop = new System.Windows.Forms.Button();
            this.label120 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.btnPreStart = new System.Windows.Forms.Button();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.txtPreAcceleration = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.txtPreSpeed = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.txtPrePositionB = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.txtPrePositionA = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.btnAutoscaleSecondary1 = new System.Windows.Forms.Button();
            this.btnAutoscalePrimary1 = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lblPortName = new System.Windows.Forms.Label();
            this.txtConnectionStatus = new System.Windows.Forms.Label();
            this.btnFaultReset = new System.Windows.Forms.Button();
            this.tmrStatus = new System.Windows.Forms.Timer(this.components);
            this.tmrLogging = new System.Windows.Forms.Timer(this.components);
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.colFunc = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNode1 = new System.Windows.Forms.Label();
            this.cmbNode = new System.Windows.Forms.ComboBox();
            this.btnFaultQuery = new System.Windows.Forms.Button();
            this.cmbPortName = new System.Windows.Forms.ComboBox();
            this.tmrTuning = new System.Windows.Forms.Timer(this.components);
            this.tabMeasure.SuspendLayout();
            this.tabBuildandRun.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxMacro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProgram)).BeginInit();
            this.tabContLogging.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel21.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartMeasure)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel23.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSwitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.panel28.SuspendLayout();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel24.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.panel33.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMeasure
            // 
            this.tabMeasure.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMeasure.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabMeasure.Controls.Add(this.tabBuildandRun);
            this.tabMeasure.Controls.Add(this.tabContLogging);
            this.tabMeasure.Controls.Add(this.tabPage1);
            this.tabMeasure.Controls.Add(this.tabPage2);
            this.tabMeasure.Controls.Add(this.tabPage3);
            this.tabMeasure.Controls.Add(this.tabPage4);
            this.tabMeasure.Controls.Add(this.tabPage5);
            this.tabMeasure.Controls.Add(this.tabPage6);
            this.tabMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabMeasure.Location = new System.Drawing.Point(0, 0);
            this.tabMeasure.Margin = new System.Windows.Forms.Padding(4);
            this.tabMeasure.Name = "tabMeasure";
            this.tabMeasure.SelectedIndex = 0;
            this.tabMeasure.Size = new System.Drawing.Size(1371, 830);
            this.tabMeasure.TabIndex = 1;
            this.tabMeasure.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabBuildandRun
            // 
            this.tabBuildandRun.Controls.Add(this.btnEnable);
            this.tabBuildandRun.Controls.Add(this.btnContactInformation1);
            this.tabBuildandRun.Controls.Add(this.pictureBox1);
            this.tabBuildandRun.Controls.Add(this.groupBox3);
            this.tabBuildandRun.Controls.Add(this.groupBox2);
            this.tabBuildandRun.Controls.Add(this.groupBox1);
            this.tabBuildandRun.Controls.Add(this.groupBoxMacro);
            this.tabBuildandRun.Controls.Add(this.btnMoveDown);
            this.tabBuildandRun.Controls.Add(this.btnMoveUp);
            this.tabBuildandRun.Controls.Add(this.btnInsertBelow);
            this.tabBuildandRun.Controls.Add(this.btnInsertAbove);
            this.tabBuildandRun.Controls.Add(this.btnDelete);
            this.tabBuildandRun.Controls.Add(this.btnApply);
            this.tabBuildandRun.Controls.Add(this.btnEdit);
            this.tabBuildandRun.Controls.Add(this.dgvProgram);
            this.tabBuildandRun.Location = new System.Drawing.Point(4, 30);
            this.tabBuildandRun.Margin = new System.Windows.Forms.Padding(4);
            this.tabBuildandRun.Name = "tabBuildandRun";
            this.tabBuildandRun.Padding = new System.Windows.Forms.Padding(4);
            this.tabBuildandRun.Size = new System.Drawing.Size(1363, 796);
            this.tabBuildandRun.TabIndex = 1;
            this.tabBuildandRun.Text = "Build Programs";
            this.tabBuildandRun.UseVisualStyleBackColor = true;
            // 
            // btnEnable
            // 
            this.btnEnable.Location = new System.Drawing.Point(754, 343);
            this.btnEnable.Margin = new System.Windows.Forms.Padding(4);
            this.btnEnable.Name = "btnEnable";
            this.btnEnable.Size = new System.Drawing.Size(113, 32);
            this.btnEnable.TabIndex = 139;
            this.btnEnable.Text = "Enable";
            this.btnEnable.UseVisualStyleBackColor = true;
            this.btnEnable.Click += new System.EventHandler(this.btnEnable_Click);
            // 
            // btnContactInformation1
            // 
            this.btnContactInformation1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContactInformation1.Location = new System.Drawing.Point(1149, 85);
            this.btnContactInformation1.Margin = new System.Windows.Forms.Padding(4);
            this.btnContactInformation1.Name = "btnContactInformation1";
            this.btnContactInformation1.Size = new System.Drawing.Size(199, 30);
            this.btnContactInformation1.TabIndex = 138;
            this.btnContactInformation1.Text = "Contact information";
            this.btnContactInformation1.UseVisualStyleBackColor = true;
            this.btnContactInformation1.Click += new System.EventHandler(this.btnContactInformation1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 140;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.cmbDownloadConfig);
            this.groupBox3.Location = new System.Drawing.Point(824, 286);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(240, 52);
            this.groupBox3.TabIndex = 110;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "  Configuration  ";
            // 
            // cmbDownloadConfig
            // 
            this.cmbDownloadConfig.Enabled = false;
            this.cmbDownloadConfig.Location = new System.Drawing.Point(7, 21);
            this.cmbDownloadConfig.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDownloadConfig.Name = "cmbDownloadConfig";
            this.cmbDownloadConfig.Size = new System.Drawing.Size(224, 31);
            this.cmbDownloadConfig.TabIndex = 107;
            this.cmbDownloadConfig.Text = "Download config file";
            this.cmbDownloadConfig.UseVisualStyleBackColor = true;
            this.cmbDownloadConfig.Click += new System.EventHandler(this.cmbDownloadConfig_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblUnits16);
            this.groupBox2.Controls.Add(this.lblName16);
            this.groupBox2.Controls.Add(this.ComboBox7);
            this.groupBox2.Controls.Add(this.lblName15);
            this.groupBox2.Controls.Add(this.lblUnits15);
            this.groupBox2.Controls.Add(this.lblUnits14);
            this.groupBox2.Controls.Add(this.lblUnits13);
            this.groupBox2.Controls.Add(this.lblName14);
            this.groupBox2.Controls.Add(this.lblName13);
            this.groupBox2.Controls.Add(this.lblUnits12);
            this.groupBox2.Controls.Add(this.lblName12);
            this.groupBox2.Controls.Add(this.lblUnits11);
            this.groupBox2.Controls.Add(this.lblName11);
            this.groupBox2.Controls.Add(this.ComboBox6);
            this.groupBox2.Controls.Add(this.lblUnits10);
            this.groupBox2.Controls.Add(this.lblName10);
            this.groupBox2.Controls.Add(this.ComboBox5);
            this.groupBox2.Controls.Add(this.btnSelect4);
            this.groupBox2.Controls.Add(this.btnSelect3);
            this.groupBox2.Controls.Add(this.btnSelect2);
            this.groupBox2.Controls.Add(this.btnSelect1);
            this.groupBox2.Controls.Add(this.lblUnits9);
            this.groupBox2.Controls.Add(this.txtComment);
            this.groupBox2.Controls.Add(this.lblComment);
            this.groupBox2.Controls.Add(this.lblUnits8);
            this.groupBox2.Controls.Add(this.lblUnits7);
            this.groupBox2.Controls.Add(this.txtParm8);
            this.groupBox2.Controls.Add(this.txtParm7);
            this.groupBox2.Controls.Add(this.ComboBox4);
            this.groupBox2.Controls.Add(this.ComboBox3);
            this.groupBox2.Controls.Add(this.ComboBox2);
            this.groupBox2.Controls.Add(this.ComboBox1);
            this.groupBox2.Controls.Add(this.lblUnits6);
            this.groupBox2.Controls.Add(this.txtParm6);
            this.groupBox2.Controls.Add(this.lblUnits5);
            this.groupBox2.Controls.Add(this.lblUnits4);
            this.groupBox2.Controls.Add(this.lblUnits3);
            this.groupBox2.Controls.Add(this.lblUnits2);
            this.groupBox2.Controls.Add(this.lblUnits1);
            this.groupBox2.Controls.Add(this.txtParm2);
            this.groupBox2.Controls.Add(this.txtParm3);
            this.groupBox2.Controls.Add(this.txtParm4);
            this.groupBox2.Controls.Add(this.txtParm5);
            this.groupBox2.Controls.Add(this.txtParm1);
            this.groupBox2.Controls.Add(this.cmbFunction);
            this.groupBox2.Controls.Add(this.lblFunction);
            this.groupBox2.Controls.Add(this.lblName9);
            this.groupBox2.Controls.Add(this.lblName8);
            this.groupBox2.Controls.Add(this.lblName7);
            this.groupBox2.Controls.Add(this.lblName6);
            this.groupBox2.Controls.Add(this.lblName5);
            this.groupBox2.Controls.Add(this.lblName4);
            this.groupBox2.Controls.Add(this.lblName3);
            this.groupBox2.Controls.Add(this.lblName2);
            this.groupBox2.Controls.Add(this.lblName1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(7, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox2.Size = new System.Drawing.Size(787, 335);
            this.groupBox2.TabIndex = 109;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "  Function Editor  ";
            // 
            // lblUnits16
            // 
            this.lblUnits16.AutoSize = true;
            this.lblUnits16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits16.Location = new System.Drawing.Point(677, 226);
            this.lblUnits16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits16.Name = "lblUnits16";
            this.lblUnits16.Size = new System.Drawing.Size(72, 18);
            this.lblUnits16.TabIndex = 515;
            this.lblUnits16.Text = "lblUnits16";
            // 
            // lblName16
            // 
            this.lblName16.AutoSize = true;
            this.lblName16.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName16.Location = new System.Drawing.Point(67, 135);
            this.lblName16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName16.Name = "lblName16";
            this.lblName16.Size = new System.Drawing.Size(78, 18);
            this.lblName16.TabIndex = 514;
            this.lblName16.Text = "lblName16";
            this.lblName16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ComboBox7
            // 
            this.ComboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox7.FormattingEnabled = true;
            this.ComboBox7.Items.AddRange(new object[] {
            "*"});
            this.ComboBox7.Location = new System.Drawing.Point(215, 159);
            this.ComboBox7.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox7.Name = "ComboBox7";
            this.ComboBox7.Size = new System.Drawing.Size(352, 26);
            this.ComboBox7.TabIndex = 513;
            this.ComboBox7.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // lblName15
            // 
            this.lblName15.AutoSize = true;
            this.lblName15.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName15.Location = new System.Drawing.Point(8, 270);
            this.lblName15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName15.Name = "lblName15";
            this.lblName15.Size = new System.Drawing.Size(78, 18);
            this.lblName15.TabIndex = 512;
            this.lblName15.Text = "lblName15";
            this.lblName15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnits15
            // 
            this.lblUnits15.AutoSize = true;
            this.lblUnits15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits15.Location = new System.Drawing.Point(677, 256);
            this.lblUnits15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits15.Name = "lblUnits15";
            this.lblUnits15.Size = new System.Drawing.Size(72, 18);
            this.lblUnits15.TabIndex = 511;
            this.lblUnits15.Text = "lblUnits15";
            // 
            // lblUnits14
            // 
            this.lblUnits14.AutoSize = true;
            this.lblUnits14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits14.Location = new System.Drawing.Point(587, 260);
            this.lblUnits14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits14.Name = "lblUnits14";
            this.lblUnits14.Size = new System.Drawing.Size(72, 18);
            this.lblUnits14.TabIndex = 122;
            this.lblUnits14.Text = "lblUnits14";
            // 
            // lblUnits13
            // 
            this.lblUnits13.AutoSize = true;
            this.lblUnits13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits13.Location = new System.Drawing.Point(587, 217);
            this.lblUnits13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits13.Name = "lblUnits13";
            this.lblUnits13.Size = new System.Drawing.Size(72, 18);
            this.lblUnits13.TabIndex = 121;
            this.lblUnits13.Text = "lblUnits13";
            // 
            // lblName14
            // 
            this.lblName14.AutoSize = true;
            this.lblName14.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName14.Location = new System.Drawing.Point(24, 267);
            this.lblName14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName14.Name = "lblName14";
            this.lblName14.Size = new System.Drawing.Size(78, 18);
            this.lblName14.TabIndex = 120;
            this.lblName14.Text = "lblName14";
            this.lblName14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName13
            // 
            this.lblName13.AutoSize = true;
            this.lblName13.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName13.Location = new System.Drawing.Point(11, 252);
            this.lblName13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName13.Name = "lblName13";
            this.lblName13.Size = new System.Drawing.Size(78, 18);
            this.lblName13.TabIndex = 119;
            this.lblName13.Text = "lblName13";
            this.lblName13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnits12
            // 
            this.lblUnits12.AutoSize = true;
            this.lblUnits12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits12.Location = new System.Drawing.Point(533, 246);
            this.lblUnits12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits12.Name = "lblUnits12";
            this.lblUnits12.Size = new System.Drawing.Size(72, 18);
            this.lblUnits12.TabIndex = 118;
            this.lblUnits12.Text = "lblUnits12";
            // 
            // lblName12
            // 
            this.lblName12.AutoSize = true;
            this.lblName12.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName12.Location = new System.Drawing.Point(19, 260);
            this.lblName12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName12.Name = "lblName12";
            this.lblName12.Size = new System.Drawing.Size(78, 18);
            this.lblName12.TabIndex = 117;
            this.lblName12.Text = "lblName12";
            this.lblName12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnits11
            // 
            this.lblUnits11.AutoSize = true;
            this.lblUnits11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits11.Location = new System.Drawing.Point(543, 223);
            this.lblUnits11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits11.Name = "lblUnits11";
            this.lblUnits11.Size = new System.Drawing.Size(72, 18);
            this.lblUnits11.TabIndex = 116;
            this.lblUnits11.Text = "lblUnits11";
            // 
            // lblName11
            // 
            this.lblName11.AutoSize = true;
            this.lblName11.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName11.Location = new System.Drawing.Point(19, 239);
            this.lblName11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName11.Name = "lblName11";
            this.lblName11.Size = new System.Drawing.Size(78, 18);
            this.lblName11.TabIndex = 115;
            this.lblName11.Text = "lblName11";
            this.lblName11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ComboBox6
            // 
            this.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox6.FormattingEnabled = true;
            this.ComboBox6.Items.AddRange(new object[] {
            "*"});
            this.ComboBox6.Location = new System.Drawing.Point(217, 142);
            this.ComboBox6.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox6.Name = "ComboBox6";
            this.ComboBox6.Size = new System.Drawing.Size(352, 26);
            this.ComboBox6.TabIndex = 114;
            this.ComboBox6.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // lblUnits10
            // 
            this.lblUnits10.AutoSize = true;
            this.lblUnits10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits10.Location = new System.Drawing.Point(535, 197);
            this.lblUnits10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits10.Name = "lblUnits10";
            this.lblUnits10.Size = new System.Drawing.Size(72, 18);
            this.lblUnits10.TabIndex = 113;
            this.lblUnits10.Text = "lblUnits10";
            // 
            // lblName10
            // 
            this.lblName10.AutoSize = true;
            this.lblName10.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName10.Location = new System.Drawing.Point(11, 215);
            this.lblName10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName10.Name = "lblName10";
            this.lblName10.Size = new System.Drawing.Size(78, 18);
            this.lblName10.TabIndex = 112;
            this.lblName10.Text = "lblName10";
            this.lblName10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ComboBox5
            // 
            this.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox5.FormattingEnabled = true;
            this.ComboBox5.Items.AddRange(new object[] {
            "*"});
            this.ComboBox5.Location = new System.Drawing.Point(173, 162);
            this.ComboBox5.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox5.Name = "ComboBox5";
            this.ComboBox5.Size = new System.Drawing.Size(352, 26);
            this.ComboBox5.TabIndex = 111;
            this.ComboBox5.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // btnSelect4
            // 
            this.btnSelect4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect4.Location = new System.Drawing.Point(641, 143);
            this.btnSelect4.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect4.Name = "btnSelect4";
            this.btnSelect4.Size = new System.Drawing.Size(44, 28);
            this.btnSelect4.TabIndex = 110;
            this.btnSelect4.Text = "Click to select";
            this.btnSelect4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect4.UseVisualStyleBackColor = true;
            this.btnSelect4.Click += new System.EventHandler(this.ObjectSelectorClick);
            // 
            // btnSelect3
            // 
            this.btnSelect3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect3.Location = new System.Drawing.Point(641, 113);
            this.btnSelect3.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect3.Name = "btnSelect3";
            this.btnSelect3.Size = new System.Drawing.Size(44, 28);
            this.btnSelect3.TabIndex = 109;
            this.btnSelect3.Text = "Click to select";
            this.btnSelect3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect3.UseVisualStyleBackColor = true;
            this.btnSelect3.Click += new System.EventHandler(this.ObjectSelectorClick);
            // 
            // btnSelect2
            // 
            this.btnSelect2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect2.Location = new System.Drawing.Point(641, 85);
            this.btnSelect2.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect2.Name = "btnSelect2";
            this.btnSelect2.Size = new System.Drawing.Size(44, 28);
            this.btnSelect2.TabIndex = 108;
            this.btnSelect2.Text = "Click to select";
            this.btnSelect2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect2.UseVisualStyleBackColor = true;
            this.btnSelect2.Click += new System.EventHandler(this.ObjectSelectorClick);
            // 
            // btnSelect1
            // 
            this.btnSelect1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect1.Location = new System.Drawing.Point(641, 54);
            this.btnSelect1.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(44, 28);
            this.btnSelect1.TabIndex = 107;
            this.btnSelect1.Text = "Click to select";
            this.btnSelect1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSelect1.UseVisualStyleBackColor = true;
            this.btnSelect1.Click += new System.EventHandler(this.ObjectSelectorClick);
            // 
            // lblUnits9
            // 
            this.lblUnits9.AutoSize = true;
            this.lblUnits9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits9.Location = new System.Drawing.Point(533, 178);
            this.lblUnits9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits9.Name = "lblUnits9";
            this.lblUnits9.Size = new System.Drawing.Size(64, 18);
            this.lblUnits9.TabIndex = 106;
            this.lblUnits9.Text = "lblUnits9";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(129, 305);
            this.txtComment.Margin = new System.Windows.Forms.Padding(4);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(505, 24);
            this.txtComment.TabIndex = 510;
            // 
            // lblComment
            // 
            this.lblComment.AutoSize = true;
            this.lblComment.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblComment.Location = new System.Drawing.Point(11, 313);
            this.lblComment.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(74, 18);
            this.lblComment.TabIndex = 103;
            this.lblComment.Text = "Comment";
            this.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnits8
            // 
            this.lblUnits8.AutoSize = true;
            this.lblUnits8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits8.Location = new System.Drawing.Point(535, 160);
            this.lblUnits8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits8.Name = "lblUnits8";
            this.lblUnits8.Size = new System.Drawing.Size(64, 18);
            this.lblUnits8.TabIndex = 93;
            this.lblUnits8.Text = "lblUnits8";
            // 
            // lblUnits7
            // 
            this.lblUnits7.AutoSize = true;
            this.lblUnits7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits7.Location = new System.Drawing.Point(535, 145);
            this.lblUnits7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits7.Name = "lblUnits7";
            this.lblUnits7.Size = new System.Drawing.Size(64, 18);
            this.lblUnits7.TabIndex = 92;
            this.lblUnits7.Text = "lblUnits7";
            // 
            // txtParm8
            // 
            this.txtParm8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm8.Location = new System.Drawing.Point(417, 231);
            this.txtParm8.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm8.Name = "txtParm8";
            this.txtParm8.Size = new System.Drawing.Size(95, 24);
            this.txtParm8.TabIndex = 91;
            this.txtParm8.Text = "Param8";
            this.txtParm8.Visible = false;
            this.txtParm8.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // txtParm7
            // 
            this.txtParm7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm7.Location = new System.Drawing.Point(417, 198);
            this.txtParm7.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm7.Name = "txtParm7";
            this.txtParm7.Size = new System.Drawing.Size(95, 24);
            this.txtParm7.TabIndex = 90;
            this.txtParm7.Text = "Param7";
            this.txtParm7.Visible = false;
            this.txtParm7.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // ComboBox4
            // 
            this.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox4.FormattingEnabled = true;
            this.ComboBox4.Items.AddRange(new object[] {
            "*"});
            this.ComboBox4.Location = new System.Drawing.Point(173, 135);
            this.ComboBox4.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox4.Name = "ComboBox4";
            this.ComboBox4.Size = new System.Drawing.Size(352, 26);
            this.ComboBox4.TabIndex = 79;
            this.ComboBox4.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // ComboBox3
            // 
            this.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox3.FormattingEnabled = true;
            this.ComboBox3.Items.AddRange(new object[] {
            "*"});
            this.ComboBox3.Location = new System.Drawing.Point(175, 104);
            this.ComboBox3.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox3.Name = "ComboBox3";
            this.ComboBox3.Size = new System.Drawing.Size(352, 26);
            this.ComboBox3.TabIndex = 78;
            this.ComboBox3.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // ComboBox2
            // 
            this.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox2.FormattingEnabled = true;
            this.ComboBox2.Items.AddRange(new object[] {
            "*"});
            this.ComboBox2.Location = new System.Drawing.Point(173, 79);
            this.ComboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox2.Name = "ComboBox2";
            this.ComboBox2.Size = new System.Drawing.Size(352, 26);
            this.ComboBox2.TabIndex = 77;
            this.ComboBox2.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // ComboBox1
            // 
            this.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Items.AddRange(new object[] {
            "*"});
            this.ComboBox1.Location = new System.Drawing.Point(173, 54);
            this.ComboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(352, 26);
            this.ComboBox1.TabIndex = 76;
            this.ComboBox1.SelectedIndexChanged += new System.EventHandler(this.UserInputChanged);
            // 
            // lblUnits6
            // 
            this.lblUnits6.AutoSize = true;
            this.lblUnits6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits6.Location = new System.Drawing.Point(535, 130);
            this.lblUnits6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits6.Name = "lblUnits6";
            this.lblUnits6.Size = new System.Drawing.Size(64, 18);
            this.lblUnits6.TabIndex = 55;
            this.lblUnits6.Text = "lblUnits6";
            // 
            // txtParm6
            // 
            this.txtParm6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm6.Location = new System.Drawing.Point(297, 252);
            this.txtParm6.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm6.Name = "txtParm6";
            this.txtParm6.Size = new System.Drawing.Size(95, 24);
            this.txtParm6.TabIndex = 39;
            this.txtParm6.Text = "Param6";
            this.txtParm6.Visible = false;
            this.txtParm6.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // lblUnits5
            // 
            this.lblUnits5.AutoSize = true;
            this.lblUnits5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits5.Location = new System.Drawing.Point(535, 116);
            this.lblUnits5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits5.Name = "lblUnits5";
            this.lblUnits5.Size = new System.Drawing.Size(64, 18);
            this.lblUnits5.TabIndex = 53;
            this.lblUnits5.Text = "lblUnits5";
            // 
            // lblUnits4
            // 
            this.lblUnits4.AutoSize = true;
            this.lblUnits4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits4.Location = new System.Drawing.Point(535, 101);
            this.lblUnits4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits4.Name = "lblUnits4";
            this.lblUnits4.Size = new System.Drawing.Size(64, 18);
            this.lblUnits4.TabIndex = 52;
            this.lblUnits4.Text = "lblUnits4";
            // 
            // lblUnits3
            // 
            this.lblUnits3.AutoSize = true;
            this.lblUnits3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits3.Location = new System.Drawing.Point(535, 85);
            this.lblUnits3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits3.Name = "lblUnits3";
            this.lblUnits3.Size = new System.Drawing.Size(64, 18);
            this.lblUnits3.TabIndex = 51;
            this.lblUnits3.Text = "lblUnits3";
            // 
            // lblUnits2
            // 
            this.lblUnits2.AutoSize = true;
            this.lblUnits2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits2.Location = new System.Drawing.Point(535, 71);
            this.lblUnits2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits2.Name = "lblUnits2";
            this.lblUnits2.Size = new System.Drawing.Size(64, 18);
            this.lblUnits2.TabIndex = 50;
            this.lblUnits2.Text = "lblUnits2";
            // 
            // lblUnits1
            // 
            this.lblUnits1.AutoSize = true;
            this.lblUnits1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnits1.Location = new System.Drawing.Point(535, 54);
            this.lblUnits1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits1.Name = "lblUnits1";
            this.lblUnits1.Size = new System.Drawing.Size(64, 18);
            this.lblUnits1.TabIndex = 49;
            this.lblUnits1.Text = "lblUnits1";
            // 
            // txtParm2
            // 
            this.txtParm2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm2.Location = new System.Drawing.Point(173, 219);
            this.txtParm2.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm2.Name = "txtParm2";
            this.txtParm2.Size = new System.Drawing.Size(95, 24);
            this.txtParm2.TabIndex = 35;
            this.txtParm2.Text = "Param2";
            this.txtParm2.Visible = false;
            this.txtParm2.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // txtParm3
            // 
            this.txtParm3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm3.Location = new System.Drawing.Point(173, 244);
            this.txtParm3.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm3.Name = "txtParm3";
            this.txtParm3.Size = new System.Drawing.Size(95, 24);
            this.txtParm3.TabIndex = 36;
            this.txtParm3.Text = "Param3";
            this.txtParm3.Visible = false;
            this.txtParm3.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // txtParm4
            // 
            this.txtParm4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm4.Location = new System.Drawing.Point(297, 198);
            this.txtParm4.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm4.Name = "txtParm4";
            this.txtParm4.Size = new System.Drawing.Size(95, 24);
            this.txtParm4.TabIndex = 37;
            this.txtParm4.Text = "Param4";
            this.txtParm4.Visible = false;
            this.txtParm4.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // txtParm5
            // 
            this.txtParm5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm5.Location = new System.Drawing.Point(297, 225);
            this.txtParm5.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm5.Name = "txtParm5";
            this.txtParm5.Size = new System.Drawing.Size(95, 24);
            this.txtParm5.TabIndex = 38;
            this.txtParm5.Text = "Param5";
            this.txtParm5.Visible = false;
            this.txtParm5.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // txtParm1
            // 
            this.txtParm1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtParm1.Location = new System.Drawing.Point(173, 194);
            this.txtParm1.Margin = new System.Windows.Forms.Padding(4);
            this.txtParm1.Name = "txtParm1";
            this.txtParm1.Size = new System.Drawing.Size(95, 24);
            this.txtParm1.TabIndex = 34;
            this.txtParm1.Text = "Param1";
            this.txtParm1.Visible = false;
            this.txtParm1.Leave += new System.EventHandler(this.UserInputChanged);
            // 
            // cmbFunction
            // 
            this.cmbFunction.DropDownHeight = 300;
            this.cmbFunction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFunction.FormattingEnabled = true;
            this.cmbFunction.IntegralHeight = false;
            this.cmbFunction.Items.AddRange(new object[] {
            "Motor On",
            "Motor Off",
            "Phase Detect",
            "-------------",
            "Position Move",
            "Force Move",
            "Velocity Move",
            "-------------",
            "Homing",
            "Soft Land",
            "Macro Call",
            "Macro Jump",
            "Repeat",
            "-------------",
            "Wait Absolute",
            "Wait for Stop",
            "Wait for Channel",
            "-------------",
            "Set Velocity",
            "Set Force",
            "Set Acceleration",
            "Write to I/O",
            "-------------",
            "Get Position",
            "Get Force",
            "Get Velocity",
            "Get Accumulator",
            "Get Analog Input",
            "-------------",
            "Add to Accumulator",
            "Subtract from Accumulator",
            "Multiply Accumulator",
            "Divide Accumulator",
            "XOR Accumulator",
            "OR Accumulator",
            "Shift Left Accumulator",
            "Shift Right Accumulator",
            "Accumulator Absolute",
            "Accumulator Compliment",
            "-------------",
            "Execute if I/O",
            "Execute if Analog",
            "Execute if Accumulator",
            "Execute if Bit"});
            this.cmbFunction.Location = new System.Drawing.Point(215, 20);
            this.cmbFunction.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFunction.Name = "cmbFunction";
            this.cmbFunction.Size = new System.Drawing.Size(420, 26);
            this.cmbFunction.TabIndex = 32;
            this.cmbFunction.SelectedIndexChanged += new System.EventHandler(this.cmbFunction_SelectedIndexChanged);
            // 
            // lblFunction
            // 
            this.lblFunction.AutoSize = true;
            this.lblFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunction.Location = new System.Drawing.Point(11, 20);
            this.lblFunction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFunction.Name = "lblFunction";
            this.lblFunction.Size = new System.Drawing.Size(65, 18);
            this.lblFunction.TabIndex = 32;
            this.lblFunction.Text = "Function";
            // 
            // lblName9
            // 
            this.lblName9.AutoSize = true;
            this.lblName9.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName9.Location = new System.Drawing.Point(8, 197);
            this.lblName9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName9.Name = "lblName9";
            this.lblName9.Size = new System.Drawing.Size(70, 18);
            this.lblName9.TabIndex = 105;
            this.lblName9.Text = "lblName9";
            this.lblName9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName8
            // 
            this.lblName8.AutoSize = true;
            this.lblName8.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblName8.Location = new System.Drawing.Point(9, 178);
            this.lblName8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName8.Name = "lblName8";
            this.lblName8.Size = new System.Drawing.Size(70, 18);
            this.lblName8.TabIndex = 89;
            this.lblName8.Text = "lblName8";
            this.lblName8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName7
            // 
            this.lblName7.AutoSize = true;
            this.lblName7.Location = new System.Drawing.Point(9, 159);
            this.lblName7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName7.Name = "lblName7";
            this.lblName7.Size = new System.Drawing.Size(70, 18);
            this.lblName7.TabIndex = 88;
            this.lblName7.Text = "lblName7";
            this.lblName7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName6
            // 
            this.lblName6.AutoSize = true;
            this.lblName6.Location = new System.Drawing.Point(9, 140);
            this.lblName6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName6.Name = "lblName6";
            this.lblName6.Size = new System.Drawing.Size(70, 18);
            this.lblName6.TabIndex = 85;
            this.lblName6.Text = "lblName6";
            this.lblName6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName5
            // 
            this.lblName5.AutoSize = true;
            this.lblName5.Location = new System.Drawing.Point(9, 126);
            this.lblName5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName5.Name = "lblName5";
            this.lblName5.Size = new System.Drawing.Size(70, 18);
            this.lblName5.TabIndex = 84;
            this.lblName5.Text = "lblName5";
            this.lblName5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName4
            // 
            this.lblName4.AutoSize = true;
            this.lblName4.Location = new System.Drawing.Point(9, 107);
            this.lblName4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName4.Name = "lblName4";
            this.lblName4.Size = new System.Drawing.Size(70, 18);
            this.lblName4.TabIndex = 83;
            this.lblName4.Text = "lblName4";
            this.lblName4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName3
            // 
            this.lblName3.AutoSize = true;
            this.lblName3.Location = new System.Drawing.Point(9, 91);
            this.lblName3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName3.Name = "lblName3";
            this.lblName3.Size = new System.Drawing.Size(70, 18);
            this.lblName3.TabIndex = 82;
            this.lblName3.Text = "lblName3";
            this.lblName3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName2
            // 
            this.lblName2.AutoSize = true;
            this.lblName2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName2.Location = new System.Drawing.Point(9, 73);
            this.lblName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName2.Name = "lblName2";
            this.lblName2.Size = new System.Drawing.Size(70, 18);
            this.lblName2.TabIndex = 81;
            this.lblName2.Text = "lblName2";
            this.lblName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName1
            // 
            this.lblName1.AutoSize = true;
            this.lblName1.Location = new System.Drawing.Point(9, 54);
            this.lblName1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName1.Name = "lblName1";
            this.lblName1.Size = new System.Drawing.Size(70, 18);
            this.lblName1.TabIndex = 80;
            this.lblName1.Text = "lblName1";
            this.lblName1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnAutoBackupInfo);
            this.groupBox1.Controls.Add(this.chkAutoBackup);
            this.groupBox1.Controls.Add(this.btnNewFile);
            this.groupBox1.Controls.Add(this.btnSaveProgram);
            this.groupBox1.Controls.Add(this.btnOpenProgram);
            this.groupBox1.Location = new System.Drawing.Point(824, 191);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(239, 92);
            this.groupBox1.TabIndex = 107;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "  Program file control  ";
            // 
            // btnAutoBackupInfo
            // 
            this.btnAutoBackupInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoBackupInfo.Location = new System.Drawing.Point(192, 59);
            this.btnAutoBackupInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoBackupInfo.Name = "btnAutoBackupInfo";
            this.btnAutoBackupInfo.Size = new System.Drawing.Size(39, 26);
            this.btnAutoBackupInfo.TabIndex = 109;
            this.btnAutoBackupInfo.Text = "?";
            this.btnAutoBackupInfo.UseVisualStyleBackColor = true;
            this.btnAutoBackupInfo.Click += new System.EventHandler(this.btnAutoBackupInfo_Click);
            // 
            // chkAutoBackup
            // 
            this.chkAutoBackup.AutoSize = true;
            this.chkAutoBackup.Location = new System.Drawing.Point(12, 63);
            this.chkAutoBackup.Margin = new System.Windows.Forms.Padding(4);
            this.chkAutoBackup.Name = "chkAutoBackup";
            this.chkAutoBackup.Size = new System.Drawing.Size(112, 22);
            this.chkAutoBackup.TabIndex = 108;
            this.chkAutoBackup.Text = "Auto backup";
            this.chkAutoBackup.UseVisualStyleBackColor = true;
            this.chkAutoBackup.CheckedChanged += new System.EventHandler(this.chkAutoBackup_CheckedChanged);
            // 
            // btnNewFile
            // 
            this.btnNewFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewFile.Location = new System.Drawing.Point(8, 23);
            this.btnNewFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnNewFile.Name = "btnNewFile";
            this.btnNewFile.Size = new System.Drawing.Size(68, 31);
            this.btnNewFile.TabIndex = 107;
            this.btnNewFile.Text = "New";
            this.btnNewFile.UseVisualStyleBackColor = true;
            this.btnNewFile.Click += new System.EventHandler(this.btnNewFile_Click);
            // 
            // btnSaveProgram
            // 
            this.btnSaveProgram.Location = new System.Drawing.Point(165, 23);
            this.btnSaveProgram.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveProgram.Name = "btnSaveProgram";
            this.btnSaveProgram.Size = new System.Drawing.Size(68, 31);
            this.btnSaveProgram.TabIndex = 106;
            this.btnSaveProgram.Text = "Save";
            this.btnSaveProgram.UseVisualStyleBackColor = true;
            this.btnSaveProgram.Click += new System.EventHandler(this.btnSaveProgram_Click);
            // 
            // btnOpenProgram
            // 
            this.btnOpenProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenProgram.Location = new System.Drawing.Point(88, 23);
            this.btnOpenProgram.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpenProgram.Name = "btnOpenProgram";
            this.btnOpenProgram.Size = new System.Drawing.Size(68, 31);
            this.btnOpenProgram.TabIndex = 28;
            this.btnOpenProgram.Text = "Open";
            this.btnOpenProgram.UseVisualStyleBackColor = true;
            this.btnOpenProgram.Click += new System.EventHandler(this.btnOpenMacro_Click);
            // 
            // groupBoxMacro
            // 
            this.groupBoxMacro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMacro.Controls.Add(this.chkApplyToAllNodes);
            this.groupBoxMacro.Controls.Add(this.chkSetProtectedAccess);
            this.groupBoxMacro.Controls.Add(this.btnEraseAllMacros);
            this.groupBoxMacro.Controls.Add(this.cmbMacroDownload);
            this.groupBoxMacro.Controls.Add(this.btnDownloadAllMacros);
            this.groupBoxMacro.Controls.Add(this.btnDownloadMacro);
            this.groupBoxMacro.Location = new System.Drawing.Point(824, 5);
            this.groupBoxMacro.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxMacro.Name = "groupBoxMacro";
            this.groupBoxMacro.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxMacro.Size = new System.Drawing.Size(240, 187);
            this.groupBoxMacro.TabIndex = 105;
            this.groupBoxMacro.TabStop = false;
            this.groupBoxMacro.Text = "  Macro Control  ";
            // 
            // chkApplyToAllNodes
            // 
            this.chkApplyToAllNodes.AutoSize = true;
            this.chkApplyToAllNodes.Location = new System.Drawing.Point(12, 151);
            this.chkApplyToAllNodes.Margin = new System.Windows.Forms.Padding(4);
            this.chkApplyToAllNodes.Name = "chkApplyToAllNodes";
            this.chkApplyToAllNodes.Size = new System.Drawing.Size(145, 22);
            this.chkApplyToAllNodes.TabIndex = 134;
            this.chkApplyToAllNodes.Text = "Apply to all nodes";
            this.chkApplyToAllNodes.UseVisualStyleBackColor = true;
            // 
            // chkSetProtectedAccess
            // 
            this.chkSetProtectedAccess.AutoSize = true;
            this.chkSetProtectedAccess.Location = new System.Drawing.Point(12, 89);
            this.chkSetProtectedAccess.Margin = new System.Windows.Forms.Padding(4);
            this.chkSetProtectedAccess.Name = "chkSetProtectedAccess";
            this.chkSetProtectedAccess.Size = new System.Drawing.Size(170, 22);
            this.chkSetProtectedAccess.TabIndex = 133;
            this.chkSetProtectedAccess.Text = "Set protected access";
            this.chkSetProtectedAccess.UseVisualStyleBackColor = true;
            this.chkSetProtectedAccess.CheckedChanged += new System.EventHandler(this.chkSetProtectedAccess_CheckedChanged);
            // 
            // btnEraseAllMacros
            // 
            this.btnEraseAllMacros.Enabled = false;
            this.btnEraseAllMacros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEraseAllMacros.Location = new System.Drawing.Point(8, 112);
            this.btnEraseAllMacros.Margin = new System.Windows.Forms.Padding(4);
            this.btnEraseAllMacros.Name = "btnEraseAllMacros";
            this.btnEraseAllMacros.Size = new System.Drawing.Size(224, 31);
            this.btnEraseAllMacros.TabIndex = 132;
            this.btnEraseAllMacros.Text = "Erase all in controller";
            this.btnEraseAllMacros.UseVisualStyleBackColor = true;
            this.btnEraseAllMacros.Click += new System.EventHandler(this.btnEraseAllMacros_Click);
            // 
            // cmbMacroDownload
            // 
            this.cmbMacroDownload.DropDownHeight = 200;
            this.cmbMacroDownload.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMacroDownload.FormattingEnabled = true;
            this.cmbMacroDownload.IntegralHeight = false;
            this.cmbMacroDownload.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59"});
            this.cmbMacroDownload.Location = new System.Drawing.Point(177, 25);
            this.cmbMacroDownload.Margin = new System.Windows.Forms.Padding(4);
            this.cmbMacroDownload.Name = "cmbMacroDownload";
            this.cmbMacroDownload.Size = new System.Drawing.Size(53, 26);
            this.cmbMacroDownload.TabIndex = 131;
            // 
            // btnDownloadAllMacros
            // 
            this.btnDownloadAllMacros.Enabled = false;
            this.btnDownloadAllMacros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadAllMacros.Location = new System.Drawing.Point(8, 57);
            this.btnDownloadAllMacros.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownloadAllMacros.Name = "btnDownloadAllMacros";
            this.btnDownloadAllMacros.Size = new System.Drawing.Size(224, 31);
            this.btnDownloadAllMacros.TabIndex = 46;
            this.btnDownloadAllMacros.Text = "Save all in controller";
            this.btnDownloadAllMacros.UseVisualStyleBackColor = true;
            this.btnDownloadAllMacros.Click += new System.EventHandler(this.btnDownloadAllMacros_Click);
            // 
            // btnDownloadMacro
            // 
            this.btnDownloadMacro.Enabled = false;
            this.btnDownloadMacro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadMacro.Location = new System.Drawing.Point(8, 23);
            this.btnDownloadMacro.Margin = new System.Windows.Forms.Padding(4);
            this.btnDownloadMacro.Name = "btnDownloadMacro";
            this.btnDownloadMacro.Size = new System.Drawing.Size(161, 31);
            this.btnDownloadMacro.TabIndex = 26;
            this.btnDownloadMacro.Text = "Save in controller";
            this.btnDownloadMacro.UseVisualStyleBackColor = true;
            this.btnDownloadMacro.Click += new System.EventHandler(this.btnDownloadMacro_Click);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Location = new System.Drawing.Point(635, 343);
            this.btnMoveDown.Margin = new System.Windows.Forms.Padding(4);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(111, 32);
            this.btnMoveDown.TabIndex = 102;
            this.btnMoveDown.Text = "Move down";
            this.btnMoveDown.UseVisualStyleBackColor = true;
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Location = new System.Drawing.Point(516, 344);
            this.btnMoveUp.Margin = new System.Windows.Forms.Padding(4);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(111, 32);
            this.btnMoveUp.TabIndex = 101;
            this.btnMoveUp.Text = "Move up";
            this.btnMoveUp.UseVisualStyleBackColor = true;
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // btnInsertBelow
            // 
            this.btnInsertBelow.Location = new System.Drawing.Point(397, 344);
            this.btnInsertBelow.Margin = new System.Windows.Forms.Padding(4);
            this.btnInsertBelow.Name = "btnInsertBelow";
            this.btnInsertBelow.Size = new System.Drawing.Size(111, 32);
            this.btnInsertBelow.TabIndex = 100;
            this.btnInsertBelow.Text = "Insert Below";
            this.btnInsertBelow.UseVisualStyleBackColor = true;
            this.btnInsertBelow.Click += new System.EventHandler(this.btnInsertBelow_Click);
            // 
            // btnInsertAbove
            // 
            this.btnInsertAbove.Location = new System.Drawing.Point(277, 344);
            this.btnInsertAbove.Margin = new System.Windows.Forms.Padding(4);
            this.btnInsertAbove.Name = "btnInsertAbove";
            this.btnInsertAbove.Size = new System.Drawing.Size(111, 32);
            this.btnInsertAbove.TabIndex = 99;
            this.btnInsertAbove.Text = "Insert Above";
            this.btnInsertAbove.UseVisualStyleBackColor = true;
            this.btnInsertAbove.Click += new System.EventHandler(this.btnInsertAbove_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(875, 343);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(77, 32);
            this.btnDelete.TabIndex = 98;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(147, 344);
            this.btnApply.Margin = new System.Windows.Forms.Padding(4);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(124, 32);
            this.btnApply.TabIndex = 97;
            this.btnApply.Text = "Apply / Paste";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(9, 344);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(129, 32);
            this.btnEdit.TabIndex = 96;
            this.btnEdit.Text = "Edit / Copy";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dgvProgram
            // 
            this.dgvProgram.AllowUserToAddRows = false;
            this.dgvProgram.AllowUserToDeleteRows = false;
            this.dgvProgram.AllowUserToResizeRows = false;
            this.dgvProgram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProgram.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProgram.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Count,
            this.Line,
            this.Command,
            this.Parameter,
            this.Comment,
            this.CommentedOut,
            this.NumberSave});
            this.dgvProgram.Location = new System.Drawing.Point(4, 377);
            this.dgvProgram.Margin = new System.Windows.Forms.Padding(4);
            this.dgvProgram.MultiSelect = false;
            this.dgvProgram.Name = "dgvProgram";
            this.dgvProgram.ReadOnly = true;
            this.dgvProgram.RowHeadersVisible = false;
            this.dgvProgram.RowHeadersWidth = 15;
            this.dgvProgram.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvProgram.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvProgram.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProgram.Size = new System.Drawing.Size(1346, 454);
            this.dgvProgram.TabIndex = 75;
            this.dgvProgram.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProgram_CellClick);
            this.dgvProgram.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProgram_CellContentDoubleClick);
            this.dgvProgram.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dgvProgram_KeyUp);
            // 
            // Count
            // 
            this.Count.HeaderText = "#";
            this.Count.Name = "Count";
            this.Count.ReadOnly = true;
            this.Count.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Count.Width = 30;
            // 
            // Line
            // 
            this.Line.HeaderText = "Line";
            this.Line.Name = "Line";
            this.Line.ReadOnly = true;
            this.Line.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Line.Width = 50;
            // 
            // Command
            // 
            this.Command.HeaderText = "Command";
            this.Command.Name = "Command";
            this.Command.ReadOnly = true;
            this.Command.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Parameter
            // 
            this.Parameter.HeaderText = "Parameter";
            this.Parameter.Name = "Parameter";
            this.Parameter.ReadOnly = true;
            this.Parameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Parameter.Width = 461;
            // 
            // Comment
            // 
            this.Comment.HeaderText = "Comment";
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            this.Comment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Comment.Width = 358;
            // 
            // CommentedOut
            // 
            this.CommentedOut.HeaderText = "X";
            this.CommentedOut.Name = "CommentedOut";
            this.CommentedOut.ReadOnly = true;
            this.CommentedOut.Visible = false;
            // 
            // NumberSave
            // 
            this.NumberSave.HeaderText = "N";
            this.NumberSave.Name = "NumberSave";
            this.NumberSave.ReadOnly = true;
            this.NumberSave.Visible = false;
            // 
            // tabContLogging
            // 
            this.tabContLogging.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabContLogging.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabContLogging.Controls.Add(this.panel6);
            this.tabContLogging.Controls.Add(this.label17);
            this.tabContLogging.Controls.Add(this.label16);
            this.tabContLogging.Controls.Add(this.textBox5);
            this.tabContLogging.Controls.Add(this.label6);
            this.tabContLogging.Controls.Add(this.textBox4);
            this.tabContLogging.Controls.Add(this.textBox3);
            this.tabContLogging.Controls.Add(this.textBox2);
            this.tabContLogging.Controls.Add(this.textBox1);
            this.tabContLogging.Controls.Add(this.panel5);
            this.tabContLogging.Controls.Add(this.label9);
            this.tabContLogging.Controls.Add(this.btnStartTraceLive);
            this.tabContLogging.Controls.Add(this.panel4);
            this.tabContLogging.Controls.Add(this.label7);
            this.tabContLogging.Controls.Add(this.btnContactInfo2);
            this.tabContLogging.Controls.Add(this.panel3);
            this.tabContLogging.Controls.Add(this.label5);
            this.tabContLogging.Controls.Add(this.panel2);
            this.tabContLogging.Controls.Add(this.cmbController);
            this.tabContLogging.Controls.Add(this.cmbNode2);
            this.tabContLogging.Controls.Add(this.cmbNode1);
            this.tabContLogging.Controls.Add(this.chkLogitem2primary);
            this.tabContLogging.Controls.Add(this.chkLogitem1secondary);
            this.tabContLogging.Controls.Add(this.labelctr2);
            this.tabContLogging.Controls.Add(this.labelctr1);
            this.tabContLogging.Controls.Add(this.chartRun);
            this.tabContLogging.Controls.Add(this.pictureBox4);
            this.tabContLogging.Controls.Add(this.pictureBox3);
            this.tabContLogging.Location = new System.Drawing.Point(4, 30);
            this.tabContLogging.Margin = new System.Windows.Forms.Padding(4);
            this.tabContLogging.Name = "tabContLogging";
            this.tabContLogging.Size = new System.Drawing.Size(1363, 796);
            this.tabContLogging.TabIndex = 4;
            this.tabContLogging.Text = "Demo";
            this.tabContLogging.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Cornsilk;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.btnAutoSeq);
            this.panel6.Location = new System.Drawing.Point(854, 32);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(186, 96);
            this.panel6.TabIndex = 504;
            this.panel6.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Teal;
            this.label21.Location = new System.Drawing.Point(4, 30);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(144, 18);
            this.label21.TabIndex = 505;
            this.label21.Text = "*Before selecting*";
            this.label21.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Teal;
            this.label20.Location = new System.Drawing.Point(4, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(160, 18);
            this.label20.TabIndex = 504;
            this.label20.Text = "*Refer user manual*";
            this.label20.Visible = false;
            // 
            // btnAutoSeq
            // 
            this.btnAutoSeq.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoSeq.BackColor = System.Drawing.Color.DarkTurquoise;
            this.btnAutoSeq.FlatAppearance.BorderSize = 2;
            this.btnAutoSeq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutoSeq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnAutoSeq.ForeColor = System.Drawing.Color.Black;
            this.btnAutoSeq.Location = new System.Drawing.Point(3, 58);
            this.btnAutoSeq.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoSeq.Name = "btnAutoSeq";
            this.btnAutoSeq.Size = new System.Drawing.Size(178, 28);
            this.btnAutoSeq.TabIndex = 503;
            this.btnAutoSeq.Text = "Actuator auto sequence";
            this.btnAutoSeq.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAutoSeq.UseVisualStyleBackColor = false;
            this.btnAutoSeq.Visible = false;
            this.btnAutoSeq.Click += new System.EventHandler(this.btnAutoSeq_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Blue;
            this.label17.Location = new System.Drawing.Point(278, 129);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 18);
            this.label17.TabIndex = 189;
            this.label17.Text = "User input";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(210, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 18);
            this.label16.TabIndex = 188;
            this.label16.Text = "Range";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox5.Location = new System.Drawing.Point(1049, 262);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(289, 50);
            this.textBox5.TabIndex = 187;
            this.textBox5.Text = "\"The ability to do work and verify quality at the same time.\"";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(1, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(205, 18);
            this.label6.TabIndex = 174;
            this.label6.Text = "Step 2: Enter control variables";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox4.Location = new System.Drawing.Point(1196, 160);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(152, 105);
            this.textBox4.TabIndex = 183;
            this.textBox4.Text = "Labelling                 Touch screen testing\r\nSwitch/latch testing  Battery win" +
    "ding\r\nAnd many more";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.ForeColor = System.Drawing.Color.DarkRed;
            this.textBox3.Location = new System.Drawing.Point(1047, 160);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(139, 100);
            this.textBox3.TabIndex = 182;
            this.textBox3.Text = "Capping\r\nEject/sorting\r\nLeak testing\r\nThread insepction\r\nMeasurement";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox2.Location = new System.Drawing.Point(1049, 137);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(255, 22);
            this.textBox2.TabIndex = 181;
            this.textBox2.Text = "SMAC Featured Applications :";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox1.Location = new System.Drawing.Point(1043, 131);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(310, 186);
            this.textBox1.TabIndex = 180;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Cornsilk;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnStartDemo);
            this.panel5.Controls.Add(this.btnMotoroff);
            this.panel5.Controls.Add(this.btnHome);
            this.panel5.Location = new System.Drawing.Point(854, 150);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(186, 167);
            this.panel5.TabIndex = 179;
            // 
            // btnStartDemo
            // 
            this.btnStartDemo.BackColor = System.Drawing.Color.Lime;
            this.btnStartDemo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartDemo.Location = new System.Drawing.Point(5, 48);
            this.btnStartDemo.Name = "btnStartDemo";
            this.btnStartDemo.Size = new System.Drawing.Size(175, 30);
            this.btnStartDemo.TabIndex = 139;
            this.btnStartDemo.Text = "Actuator Start";
            this.btnStartDemo.UseVisualStyleBackColor = false;
            this.btnStartDemo.Click += new System.EventHandler(this.btnStartDemo_Click);
            // 
            // btnMotoroff
            // 
            this.btnMotoroff.BackColor = System.Drawing.Color.Red;
            this.btnMotoroff.Enabled = false;
            this.btnMotoroff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMotoroff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMotoroff.Location = new System.Drawing.Point(5, 82);
            this.btnMotoroff.Margin = new System.Windows.Forms.Padding(4);
            this.btnMotoroff.Name = "btnMotoroff";
            this.btnMotoroff.Size = new System.Drawing.Size(175, 32);
            this.btnMotoroff.TabIndex = 503;
            this.btnMotoroff.Text = "Actuator Stop";
            this.btnMotoroff.UseVisualStyleBackColor = false;
            this.btnMotoroff.Click += new System.EventHandler(this.btnMotoroff_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Yellow;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Location = new System.Drawing.Point(5, 14);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(176, 31);
            this.btnHome.TabIndex = 150;
            this.btnHome.Text = "Move to Home";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(856, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(171, 18);
            this.label9.TabIndex = 178;
            this.label9.Text = "Step 4: Execute program";
            // 
            // btnStartTraceLive
            // 
            this.btnStartTraceLive.Enabled = false;
            this.btnStartTraceLive.Location = new System.Drawing.Point(1067, 314);
            this.btnStartTraceLive.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartTraceLive.Name = "btnStartTraceLive";
            this.btnStartTraceLive.Size = new System.Drawing.Size(149, 31);
            this.btnStartTraceLive.TabIndex = 42;
            this.btnStartTraceLive.Text = "Start Logging";
            this.btnStartTraceLive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStartTraceLive.UseVisualStyleBackColor = true;
            this.btnStartTraceLive.Visible = false;
            this.btnStartTraceLive.Click += new System.EventHandler(this.btnStartLogging_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Cornsilk;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.groupBox16);
            this.panel4.Controls.Add(this.btnSaveLogs);
            this.panel4.Controls.Add(this.groupBox17);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.chkLogitem2secondary);
            this.panel4.Controls.Add(this.txtValue2);
            this.panel4.Controls.Add(this.txtValue1);
            this.panel4.Controls.Add(this.cmbSamples);
            this.panel4.Controls.Add(this.chkLogitem1primary);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.txtObjectindex1);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.cmbIntervalLive);
            this.panel4.Controls.Add(this.cmbBackgroundobject2);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.lblInterval);
            this.panel4.Controls.Add(this.cmbBackgroundobject1);
            this.panel4.Controls.Add(this.txtObjectindex2);
            this.panel4.Location = new System.Drawing.Point(444, 150);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(405, 167);
            this.panel4.TabIndex = 177;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(290, 67);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 18);
            this.label15.TabIndex = 182;
            this.label15.Text = "Counts";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(290, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 18);
            this.label14.TabIndex = 155;
            this.label14.Text = "Counts";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(284, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 18);
            this.label13.TabIndex = 181;
            this.label13.Text = "Check to Plot";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(167, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 18);
            this.label12.TabIndex = 180;
            this.label12.Text = "=";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(167, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 18);
            this.label1.TabIndex = 179;
            this.label1.Text = "=";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(558, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 18);
            this.label8.TabIndex = 178;
            this.label8.Text = "Chart(Scale)";
            this.label8.Visible = false;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.btnAutoscaleSecondary);
            this.groupBox16.Controls.Add(this.txtMaxSecondaryRun);
            this.groupBox16.Controls.Add(this.txtMinSecondaryRun);
            this.groupBox16.Controls.Add(this.label66);
            this.groupBox16.Controls.Add(this.label68);
            this.groupBox16.Location = new System.Drawing.Point(658, 20);
            this.groupBox16.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox16.Size = new System.Drawing.Size(161, 121);
            this.groupBox16.TabIndex = 166;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Y axis-Right";
            this.groupBox16.Visible = false;
            // 
            // btnAutoscaleSecondary
            // 
            this.btnAutoscaleSecondary.Location = new System.Drawing.Point(49, 86);
            this.btnAutoscaleSecondary.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoscaleSecondary.Name = "btnAutoscaleSecondary";
            this.btnAutoscaleSecondary.Size = new System.Drawing.Size(105, 31);
            this.btnAutoscaleSecondary.TabIndex = 167;
            this.btnAutoscaleSecondary.TabStop = false;
            this.btnAutoscaleSecondary.Text = "Auto scale";
            this.btnAutoscaleSecondary.UseVisualStyleBackColor = true;
            // 
            // txtMaxSecondaryRun
            // 
            this.txtMaxSecondaryRun.BackColor = System.Drawing.Color.White;
            this.txtMaxSecondaryRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxSecondaryRun.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtMaxSecondaryRun.Location = new System.Drawing.Point(49, 27);
            this.txtMaxSecondaryRun.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxSecondaryRun.Name = "txtMaxSecondaryRun";
            this.txtMaxSecondaryRun.Size = new System.Drawing.Size(104, 24);
            this.txtMaxSecondaryRun.TabIndex = 230;
            this.txtMaxSecondaryRun.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxSecondaryRun_KeyUp);
            this.txtMaxSecondaryRun.Leave += new System.EventHandler(this.txtMaxSecondaryRun_Leave);
            // 
            // txtMinSecondaryRun
            // 
            this.txtMinSecondaryRun.BackColor = System.Drawing.Color.White;
            this.txtMinSecondaryRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinSecondaryRun.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtMinSecondaryRun.Location = new System.Drawing.Point(49, 53);
            this.txtMinSecondaryRun.Margin = new System.Windows.Forms.Padding(4);
            this.txtMinSecondaryRun.Name = "txtMinSecondaryRun";
            this.txtMinSecondaryRun.Size = new System.Drawing.Size(104, 24);
            this.txtMinSecondaryRun.TabIndex = 231;
            this.txtMinSecondaryRun.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinSecondaryRun_KeyUp);
            this.txtMinSecondaryRun.Leave += new System.EventHandler(this.txtMinSecondaryRun_Leave);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(4, 57);
            this.label66.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(32, 18);
            this.label66.TabIndex = 123;
            this.label66.Text = "Min";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(4, 31);
            this.label68.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(36, 18);
            this.label68.TabIndex = 123;
            this.label68.Text = "Max";
            // 
            // btnSaveLogs
            // 
            this.btnSaveLogs.BackColor = System.Drawing.Color.Blue;
            this.btnSaveLogs.Enabled = false;
            this.btnSaveLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveLogs.ForeColor = System.Drawing.Color.Yellow;
            this.btnSaveLogs.Location = new System.Drawing.Point(17, 97);
            this.btnSaveLogs.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveLogs.Name = "btnSaveLogs";
            this.btnSaveLogs.Size = new System.Drawing.Size(145, 41);
            this.btnSaveLogs.TabIndex = 46;
            this.btnSaveLogs.Text = "Save chart";
            this.btnSaveLogs.UseVisualStyleBackColor = false;
            this.btnSaveLogs.Click += new System.EventHandler(this.btnSaveLogs_Click);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.btnAutoscalePrimary);
            this.groupBox17.Controls.Add(this.txtMaxPrimaryRun);
            this.groupBox17.Controls.Add(this.txtMinPrimaryRun);
            this.groupBox17.Controls.Add(this.label69);
            this.groupBox17.Controls.Add(this.label70);
            this.groupBox17.Location = new System.Drawing.Point(474, 20);
            this.groupBox17.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox17.Size = new System.Drawing.Size(163, 121);
            this.groupBox17.TabIndex = 165;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Y axis-Left";
            this.groupBox17.Visible = false;
            // 
            // btnAutoscalePrimary
            // 
            this.btnAutoscalePrimary.Location = new System.Drawing.Point(49, 85);
            this.btnAutoscalePrimary.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoscalePrimary.Name = "btnAutoscalePrimary";
            this.btnAutoscalePrimary.Size = new System.Drawing.Size(105, 31);
            this.btnAutoscalePrimary.TabIndex = 167;
            this.btnAutoscalePrimary.TabStop = false;
            this.btnAutoscalePrimary.Text = "Auto scale";
            this.btnAutoscalePrimary.UseVisualStyleBackColor = true;
            this.btnAutoscalePrimary.Click += new System.EventHandler(this.btnAutoscalePrimary1_Click);
            // 
            // txtMaxPrimaryRun
            // 
            this.txtMaxPrimaryRun.BackColor = System.Drawing.Color.White;
            this.txtMaxPrimaryRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaxPrimaryRun.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtMaxPrimaryRun.Location = new System.Drawing.Point(49, 27);
            this.txtMaxPrimaryRun.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxPrimaryRun.Name = "txtMaxPrimaryRun";
            this.txtMaxPrimaryRun.Size = new System.Drawing.Size(104, 24);
            this.txtMaxPrimaryRun.TabIndex = 228;
            this.txtMaxPrimaryRun.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxPrimaryRun_KeyUp);
            this.txtMaxPrimaryRun.Leave += new System.EventHandler(this.txtMaxPrimaryRun_Leave);
            // 
            // txtMinPrimaryRun
            // 
            this.txtMinPrimaryRun.BackColor = System.Drawing.Color.White;
            this.txtMinPrimaryRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMinPrimaryRun.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtMinPrimaryRun.Location = new System.Drawing.Point(49, 53);
            this.txtMinPrimaryRun.Margin = new System.Windows.Forms.Padding(4);
            this.txtMinPrimaryRun.Name = "txtMinPrimaryRun";
            this.txtMinPrimaryRun.Size = new System.Drawing.Size(104, 24);
            this.txtMinPrimaryRun.TabIndex = 229;
            this.txtMinPrimaryRun.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinPrimaryRun_KeyUp);
            this.txtMinPrimaryRun.Leave += new System.EventHandler(this.txtMinPrimaryRun_Leave);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(4, 57);
            this.label69.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(32, 18);
            this.label69.TabIndex = 123;
            this.label69.Text = "Min";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(4, 31);
            this.label70.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(36, 18);
            this.label70.TabIndex = 123;
            this.label70.Text = "Max";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 18);
            this.label2.TabIndex = 119;
            this.label2.Text = "Chart data";
            // 
            // chkLogitem2secondary
            // 
            this.chkLogitem2secondary.AutoCheck = false;
            this.chkLogitem2secondary.AutoSize = true;
            this.chkLogitem2secondary.Location = new System.Drawing.Point(376, 68);
            this.chkLogitem2secondary.Margin = new System.Windows.Forms.Padding(4);
            this.chkLogitem2secondary.Name = "chkLogitem2secondary";
            this.chkLogitem2secondary.Size = new System.Drawing.Size(18, 17);
            this.chkLogitem2secondary.TabIndex = 154;
            this.chkLogitem2secondary.UseVisualStyleBackColor = true;
            this.chkLogitem2secondary.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLogitem2secondary_MouseClick);
            // 
            // txtValue2
            // 
            this.txtValue2.BackColor = System.Drawing.Color.White;
            this.txtValue2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue2.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtValue2.Location = new System.Drawing.Point(193, 62);
            this.txtValue2.Margin = new System.Windows.Forms.Padding(4);
            this.txtValue2.Name = "txtValue2";
            this.txtValue2.ReadOnly = true;
            this.txtValue2.Size = new System.Drawing.Size(93, 24);
            this.txtValue2.TabIndex = 150;
            // 
            // txtValue1
            // 
            this.txtValue1.BackColor = System.Drawing.Color.White;
            this.txtValue1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue1.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtValue1.Location = new System.Drawing.Point(193, 34);
            this.txtValue1.Margin = new System.Windows.Forms.Padding(4);
            this.txtValue1.Name = "txtValue1";
            this.txtValue1.ReadOnly = true;
            this.txtValue1.Size = new System.Drawing.Size(93, 24);
            this.txtValue1.TabIndex = 148;
            // 
            // cmbSamples
            // 
            this.cmbSamples.BackColor = System.Drawing.Color.White;
            this.cmbSamples.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSamples.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSamples.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbSamples.FormattingEnabled = true;
            this.cmbSamples.Items.AddRange(new object[] {
            "100",
            "200",
            "500"});
            this.cmbSamples.Location = new System.Drawing.Point(322, 112);
            this.cmbSamples.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSamples.Name = "cmbSamples";
            this.cmbSamples.Size = new System.Drawing.Size(75, 26);
            this.cmbSamples.TabIndex = 131;
            this.cmbSamples.Visible = false;
            // 
            // chkLogitem1primary
            // 
            this.chkLogitem1primary.AutoCheck = false;
            this.chkLogitem1primary.AutoSize = true;
            this.chkLogitem1primary.Checked = true;
            this.chkLogitem1primary.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLogitem1primary.Location = new System.Drawing.Point(376, 38);
            this.chkLogitem1primary.Margin = new System.Windows.Forms.Padding(4);
            this.chkLogitem1primary.Name = "chkLogitem1primary";
            this.chkLogitem1primary.Size = new System.Drawing.Size(18, 17);
            this.chkLogitem1primary.TabIndex = 154;
            this.chkLogitem1primary.UseVisualStyleBackColor = true;
            this.chkLogitem1primary.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLogitem1primary_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(268, 119);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 18);
            this.label10.TabIndex = 129;
            this.label10.Text = "msec";
            this.label10.Visible = false;
            // 
            // txtObjectindex1
            // 
            this.txtObjectindex1.Location = new System.Drawing.Point(189, 34);
            this.txtObjectindex1.Margin = new System.Windows.Forms.Padding(4);
            this.txtObjectindex1.Name = "txtObjectindex1";
            this.txtObjectindex1.ReadOnly = true;
            this.txtObjectindex1.Size = new System.Drawing.Size(65, 24);
            this.txtObjectindex1.TabIndex = 133;
            this.txtObjectindex1.Text = "Invisible";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(324, 90);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 18);
            this.label11.TabIndex = 130;
            this.label11.Text = "Samples";
            this.label11.Visible = false;
            // 
            // cmbIntervalLive
            // 
            this.cmbIntervalLive.BackColor = System.Drawing.Color.White;
            this.cmbIntervalLive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIntervalLive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervalLive.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbIntervalLive.FormattingEnabled = true;
            this.cmbIntervalLive.Items.AddRange(new object[] {
            "20",
            "50",
            "100",
            "500",
            "1000",
            "2000",
            "5000",
            "10000"});
            this.cmbIntervalLive.Location = new System.Drawing.Point(189, 113);
            this.cmbIntervalLive.Margin = new System.Windows.Forms.Padding(4);
            this.cmbIntervalLive.Name = "cmbIntervalLive";
            this.cmbIntervalLive.Size = new System.Drawing.Size(73, 26);
            this.cmbIntervalLive.TabIndex = 44;
            this.cmbIntervalLive.Visible = false;
            // 
            // cmbBackgroundobject2
            // 
            this.cmbBackgroundobject2.BackColor = System.Drawing.Color.White;
            this.cmbBackgroundobject2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBackgroundobject2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBackgroundobject2.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbBackgroundobject2.FormattingEnabled = true;
            this.cmbBackgroundobject2.Location = new System.Drawing.Point(3, 60);
            this.cmbBackgroundobject2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBackgroundobject2.Name = "cmbBackgroundobject2";
            this.cmbBackgroundobject2.Size = new System.Drawing.Size(159, 26);
            this.cmbBackgroundobject2.TabIndex = 116;
            this.cmbBackgroundobject2.SelectedIndexChanged += new System.EventHandler(this.cmbBackgroundobject2_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(187, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 18);
            this.label3.TabIndex = 149;
            this.label3.Text = "Value";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInterval.Location = new System.Drawing.Point(186, 90);
            this.lblInterval.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(62, 18);
            this.lblInterval.TabIndex = 43;
            this.lblInterval.Text = "Interval";
            this.lblInterval.Visible = false;
            // 
            // cmbBackgroundobject1
            // 
            this.cmbBackgroundobject1.BackColor = System.Drawing.Color.White;
            this.cmbBackgroundobject1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBackgroundobject1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBackgroundobject1.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbBackgroundobject1.FormattingEnabled = true;
            this.cmbBackgroundobject1.Location = new System.Drawing.Point(3, 32);
            this.cmbBackgroundobject1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbBackgroundobject1.Name = "cmbBackgroundobject1";
            this.cmbBackgroundobject1.Size = new System.Drawing.Size(159, 26);
            this.cmbBackgroundobject1.TabIndex = 115;
            this.cmbBackgroundobject1.SelectedIndexChanged += new System.EventHandler(this.cmbBackgroundobject1_SelectedIndexChanged);
            // 
            // txtObjectindex2
            // 
            this.txtObjectindex2.Location = new System.Drawing.Point(189, 62);
            this.txtObjectindex2.Margin = new System.Windows.Forms.Padding(4);
            this.txtObjectindex2.Name = "txtObjectindex2";
            this.txtObjectindex2.ReadOnly = true;
            this.txtObjectindex2.Size = new System.Drawing.Size(65, 24);
            this.txtObjectindex2.TabIndex = 135;
            this.txtObjectindex2.Text = "Invisible";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(440, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(239, 18);
            this.label7.TabIndex = 176;
            this.label7.Text = "Step 3: Select the chart parameters";
            // 
            // btnContactInfo2
            // 
            this.btnContactInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContactInfo2.Location = new System.Drawing.Point(1157, 98);
            this.btnContactInfo2.Margin = new System.Windows.Forms.Padding(4);
            this.btnContactInfo2.Name = "btnContactInfo2";
            this.btnContactInfo2.Size = new System.Drawing.Size(196, 30);
            this.btnContactInfo2.TabIndex = 137;
            this.btnContactInfo2.Text = "Contact information";
            this.btnContactInfo2.UseVisualStyleBackColor = true;
            this.btnContactInfo2.Click += new System.EventHandler(this.btnContactInfo2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Cornsilk;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtForceSlopeRange);
            this.panel3.Controls.Add(this.txtForceRange);
            this.panel3.Controls.Add(this.txtSoftlandRange);
            this.panel3.Controls.Add(this.txtSpeedRange);
            this.panel3.Controls.Add(this.txtPosition);
            this.panel3.Controls.Add(this.lblFS1);
            this.panel3.Controls.Add(this.lblForce1);
            this.panel3.Controls.Add(this.lblSS1);
            this.panel3.Controls.Add(this.lblSpeed1);
            this.panel3.Controls.Add(this.lbltargetPosition1);
            this.panel3.Controls.Add(this.txtTargetPositionDemo);
            this.panel3.Controls.Add(this.lblTargetPositionDemo);
            this.panel3.Controls.Add(this.txtProfileVelocityDemo);
            this.panel3.Controls.Add(this.lblSpeedDemo);
            this.panel3.Controls.Add(this.txtSoftlandErrorDemo);
            this.panel3.Controls.Add(this.lblSoftlandSensitivityDemo);
            this.panel3.Controls.Add(this.lblForceDemo);
            this.panel3.Controls.Add(this.txtTargetForceDemo);
            this.panel3.Controls.Add(this.txtForceSlopeDemo);
            this.panel3.Controls.Add(this.lblForceSlopeDemo);
            this.panel3.Location = new System.Drawing.Point(9, 150);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(430, 167);
            this.panel3.TabIndex = 175;
            // 
            // txtForceSlopeRange
            // 
            this.txtForceSlopeRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForceSlopeRange.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtForceSlopeRange.Location = new System.Drawing.Point(161, 127);
            this.txtForceSlopeRange.Name = "txtForceSlopeRange";
            this.txtForceSlopeRange.ReadOnly = true;
            this.txtForceSlopeRange.Size = new System.Drawing.Size(91, 24);
            this.txtForceSlopeRange.TabIndex = 176;
            // 
            // txtForceRange
            // 
            this.txtForceRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForceRange.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtForceRange.Location = new System.Drawing.Point(161, 101);
            this.txtForceRange.Name = "txtForceRange";
            this.txtForceRange.ReadOnly = true;
            this.txtForceRange.Size = new System.Drawing.Size(91, 24);
            this.txtForceRange.TabIndex = 175;
            // 
            // txtSoftlandRange
            // 
            this.txtSoftlandRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoftlandRange.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtSoftlandRange.Location = new System.Drawing.Point(177, 72);
            this.txtSoftlandRange.Name = "txtSoftlandRange";
            this.txtSoftlandRange.ReadOnly = true;
            this.txtSoftlandRange.Size = new System.Drawing.Size(75, 24);
            this.txtSoftlandRange.TabIndex = 174;
            // 
            // txtSpeedRange
            // 
            this.txtSpeedRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSpeedRange.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtSpeedRange.Location = new System.Drawing.Point(129, 44);
            this.txtSpeedRange.Name = "txtSpeedRange";
            this.txtSpeedRange.ReadOnly = true;
            this.txtSpeedRange.Size = new System.Drawing.Size(123, 24);
            this.txtSpeedRange.TabIndex = 173;
            // 
            // txtPosition
            // 
            this.txtPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPosition.ForeColor = System.Drawing.Color.DarkGreen;
            this.txtPosition.Location = new System.Drawing.Point(161, 16);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.ReadOnly = true;
            this.txtPosition.Size = new System.Drawing.Size(91, 24);
            this.txtPosition.TabIndex = 172;
            this.txtPosition.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPosition_KeyUp);
            this.txtPosition.Leave += new System.EventHandler(this.txtPosition_Leave);
            // 
            // lblFS1
            // 
            this.lblFS1.AutoSize = true;
            this.lblFS1.Location = new System.Drawing.Point(362, 128);
            this.lblFS1.Name = "lblFS1";
            this.lblFS1.Size = new System.Drawing.Size(44, 18);
            this.lblFS1.TabIndex = 154;
            this.lblFS1.Text = "Value";
            // 
            // lblForce1
            // 
            this.lblForce1.AutoSize = true;
            this.lblForce1.Location = new System.Drawing.Point(362, 100);
            this.lblForce1.Name = "lblForce1";
            this.lblForce1.Size = new System.Drawing.Size(44, 18);
            this.lblForce1.TabIndex = 153;
            this.lblForce1.Text = "Value";
            // 
            // lblSS1
            // 
            this.lblSS1.AutoSize = true;
            this.lblSS1.Location = new System.Drawing.Point(362, 74);
            this.lblSS1.Name = "lblSS1";
            this.lblSS1.Size = new System.Drawing.Size(56, 18);
            this.lblSS1.TabIndex = 152;
            this.lblSS1.Text = "Counts";
            // 
            // lblSpeed1
            // 
            this.lblSpeed1.AutoSize = true;
            this.lblSpeed1.Location = new System.Drawing.Point(362, 48);
            this.lblSpeed1.Name = "lblSpeed1";
            this.lblSpeed1.Size = new System.Drawing.Size(61, 18);
            this.lblSpeed1.TabIndex = 151;
            this.lblSpeed1.Text = "Cnt/Sec";
            // 
            // lbltargetPosition1
            // 
            this.lbltargetPosition1.AutoSize = true;
            this.lbltargetPosition1.Location = new System.Drawing.Point(362, 19);
            this.lbltargetPosition1.Name = "lbltargetPosition1";
            this.lbltargetPosition1.Size = new System.Drawing.Size(56, 18);
            this.lbltargetPosition1.TabIndex = 150;
            this.lbltargetPosition1.Text = "Counts";
            // 
            // txtTargetPositionDemo
            // 
            this.txtTargetPositionDemo.BackColor = System.Drawing.Color.White;
            this.txtTargetPositionDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTargetPositionDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtTargetPositionDemo.Location = new System.Drawing.Point(258, 16);
            this.txtTargetPositionDemo.Multiline = true;
            this.txtTargetPositionDemo.Name = "txtTargetPositionDemo";
            this.txtTargetPositionDemo.Size = new System.Drawing.Size(100, 24);
            this.txtTargetPositionDemo.TabIndex = 140;
            this.txtTargetPositionDemo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTargetPositionDemo_KeyUp);
            this.txtTargetPositionDemo.Leave += new System.EventHandler(this.txtTargetPositionDemo_Leave);
            // 
            // lblTargetPositionDemo
            // 
            this.lblTargetPositionDemo.AutoSize = true;
            this.lblTargetPositionDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTargetPositionDemo.ForeColor = System.Drawing.Color.Blue;
            this.lblTargetPositionDemo.Location = new System.Drawing.Point(6, 20);
            this.lblTargetPositionDemo.Name = "lblTargetPositionDemo";
            this.lblTargetPositionDemo.Size = new System.Drawing.Size(123, 18);
            this.lblTargetPositionDemo.TabIndex = 141;
            this.lblTargetPositionDemo.Text = "Target Position";
            // 
            // txtProfileVelocityDemo
            // 
            this.txtProfileVelocityDemo.BackColor = System.Drawing.Color.White;
            this.txtProfileVelocityDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProfileVelocityDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtProfileVelocityDemo.Location = new System.Drawing.Point(258, 44);
            this.txtProfileVelocityDemo.Multiline = true;
            this.txtProfileVelocityDemo.Name = "txtProfileVelocityDemo";
            this.txtProfileVelocityDemo.Size = new System.Drawing.Size(100, 24);
            this.txtProfileVelocityDemo.TabIndex = 142;
            this.txtProfileVelocityDemo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtProfileVelocityDemo_KeyUp);
            this.txtProfileVelocityDemo.Leave += new System.EventHandler(this.txtProfileVelocityDemo_Leave);
            // 
            // lblSpeedDemo
            // 
            this.lblSpeedDemo.AutoSize = true;
            this.lblSpeedDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeedDemo.ForeColor = System.Drawing.Color.Red;
            this.lblSpeedDemo.Location = new System.Drawing.Point(6, 47);
            this.lblSpeedDemo.Name = "lblSpeedDemo";
            this.lblSpeedDemo.Size = new System.Drawing.Size(55, 18);
            this.lblSpeedDemo.TabIndex = 143;
            this.lblSpeedDemo.Text = "Speed";
            // 
            // txtSoftlandErrorDemo
            // 
            this.txtSoftlandErrorDemo.BackColor = System.Drawing.Color.White;
            this.txtSoftlandErrorDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoftlandErrorDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtSoftlandErrorDemo.Location = new System.Drawing.Point(258, 72);
            this.txtSoftlandErrorDemo.Multiline = true;
            this.txtSoftlandErrorDemo.Name = "txtSoftlandErrorDemo";
            this.txtSoftlandErrorDemo.Size = new System.Drawing.Size(100, 24);
            this.txtSoftlandErrorDemo.TabIndex = 144;
            this.txtSoftlandErrorDemo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSoftlandErrorDemo_KeyUp);
            this.txtSoftlandErrorDemo.Leave += new System.EventHandler(this.txtSoftlandErrorDemo_Leave);
            // 
            // lblSoftlandSensitivityDemo
            // 
            this.lblSoftlandSensitivityDemo.AutoSize = true;
            this.lblSoftlandSensitivityDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftlandSensitivityDemo.ForeColor = System.Drawing.Color.Purple;
            this.lblSoftlandSensitivityDemo.Location = new System.Drawing.Point(4, 75);
            this.lblSoftlandSensitivityDemo.Name = "lblSoftlandSensitivityDemo";
            this.lblSoftlandSensitivityDemo.Size = new System.Drawing.Size(151, 18);
            this.lblSoftlandSensitivityDemo.TabIndex = 145;
            this.lblSoftlandSensitivityDemo.Text = "Softland Sensitivity";
            // 
            // lblForceDemo
            // 
            this.lblForceDemo.AutoSize = true;
            this.lblForceDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForceDemo.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblForceDemo.Location = new System.Drawing.Point(6, 99);
            this.lblForceDemo.Name = "lblForceDemo";
            this.lblForceDemo.Size = new System.Drawing.Size(52, 18);
            this.lblForceDemo.TabIndex = 147;
            this.lblForceDemo.Text = "Force";
            // 
            // txtTargetForceDemo
            // 
            this.txtTargetForceDemo.BackColor = System.Drawing.Color.White;
            this.txtTargetForceDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTargetForceDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtTargetForceDemo.Location = new System.Drawing.Point(258, 99);
            this.txtTargetForceDemo.Multiline = true;
            this.txtTargetForceDemo.Name = "txtTargetForceDemo";
            this.txtTargetForceDemo.Size = new System.Drawing.Size(100, 24);
            this.txtTargetForceDemo.TabIndex = 146;
            this.txtTargetForceDemo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTargetForceDemo_KeyUp);
            this.txtTargetForceDemo.Leave += new System.EventHandler(this.txtTargetForceDemo_Leave);
            // 
            // txtForceSlopeDemo
            // 
            this.txtForceSlopeDemo.BackColor = System.Drawing.Color.White;
            this.txtForceSlopeDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForceSlopeDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.txtForceSlopeDemo.Location = new System.Drawing.Point(259, 126);
            this.txtForceSlopeDemo.Multiline = true;
            this.txtForceSlopeDemo.Name = "txtForceSlopeDemo";
            this.txtForceSlopeDemo.Size = new System.Drawing.Size(100, 24);
            this.txtForceSlopeDemo.TabIndex = 148;
            this.txtForceSlopeDemo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtForceSlopeDemo_KeyUp);
            this.txtForceSlopeDemo.Leave += new System.EventHandler(this.txtForceSlopeDemo_Leave);
            // 
            // lblForceSlopeDemo
            // 
            this.lblForceSlopeDemo.AutoSize = true;
            this.lblForceSlopeDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForceSlopeDemo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblForceSlopeDemo.Location = new System.Drawing.Point(7, 129);
            this.lblForceSlopeDemo.Name = "lblForceSlopeDemo";
            this.lblForceSlopeDemo.Size = new System.Drawing.Size(100, 18);
            this.lblForceSlopeDemo.TabIndex = 149;
            this.lblForceSlopeDemo.Text = "Force Slope";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(428, 18);
            this.label5.TabIndex = 173;
            this.label5.Text = "Step 1: Select the actuator type and choose the demo sequence";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Cornsilk;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cmbActuatorDemo);
            this.panel2.Controls.Add(this.lblActuator);
            this.panel2.Controls.Add(this.cmbProgramTypeDemo);
            this.panel2.Controls.Add(this.lblProgramType);
            this.panel2.Controls.Add(this.btnInfo);
            this.panel2.Location = new System.Drawing.Point(9, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(840, 96);
            this.panel2.TabIndex = 172;
            // 
            // cmbActuatorDemo
            // 
            this.cmbActuatorDemo.BackColor = System.Drawing.Color.White;
            this.cmbActuatorDemo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActuatorDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbActuatorDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbActuatorDemo.FormattingEnabled = true;
            this.cmbActuatorDemo.Items.AddRange(new object[] {
            "LAL35-xxx-xxx"});
            this.cmbActuatorDemo.Location = new System.Drawing.Point(164, 10);
            this.cmbActuatorDemo.Name = "cmbActuatorDemo";
            this.cmbActuatorDemo.Size = new System.Drawing.Size(177, 26);
            this.cmbActuatorDemo.TabIndex = 169;
            this.cmbActuatorDemo.SelectedIndexChanged += new System.EventHandler(this.cmbActuatorDemo_SelectedIndexChanged);
            // 
            // lblActuator
            // 
            this.lblActuator.AutoSize = true;
            this.lblActuator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActuator.Location = new System.Drawing.Point(1, 13);
            this.lblActuator.Name = "lblActuator";
            this.lblActuator.Size = new System.Drawing.Size(123, 18);
            this.lblActuator.TabIndex = 171;
            this.lblActuator.Text = "Select Actuator";
            // 
            // cmbProgramTypeDemo
            // 
            this.cmbProgramTypeDemo.BackColor = System.Drawing.Color.White;
            this.cmbProgramTypeDemo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProgramTypeDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbProgramTypeDemo.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbProgramTypeDemo.FormattingEnabled = true;
            this.cmbProgramTypeDemo.Items.AddRange(new object[] {
            "Move to specific position",
            "Move to position, then softland on object, hold position for 2 sec, return home",
            "Move to position, softand on object, then apply force, hold 2 sec, return home"});
            this.cmbProgramTypeDemo.Location = new System.Drawing.Point(164, 46);
            this.cmbProgramTypeDemo.Name = "cmbProgramTypeDemo";
            this.cmbProgramTypeDemo.Size = new System.Drawing.Size(670, 26);
            this.cmbProgramTypeDemo.TabIndex = 138;
            this.cmbProgramTypeDemo.SelectedIndexChanged += new System.EventHandler(this.cmbProgramTypeDemo_SelectedIndexChanged);
            // 
            // lblProgramType
            // 
            this.lblProgramType.AutoSize = true;
            this.lblProgramType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgramType.ForeColor = System.Drawing.Color.Black;
            this.lblProgramType.Location = new System.Drawing.Point(1, 50);
            this.lblProgramType.Name = "lblProgramType";
            this.lblProgramType.Size = new System.Drawing.Size(146, 18);
            this.lblProgramType.TabIndex = 168;
            this.lblProgramType.Text = "Choose Sequence";
            // 
            // btnInfo
            // 
            this.btnInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInfo.BackColor = System.Drawing.Color.Purple;
            this.btnInfo.FlatAppearance.BorderSize = 2;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnInfo.ForeColor = System.Drawing.Color.White;
            this.btnInfo.Location = new System.Drawing.Point(705, 5);
            this.btnInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(130, 31);
            this.btnInfo.TabIndex = 502;
            this.btnInfo.Text = "Connection Info";
            this.btnInfo.UseVisualStyleBackColor = false;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // cmbController
            // 
            this.cmbController.BackColor = System.Drawing.Color.AliceBlue;
            this.cmbController.ForeColor = System.Drawing.Color.Yellow;
            this.cmbController.FormattingEnabled = true;
            this.cmbController.Location = new System.Drawing.Point(1222, 317);
            this.cmbController.Name = "cmbController";
            this.cmbController.Size = new System.Drawing.Size(121, 26);
            this.cmbController.TabIndex = 170;
            this.cmbController.Visible = false;
            this.cmbController.SelectedIndexChanged += new System.EventHandler(this.cmbController_SelectedIndexChanged);
            // 
            // cmbNode2
            // 
            this.cmbNode2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNode2.FormattingEnabled = true;
            this.cmbNode2.Location = new System.Drawing.Point(1279, 391);
            this.cmbNode2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbNode2.Name = "cmbNode2";
            this.cmbNode2.Size = new System.Drawing.Size(63, 26);
            this.cmbNode2.TabIndex = 167;
            this.cmbNode2.Visible = false;
            // 
            // cmbNode1
            // 
            this.cmbNode1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNode1.FormattingEnabled = true;
            this.cmbNode1.Location = new System.Drawing.Point(1279, 345);
            this.cmbNode1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbNode1.Name = "cmbNode1";
            this.cmbNode1.Size = new System.Drawing.Size(63, 26);
            this.cmbNode1.TabIndex = 167;
            this.cmbNode1.Visible = false;
            this.cmbNode1.SelectedIndexChanged += new System.EventHandler(this.cmbNode1_SelectedIndexChanged);
            // 
            // chkLogitem2primary
            // 
            this.chkLogitem2primary.AutoCheck = false;
            this.chkLogitem2primary.AutoSize = true;
            this.chkLogitem2primary.Location = new System.Drawing.Point(1231, 391);
            this.chkLogitem2primary.Margin = new System.Windows.Forms.Padding(4);
            this.chkLogitem2primary.Name = "chkLogitem2primary";
            this.chkLogitem2primary.Size = new System.Drawing.Size(18, 17);
            this.chkLogitem2primary.TabIndex = 154;
            this.chkLogitem2primary.UseVisualStyleBackColor = true;
            this.chkLogitem2primary.Visible = false;
            this.chkLogitem2primary.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLogitem2primary_MouseClick);
            // 
            // chkLogitem1secondary
            // 
            this.chkLogitem1secondary.AutoCheck = false;
            this.chkLogitem1secondary.AutoSize = true;
            this.chkLogitem1secondary.Location = new System.Drawing.Point(1231, 354);
            this.chkLogitem1secondary.Margin = new System.Windows.Forms.Padding(4);
            this.chkLogitem1secondary.Name = "chkLogitem1secondary";
            this.chkLogitem1secondary.Size = new System.Drawing.Size(18, 17);
            this.chkLogitem1secondary.TabIndex = 154;
            this.chkLogitem1secondary.UseVisualStyleBackColor = true;
            this.chkLogitem1secondary.Visible = false;
            this.chkLogitem1secondary.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLogitem1secondary_MouseClick);
            // 
            // labelctr2
            // 
            this.labelctr2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelctr2.AutoSize = true;
            this.labelctr2.Location = new System.Drawing.Point(1221, 394);
            this.labelctr2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelctr2.Name = "labelctr2";
            this.labelctr2.Size = new System.Drawing.Size(73, 18);
            this.labelctr2.TabIndex = 134;
            this.labelctr2.Text = "Counter 2";
            this.labelctr2.Visible = false;
            // 
            // labelctr1
            // 
            this.labelctr1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelctr1.AutoSize = true;
            this.labelctr1.Location = new System.Drawing.Point(1221, 375);
            this.labelctr1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelctr1.Name = "labelctr1";
            this.labelctr1.Size = new System.Drawing.Size(73, 18);
            this.labelctr1.TabIndex = 133;
            this.labelctr1.Text = "Counter 1";
            this.labelctr1.Visible = false;
            // 
            // chartRun
            // 
            this.chartRun.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chartRun.BackColor = System.Drawing.Color.Black;
            chartArea1.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisX.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.MajorGrid.Interval = 0D;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.MajorTickMark.Interval = 0D;
            chartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisX.MinorGrid.Enabled = true;
            chartArea1.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisX.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.ScrollBar.Enabled = false;
            chartArea1.AxisX.Title = "Time (msec)";
            chartArea1.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea1.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisY.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY.MajorGrid.Interval = 0D;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY.MajorTickMark.Interval = 0D;
            chartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY.Maximum = 2000D;
            chartArea1.AxisY.Minimum = -2000D;
            chartArea1.AxisY.MinorGrid.Enabled = true;
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY.Title = "Current Position";
            chartArea1.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea1.AxisY.TitleForeColor = System.Drawing.Color.White;
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY2.MajorGrid.Interval = 0D;
            chartArea1.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisY2.MajorTickMark.Interval = 0D;
            chartArea1.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY2.MinorGrid.Enabled = true;
            chartArea1.AxisY2.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea1.AxisY2.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea1.AxisY2.Title = "Actual Force ";
            chartArea1.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea1.AxisY2.TitleForeColor = System.Drawing.Color.White;
            chartArea1.BackColor = System.Drawing.Color.DimGray;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 72.04957F;
            chartArea1.Position.Width = 94F;
            chartArea1.Position.X = 3F;
            chartArea1.Position.Y = 24.95043F;
            this.chartRun.ChartAreas.Add(chartArea1);
            legend1.BackColor = System.Drawing.Color.Transparent;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend1.Enabled = false;
            legend1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            legend1.ForeColor = System.Drawing.Color.White;
            legend1.IsDockedInsideChartArea = false;
            legend1.IsTextAutoFit = false;
            legend1.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend1.Name = "LegendPrimary";
            legend1.Position.Auto = false;
            legend1.Position.Height = 20F;
            legend1.Position.Width = 26.47528F;
            legend1.Position.X = 3F;
            legend1.Position.Y = 3F;
            legend1.TextWrapThreshold = 75;
            legend2.Alignment = System.Drawing.StringAlignment.Far;
            legend2.BackColor = System.Drawing.Color.Transparent;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend2.Enabled = false;
            legend2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            legend2.ForeColor = System.Drawing.Color.White;
            legend2.IsDockedInsideChartArea = false;
            legend2.IsTextAutoFit = false;
            legend2.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend2.Name = "LegendSecondary";
            legend2.Position.Auto = false;
            legend2.Position.Height = 20F;
            legend2.Position.Width = 28.86762F;
            legend2.Position.X = 68.13238F;
            legend2.Position.Y = 3F;
            legend2.TextWrapThreshold = 75;
            this.chartRun.Legends.Add(legend1);
            this.chartRun.Legends.Add(legend2);
            this.chartRun.Location = new System.Drawing.Point(9, 324);
            this.chartRun.Margin = new System.Windows.Forms.Padding(4);
            this.chartRun.Name = "chartRun";
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.LimeGreen;
            series1.Enabled = false;
            series1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            series1.LabelForeColor = System.Drawing.Color.White;
            series1.Legend = "LegendPrimary";
            series1.LegendText = "Actual Position";
            series1.Name = "Current Position";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.BorderWidth = 2;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Lime;
            series2.Enabled = false;
            series2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            series2.LabelForeColor = System.Drawing.Color.White;
            series2.Legend = "LegendPrimary";
            series2.LegendText = "Actual Force(% of rated current)";
            series2.Name = "Actual Force";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartRun.Series.Add(series1);
            this.chartRun.Series.Add(series2);
            this.chartRun.Size = new System.Drawing.Size(1346, 459);
            this.chartRun.TabIndex = 113;
            this.chartRun.Text = "chartRun";
            this.chartRun.Customize += new System.EventHandler(this.chartRun_Customize);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = global::Salesman_GUI.Properties.Resources.SMAC_Electric_Cylinder;
            this.pictureBox4.Location = new System.Drawing.Point(1043, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(113, 123);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 186;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.LightGray;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox3.Location = new System.Drawing.Point(1157, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(196, 94);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 184;
            this.pictureBox3.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.panel11);
            this.tabPage1.Controls.Add(this.panel25);
            this.tabPage1.Controls.Add(this.panel21);
            this.tabPage1.Controls.Add(this.btnASP1);
            this.tabPage1.Controls.Add(this.txtMinSecondaryRun1);
            this.tabPage1.Controls.Add(this.txtMinPrimaryRun1);
            this.tabPage1.Controls.Add(this.txtMaxSecondaryRun1);
            this.tabPage1.Controls.Add(this.txtMaxPrimaryRun1);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.panel10);
            this.tabPage1.Controls.Add(this.cmbNode2Measure);
            this.tabPage1.Controls.Add(this.cmbNode1Measure);
            this.tabPage1.Controls.Add(this.chkLP2Measure);
            this.tabPage1.Controls.Add(this.chkLS1Measure);
            this.tabPage1.Controls.Add(this.chkLP1Measure);
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Controls.Add(this.chartMeasure);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Location = new System.Drawing.Point(4, 30);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1363, 796);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "SMACMeasure";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(704, 103);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 18);
            this.label31.TabIndex = 35;
            this.label31.Text = "Step 4: Result";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Cornsilk;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.label55);
            this.panel11.Controls.Add(this.label71);
            this.panel11.Controls.Add(this.label33);
            this.panel11.Controls.Add(this.txtMeasureResult);
            this.panel11.Location = new System.Drawing.Point(707, 124);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(295, 166);
            this.panel11.TabIndex = 28;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.DarkBlue;
            this.label55.Location = new System.Drawing.Point(225, 116);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(60, 31);
            this.label55.TabIndex = 5;
            this.label55.Text = "mm";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.DarkBlue;
            this.label71.Location = new System.Drawing.Point(49, 36);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(72, 25);
            this.label71.TabIndex = 4;
            this.label71.Text = "Result";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.DarkBlue;
            this.label33.Location = new System.Drawing.Point(47, 8);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(143, 25);
            this.label33.TabIndex = 3;
            this.label33.Text = "Measurement";
            // 
            // txtMeasureResult
            // 
            this.txtMeasureResult.BackColor = System.Drawing.Color.Black;
            this.txtMeasureResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeasureResult.ForeColor = System.Drawing.Color.White;
            this.txtMeasureResult.Location = new System.Drawing.Point(38, 68);
            this.txtMeasureResult.Multiline = true;
            this.txtMeasureResult.Name = "txtMeasureResult";
            this.txtMeasureResult.ReadOnly = true;
            this.txtMeasureResult.Size = new System.Drawing.Size(181, 82);
            this.txtMeasureResult.TabIndex = 1;
            this.txtMeasureResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.White;
            this.panel25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel25.Controls.Add(this.pictureBox2);
            this.panel25.Controls.Add(this.textBox9);
            this.panel25.Controls.Add(this.pictureBox14);
            this.panel25.Controls.Add(this.pictureBox12);
            this.panel25.Controls.Add(this.pictureBox7);
            this.panel25.Controls.Add(this.pictureBox5);
            this.panel25.Location = new System.Drawing.Point(14, 296);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1346, 493);
            this.panel25.TabIndex = 34;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::Salesman_GUI.Properties.Resources.vyKgZECA;
            this.pictureBox2.Location = new System.Drawing.Point(1023, 40);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(315, 249);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 36;
            this.pictureBox2.TabStop = false;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.White;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox9.Location = new System.Drawing.Point(435, 384);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(725, 115);
            this.textBox9.TabIndex = 35;
            this.textBox9.Text = "\"The ability to do work and verify quality at same time...\"";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox14.Location = new System.Drawing.Point(7, 309);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(376, 170);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 34;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox12.Image = global::Salesman_GUI.Properties.Resources._39bj1AFA;
            this.pictureBox12.Location = new System.Drawing.Point(690, 40);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(330, 249);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 2;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Image = global::Salesman_GUI.Properties.Resources.PjK9d4Dg;
            this.pictureBox7.Location = new System.Drawing.Point(358, 40);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(329, 249);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 1;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Image = global::Salesman_GUI.Properties.Resources.p__dfkmQ;
            this.pictureBox5.Location = new System.Drawing.Point(6, 40);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(349, 249);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.PaleGreen;
            this.panel21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel21.Controls.Add(this.label72);
            this.panel21.Controls.Add(this.label67);
            this.panel21.Controls.Add(this.label65);
            this.panel21.Controls.Add(this.label64);
            this.panel21.Controls.Add(this.label63);
            this.panel21.Controls.Add(this.label32);
            this.panel21.Location = new System.Drawing.Point(1008, 124);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(352, 166);
            this.panel21.TabIndex = 29;
            this.panel21.Paint += new System.Windows.Forms.PaintEventHandler(this.panel21_Paint);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label72.Location = new System.Drawing.Point(104, 61);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(141, 24);
            this.label72.TabIndex = 5;
            this.label72.Text = "- Thread guage";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label67.Location = new System.Drawing.Point(105, 131);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(130, 24);
            this.label67.TabIndex = 4;
            this.label67.Text = "- Depth guage";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label65.Location = new System.Drawing.Point(105, 106);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(118, 24);
            this.label65.TabIndex = 3;
            this.label65.Text = "- Plug guage";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label64.Location = new System.Drawing.Point(105, 82);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(155, 24);
            this.label64.TabIndex = 2;
            this.label64.Text = "- Diameter guage";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.SaddleBrown;
            this.label63.Location = new System.Drawing.Point(104, 36);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(135, 24);
            this.label63.TabIndex = 1;
            this.label63.Text = "- Height guage";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label32.Location = new System.Drawing.Point(7, 5);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(253, 24);
            this.label32.TabIndex = 0;
            this.label32.Text = "Measurement capabilities:";
            // 
            // btnASP1
            // 
            this.btnASP1.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnASP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnASP1.ForeColor = System.Drawing.SystemColors.Menu;
            this.btnASP1.Location = new System.Drawing.Point(884, 349);
            this.btnASP1.Name = "btnASP1";
            this.btnASP1.Size = new System.Drawing.Size(89, 27);
            this.btnASP1.TabIndex = 25;
            this.btnASP1.UseVisualStyleBackColor = false;
            this.btnASP1.Visible = false;
            // 
            // txtMinSecondaryRun1
            // 
            this.txtMinSecondaryRun1.Location = new System.Drawing.Point(717, 378);
            this.txtMinSecondaryRun1.Name = "txtMinSecondaryRun1";
            this.txtMinSecondaryRun1.Size = new System.Drawing.Size(100, 24);
            this.txtMinSecondaryRun1.TabIndex = 32;
            this.txtMinSecondaryRun1.Visible = false;
            this.txtMinSecondaryRun1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinSecondaryRun1_KeyUp);
            this.txtMinSecondaryRun1.Leave += new System.EventHandler(this.txtMinSecondaryRun1_Leave);
            // 
            // txtMinPrimaryRun1
            // 
            this.txtMinPrimaryRun1.Location = new System.Drawing.Point(585, 378);
            this.txtMinPrimaryRun1.Name = "txtMinPrimaryRun1";
            this.txtMinPrimaryRun1.Size = new System.Drawing.Size(100, 24);
            this.txtMinPrimaryRun1.TabIndex = 31;
            this.txtMinPrimaryRun1.Visible = false;
            this.txtMinPrimaryRun1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinPrimaryRun1_KeyUp);
            this.txtMinPrimaryRun1.Leave += new System.EventHandler(this.txtMinPrimaryRun1_Leave);
            // 
            // txtMaxSecondaryRun1
            // 
            this.txtMaxSecondaryRun1.Location = new System.Drawing.Point(717, 329);
            this.txtMaxSecondaryRun1.Name = "txtMaxSecondaryRun1";
            this.txtMaxSecondaryRun1.Size = new System.Drawing.Size(100, 24);
            this.txtMaxSecondaryRun1.TabIndex = 30;
            this.txtMaxSecondaryRun1.Visible = false;
            this.txtMaxSecondaryRun1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxSecondaryRun1_KeyUp);
            this.txtMaxSecondaryRun1.Leave += new System.EventHandler(this.txtMaxSecondaryRun1_Leave);
            // 
            // txtMaxPrimaryRun1
            // 
            this.txtMaxPrimaryRun1.Location = new System.Drawing.Point(585, 329);
            this.txtMaxPrimaryRun1.Name = "txtMaxPrimaryRun1";
            this.txtMaxPrimaryRun1.Size = new System.Drawing.Size(100, 24);
            this.txtMaxPrimaryRun1.TabIndex = 29;
            this.txtMaxPrimaryRun1.Visible = false;
            this.txtMaxPrimaryRun1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxPrimaryRun1_KeyUp);
            this.txtMaxPrimaryRun1.Leave += new System.EventHandler(this.txtMaxPrimaryRun1_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(497, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(177, 18);
            this.label30.TabIndex = 27;
            this.label30.Text = "Step 3: Choose operation";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Cornsilk;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.btnTeachDatum);
            this.panel10.Controls.Add(this.btnMeasureStop);
            this.panel10.Controls.Add(this.btnASMeasure);
            this.panel10.Controls.Add(this.btnMeasureHome);
            this.panel10.Location = new System.Drawing.Point(499, 123);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 167);
            this.panel10.TabIndex = 26;
            // 
            // btnTeachDatum
            // 
            this.btnTeachDatum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnTeachDatum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTeachDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTeachDatum.Location = new System.Drawing.Point(10, 45);
            this.btnTeachDatum.Name = "btnTeachDatum";
            this.btnTeachDatum.Size = new System.Drawing.Size(178, 30);
            this.btnTeachDatum.TabIndex = 3;
            this.btnTeachDatum.Text = "Teach Datum";
            this.btnTeachDatum.UseVisualStyleBackColor = false;
            this.btnTeachDatum.Click += new System.EventHandler(this.btnTeachDatum_Click_1);
            // 
            // btnMeasureStop
            // 
            this.btnMeasureStop.BackColor = System.Drawing.Color.Red;
            this.btnMeasureStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMeasureStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasureStop.Location = new System.Drawing.Point(10, 119);
            this.btnMeasureStop.Name = "btnMeasureStop";
            this.btnMeasureStop.Size = new System.Drawing.Size(178, 30);
            this.btnMeasureStop.TabIndex = 2;
            this.btnMeasureStop.Text = "Actuator Stop";
            this.btnMeasureStop.UseVisualStyleBackColor = false;
            this.btnMeasureStop.Click += new System.EventHandler(this.btnMeasureStop_Click);
            // 
            // btnASMeasure
            // 
            this.btnASMeasure.BackColor = System.Drawing.Color.Lime;
            this.btnASMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnASMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnASMeasure.Location = new System.Drawing.Point(10, 82);
            this.btnASMeasure.Name = "btnASMeasure";
            this.btnASMeasure.Size = new System.Drawing.Size(178, 30);
            this.btnASMeasure.TabIndex = 1;
            this.btnASMeasure.Text = "Actuator Start";
            this.btnASMeasure.UseVisualStyleBackColor = false;
            this.btnASMeasure.Click += new System.EventHandler(this.btnASMeasure_Click_1);
            // 
            // btnMeasureHome
            // 
            this.btnMeasureHome.BackColor = System.Drawing.Color.Yellow;
            this.btnMeasureHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMeasureHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasureHome.Location = new System.Drawing.Point(10, 8);
            this.btnMeasureHome.Name = "btnMeasureHome";
            this.btnMeasureHome.Size = new System.Drawing.Size(178, 30);
            this.btnMeasureHome.TabIndex = 0;
            this.btnMeasureHome.Text = "Move To Home";
            this.btnMeasureHome.UseVisualStyleBackColor = false;
            this.btnMeasureHome.Click += new System.EventHandler(this.btnMeasureHome_Click_1);
            // 
            // cmbNode2Measure
            // 
            this.cmbNode2Measure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNode2Measure.FormattingEnabled = true;
            this.cmbNode2Measure.Location = new System.Drawing.Point(1205, 354);
            this.cmbNode2Measure.Name = "cmbNode2Measure";
            this.cmbNode2Measure.Size = new System.Drawing.Size(52, 26);
            this.cmbNode2Measure.TabIndex = 25;
            this.cmbNode2Measure.Visible = false;
            // 
            // cmbNode1Measure
            // 
            this.cmbNode1Measure.BackColor = System.Drawing.SystemColors.Window;
            this.cmbNode1Measure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNode1Measure.FormattingEnabled = true;
            this.cmbNode1Measure.Location = new System.Drawing.Point(1205, 326);
            this.cmbNode1Measure.Name = "cmbNode1Measure";
            this.cmbNode1Measure.Size = new System.Drawing.Size(52, 26);
            this.cmbNode1Measure.TabIndex = 24;
            this.cmbNode1Measure.Visible = false;
            // 
            // chkLP2Measure
            // 
            this.chkLP2Measure.AutoCheck = false;
            this.chkLP2Measure.AutoSize = true;
            this.chkLP2Measure.BackColor = System.Drawing.Color.Cornsilk;
            this.chkLP2Measure.Location = new System.Drawing.Point(1157, 354);
            this.chkLP2Measure.Name = "chkLP2Measure";
            this.chkLP2Measure.Size = new System.Drawing.Size(18, 17);
            this.chkLP2Measure.TabIndex = 23;
            this.chkLP2Measure.UseVisualStyleBackColor = true;
            this.chkLP2Measure.Visible = false;
            this.chkLP2Measure.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLP2Measure_MouseClick);
            // 
            // chkLS1Measure
            // 
            this.chkLS1Measure.AutoCheck = false;
            this.chkLS1Measure.AutoSize = true;
            this.chkLS1Measure.BackColor = System.Drawing.Color.Cornsilk;
            this.chkLS1Measure.Location = new System.Drawing.Point(1157, 331);
            this.chkLS1Measure.Name = "chkLS1Measure";
            this.chkLS1Measure.Size = new System.Drawing.Size(18, 17);
            this.chkLS1Measure.TabIndex = 22;
            this.chkLS1Measure.UseVisualStyleBackColor = true;
            this.chkLS1Measure.Visible = false;
            this.chkLS1Measure.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLS1Measure_MouseClick);
            // 
            // chkLP1Measure
            // 
            this.chkLP1Measure.AutoCheck = false;
            this.chkLP1Measure.AutoSize = true;
            this.chkLP1Measure.BackColor = System.Drawing.Color.Cornsilk;
            this.chkLP1Measure.Checked = true;
            this.chkLP1Measure.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLP1Measure.Location = new System.Drawing.Point(752, 143);
            this.chkLP1Measure.Name = "chkLP1Measure";
            this.chkLP1Measure.Size = new System.Drawing.Size(18, 17);
            this.chkLP1Measure.TabIndex = 4;
            this.chkLP1Measure.UseVisualStyleBackColor = true;
            this.chkLP1Measure.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLP1Measure_MouseClick);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Cornsilk;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.btnSavelogMeasure);
            this.panel9.Controls.Add(this.cmbSamMeasure);
            this.panel9.Controls.Add(this.cmbILMeasure);
            this.panel9.Controls.Add(this.chkLS2Measure);
            this.panel9.Controls.Add(this.txtMeasureValue2);
            this.panel9.Controls.Add(this.txtMeasureValue1);
            this.panel9.Controls.Add(this.cmbBGobject2);
            this.panel9.Controls.Add(this.cmbBGobject1);
            this.panel9.Controls.Add(this.txtObIn1Measure);
            this.panel9.Controls.Add(this.txtObIn2Measure);
            this.panel9.Location = new System.Drawing.Point(493, 124);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(283, 166);
            this.panel9.TabIndex = 20;
            this.panel9.Visible = false;
            // 
            // btnSavelogMeasure
            // 
            this.btnSavelogMeasure.BackColor = System.Drawing.Color.Blue;
            this.btnSavelogMeasure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavelogMeasure.Location = new System.Drawing.Point(33, 112);
            this.btnSavelogMeasure.Name = "btnSavelogMeasure";
            this.btnSavelogMeasure.Size = new System.Drawing.Size(125, 33);
            this.btnSavelogMeasure.TabIndex = 24;
            this.btnSavelogMeasure.Text = "Save Chart";
            this.btnSavelogMeasure.UseVisualStyleBackColor = false;
            // 
            // cmbSamMeasure
            // 
            this.cmbSamMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSamMeasure.FormattingEnabled = true;
            this.cmbSamMeasure.Location = new System.Drawing.Point(188, 116);
            this.cmbSamMeasure.Name = "cmbSamMeasure";
            this.cmbSamMeasure.Size = new System.Drawing.Size(52, 26);
            this.cmbSamMeasure.TabIndex = 23;
            this.cmbSamMeasure.Visible = false;
            // 
            // cmbILMeasure
            // 
            this.cmbILMeasure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbILMeasure.FormattingEnabled = true;
            this.cmbILMeasure.Location = new System.Drawing.Point(123, 116);
            this.cmbILMeasure.Name = "cmbILMeasure";
            this.cmbILMeasure.Size = new System.Drawing.Size(52, 26);
            this.cmbILMeasure.TabIndex = 22;
            this.cmbILMeasure.Visible = false;
            // 
            // chkLS2Measure
            // 
            this.chkLS2Measure.AutoCheck = false;
            this.chkLS2Measure.AutoSize = true;
            this.chkLS2Measure.Location = new System.Drawing.Point(257, 49);
            this.chkLS2Measure.Name = "chkLS2Measure";
            this.chkLS2Measure.Size = new System.Drawing.Size(18, 17);
            this.chkLS2Measure.TabIndex = 21;
            this.chkLS2Measure.UseVisualStyleBackColor = true;
            this.chkLS2Measure.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chkLS2Measure_MouseClick);
            // 
            // txtMeasureValue2
            // 
            this.txtMeasureValue2.Location = new System.Drawing.Point(164, 44);
            this.txtMeasureValue2.Name = "txtMeasureValue2";
            this.txtMeasureValue2.Size = new System.Drawing.Size(78, 24);
            this.txtMeasureValue2.TabIndex = 3;
            // 
            // txtMeasureValue1
            // 
            this.txtMeasureValue1.Location = new System.Drawing.Point(163, 9);
            this.txtMeasureValue1.Name = "txtMeasureValue1";
            this.txtMeasureValue1.Size = new System.Drawing.Size(79, 24);
            this.txtMeasureValue1.TabIndex = 2;
            // 
            // cmbBGobject2
            // 
            this.cmbBGobject2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBGobject2.FormattingEnabled = true;
            this.cmbBGobject2.Location = new System.Drawing.Point(7, 42);
            this.cmbBGobject2.Name = "cmbBGobject2";
            this.cmbBGobject2.Size = new System.Drawing.Size(133, 26);
            this.cmbBGobject2.TabIndex = 1;
            // 
            // cmbBGobject1
            // 
            this.cmbBGobject1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBGobject1.FormattingEnabled = true;
            this.cmbBGobject1.Location = new System.Drawing.Point(7, 8);
            this.cmbBGobject1.Name = "cmbBGobject1";
            this.cmbBGobject1.Size = new System.Drawing.Size(133, 26);
            this.cmbBGobject1.TabIndex = 0;
            this.cmbBGobject1.SelectedIndexChanged += new System.EventHandler(this.cmbBGobject1_SelectedIndexChanged);
            // 
            // txtObIn1Measure
            // 
            this.txtObIn1Measure.Location = new System.Drawing.Point(160, 9);
            this.txtObIn1Measure.Name = "txtObIn1Measure";
            this.txtObIn1Measure.Size = new System.Drawing.Size(27, 24);
            this.txtObIn1Measure.TabIndex = 26;
            this.txtObIn1Measure.Text = "Invisible";
            // 
            // txtObIn2Measure
            // 
            this.txtObIn2Measure.Location = new System.Drawing.Point(160, 44);
            this.txtObIn2Measure.Name = "txtObIn2Measure";
            this.txtObIn2Measure.Size = new System.Drawing.Size(27, 24);
            this.txtObIn2Measure.TabIndex = 27;
            this.txtObIn2Measure.Text = "Invisible";
            // 
            // chartMeasure
            // 
            this.chartMeasure.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chartMeasure.BackColor = System.Drawing.Color.Black;
            chartArea2.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea2.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea2.AxisX.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.MajorGrid.Interval = 0D;
            chartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisX.MajorTickMark.Interval = 0D;
            chartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisX.MinorGrid.Enabled = true;
            chartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisX.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisX.Title = "Time(msec)";
            chartArea2.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea2.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea2.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea2.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea2.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea2.AxisY.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY.MajorGrid.Interval = 0D;
            chartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisY.MajorTickMark.Interval = 0D;
            chartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY.Maximum = 2000D;
            chartArea2.AxisY.Minimum = -2000D;
            chartArea2.AxisY.MinorGrid.Enabled = true;
            chartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY.Title = "Current Position";
            chartArea2.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea2.AxisY.TitleForeColor = System.Drawing.Color.White;
            chartArea2.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea2.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea2.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY2.MajorGrid.Interval = 0D;
            chartArea2.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea2.AxisY2.MajorTickMark.Interval = 0D;
            chartArea2.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY2.MinorGrid.Enabled = true;
            chartArea2.AxisY2.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea2.AxisY2.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea2.AxisY2.Title = "Actual Force";
            chartArea2.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea2.AxisY2.TitleForeColor = System.Drawing.Color.White;
            chartArea2.BackColor = System.Drawing.Color.DimGray;
            chartArea2.Name = "ChartArea2";
            this.chartMeasure.ChartAreas.Add(chartArea2);
            legend3.BackColor = System.Drawing.Color.Transparent;
            legend3.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend3.ForeColor = System.Drawing.Color.White;
            legend3.IsDockedInsideChartArea = false;
            legend3.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend3.Name = "LegendPrimary1";
            legend3.TextWrapThreshold = 75;
            legend4.Alignment = System.Drawing.StringAlignment.Far;
            legend4.BackColor = System.Drawing.Color.Transparent;
            legend4.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend4.ForeColor = System.Drawing.Color.White;
            legend4.IsDockedInsideChartArea = false;
            legend4.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend4.Name = "LegendSecondary1";
            legend4.TextWrapThreshold = 75;
            this.chartMeasure.Legends.Add(legend3);
            this.chartMeasure.Legends.Add(legend4);
            this.chartMeasure.Location = new System.Drawing.Point(14, 296);
            this.chartMeasure.Name = "chartMeasure";
            series3.BorderWidth = 2;
            series3.ChartArea = "ChartArea2";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.LimeGreen;
            series3.Enabled = false;
            series3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            series3.LabelForeColor = System.Drawing.Color.White;
            series3.Legend = "LegendPrimary1";
            series3.LegendText = "Actual Position";
            series3.Name = "Current Position";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.BorderWidth = 2;
            series4.ChartArea = "ChartArea2";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.Lime;
            series4.Enabled = false;
            series4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            series4.LabelForeColor = System.Drawing.Color.White;
            series4.Legend = "LegendPrimary1";
            series4.LegendText = "Actual Force(% of Rated Current)";
            series4.Name = "Actual Force";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series4.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartMeasure.Series.Add(series3);
            this.chartMeasure.Series.Add(series4);
            this.chartMeasure.Size = new System.Drawing.Size(1328, 478);
            this.chartMeasure.TabIndex = 6;
            this.chartMeasure.Text = "chartMeasure";
            this.chartMeasure.Visible = false;
            this.chartMeasure.Customize += new System.EventHandler(this.chartMeasure_Customize);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(315, 103);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(103, 18);
            this.label25.TabIndex = 5;
            this.label25.Text = "Control values";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(252, 103);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 18);
            this.label24.TabIndex = 4;
            this.label24.Text = "Range";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(11, 101);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(205, 18);
            this.label23.TabIndex = 3;
            this.label23.Text = "Step 2: Enter control variables";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Cornsilk;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.txtMeasureDT);
            this.panel8.Controls.Add(this.txtMsRangeDT);
            this.panel8.Controls.Add(this.lblMeasureDT);
            this.panel8.Controls.Add(this.label29);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Controls.Add(this.txtMsRangeF);
            this.panel8.Controls.Add(this.txtMsRangeSen);
            this.panel8.Controls.Add(this.txtMsRangeSpeed);
            this.panel8.Controls.Add(this.txtMsRangePos);
            this.panel8.Controls.Add(this.txtMeasureForce);
            this.panel8.Controls.Add(this.lblMeasureForce);
            this.panel8.Controls.Add(this.txtMeasureSensitivity);
            this.panel8.Controls.Add(this.lblMeasureSensitivity);
            this.panel8.Controls.Add(this.txtMeasureSpeed);
            this.panel8.Controls.Add(this.lblMeasureSpeed);
            this.panel8.Controls.Add(this.txtPosCloseToPart);
            this.panel8.Controls.Add(this.lblPosCloseToPart);
            this.panel8.Location = new System.Drawing.Point(14, 124);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(476, 166);
            this.panel8.TabIndex = 2;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(408, 129);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(45, 18);
            this.label34.TabIndex = 22;
            this.label34.Text = "msec";
            // 
            // txtMeasureDT
            // 
            this.txtMeasureDT.Location = new System.Drawing.Point(301, 125);
            this.txtMeasureDT.Name = "txtMeasureDT";
            this.txtMeasureDT.Size = new System.Drawing.Size(100, 24);
            this.txtMeasureDT.TabIndex = 21;
            // 
            // txtMsRangeDT
            // 
            this.txtMsRangeDT.Location = new System.Drawing.Point(182, 125);
            this.txtMsRangeDT.Name = "txtMsRangeDT";
            this.txtMsRangeDT.ReadOnly = true;
            this.txtMsRangeDT.Size = new System.Drawing.Size(113, 24);
            this.txtMsRangeDT.TabIndex = 20;
            // 
            // lblMeasureDT
            // 
            this.lblMeasureDT.AutoSize = true;
            this.lblMeasureDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeasureDT.ForeColor = System.Drawing.Color.Black;
            this.lblMeasureDT.Location = new System.Drawing.Point(4, 125);
            this.lblMeasureDT.Name = "lblMeasureDT";
            this.lblMeasureDT.Size = new System.Drawing.Size(91, 18);
            this.lblMeasureDT.TabIndex = 19;
            this.lblMeasureDT.Text = "Dwell Time";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(408, 98);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 18);
            this.label29.TabIndex = 18;
            this.label29.Text = "Value";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(408, 70);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(56, 18);
            this.label28.TabIndex = 17;
            this.label28.Text = "Counts";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(406, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 18);
            this.label27.TabIndex = 16;
            this.label27.Text = "mm/Sec";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(408, 11);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 18);
            this.label26.TabIndex = 15;
            this.label26.Text = "mm";
            // 
            // txtMsRangeF
            // 
            this.txtMsRangeF.Location = new System.Drawing.Point(182, 97);
            this.txtMsRangeF.Name = "txtMsRangeF";
            this.txtMsRangeF.ReadOnly = true;
            this.txtMsRangeF.Size = new System.Drawing.Size(113, 24);
            this.txtMsRangeF.TabIndex = 13;
            // 
            // txtMsRangeSen
            // 
            this.txtMsRangeSen.Location = new System.Drawing.Point(182, 68);
            this.txtMsRangeSen.Name = "txtMsRangeSen";
            this.txtMsRangeSen.ReadOnly = true;
            this.txtMsRangeSen.Size = new System.Drawing.Size(113, 24);
            this.txtMsRangeSen.TabIndex = 12;
            // 
            // txtMsRangeSpeed
            // 
            this.txtMsRangeSpeed.Location = new System.Drawing.Point(182, 39);
            this.txtMsRangeSpeed.Name = "txtMsRangeSpeed";
            this.txtMsRangeSpeed.ReadOnly = true;
            this.txtMsRangeSpeed.Size = new System.Drawing.Size(113, 24);
            this.txtMsRangeSpeed.TabIndex = 11;
            // 
            // txtMsRangePos
            // 
            this.txtMsRangePos.Location = new System.Drawing.Point(208, 10);
            this.txtMsRangePos.Name = "txtMsRangePos";
            this.txtMsRangePos.ReadOnly = true;
            this.txtMsRangePos.Size = new System.Drawing.Size(87, 24);
            this.txtMsRangePos.TabIndex = 10;
            // 
            // txtMeasureForce
            // 
            this.txtMeasureForce.Location = new System.Drawing.Point(301, 97);
            this.txtMeasureForce.Name = "txtMeasureForce";
            this.txtMeasureForce.Size = new System.Drawing.Size(100, 24);
            this.txtMeasureForce.TabIndex = 7;
            // 
            // lblMeasureForce
            // 
            this.lblMeasureForce.AutoSize = true;
            this.lblMeasureForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeasureForce.ForeColor = System.Drawing.Color.Green;
            this.lblMeasureForce.Location = new System.Drawing.Point(4, 95);
            this.lblMeasureForce.Name = "lblMeasureForce";
            this.lblMeasureForce.Size = new System.Drawing.Size(52, 18);
            this.lblMeasureForce.TabIndex = 6;
            this.lblMeasureForce.Text = "Force";
            // 
            // txtMeasureSensitivity
            // 
            this.txtMeasureSensitivity.Location = new System.Drawing.Point(301, 68);
            this.txtMeasureSensitivity.Name = "txtMeasureSensitivity";
            this.txtMeasureSensitivity.Size = new System.Drawing.Size(100, 24);
            this.txtMeasureSensitivity.TabIndex = 5;
            // 
            // lblMeasureSensitivity
            // 
            this.lblMeasureSensitivity.AutoSize = true;
            this.lblMeasureSensitivity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeasureSensitivity.ForeColor = System.Drawing.Color.Purple;
            this.lblMeasureSensitivity.Location = new System.Drawing.Point(4, 65);
            this.lblMeasureSensitivity.Name = "lblMeasureSensitivity";
            this.lblMeasureSensitivity.Size = new System.Drawing.Size(84, 18);
            this.lblMeasureSensitivity.TabIndex = 4;
            this.lblMeasureSensitivity.Text = "Sensitivity";
            // 
            // txtMeasureSpeed
            // 
            this.txtMeasureSpeed.Location = new System.Drawing.Point(301, 39);
            this.txtMeasureSpeed.MaxLength = 7;
            this.txtMeasureSpeed.Name = "txtMeasureSpeed";
            this.txtMeasureSpeed.Size = new System.Drawing.Size(100, 24);
            this.txtMeasureSpeed.TabIndex = 3;
            // 
            // lblMeasureSpeed
            // 
            this.lblMeasureSpeed.AutoSize = true;
            this.lblMeasureSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeasureSpeed.ForeColor = System.Drawing.Color.Red;
            this.lblMeasureSpeed.Location = new System.Drawing.Point(4, 35);
            this.lblMeasureSpeed.Name = "lblMeasureSpeed";
            this.lblMeasureSpeed.Size = new System.Drawing.Size(55, 18);
            this.lblMeasureSpeed.TabIndex = 2;
            this.lblMeasureSpeed.Text = "Speed";
            // 
            // txtPosCloseToPart
            // 
            this.txtPosCloseToPart.Location = new System.Drawing.Point(301, 10);
            this.txtPosCloseToPart.MaxLength = 5;
            this.txtPosCloseToPart.Name = "txtPosCloseToPart";
            this.txtPosCloseToPart.Size = new System.Drawing.Size(100, 24);
            this.txtPosCloseToPart.TabIndex = 1;
            this.txtPosCloseToPart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPosCloseToPart_KeyUp);
            this.txtPosCloseToPart.Leave += new System.EventHandler(this.txtPosCloseToPart_Leave);
            // 
            // lblPosCloseToPart
            // 
            this.lblPosCloseToPart.AutoSize = true;
            this.lblPosCloseToPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPosCloseToPart.ForeColor = System.Drawing.Color.Blue;
            this.lblPosCloseToPart.Location = new System.Drawing.Point(4, 8);
            this.lblPosCloseToPart.Name = "lblPosCloseToPart";
            this.lblPosCloseToPart.Size = new System.Drawing.Size(170, 18);
            this.lblPosCloseToPart.TabIndex = 0;
            this.lblPosCloseToPart.Text = "Position close to part";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(11, 8);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(344, 18);
            this.label22.TabIndex = 1;
            this.label22.Text = "Step 1: Choose the type of measurement operation";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Cornsilk;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.btnClearChart);
            this.panel7.Controls.Add(this.cmbMeasureType);
            this.panel7.Controls.Add(this.lblMeasureType);
            this.panel7.Location = new System.Drawing.Point(14, 30);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1346, 67);
            this.panel7.TabIndex = 0;
            // 
            // btnClearChart
            // 
            this.btnClearChart.BackColor = System.Drawing.Color.Purple;
            this.btnClearChart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearChart.ForeColor = System.Drawing.Color.White;
            this.btnClearChart.Location = new System.Drawing.Point(778, 12);
            this.btnClearChart.Name = "btnClearChart";
            this.btnClearChart.Size = new System.Drawing.Size(178, 41);
            this.btnClearChart.TabIndex = 2;
            this.btnClearChart.Text = "Clear chart";
            this.btnClearChart.UseVisualStyleBackColor = false;
            // 
            // cmbMeasureType
            // 
            this.cmbMeasureType.BackColor = System.Drawing.Color.White;
            this.cmbMeasureType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMeasureType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMeasureType.ForeColor = System.Drawing.Color.DarkBlue;
            this.cmbMeasureType.FormattingEnabled = true;
            this.cmbMeasureType.Items.AddRange(new object[] {
            "Move to position, softand on object, then apply force, measure"});
            this.cmbMeasureType.Location = new System.Drawing.Point(179, 19);
            this.cmbMeasureType.Name = "cmbMeasureType";
            this.cmbMeasureType.Size = new System.Drawing.Size(570, 26);
            this.cmbMeasureType.TabIndex = 1;
            this.cmbMeasureType.SelectedIndexChanged += new System.EventHandler(this.cmbMeasureType_SelectedIndexChanged);
            // 
            // lblMeasureType
            // 
            this.lblMeasureType.AutoSize = true;
            this.lblMeasureType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMeasureType.Location = new System.Drawing.Point(4, 21);
            this.lblMeasureType.Name = "lblMeasureType";
            this.lblMeasureType.Size = new System.Drawing.Size(151, 18);
            this.lblMeasureType.TabIndex = 0;
            this.lblMeasureType.Text = "Measurement Type";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage2.Controls.Add(this.panel12);
            this.tabPage2.Controls.Add(this.panel26);
            this.tabPage2.Controls.Add(this.panel23);
            this.tabPage2.Controls.Add(this.panel16);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.panel15);
            this.tabPage2.Controls.Add(this.chkLP2Switch);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.panel14);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.panel13);
            this.tabPage2.Controls.Add(this.chartSwitch);
            this.tabPage2.Controls.Add(this.pictureBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 30);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1363, 796);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "SMACSwitchTest";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Cornsilk;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.label43);
            this.panel12.Controls.Add(this.label41);
            this.panel12.Controls.Add(this.label40);
            this.panel12.Controls.Add(this.label39);
            this.panel12.Controls.Add(this.txtPosSwitchRange);
            this.panel12.Controls.Add(this.txtErrorSwitchRange);
            this.panel12.Controls.Add(this.txtSwitchSpeedRange);
            this.panel12.Controls.Add(this.txtSSSRange);
            this.panel12.Controls.Add(this.label36);
            this.panel12.Controls.Add(this.label38);
            this.panel12.Controls.Add(this.txtPosCloseSwitch);
            this.panel12.Controls.Add(this.txtErrorSwitch);
            this.panel12.Controls.Add(this.txtSwitchSpeed);
            this.panel12.Controls.Add(this.label35);
            this.panel12.Controls.Add(this.lblPositionSwitch);
            this.panel12.Controls.Add(this.txtSoftlandSpeedSwitch);
            this.panel12.Location = new System.Drawing.Point(8, 148);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(567, 182);
            this.panel12.TabIndex = 11;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(474, 102);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 18);
            this.label43.TabIndex = 20;
            this.label43.Text = "mm/Sec";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(474, 147);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(56, 18);
            this.label41.TabIndex = 18;
            this.label41.Text = "Counts";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(474, 55);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(64, 18);
            this.label40.TabIndex = 17;
            this.label40.Text = "mm/Sec";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(473, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(34, 18);
            this.label39.TabIndex = 16;
            this.label39.Text = "mm";
            // 
            // txtPosSwitchRange
            // 
            this.txtPosSwitchRange.Location = new System.Drawing.Point(254, 10);
            this.txtPosSwitchRange.Name = "txtPosSwitchRange";
            this.txtPosSwitchRange.ReadOnly = true;
            this.txtPosSwitchRange.Size = new System.Drawing.Size(108, 24);
            this.txtPosSwitchRange.TabIndex = 11;
            // 
            // txtErrorSwitchRange
            // 
            this.txtErrorSwitchRange.Location = new System.Drawing.Point(254, 141);
            this.txtErrorSwitchRange.Name = "txtErrorSwitchRange";
            this.txtErrorSwitchRange.ReadOnly = true;
            this.txtErrorSwitchRange.Size = new System.Drawing.Size(109, 24);
            this.txtErrorSwitchRange.TabIndex = 12;
            // 
            // txtSwitchSpeedRange
            // 
            this.txtSwitchSpeedRange.Location = new System.Drawing.Point(254, 52);
            this.txtSwitchSpeedRange.Name = "txtSwitchSpeedRange";
            this.txtSwitchSpeedRange.ReadOnly = true;
            this.txtSwitchSpeedRange.Size = new System.Drawing.Size(108, 24);
            this.txtSwitchSpeedRange.TabIndex = 13;
            // 
            // txtSSSRange
            // 
            this.txtSSSRange.Location = new System.Drawing.Point(276, 98);
            this.txtSSSRange.Name = "txtSSSRange";
            this.txtSSSRange.ReadOnly = true;
            this.txtSSSRange.Size = new System.Drawing.Size(86, 24);
            this.txtSSSRange.TabIndex = 15;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Purple;
            this.label36.Location = new System.Drawing.Point(3, 144);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(114, 18);
            this.label36.TabIndex = 8;
            this.label36.Text = "Softland Error";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(3, 102);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(122, 18);
            this.label38.TabIndex = 10;
            this.label38.Text = "Softland Speed";
            // 
            // txtPosCloseSwitch
            // 
            this.txtPosCloseSwitch.Location = new System.Drawing.Point(368, 10);
            this.txtPosCloseSwitch.MaxLength = 5;
            this.txtPosCloseSwitch.Name = "txtPosCloseSwitch";
            this.txtPosCloseSwitch.Size = new System.Drawing.Size(100, 24);
            this.txtPosCloseSwitch.TabIndex = 0;
            this.txtPosCloseSwitch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPosCloseSwitch_KeyUp);
            this.txtPosCloseSwitch.Leave += new System.EventHandler(this.txtPosCloseSwitch_Leave);
            // 
            // txtErrorSwitch
            // 
            this.txtErrorSwitch.Location = new System.Drawing.Point(368, 141);
            this.txtErrorSwitch.Name = "txtErrorSwitch";
            this.txtErrorSwitch.Size = new System.Drawing.Size(100, 24);
            this.txtErrorSwitch.TabIndex = 1;
            this.txtErrorSwitch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtErrorSwitch_KeyUp);
            this.txtErrorSwitch.Leave += new System.EventHandler(this.txtErrorSwitch_Leave);
            // 
            // txtSwitchSpeed
            // 
            this.txtSwitchSpeed.Location = new System.Drawing.Point(368, 52);
            this.txtSwitchSpeed.MaxLength = 7;
            this.txtSwitchSpeed.Name = "txtSwitchSpeed";
            this.txtSwitchSpeed.Size = new System.Drawing.Size(100, 24);
            this.txtSwitchSpeed.TabIndex = 2;
            this.txtSwitchSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSwitchSpeed_KeyUp);
            this.txtSwitchSpeed.Leave += new System.EventHandler(this.txtSwitchSpeed_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(3, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(55, 18);
            this.label35.TabIndex = 7;
            this.label35.Text = "Speed";
            // 
            // lblPositionSwitch
            // 
            this.lblPositionSwitch.AutoSize = true;
            this.lblPositionSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPositionSwitch.ForeColor = System.Drawing.Color.Blue;
            this.lblPositionSwitch.Location = new System.Drawing.Point(3, 12);
            this.lblPositionSwitch.Name = "lblPositionSwitch";
            this.lblPositionSwitch.Size = new System.Drawing.Size(204, 18);
            this.lblPositionSwitch.TabIndex = 6;
            this.lblPositionSwitch.Text = "Position Close To Switch ";
            // 
            // txtSoftlandSpeedSwitch
            // 
            this.txtSoftlandSpeedSwitch.Location = new System.Drawing.Point(367, 98);
            this.txtSoftlandSpeedSwitch.MaxLength = 7;
            this.txtSoftlandSpeedSwitch.Name = "txtSoftlandSpeedSwitch";
            this.txtSoftlandSpeedSwitch.Size = new System.Drawing.Size(100, 24);
            this.txtSoftlandSpeedSwitch.TabIndex = 4;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.White;
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel26.Controls.Add(this.pictureBox8);
            this.panel26.Controls.Add(this.pictureBox11);
            this.panel26.Controls.Add(this.pictureBox9);
            this.panel26.Controls.Add(this.pictureBox10);
            this.panel26.Location = new System.Drawing.Point(8, 498);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1343, 286);
            this.panel26.TabIndex = 8;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Salesman_GUI.Properties.Resources._38786_2420847;
            this.pictureBox8.Location = new System.Drawing.Point(4, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(336, 296);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 24;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Salesman_GUI.Properties.Resources.cbl35;
            this.pictureBox11.Location = new System.Drawing.Point(954, 4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(384, 296);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 27;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox9.Location = new System.Drawing.Point(342, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(312, 296);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 25;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Salesman_GUI.Properties.Resources.thumb;
            this.pictureBox10.Location = new System.Drawing.Point(654, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(312, 296);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 26;
            this.pictureBox10.TabStop = false;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.PaleGreen;
            this.panel23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel23.Controls.Add(this.textBox7);
            this.panel23.Controls.Add(this.label90);
            this.panel23.Controls.Add(this.label89);
            this.panel23.Controls.Add(this.label88);
            this.panel23.Controls.Add(this.label84);
            this.panel23.Controls.Add(this.label83);
            this.panel23.Controls.Add(this.label82);
            this.panel23.Location = new System.Drawing.Point(904, 148);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(447, 182);
            this.panel23.TabIndex = 21;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.PaleGreen;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox7.Location = new System.Drawing.Point(66, 124);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(322, 52);
            this.textBox7.TabIndex = 8;
            this.textBox7.Text = "\"The ability to do work and verify quality at same time.\"";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Brown;
            this.label90.Location = new System.Drawing.Point(6, 96);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(204, 25);
            this.label90.TabIndex = 7;
            this.label90.Text = "- Assemble and Verify";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Brown;
            this.label89.Location = new System.Drawing.Point(251, 65);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(184, 25);
            this.label89.TabIndex = 6;
            this.label89.Text = "- Functional Testing";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Brown;
            this.label88.Location = new System.Drawing.Point(6, 65);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(234, 25);
            this.label88.TabIndex = 5;
            this.label88.Text = "- Accelerated Life Testing";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Brown;
            this.label84.Location = new System.Drawing.Point(6, 36);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(178, 25);
            this.label84.TabIndex = 2;
            this.label84.Text = "- Position vs Force ";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Brown;
            this.label83.Location = new System.Drawing.Point(249, 36);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(173, 25);
            this.label83.TabIndex = 1;
            this.label83.Text = "- Force vs Position";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.DarkBlue;
            this.label82.Location = new System.Drawing.Point(8, 7);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(257, 25);
            this.label82.TabIndex = 0;
            this.label82.Text = "Switch Test Capabilities :";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Cornsilk;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.label111);
            this.panel16.Controls.Add(this.label93);
            this.panel16.Controls.Add(this.label49);
            this.panel16.Controls.Add(this.label52);
            this.panel16.Controls.Add(this.label51);
            this.panel16.Controls.Add(this.label50);
            this.panel16.Controls.Add(this.txtIniPosResult);
            this.panel16.Controls.Add(this.txtLastPosResult);
            this.panel16.Controls.Add(this.txtForceResult);
            this.panel16.Location = new System.Drawing.Point(9, 331);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1342, 167);
            this.panel16.TabIndex = 19;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(1091, 66);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(43, 39);
            this.label111.TabIndex = 8;
            this.label111.Text = "N";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(736, 66);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(85, 39);
            this.label93.TabIndex = 7;
            this.label93.Text = "mm ";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(392, 66);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(85, 39);
            this.label49.TabIndex = 6;
            this.label49.Text = "mm ";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(225, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(166, 20);
            this.label52.TabIndex = 5;
            this.label52.Text = "Switch Full Travel ";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(566, 18);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(166, 20);
            this.label51.TabIndex = 4;
            this.label51.Text = "Activation Position";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(893, 15);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(146, 20);
            this.label50.TabIndex = 3;
            this.label50.Text = "Activation Force";
            // 
            // txtIniPosResult
            // 
            this.txtIniPosResult.BackColor = System.Drawing.Color.Orange;
            this.txtIniPosResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIniPosResult.Location = new System.Drawing.Point(228, 50);
            this.txtIniPosResult.Multiline = true;
            this.txtIniPosResult.Name = "txtIniPosResult";
            this.txtIniPosResult.ReadOnly = true;
            this.txtIniPosResult.Size = new System.Drawing.Size(158, 76);
            this.txtIniPosResult.TabIndex = 2;
            this.txtIniPosResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLastPosResult
            // 
            this.txtLastPosResult.BackColor = System.Drawing.Color.Pink;
            this.txtLastPosResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLastPosResult.Location = new System.Drawing.Point(570, 52);
            this.txtLastPosResult.Multiline = true;
            this.txtLastPosResult.Name = "txtLastPosResult";
            this.txtLastPosResult.ReadOnly = true;
            this.txtLastPosResult.Size = new System.Drawing.Size(159, 76);
            this.txtLastPosResult.TabIndex = 1;
            this.txtLastPosResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtForceResult
            // 
            this.txtForceResult.BackColor = System.Drawing.Color.Black;
            this.txtForceResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtForceResult.ForeColor = System.Drawing.Color.White;
            this.txtForceResult.Location = new System.Drawing.Point(894, 51);
            this.txtForceResult.Multiline = true;
            this.txtForceResult.Name = "txtForceResult";
            this.txtForceResult.ReadOnly = true;
            this.txtForceResult.Size = new System.Drawing.Size(183, 76);
            this.txtForceResult.TabIndex = 0;
            this.txtForceResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(577, 122);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(204, 18);
            this.label48.TabIndex = 18;
            this.label48.Text = "Step 3 :Choose Operation";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Cornsilk;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.btnClearSwitch);
            this.panel15.Controls.Add(this.btnStopSwitch);
            this.panel15.Controls.Add(this.btnStartSwitch);
            this.panel15.Controls.Add(this.btnHomeSwitch);
            this.panel15.Location = new System.Drawing.Point(581, 148);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(319, 182);
            this.panel15.TabIndex = 17;
            // 
            // btnClearSwitch
            // 
            this.btnClearSwitch.BackColor = System.Drawing.Color.Silver;
            this.btnClearSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearSwitch.ForeColor = System.Drawing.Color.Black;
            this.btnClearSwitch.Location = new System.Drawing.Point(51, 132);
            this.btnClearSwitch.Name = "btnClearSwitch";
            this.btnClearSwitch.Size = new System.Drawing.Size(220, 28);
            this.btnClearSwitch.TabIndex = 3;
            this.btnClearSwitch.Text = "Clear Result";
            this.btnClearSwitch.UseVisualStyleBackColor = false;
            this.btnClearSwitch.Click += new System.EventHandler(this.btnClearSwitch_Click);
            // 
            // btnStopSwitch
            // 
            this.btnStopSwitch.BackColor = System.Drawing.Color.Red;
            this.btnStopSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStopSwitch.ForeColor = System.Drawing.Color.Black;
            this.btnStopSwitch.Location = new System.Drawing.Point(51, 94);
            this.btnStopSwitch.Name = "btnStopSwitch";
            this.btnStopSwitch.Size = new System.Drawing.Size(220, 31);
            this.btnStopSwitch.TabIndex = 2;
            this.btnStopSwitch.Text = "Actuator Stop";
            this.btnStopSwitch.UseVisualStyleBackColor = false;
            this.btnStopSwitch.Click += new System.EventHandler(this.btnStopSwitch_Click);
            // 
            // btnStartSwitch
            // 
            this.btnStartSwitch.BackColor = System.Drawing.Color.LawnGreen;
            this.btnStartSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartSwitch.ForeColor = System.Drawing.Color.Black;
            this.btnStartSwitch.Location = new System.Drawing.Point(51, 57);
            this.btnStartSwitch.Name = "btnStartSwitch";
            this.btnStartSwitch.Size = new System.Drawing.Size(220, 28);
            this.btnStartSwitch.TabIndex = 1;
            this.btnStartSwitch.Text = "Actuator Start";
            this.btnStartSwitch.UseVisualStyleBackColor = false;
            this.btnStartSwitch.Click += new System.EventHandler(this.btnStartSwitch_Click);
            // 
            // btnHomeSwitch
            // 
            this.btnHomeSwitch.BackColor = System.Drawing.Color.Yellow;
            this.btnHomeSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeSwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomeSwitch.ForeColor = System.Drawing.Color.Black;
            this.btnHomeSwitch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHomeSwitch.Location = new System.Drawing.Point(51, 21);
            this.btnHomeSwitch.Name = "btnHomeSwitch";
            this.btnHomeSwitch.Size = new System.Drawing.Size(220, 30);
            this.btnHomeSwitch.TabIndex = 0;
            this.btnHomeSwitch.Text = "Move To Home";
            this.btnHomeSwitch.UseVisualStyleBackColor = false;
            this.btnHomeSwitch.Click += new System.EventHandler(this.btnHomeSwitch_Click);
            // 
            // chkLP2Switch
            // 
            this.chkLP2Switch.AutoSize = true;
            this.chkLP2Switch.BackColor = System.Drawing.SystemColors.Window;
            this.chkLP2Switch.Location = new System.Drawing.Point(706, 212);
            this.chkLP2Switch.Name = "chkLP2Switch";
            this.chkLP2Switch.Size = new System.Drawing.Size(18, 17);
            this.chkLP2Switch.TabIndex = 5;
            this.chkLP2Switch.UseVisualStyleBackColor = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(374, 122);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(189, 18);
            this.label47.TabIndex = 16;
            this.label47.Text = "Step 3 : Log parameters";
            this.label47.Visible = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Cornsilk;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.btnSavelogSwitch);
            this.panel14.Controls.Add(this.chkLP1Switch);
            this.panel14.Controls.Add(this.txtSwitchValue2);
            this.panel14.Controls.Add(this.txtSwitchValue1);
            this.panel14.Controls.Add(this.cmbSwitchObject2);
            this.panel14.Controls.Add(this.cmbSwitchObject1);
            this.panel14.Controls.Add(this.txtObject1Switch);
            this.panel14.Controls.Add(this.txtObject2Switch);
            this.panel14.Location = new System.Drawing.Point(434, 148);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(299, 182);
            this.panel14.TabIndex = 15;
            this.panel14.Visible = false;
            // 
            // btnSavelogSwitch
            // 
            this.btnSavelogSwitch.BackColor = System.Drawing.Color.Blue;
            this.btnSavelogSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSavelogSwitch.Location = new System.Drawing.Point(15, 117);
            this.btnSavelogSwitch.Name = "btnSavelogSwitch";
            this.btnSavelogSwitch.Size = new System.Drawing.Size(106, 36);
            this.btnSavelogSwitch.TabIndex = 7;
            this.btnSavelogSwitch.Text = "Save Chart";
            this.btnSavelogSwitch.UseVisualStyleBackColor = false;
            // 
            // chkLP1Switch
            // 
            this.chkLP1Switch.AutoSize = true;
            this.chkLP1Switch.Location = new System.Drawing.Point(271, 22);
            this.chkLP1Switch.Name = "chkLP1Switch";
            this.chkLP1Switch.Size = new System.Drawing.Size(18, 17);
            this.chkLP1Switch.TabIndex = 4;
            this.chkLP1Switch.UseVisualStyleBackColor = true;
            // 
            // txtSwitchValue2
            // 
            this.txtSwitchValue2.Location = new System.Drawing.Point(162, 59);
            this.txtSwitchValue2.Name = "txtSwitchValue2";
            this.txtSwitchValue2.Size = new System.Drawing.Size(100, 24);
            this.txtSwitchValue2.TabIndex = 3;
            // 
            // txtSwitchValue1
            // 
            this.txtSwitchValue1.Location = new System.Drawing.Point(162, 18);
            this.txtSwitchValue1.Name = "txtSwitchValue1";
            this.txtSwitchValue1.Size = new System.Drawing.Size(100, 24);
            this.txtSwitchValue1.TabIndex = 2;
            // 
            // cmbSwitchObject2
            // 
            this.cmbSwitchObject2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSwitchObject2.FormattingEnabled = true;
            this.cmbSwitchObject2.Location = new System.Drawing.Point(5, 59);
            this.cmbSwitchObject2.Name = "cmbSwitchObject2";
            this.cmbSwitchObject2.Size = new System.Drawing.Size(121, 26);
            this.cmbSwitchObject2.TabIndex = 1;
            // 
            // cmbSwitchObject1
            // 
            this.cmbSwitchObject1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSwitchObject1.FormattingEnabled = true;
            this.cmbSwitchObject1.Location = new System.Drawing.Point(5, 18);
            this.cmbSwitchObject1.Name = "cmbSwitchObject1";
            this.cmbSwitchObject1.Size = new System.Drawing.Size(121, 26);
            this.cmbSwitchObject1.TabIndex = 0;
            // 
            // txtObject1Switch
            // 
            this.txtObject1Switch.Location = new System.Drawing.Point(146, 18);
            this.txtObject1Switch.Name = "txtObject1Switch";
            this.txtObject1Switch.Size = new System.Drawing.Size(25, 24);
            this.txtObject1Switch.TabIndex = 5;
            // 
            // txtObject2Switch
            // 
            this.txtObject2Switch.Location = new System.Drawing.Point(148, 59);
            this.txtObject2Switch.Name = "txtObject2Switch";
            this.txtObject2Switch.Size = new System.Drawing.Size(25, 24);
            this.txtObject2Switch.TabIndex = 6;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(10, 122);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(269, 18);
            this.label46.TabIndex = 14;
            this.label46.Text = "Step 2 : Enter the control variables";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(10, 25);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(386, 18);
            this.label45.TabIndex = 13;
            this.label45.Text = "Step 1 : Choose the type of switch test application";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Cornsilk;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.label44);
            this.panel13.Controls.Add(this.cmbSwitchType);
            this.panel13.Location = new System.Drawing.Point(9, 52);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(724, 52);
            this.panel13.TabIndex = 12;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(14, 15);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(137, 18);
            this.label44.TabIndex = 1;
            this.label44.Text = "Switch Test Type";
            // 
            // cmbSwitchType
            // 
            this.cmbSwitchType.BackColor = System.Drawing.Color.White;
            this.cmbSwitchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSwitchType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSwitchType.FormattingEnabled = true;
            this.cmbSwitchType.Items.AddRange(new object[] {
            "Functional Test"});
            this.cmbSwitchType.Location = new System.Drawing.Point(204, 13);
            this.cmbSwitchType.Name = "cmbSwitchType";
            this.cmbSwitchType.Size = new System.Drawing.Size(286, 26);
            this.cmbSwitchType.TabIndex = 0;
            this.cmbSwitchType.SelectedIndexChanged += new System.EventHandler(this.cmbSwitchType_SelectedIndexChanged);
            // 
            // chartSwitch
            // 
            this.chartSwitch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chartSwitch.BackColor = System.Drawing.Color.Black;
            chartArea3.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea3.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea3.AxisX.LineColor = System.Drawing.Color.White;
            chartArea3.AxisX.MajorGrid.Interval = 0D;
            chartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea3.AxisX.MajorTickMark.Interval = 0D;
            chartArea3.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea3.AxisX.MinorGrid.Enabled = true;
            chartArea3.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisX.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea3.AxisX.Title = "Time (msec)";
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea3.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea3.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea3.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea3.AxisY.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY.MajorGrid.Interval = 0D;
            chartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea3.AxisY.MajorTickMark.Interval = 0D;
            chartArea3.AxisY.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY.Maximum = 2000D;
            chartArea3.AxisY.Minimum = -2000D;
            chartArea3.AxisY.MinorGrid.Enabled = true;
            chartArea3.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisY.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY.Title = "Current Position";
            chartArea3.AxisY.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea3.AxisY.TitleForeColor = System.Drawing.Color.White;
            chartArea3.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea3.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea3.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY2.MajorGrid.Interval = 0D;
            chartArea3.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea3.AxisY2.MajorTickMark.Interval = 0D;
            chartArea3.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY2.MinorGrid.Enabled = true;
            chartArea3.AxisY2.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea3.AxisY2.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea3.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            chartArea3.AxisY2.TitleForeColor = System.Drawing.Color.White;
            chartArea3.BackColor = System.Drawing.Color.DimGray;
            chartArea3.Name = "ChartArea3";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 72.04957F;
            chartArea3.Position.Width = 94F;
            chartArea3.Position.X = 3F;
            chartArea3.Position.Y = 24.95043F;
            this.chartSwitch.ChartAreas.Add(chartArea3);
            legend5.BackColor = System.Drawing.Color.Transparent;
            legend5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend5.ForeColor = System.Drawing.Color.White;
            legend5.IsDockedInsideChartArea = false;
            legend5.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend5.Name = "LegendPrimary";
            legend5.TextWrapThreshold = 75;
            legend6.Alignment = System.Drawing.StringAlignment.Far;
            legend6.BackColor = System.Drawing.Color.Transparent;
            legend6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend6.ForeColor = System.Drawing.Color.White;
            legend6.IsDockedInsideChartArea = false;
            legend6.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend6.Name = "LegendSecondary";
            this.chartSwitch.Legends.Add(legend5);
            this.chartSwitch.Legends.Add(legend6);
            this.chartSwitch.Location = new System.Drawing.Point(9, 340);
            this.chartSwitch.Name = "chartSwitch";
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea3";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.Blue;
            series5.EmptyPointStyle.Color = System.Drawing.Color.Black;
            series5.EmptyPointStyle.LabelForeColor = System.Drawing.Color.White;
            series5.Enabled = false;
            series5.LabelForeColor = System.Drawing.Color.White;
            series5.Legend = "LegendPrimary";
            series5.Name = "Current Position";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series5.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea3";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.Yellow;
            series6.EmptyPointStyle.Color = System.Drawing.Color.Black;
            series6.EmptyPointStyle.LabelForeColor = System.Drawing.Color.White;
            series6.Enabled = false;
            series6.LabelForeColor = System.Drawing.Color.White;
            series6.Legend = "LegendPrimary";
            series6.Name = "Force";
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series6.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chartSwitch.Series.Add(series5);
            this.chartSwitch.Series.Add(series6);
            this.chartSwitch.Size = new System.Drawing.Size(1342, 444);
            this.chartSwitch.TabIndex = 5;
            this.chartSwitch.Text = "chartSwitch";
            this.chartSwitch.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox6.Location = new System.Drawing.Point(904, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(447, 146);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 22;
            this.pictureBox6.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage3.Controls.Add(this.pictureBox18);
            this.tabPage3.Controls.Add(this.panel28);
            this.tabPage3.Controls.Add(this.panel27);
            this.tabPage3.Controls.Add(this.panel22);
            this.tabPage3.Controls.Add(this.label73);
            this.tabPage3.Controls.Add(this.panel20);
            this.tabPage3.Controls.Add(this.panel19);
            this.tabPage3.Controls.Add(this.label57);
            this.tabPage3.Controls.Add(this.label56);
            this.tabPage3.Controls.Add(this.panel18);
            this.tabPage3.Controls.Add(this.panel17);
            this.tabPage3.Controls.Add(this.chart1);
            this.tabPage3.Controls.Add(this.label74);
            this.tabPage3.Location = new System.Drawing.Point(4, 30);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1363, 796);
            this.tabPage3.TabIndex = 7;
            this.tabPage3.Text = "SMACLeakTest";
            // 
            // pictureBox18
            // 
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox18.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox18.Location = new System.Drawing.Point(905, 10);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(446, 156);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 4;
            this.pictureBox18.TabStop = false;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.PaleGreen;
            this.panel28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel28.Controls.Add(this.label96);
            this.panel28.Location = new System.Drawing.Point(905, 175);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(446, 302);
            this.panel28.TabIndex = 8;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.DarkBlue;
            this.label96.Location = new System.Drawing.Point(14, 8);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(246, 25);
            this.label96.TabIndex = 0;
            this.label96.Text = "Leak Test Capabilities : ";
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.White;
            this.panel27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel27.Controls.Add(this.pictureBox17);
            this.panel27.Controls.Add(this.pictureBox16);
            this.panel27.Controls.Add(this.pictureBox15);
            this.panel27.Controls.Add(this.pictureBox13);
            this.panel27.Location = new System.Drawing.Point(9, 484);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1342, 298);
            this.panel27.TabIndex = 12;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Salesman_GUI.Properties.Resources._38786_2420847;
            this.pictureBox17.Location = new System.Drawing.Point(978, 19);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(281, 263);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 3;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Salesman_GUI.Properties.Resources.thumb;
            this.pictureBox16.Location = new System.Drawing.Point(692, 19);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(281, 263);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 2;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox15.Location = new System.Drawing.Point(408, 19);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(281, 263);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 1;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Salesman_GUI.Properties.Resources.cbl35;
            this.pictureBox13.Location = new System.Drawing.Point(121, 19);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(281, 263);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 0;
            this.pictureBox13.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Cornsilk;
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel22.Controls.Add(this.label95);
            this.panel22.Controls.Add(this.label94);
            this.panel22.Controls.Add(this.label75);
            this.panel22.Controls.Add(this.label92);
            this.panel22.Controls.Add(this.txtFinalLeak);
            this.panel22.Controls.Add(this.label91);
            this.panel22.Controls.Add(this.txtSoftlandLeak);
            this.panel22.Controls.Add(this.label87);
            this.panel22.Controls.Add(this.txtLeakResult);
            this.panel22.Location = new System.Drawing.Point(8, 332);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(891, 145);
            this.panel22.TabIndex = 11;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(821, 92);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(59, 29);
            this.label95.TabIndex = 8;
            this.label95.Text = "mm";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Black;
            this.label94.Location = new System.Drawing.Point(639, 92);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(59, 29);
            this.label94.TabIndex = 7;
            this.label94.Text = "mm";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(276, 81);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(59, 29);
            this.label75.TabIndex = 6;
            this.label75.Text = "mm";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Blue;
            this.label92.Location = new System.Drawing.Point(704, 29);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(130, 20);
            this.label92.TabIndex = 5;
            this.label92.Text = "Final Position ";
            this.label92.Click += new System.EventHandler(this.label92_Click);
            // 
            // txtFinalLeak
            // 
            this.txtFinalLeak.BackColor = System.Drawing.Color.Cyan;
            this.txtFinalLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinalLeak.Location = new System.Drawing.Point(721, 70);
            this.txtFinalLeak.Multiline = true;
            this.txtFinalLeak.Name = "txtFinalLeak";
            this.txtFinalLeak.Size = new System.Drawing.Size(94, 52);
            this.txtFinalLeak.TabIndex = 4;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Blue;
            this.label91.Location = new System.Drawing.Point(525, 29);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(158, 20);
            this.label91.TabIndex = 3;
            this.label91.Text = "Softland Position ";
            this.label91.Click += new System.EventHandler(this.label91_Click);
            // 
            // txtSoftlandLeak
            // 
            this.txtSoftlandLeak.BackColor = System.Drawing.Color.Cyan;
            this.txtSoftlandLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoftlandLeak.Location = new System.Drawing.Point(537, 70);
            this.txtSoftlandLeak.Multiline = true;
            this.txtSoftlandLeak.Name = "txtSoftlandLeak";
            this.txtSoftlandLeak.Size = new System.Drawing.Size(84, 52);
            this.txtSoftlandLeak.TabIndex = 2;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Blue;
            this.label87.Location = new System.Drawing.Point(87, 13);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(214, 36);
            this.label87.TabIndex = 1;
            this.label87.Text = "Displacement ";
            // 
            // txtLeakResult
            // 
            this.txtLeakResult.BackColor = System.Drawing.Color.Cyan;
            this.txtLeakResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLeakResult.Location = new System.Drawing.Point(93, 57);
            this.txtLeakResult.Multiline = true;
            this.txtLeakResult.Name = "txtLeakResult";
            this.txtLeakResult.Size = new System.Drawing.Size(177, 74);
            this.txtLeakResult.TabIndex = 0;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(463, 105);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(145, 18);
            this.label73.TabIndex = 9;
            this.label73.Text = "Step 4 : Operation";
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Cornsilk;
            this.panel20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel20.Controls.Add(this.button2);
            this.panel20.Controls.Add(this.btnActuatorStopLeak);
            this.panel20.Controls.Add(this.btnActuatorStartLeak);
            this.panel20.Controls.Add(this.btnHomeLeak);
            this.panel20.Location = new System.Drawing.Point(466, 130);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(433, 196);
            this.panel20.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkGray;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(109, 135);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(225, 31);
            this.button2.TabIndex = 3;
            this.button2.Text = "Clear Result";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnActuatorStopLeak
            // 
            this.btnActuatorStopLeak.BackColor = System.Drawing.Color.Red;
            this.btnActuatorStopLeak.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActuatorStopLeak.ForeColor = System.Drawing.Color.Black;
            this.btnActuatorStopLeak.Location = new System.Drawing.Point(109, 97);
            this.btnActuatorStopLeak.Name = "btnActuatorStopLeak";
            this.btnActuatorStopLeak.Size = new System.Drawing.Size(225, 31);
            this.btnActuatorStopLeak.TabIndex = 2;
            this.btnActuatorStopLeak.Text = "Actuator Stop";
            this.btnActuatorStopLeak.UseVisualStyleBackColor = false;
            this.btnActuatorStopLeak.Click += new System.EventHandler(this.btnActuatorStopLeak_Click);
            // 
            // btnActuatorStartLeak
            // 
            this.btnActuatorStartLeak.BackColor = System.Drawing.Color.Lime;
            this.btnActuatorStartLeak.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActuatorStartLeak.ForeColor = System.Drawing.Color.Black;
            this.btnActuatorStartLeak.Location = new System.Drawing.Point(109, 60);
            this.btnActuatorStartLeak.Name = "btnActuatorStartLeak";
            this.btnActuatorStartLeak.Size = new System.Drawing.Size(225, 31);
            this.btnActuatorStartLeak.TabIndex = 1;
            this.btnActuatorStartLeak.Text = "Actuator Start";
            this.btnActuatorStartLeak.UseVisualStyleBackColor = false;
            this.btnActuatorStartLeak.Click += new System.EventHandler(this.btnActuatorStartLeak_Click);
            // 
            // btnHomeLeak
            // 
            this.btnHomeLeak.BackColor = System.Drawing.Color.Yellow;
            this.btnHomeLeak.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeLeak.ForeColor = System.Drawing.Color.Black;
            this.btnHomeLeak.Location = new System.Drawing.Point(109, 23);
            this.btnHomeLeak.Name = "btnHomeLeak";
            this.btnHomeLeak.Size = new System.Drawing.Size(225, 31);
            this.btnHomeLeak.TabIndex = 0;
            this.btnHomeLeak.Text = "Move To Home";
            this.btnHomeLeak.UseVisualStyleBackColor = false;
            this.btnHomeLeak.Click += new System.EventHandler(this.btnHomeLeak_Click);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Cornsilk;
            this.panel19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel19.Location = new System.Drawing.Point(464, 130);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(233, 196);
            this.panel19.TabIndex = 7;
            this.panel19.Visible = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(11, 105);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(269, 18);
            this.label57.TabIndex = 6;
            this.label57.Text = "Step 2 : Enter the control variables";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Red;
            this.label56.Location = new System.Drawing.Point(11, 10);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(235, 18);
            this.label56.TabIndex = 5;
            this.label56.Text = "Step 1 : Choose the sequence";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Cornsilk;
            this.panel18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel18.Controls.Add(this.label62);
            this.panel18.Controls.Add(this.label61);
            this.panel18.Controls.Add(this.label60);
            this.panel18.Controls.Add(this.label59);
            this.panel18.Controls.Add(this.label58);
            this.panel18.Controls.Add(this.txtDwellLeakRange);
            this.panel18.Controls.Add(this.txtForceLeakRange);
            this.panel18.Controls.Add(this.txtSpeedLeakRange);
            this.panel18.Controls.Add(this.txtSensitivityLeakRange);
            this.panel18.Controls.Add(this.txtPositionLeakRange);
            this.panel18.Controls.Add(this.lblDwellLeak);
            this.panel18.Controls.Add(this.lblForceLeak);
            this.panel18.Controls.Add(this.lblSensitivityLeak);
            this.panel18.Controls.Add(this.lblSpeedLeak);
            this.panel18.Controls.Add(this.lblPostionLeak);
            this.panel18.Controls.Add(this.txtDwellLeak);
            this.panel18.Controls.Add(this.txtForceLeak);
            this.panel18.Controls.Add(this.txtSpeedLeak);
            this.panel18.Controls.Add(this.txtSensitivityLeak);
            this.panel18.Controls.Add(this.txtPositionLeak);
            this.panel18.Location = new System.Drawing.Point(8, 130);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(452, 196);
            this.panel18.TabIndex = 4;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Cornsilk;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(384, 138);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(45, 18);
            this.label62.TabIndex = 19;
            this.label62.Text = "msec";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Cornsilk;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(384, 107);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(44, 18);
            this.label61.TabIndex = 18;
            this.label61.Text = "Value";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Cornsilk;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(384, 76);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 18);
            this.label60.TabIndex = 17;
            this.label60.Text = "Counts";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Cornsilk;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(384, 46);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(64, 18);
            this.label59.TabIndex = 16;
            this.label59.Text = "mm/Sec";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Cornsilk;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(384, 17);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(34, 18);
            this.label58.TabIndex = 15;
            this.label58.Text = "mm";
            // 
            // txtDwellLeakRange
            // 
            this.txtDwellLeakRange.Location = new System.Drawing.Point(162, 134);
            this.txtDwellLeakRange.Name = "txtDwellLeakRange";
            this.txtDwellLeakRange.ReadOnly = true;
            this.txtDwellLeakRange.Size = new System.Drawing.Size(110, 24);
            this.txtDwellLeakRange.TabIndex = 14;
            // 
            // txtForceLeakRange
            // 
            this.txtForceLeakRange.Location = new System.Drawing.Point(162, 104);
            this.txtForceLeakRange.Name = "txtForceLeakRange";
            this.txtForceLeakRange.ReadOnly = true;
            this.txtForceLeakRange.Size = new System.Drawing.Size(110, 24);
            this.txtForceLeakRange.TabIndex = 13;
            // 
            // txtSpeedLeakRange
            // 
            this.txtSpeedLeakRange.Location = new System.Drawing.Point(162, 44);
            this.txtSpeedLeakRange.Name = "txtSpeedLeakRange";
            this.txtSpeedLeakRange.ReadOnly = true;
            this.txtSpeedLeakRange.Size = new System.Drawing.Size(110, 24);
            this.txtSpeedLeakRange.TabIndex = 12;
            // 
            // txtSensitivityLeakRange
            // 
            this.txtSensitivityLeakRange.Location = new System.Drawing.Point(186, 74);
            this.txtSensitivityLeakRange.Name = "txtSensitivityLeakRange";
            this.txtSensitivityLeakRange.ReadOnly = true;
            this.txtSensitivityLeakRange.Size = new System.Drawing.Size(86, 24);
            this.txtSensitivityLeakRange.TabIndex = 11;
            // 
            // txtPositionLeakRange
            // 
            this.txtPositionLeakRange.Location = new System.Drawing.Point(162, 14);
            this.txtPositionLeakRange.Name = "txtPositionLeakRange";
            this.txtPositionLeakRange.ReadOnly = true;
            this.txtPositionLeakRange.Size = new System.Drawing.Size(110, 24);
            this.txtPositionLeakRange.TabIndex = 10;
            // 
            // lblDwellLeak
            // 
            this.lblDwellLeak.AutoSize = true;
            this.lblDwellLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDwellLeak.ForeColor = System.Drawing.Color.Magenta;
            this.lblDwellLeak.Location = new System.Drawing.Point(5, 136);
            this.lblDwellLeak.Name = "lblDwellLeak";
            this.lblDwellLeak.Size = new System.Drawing.Size(91, 18);
            this.lblDwellLeak.TabIndex = 9;
            this.lblDwellLeak.Text = "Dwell Time";
            // 
            // lblForceLeak
            // 
            this.lblForceLeak.AutoSize = true;
            this.lblForceLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblForceLeak.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblForceLeak.Location = new System.Drawing.Point(5, 107);
            this.lblForceLeak.Name = "lblForceLeak";
            this.lblForceLeak.Size = new System.Drawing.Size(52, 18);
            this.lblForceLeak.TabIndex = 8;
            this.lblForceLeak.Text = "Force";
            // 
            // lblSensitivityLeak
            // 
            this.lblSensitivityLeak.AutoSize = true;
            this.lblSensitivityLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSensitivityLeak.ForeColor = System.Drawing.Color.Purple;
            this.lblSensitivityLeak.Location = new System.Drawing.Point(5, 77);
            this.lblSensitivityLeak.Name = "lblSensitivityLeak";
            this.lblSensitivityLeak.Size = new System.Drawing.Size(151, 18);
            this.lblSensitivityLeak.TabIndex = 7;
            this.lblSensitivityLeak.Text = "Softland Sensitivity";
            // 
            // lblSpeedLeak
            // 
            this.lblSpeedLeak.AutoSize = true;
            this.lblSpeedLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeedLeak.ForeColor = System.Drawing.Color.Red;
            this.lblSpeedLeak.Location = new System.Drawing.Point(5, 47);
            this.lblSpeedLeak.Name = "lblSpeedLeak";
            this.lblSpeedLeak.Size = new System.Drawing.Size(55, 18);
            this.lblSpeedLeak.TabIndex = 6;
            this.lblSpeedLeak.Text = "Speed";
            // 
            // lblPostionLeak
            // 
            this.lblPostionLeak.AutoSize = true;
            this.lblPostionLeak.BackColor = System.Drawing.Color.Cornsilk;
            this.lblPostionLeak.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostionLeak.ForeColor = System.Drawing.Color.Blue;
            this.lblPostionLeak.Location = new System.Drawing.Point(3, 17);
            this.lblPostionLeak.Name = "lblPostionLeak";
            this.lblPostionLeak.Size = new System.Drawing.Size(124, 18);
            this.lblPostionLeak.TabIndex = 5;
            this.lblPostionLeak.Text = "Position Close ";
            // 
            // txtDwellLeak
            // 
            this.txtDwellLeak.Location = new System.Drawing.Point(278, 134);
            this.txtDwellLeak.Name = "txtDwellLeak";
            this.txtDwellLeak.Size = new System.Drawing.Size(100, 24);
            this.txtDwellLeak.TabIndex = 4;
            this.txtDwellLeak.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDwellLeak_KeyUp);
            this.txtDwellLeak.Leave += new System.EventHandler(this.txtDwellLeak_Leave);
            // 
            // txtForceLeak
            // 
            this.txtForceLeak.Location = new System.Drawing.Point(278, 104);
            this.txtForceLeak.Name = "txtForceLeak";
            this.txtForceLeak.Size = new System.Drawing.Size(100, 24);
            this.txtForceLeak.TabIndex = 3;
            // 
            // txtSpeedLeak
            // 
            this.txtSpeedLeak.Location = new System.Drawing.Point(278, 44);
            this.txtSpeedLeak.MaxLength = 3;
            this.txtSpeedLeak.Name = "txtSpeedLeak";
            this.txtSpeedLeak.Size = new System.Drawing.Size(100, 24);
            this.txtSpeedLeak.TabIndex = 2;
            // 
            // txtSensitivityLeak
            // 
            this.txtSensitivityLeak.Location = new System.Drawing.Point(278, 74);
            this.txtSensitivityLeak.Name = "txtSensitivityLeak";
            this.txtSensitivityLeak.Size = new System.Drawing.Size(100, 24);
            this.txtSensitivityLeak.TabIndex = 1;
            // 
            // txtPositionLeak
            // 
            this.txtPositionLeak.Location = new System.Drawing.Point(278, 14);
            this.txtPositionLeak.MaxLength = 5;
            this.txtPositionLeak.Name = "txtPositionLeak";
            this.txtPositionLeak.Size = new System.Drawing.Size(100, 24);
            this.txtPositionLeak.TabIndex = 0;
            this.txtPositionLeak.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPositionLeak_KeyUp);
            this.txtPositionLeak.Leave += new System.EventHandler(this.txtPositionLeak_Leave);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Cornsilk;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.cmbLeakTestType);
            this.panel17.Controls.Add(this.label53);
            this.panel17.Location = new System.Drawing.Point(8, 35);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(891, 62);
            this.panel17.TabIndex = 3;
            // 
            // cmbLeakTestType
            // 
            this.cmbLeakTestType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLeakTestType.FormattingEnabled = true;
            this.cmbLeakTestType.Items.AddRange(new object[] {
            "Softland and Force Move"});
            this.cmbLeakTestType.Location = new System.Drawing.Point(153, 17);
            this.cmbLeakTestType.Name = "cmbLeakTestType";
            this.cmbLeakTestType.Size = new System.Drawing.Size(390, 26);
            this.cmbLeakTestType.TabIndex = 1;
            this.cmbLeakTestType.SelectedIndexChanged += new System.EventHandler(this.cmbLeakTestType_SelectedIndexChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(10, 20);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(123, 18);
            this.label53.TabIndex = 2;
            this.label53.Text = "Leak Test Type";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.Black;
            chartArea4.AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea4.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea4.AxisX.LineColor = System.Drawing.Color.White;
            chartArea4.AxisX.MajorGrid.Interval = 0D;
            chartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea4.AxisX.MajorTickMark.Interval = 0D;
            chartArea4.AxisX.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea4.AxisX.MinorGrid.Enabled = true;
            chartArea4.AxisX.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisX.MinorTickMark.LineColor = System.Drawing.Color.Silver;
            chartArea4.AxisX.Title = "Time(msec)";
            chartArea4.AxisX.TitleForeColor = System.Drawing.Color.White;
            chartArea4.AxisX2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea4.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea4.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea4.AxisY.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY.MajorGrid.Interval = 0D;
            chartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea4.AxisY.MajorTickMark.Interval = 0D;
            chartArea4.AxisY.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY.Maximum = 2000D;
            chartArea4.AxisY.Minimum = -2000D;
            chartArea4.AxisY.MinorGrid.Enabled = true;
            chartArea4.AxisY.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY.Title = "Current Position";
            chartArea4.AxisY.TitleForeColor = System.Drawing.Color.White;
            chartArea4.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea4.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea4.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY2.MajorGrid.Interval = 0D;
            chartArea4.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea4.AxisY2.MajorTickMark.Interval = 0D;
            chartArea4.AxisY2.MajorTickMark.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY2.MinorGrid.Enabled = true;
            chartArea4.AxisY2.MinorGrid.LineColor = System.Drawing.Color.Gray;
            chartArea4.AxisY2.MinorTickMark.LineColor = System.Drawing.Color.White;
            chartArea4.AxisY2.Title = "Actual Force";
            chartArea4.AxisY2.TitleForeColor = System.Drawing.Color.White;
            chartArea4.BackColor = System.Drawing.Color.DimGray;
            chartArea4.Name = "ChartArea4";
            this.chart1.ChartAreas.Add(chartArea4);
            legend7.BackColor = System.Drawing.Color.Transparent;
            legend7.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend7.Enabled = false;
            legend7.ForeColor = System.Drawing.Color.White;
            legend7.IsDockedInsideChartArea = false;
            legend7.IsTextAutoFit = false;
            legend7.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend7.Name = "LegendPrimary5";
            legend7.Position.Auto = false;
            legend7.Position.Height = 20F;
            legend7.Position.Width = 26.47528F;
            legend7.Position.X = 3F;
            legend7.Position.Y = 3F;
            legend7.TextWrapThreshold = 75;
            legend8.Alignment = System.Drawing.StringAlignment.Far;
            legend8.BackColor = System.Drawing.Color.Transparent;
            legend8.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend8.Enabled = false;
            legend8.IsDockedInsideChartArea = false;
            legend8.IsTextAutoFit = false;
            legend8.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Column;
            legend8.Name = "LegendSecondary6";
            legend8.TextWrapThreshold = 75;
            this.chart1.Legends.Add(legend7);
            this.chart1.Legends.Add(legend8);
            this.chart1.Location = new System.Drawing.Point(8, 346);
            this.chart1.Name = "chart1";
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea4";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Lime;
            series7.Enabled = false;
            series7.LabelForeColor = System.Drawing.Color.White;
            series7.Legend = "LegendPrimary5";
            series7.LegendText = "Actual Position";
            series7.Name = "Current Position";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series8.BorderWidth = 2;
            series8.ChartArea = "ChartArea4";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Lime;
            series8.Enabled = false;
            series8.LabelForeColor = System.Drawing.Color.White;
            series8.Legend = "LegendPrimary5";
            series8.LegendText = "Actual Force(% of Rated Current)";
            series8.Name = "Actual Force";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series8.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Size = new System.Drawing.Size(1343, 436);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            this.chart1.Visible = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(453, 105);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(236, 18);
            this.label74.TabIndex = 10;
            this.label74.Text = "Step 3 : Select log parameters";
            this.label74.Visible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage4.Controls.Add(this.panel31);
            this.tabPage4.Controls.Add(this.panel30);
            this.tabPage4.Controls.Add(this.panel24);
            this.tabPage4.Location = new System.Drawing.Point(4, 30);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1363, 796);
            this.tabPage4.TabIndex = 8;
            this.tabPage4.Text = "SMACHighSpeedTest";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.White;
            this.panel31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel31.Controls.Add(this.label54);
            this.panel31.Controls.Add(this.pictureBox23);
            this.panel31.Controls.Add(this.pictureBox22);
            this.panel31.Controls.Add(this.pictureBox21);
            this.panel31.Controls.Add(this.pictureBox20);
            this.panel31.Controls.Add(this.pictureBox19);
            this.panel31.Location = new System.Drawing.Point(9, 288);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(1342, 492);
            this.panel31.TabIndex = 2;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.DarkBlue;
            this.label54.Location = new System.Drawing.Point(543, 377);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(636, 31);
            this.label54.TabIndex = 5;
            this.label54.Text = "\"The Ability to work and verify quality at same time.\"";
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox23.Location = new System.Drawing.Point(8, 302);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(471, 180);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 4;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::Salesman_GUI.Properties.Resources.thumb;
            this.pictureBox22.Location = new System.Drawing.Point(990, 13);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(339, 288);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 3;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox21.Location = new System.Drawing.Point(669, 13);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(339, 288);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 2;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Salesman_GUI.Properties.Resources.cbl35;
            this.pictureBox20.Location = new System.Drawing.Point(337, 13);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(339, 288);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 1;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Salesman_GUI.Properties.Resources._38786_2420847;
            this.pictureBox19.Location = new System.Drawing.Point(8, 13);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(339, 288);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 0;
            this.pictureBox19.TabStop = false;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.LightPink;
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel30.Controls.Add(this.button3);
            this.panel30.Controls.Add(this.label110);
            this.panel30.Controls.Add(this.label109);
            this.panel30.Controls.Add(this.label108);
            this.panel30.Controls.Add(this.label107);
            this.panel30.Controls.Add(this.label106);
            this.panel30.Controls.Add(this.txtHighSpeedCycleFreq);
            this.panel30.Controls.Add(this.txtHighSpeedCycleCount);
            this.panel30.Controls.Add(this.txtHighSpeedCycleTime);
            this.panel30.Location = new System.Drawing.Point(432, 19);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(919, 262);
            this.panel30.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Lime;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(14, 226);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 31);
            this.button3.TabIndex = 16;
            this.button3.Text = "Start Sequence";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(721, 193);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(73, 29);
            this.label110.TabIndex = 7;
            this.label110.Text = "Hertz";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(721, 46);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(75, 29);
            this.label109.TabIndex = 6;
            this.label109.Text = "msec";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(150, 177);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(320, 46);
            this.label108.TabIndex = 5;
            this.label108.Text = "Cycle Frequency";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(151, 106);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(259, 46);
            this.label107.TabIndex = 4;
            this.label107.Text = "Cycle Counts";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(151, 34);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(220, 46);
            this.label106.TabIndex = 3;
            this.label106.Text = "Cycle Time";
            // 
            // txtHighSpeedCycleFreq
            // 
            this.txtHighSpeedCycleFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedCycleFreq.Location = new System.Drawing.Point(563, 168);
            this.txtHighSpeedCycleFreq.Multiline = true;
            this.txtHighSpeedCycleFreq.Name = "txtHighSpeedCycleFreq";
            this.txtHighSpeedCycleFreq.Size = new System.Drawing.Size(133, 57);
            this.txtHighSpeedCycleFreq.TabIndex = 2;
            this.txtHighSpeedCycleFreq.TextChanged += new System.EventHandler(this.txtHighSpeedCycleFreq_TextChanged);
            // 
            // txtHighSpeedCycleCount
            // 
            this.txtHighSpeedCycleCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedCycleCount.Location = new System.Drawing.Point(563, 98);
            this.txtHighSpeedCycleCount.Multiline = true;
            this.txtHighSpeedCycleCount.Name = "txtHighSpeedCycleCount";
            this.txtHighSpeedCycleCount.ReadOnly = true;
            this.txtHighSpeedCycleCount.Size = new System.Drawing.Size(133, 57);
            this.txtHighSpeedCycleCount.TabIndex = 1;
            this.txtHighSpeedCycleCount.Text = "10";
            this.txtHighSpeedCycleCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtHighSpeedCycleTime
            // 
            this.txtHighSpeedCycleTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedCycleTime.Location = new System.Drawing.Point(563, 26);
            this.txtHighSpeedCycleTime.Multiline = true;
            this.txtHighSpeedCycleTime.Name = "txtHighSpeedCycleTime";
            this.txtHighSpeedCycleTime.Size = new System.Drawing.Size(133, 57);
            this.txtHighSpeedCycleTime.TabIndex = 0;
            this.txtHighSpeedCycleTime.TextChanged += new System.EventHandler(this.txtHighSpeedCycleTime_TextChanged);
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.LightPink;
            this.panel24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel24.Controls.Add(this.btnHighSpeedClear);
            this.panel24.Controls.Add(this.label85);
            this.panel24.Controls.Add(this.cmbHighSpeed);
            this.panel24.Controls.Add(this.label81);
            this.panel24.Controls.Add(this.label80);
            this.panel24.Controls.Add(this.label79);
            this.panel24.Controls.Add(this.label78);
            this.panel24.Controls.Add(this.label77);
            this.panel24.Controls.Add(this.label76);
            this.panel24.Controls.Add(this.label42);
            this.panel24.Controls.Add(this.txtHighSpeedVelocity);
            this.panel24.Controls.Add(this.txtHighSpeedTarget2);
            this.panel24.Controls.Add(this.txtHighSpeedTarget1);
            this.panel24.Controls.Add(this.btnHighSpeedHome);
            this.panel24.Controls.Add(this.btnHighSpeedStop);
            this.panel24.Controls.Add(this.btnHighSpeedStart);
            this.panel24.Location = new System.Drawing.Point(9, 19);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(414, 262);
            this.panel24.TabIndex = 0;
            // 
            // btnHighSpeedClear
            // 
            this.btnHighSpeedClear.BackColor = System.Drawing.Color.Cyan;
            this.btnHighSpeedClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHighSpeedClear.Location = new System.Drawing.Point(161, 226);
            this.btnHighSpeedClear.Name = "btnHighSpeedClear";
            this.btnHighSpeedClear.Size = new System.Drawing.Size(142, 31);
            this.btnHighSpeedClear.TabIndex = 15;
            this.btnHighSpeedClear.Text = "Update Result";
            this.btnHighSpeedClear.UseVisualStyleBackColor = false;
            this.btnHighSpeedClear.Click += new System.EventHandler(this.btnHighSpeedClear_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(4, 37);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(146, 18);
            this.label85.TabIndex = 14;
            this.label85.Text = "Choose Sequence";
            // 
            // cmbHighSpeed
            // 
            this.cmbHighSpeed.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHighSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHighSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHighSpeed.FormattingEnabled = true;
            this.cmbHighSpeed.Items.AddRange(new object[] {
            "High speed test loop"});
            this.cmbHighSpeed.Location = new System.Drawing.Point(182, 34);
            this.cmbHighSpeed.Name = "cmbHighSpeed";
            this.cmbHighSpeed.Size = new System.Drawing.Size(221, 26);
            this.cmbHighSpeed.TabIndex = 13;
            this.cmbHighSpeed.SelectedIndexChanged += new System.EventHandler(this.cmbHighSpeed_SelectedIndexChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.DarkCyan;
            this.label81.Location = new System.Drawing.Point(51, 4);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(304, 25);
            this.label81.TabIndex = 12;
            this.label81.Text = "High Speed / Oscillation Move";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(291, 155);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(62, 18);
            this.label80.TabIndex = 11;
            this.label80.Text = "mm/sec";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(292, 111);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(34, 18);
            this.label79.TabIndex = 10;
            this.label79.Text = "mm";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(290, 71);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(34, 18);
            this.label78.TabIndex = 9;
            this.label78.Text = "mm";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(4, 152);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(55, 18);
            this.label77.TabIndex = 8;
            this.label77.Text = "Speed";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(4, 111);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(86, 18);
            this.label76.TabIndex = 7;
            this.label76.Text = "Position B";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(4, 71);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(85, 18);
            this.label42.TabIndex = 6;
            this.label42.Text = "Position A";
            // 
            // txtHighSpeedVelocity
            // 
            this.txtHighSpeedVelocity.BackColor = System.Drawing.Color.Silver;
            this.txtHighSpeedVelocity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHighSpeedVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedVelocity.ForeColor = System.Drawing.Color.Blue;
            this.txtHighSpeedVelocity.Location = new System.Drawing.Point(182, 149);
            this.txtHighSpeedVelocity.MaxLength = 3;
            this.txtHighSpeedVelocity.Name = "txtHighSpeedVelocity";
            this.txtHighSpeedVelocity.Size = new System.Drawing.Size(100, 24);
            this.txtHighSpeedVelocity.TabIndex = 5;
            this.txtHighSpeedVelocity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHighSpeedVelocity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHighSpeedVelocity_KeyUp);
            this.txtHighSpeedVelocity.Leave += new System.EventHandler(this.txtHighSpeedVelocity_Leave);
            // 
            // txtHighSpeedTarget2
            // 
            this.txtHighSpeedTarget2.BackColor = System.Drawing.Color.Silver;
            this.txtHighSpeedTarget2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHighSpeedTarget2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedTarget2.ForeColor = System.Drawing.Color.Blue;
            this.txtHighSpeedTarget2.Location = new System.Drawing.Point(182, 108);
            this.txtHighSpeedTarget2.MaxLength = 4;
            this.txtHighSpeedTarget2.Name = "txtHighSpeedTarget2";
            this.txtHighSpeedTarget2.Size = new System.Drawing.Size(100, 24);
            this.txtHighSpeedTarget2.TabIndex = 4;
            this.txtHighSpeedTarget2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHighSpeedTarget2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHighSpeedTarget2_KeyUp);
            this.txtHighSpeedTarget2.Leave += new System.EventHandler(this.txtHighSpeedTarget2_Leave);
            // 
            // txtHighSpeedTarget1
            // 
            this.txtHighSpeedTarget1.BackColor = System.Drawing.Color.Silver;
            this.txtHighSpeedTarget1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHighSpeedTarget1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighSpeedTarget1.ForeColor = System.Drawing.Color.Blue;
            this.txtHighSpeedTarget1.Location = new System.Drawing.Point(182, 68);
            this.txtHighSpeedTarget1.MaxLength = 4;
            this.txtHighSpeedTarget1.Name = "txtHighSpeedTarget1";
            this.txtHighSpeedTarget1.Size = new System.Drawing.Size(100, 24);
            this.txtHighSpeedTarget1.TabIndex = 3;
            this.txtHighSpeedTarget1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHighSpeedTarget1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHighSpeedTarget1_KeyUp);
            this.txtHighSpeedTarget1.Leave += new System.EventHandler(this.txtHighSpeedTarget1_Leave);
            // 
            // btnHighSpeedHome
            // 
            this.btnHighSpeedHome.BackColor = System.Drawing.Color.Yellow;
            this.btnHighSpeedHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHighSpeedHome.Location = new System.Drawing.Point(309, 185);
            this.btnHighSpeedHome.Name = "btnHighSpeedHome";
            this.btnHighSpeedHome.Size = new System.Drawing.Size(95, 31);
            this.btnHighSpeedHome.TabIndex = 2;
            this.btnHighSpeedHome.Text = "Home";
            this.btnHighSpeedHome.UseVisualStyleBackColor = false;
            this.btnHighSpeedHome.Click += new System.EventHandler(this.btnHighSpeedHome_Click);
            // 
            // btnHighSpeedStop
            // 
            this.btnHighSpeedStop.BackColor = System.Drawing.Color.OrangeRed;
            this.btnHighSpeedStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHighSpeedStop.Location = new System.Drawing.Point(161, 185);
            this.btnHighSpeedStop.Name = "btnHighSpeedStop";
            this.btnHighSpeedStop.Size = new System.Drawing.Size(142, 31);
            this.btnHighSpeedStop.TabIndex = 1;
            this.btnHighSpeedStop.Text = "Stop Sequence";
            this.btnHighSpeedStop.UseVisualStyleBackColor = false;
            this.btnHighSpeedStop.Click += new System.EventHandler(this.btnHighSpeedStop_Click);
            // 
            // btnHighSpeedStart
            // 
            this.btnHighSpeedStart.BackColor = System.Drawing.Color.Lime;
            this.btnHighSpeedStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHighSpeedStart.Location = new System.Drawing.Point(5, 185);
            this.btnHighSpeedStart.Name = "btnHighSpeedStart";
            this.btnHighSpeedStart.Size = new System.Drawing.Size(150, 31);
            this.btnHighSpeedStart.TabIndex = 0;
            this.btnHighSpeedStart.Text = "Start Sequence";
            this.btnHighSpeedStart.UseVisualStyleBackColor = false;
            this.btnHighSpeedStart.Click += new System.EventHandler(this.btnHighSpeedStart_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage5.Controls.Add(this.panel32);
            this.tabPage5.Controls.Add(this.panel29);
            this.tabPage5.Location = new System.Drawing.Point(4, 30);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1363, 796);
            this.tabPage5.TabIndex = 9;
            this.tabPage5.Text = "SMAC-Softland-1N-4N-8N-Test";
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.White;
            this.panel32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel32.Controls.Add(this.pictureBox27);
            this.panel32.Controls.Add(this.pictureBox26);
            this.panel32.Controls.Add(this.pictureBox25);
            this.panel32.Controls.Add(this.pictureBox24);
            this.panel32.Location = new System.Drawing.Point(9, 375);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(1342, 415);
            this.panel32.TabIndex = 1;
            // 
            // pictureBox27
            // 
            this.pictureBox27.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox27.Location = new System.Drawing.Point(1008, 20);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(342, 355);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 3;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox26.Location = new System.Drawing.Point(686, 20);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(342, 355);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 2;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = global::Salesman_GUI.Properties.Resources.cbl35;
            this.pictureBox25.Location = new System.Drawing.Point(322, 20);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(398, 355);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 1;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = global::Salesman_GUI.Properties.Resources._38786_2420847;
            this.pictureBox24.Location = new System.Drawing.Point(4, 20);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(342, 355);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 0;
            this.pictureBox24.TabStop = false;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.panel29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel29.Controls.Add(this.pictureBox28);
            this.panel29.Controls.Add(this.button1);
            this.panel29.Controls.Add(this.label105);
            this.panel29.Controls.Add(this.label104);
            this.panel29.Controls.Add(this.label103);
            this.panel29.Controls.Add(this.label102);
            this.panel29.Controls.Add(this.label101);
            this.panel29.Controls.Add(this.label100);
            this.panel29.Controls.Add(this.txtPosition3StepForce);
            this.panel29.Controls.Add(this.label99);
            this.panel29.Controls.Add(this.txtPosition2StepForce);
            this.panel29.Controls.Add(this.label98);
            this.panel29.Controls.Add(this.txtPosition1StepForce);
            this.panel29.Controls.Add(this.label97);
            this.panel29.Controls.Add(this.txtSoftlandStepForce);
            this.panel29.Controls.Add(this.btnHomeStepForce);
            this.panel29.Controls.Add(this.btnStopStepForce);
            this.panel29.Controls.Add(this.btnStartStepForce);
            this.panel29.Location = new System.Drawing.Point(9, 20);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(1342, 348);
            this.panel29.TabIndex = 0;
            this.panel29.Paint += new System.Windows.Forms.PaintEventHandler(this.panel29_Paint);
            // 
            // pictureBox28
            // 
            this.pictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox28.Image = global::Salesman_GUI.Properties.Resources.Smac_logo1;
            this.pictureBox28.Location = new System.Drawing.Point(6, 81);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(301, 181);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox28.TabIndex = 17;
            this.pictureBox28.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Cyan;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(504, 253);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 45);
            this.button1.TabIndex = 16;
            this.button1.Text = "Clear Result";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Black;
            this.label105.Location = new System.Drawing.Point(1262, 268);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(59, 29);
            this.label105.TabIndex = 15;
            this.label105.Text = "mm";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Black;
            this.label104.Location = new System.Drawing.Point(1262, 191);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(59, 29);
            this.label104.TabIndex = 14;
            this.label104.Text = "mm";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(1262, 114);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(59, 29);
            this.label103.TabIndex = 13;
            this.label103.Text = "mm";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Black;
            this.label102.Location = new System.Drawing.Point(726, 118);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(59, 29);
            this.label102.TabIndex = 12;
            this.label102.Text = "mm";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Maroon;
            this.label101.Location = new System.Drawing.Point(609, 14);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(276, 29);
            this.label101.TabIndex = 11;
            this.label101.Text = "Step Force Sequence";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.White;
            this.label100.Location = new System.Drawing.Point(871, 250);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(176, 29);
            this.label100.TabIndex = 10;
            this.label100.Text = "Position at 8N";
            // 
            // txtPosition3StepForce
            // 
            this.txtPosition3StepForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPosition3StepForce.ForeColor = System.Drawing.Color.Black;
            this.txtPosition3StepForce.Location = new System.Drawing.Point(1099, 231);
            this.txtPosition3StepForce.Multiline = true;
            this.txtPosition3StepForce.Name = "txtPosition3StepForce";
            this.txtPosition3StepForce.Size = new System.Drawing.Size(157, 72);
            this.txtPosition3StepForce.TabIndex = 9;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.White;
            this.label99.Location = new System.Drawing.Point(871, 173);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(176, 29);
            this.label99.TabIndex = 8;
            this.label99.Text = "Position at 4N";
            // 
            // txtPosition2StepForce
            // 
            this.txtPosition2StepForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPosition2StepForce.ForeColor = System.Drawing.Color.Black;
            this.txtPosition2StepForce.Location = new System.Drawing.Point(1099, 154);
            this.txtPosition2StepForce.Multiline = true;
            this.txtPosition2StepForce.Name = "txtPosition2StepForce";
            this.txtPosition2StepForce.Size = new System.Drawing.Size(157, 71);
            this.txtPosition2StepForce.TabIndex = 7;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.White;
            this.label98.Location = new System.Drawing.Point(871, 96);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(176, 29);
            this.label98.TabIndex = 6;
            this.label98.Text = "Position at 1N";
            // 
            // txtPosition1StepForce
            // 
            this.txtPosition1StepForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPosition1StepForce.Location = new System.Drawing.Point(1099, 77);
            this.txtPosition1StepForce.Multiline = true;
            this.txtPosition1StepForce.Name = "txtPosition1StepForce";
            this.txtPosition1StepForce.Size = new System.Drawing.Size(157, 71);
            this.txtPosition1StepForce.TabIndex = 5;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.MintCream;
            this.label97.Location = new System.Drawing.Point(335, 100);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(211, 29);
            this.label97.TabIndex = 4;
            this.label97.Text = "Softland Position";
            // 
            // txtSoftlandStepForce
            // 
            this.txtSoftlandStepForce.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoftlandStepForce.Location = new System.Drawing.Point(563, 81);
            this.txtSoftlandStepForce.Multiline = true;
            this.txtSoftlandStepForce.Name = "txtSoftlandStepForce";
            this.txtSoftlandStepForce.Size = new System.Drawing.Size(157, 68);
            this.txtSoftlandStepForce.TabIndex = 3;
            // 
            // btnHomeStepForce
            // 
            this.btnHomeStepForce.BackColor = System.Drawing.Color.Yellow;
            this.btnHomeStepForce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHomeStepForce.Location = new System.Drawing.Point(657, 193);
            this.btnHomeStepForce.Name = "btnHomeStepForce";
            this.btnHomeStepForce.Size = new System.Drawing.Size(144, 45);
            this.btnHomeStepForce.TabIndex = 2;
            this.btnHomeStepForce.Text = "Home";
            this.btnHomeStepForce.UseVisualStyleBackColor = false;
            this.btnHomeStepForce.Click += new System.EventHandler(this.btnHomeStepForce_Click);
            // 
            // btnStopStepForce
            // 
            this.btnStopStepForce.BackColor = System.Drawing.Color.OrangeRed;
            this.btnStopStepForce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopStepForce.Location = new System.Drawing.Point(504, 193);
            this.btnStopStepForce.Name = "btnStopStepForce";
            this.btnStopStepForce.Size = new System.Drawing.Size(144, 45);
            this.btnStopStepForce.TabIndex = 1;
            this.btnStopStepForce.Text = "Stop Sequence";
            this.btnStopStepForce.UseVisualStyleBackColor = false;
            this.btnStopStepForce.Click += new System.EventHandler(this.btnStopStepForce_Click);
            // 
            // btnStartStepForce
            // 
            this.btnStartStepForce.BackColor = System.Drawing.Color.SpringGreen;
            this.btnStartStepForce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartStepForce.Location = new System.Drawing.Point(349, 193);
            this.btnStartStepForce.Name = "btnStartStepForce";
            this.btnStartStepForce.Size = new System.Drawing.Size(144, 45);
            this.btnStartStepForce.TabIndex = 0;
            this.btnStartStepForce.Text = "Start Sequence";
            this.btnStartStepForce.UseVisualStyleBackColor = false;
            this.btnStartStepForce.Click += new System.EventHandler(this.btnStartStepForce_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.LightSteelBlue;
            this.tabPage6.Controls.Add(this.panel36);
            this.tabPage6.Controls.Add(this.panel33);
            this.tabPage6.Location = new System.Drawing.Point(4, 30);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1363, 796);
            this.tabPage6.TabIndex = 10;
            this.tabPage6.Text = "SMACPositionMove";
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.White;
            this.panel36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel36.Controls.Add(this.pictureBox32);
            this.panel36.Controls.Add(this.pictureBox31);
            this.panel36.Controls.Add(this.pictureBox30);
            this.panel36.Controls.Add(this.pictureBox29);
            this.panel36.Location = new System.Drawing.Point(9, 510);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(1342, 280);
            this.panel36.TabIndex = 28;
            // 
            // pictureBox32
            // 
            this.pictureBox32.Image = global::Salesman_GUI.Properties.Resources._38786_2420847;
            this.pictureBox32.Location = new System.Drawing.Point(990, 8);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(319, 264);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox32.TabIndex = 3;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.Image = global::Salesman_GUI.Properties.Resources.thumb;
            this.pictureBox31.Location = new System.Drawing.Point(671, 8);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(319, 264);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox31.TabIndex = 2;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.Image = global::Salesman_GUI.Properties.Resources.lcr13_linear_rotary_actuator;
            this.pictureBox30.Location = new System.Drawing.Point(351, 9);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(319, 264);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox30.TabIndex = 1;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.Image = global::Salesman_GUI.Properties.Resources.cbl35;
            this.pictureBox29.Location = new System.Drawing.Point(30, 9);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(319, 264);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 0;
            this.pictureBox29.TabStop = false;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel33.Controls.Add(this.panel35);
            this.panel33.Controls.Add(this.label126);
            this.panel33.Controls.Add(this.panel34);
            this.panel33.Controls.Add(this.label125);
            this.panel33.Location = new System.Drawing.Point(8, 22);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(1343, 481);
            this.panel33.TabIndex = 27;
            this.panel33.Paint += new System.Windows.Forms.PaintEventHandler(this.panel33_Paint);
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.SlateGray;
            this.panel35.Controls.Add(this.txtPreCycleRes);
            this.panel35.Controls.Add(this.txtPreStroke);
            this.panel35.Controls.Add(this.label119);
            this.panel35.Controls.Add(this.label116);
            this.panel35.Controls.Add(this.label118);
            this.panel35.Controls.Add(this.label117);
            this.panel35.Location = new System.Drawing.Point(671, 56);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(612, 410);
            this.panel35.TabIndex = 28;
            // 
            // txtPreCycleRes
            // 
            this.txtPreCycleRes.BackColor = System.Drawing.Color.Yellow;
            this.txtPreCycleRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreCycleRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreCycleRes.ForeColor = System.Drawing.Color.Red;
            this.txtPreCycleRes.Location = new System.Drawing.Point(283, 175);
            this.txtPreCycleRes.Multiline = true;
            this.txtPreCycleRes.Name = "txtPreCycleRes";
            this.txtPreCycleRes.Size = new System.Drawing.Size(119, 62);
            this.txtPreCycleRes.TabIndex = 14;
            // 
            // txtPreStroke
            // 
            this.txtPreStroke.BackColor = System.Drawing.Color.Yellow;
            this.txtPreStroke.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreStroke.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreStroke.ForeColor = System.Drawing.Color.Red;
            this.txtPreStroke.Location = new System.Drawing.Point(251, 93);
            this.txtPreStroke.Multiline = true;
            this.txtPreStroke.Name = "txtPreStroke";
            this.txtPreStroke.Size = new System.Drawing.Size(151, 62);
            this.txtPreStroke.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label119.Location = new System.Drawing.Point(414, 131);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(46, 25);
            this.label119.TabIndex = 21;
            this.label119.Text = "mm";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label116.Location = new System.Drawing.Point(90, 113);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(107, 31);
            this.label116.TabIndex = 18;
            this.label116.Text = "Stroke ";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label118.Location = new System.Drawing.Point(414, 211);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(40, 25);
            this.label118.TabIndex = 20;
            this.label118.Text = "ms";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label117.Location = new System.Drawing.Point(90, 193);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(78, 31);
            this.label117.TabIndex = 19;
            this.label117.Text = "Time";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.Location = new System.Drawing.Point(926, 16);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(106, 36);
            this.label126.TabIndex = 28;
            this.label126.Text = "Result";
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.SlateGray;
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel34.Controls.Add(this.label129);
            this.panel34.Controls.Add(this.txtSettleTime);
            this.panel34.Controls.Add(this.label130);
            this.panel34.Controls.Add(this.label127);
            this.panel34.Controls.Add(this.txtPreDecel);
            this.panel34.Controls.Add(this.label128);
            this.panel34.Controls.Add(this.label115);
            this.panel34.Controls.Add(this.label37);
            this.panel34.Controls.Add(this.btnPreUpdateRes);
            this.panel34.Controls.Add(this.btnPreHome);
            this.panel34.Controls.Add(this.txtPreForce);
            this.panel34.Controls.Add(this.btnPreStop);
            this.panel34.Controls.Add(this.label120);
            this.panel34.Controls.Add(this.label124);
            this.panel34.Controls.Add(this.btnPreStart);
            this.panel34.Controls.Add(this.comboBox8);
            this.panel34.Controls.Add(this.txtPreAcceleration);
            this.panel34.Controls.Add(this.label123);
            this.panel34.Controls.Add(this.label114);
            this.panel34.Controls.Add(this.txtPreSpeed);
            this.panel34.Controls.Add(this.label122);
            this.panel34.Controls.Add(this.label113);
            this.panel34.Controls.Add(this.label86);
            this.panel34.Controls.Add(this.txtPrePositionB);
            this.panel34.Controls.Add(this.label121);
            this.panel34.Controls.Add(this.txtPrePositionA);
            this.panel34.Controls.Add(this.label112);
            this.panel34.Location = new System.Drawing.Point(50, 56);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(615, 410);
            this.panel34.TabIndex = 28;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label129.Location = new System.Drawing.Point(294, 245);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(49, 18);
            this.label129.TabIndex = 36;
            this.label129.Text = "msec";
            // 
            // txtSettleTime
            // 
            this.txtSettleTime.Location = new System.Drawing.Point(188, 242);
            this.txtSettleTime.Name = "txtSettleTime";
            this.txtSettleTime.Size = new System.Drawing.Size(100, 24);
            this.txtSettleTime.TabIndex = 35;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label130.Location = new System.Drawing.Point(24, 244);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(106, 18);
            this.label130.TabIndex = 34;
            this.label130.Text = "Settling Time";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label127.Location = new System.Drawing.Point(294, 180);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(110, 18);
            this.label127.TabIndex = 33;
            this.label127.Text = "Counts/ Sec2";
            // 
            // txtPreDecel
            // 
            this.txtPreDecel.Location = new System.Drawing.Point(188, 177);
            this.txtPreDecel.Name = "txtPreDecel";
            this.txtPreDecel.Size = new System.Drawing.Size(100, 24);
            this.txtPreDecel.TabIndex = 32;
            this.txtPreDecel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPreDecel_KeyUp);
            this.txtPreDecel.Leave += new System.EventHandler(this.txtPreDecel_Leave);
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label128.Location = new System.Drawing.Point(24, 179);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(112, 18);
            this.label128.TabIndex = 31;
            this.label128.Text = "Decceleration";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label115.Location = new System.Drawing.Point(294, 212);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(49, 18);
            this.label115.TabIndex = 30;
            this.label115.Text = "Value";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label37.Location = new System.Drawing.Point(23, 14);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(146, 18);
            this.label37.TabIndex = 1;
            this.label37.Text = "Choose Operation";
            // 
            // btnPreUpdateRes
            // 
            this.btnPreUpdateRes.BackColor = System.Drawing.Color.Tan;
            this.btnPreUpdateRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreUpdateRes.Location = new System.Drawing.Point(180, 353);
            this.btnPreUpdateRes.Name = "btnPreUpdateRes";
            this.btnPreUpdateRes.Size = new System.Drawing.Size(153, 41);
            this.btnPreUpdateRes.TabIndex = 13;
            this.btnPreUpdateRes.Text = "Clear Result";
            this.btnPreUpdateRes.UseVisualStyleBackColor = false;
            this.btnPreUpdateRes.Click += new System.EventHandler(this.btnPreUpdateRes_Click);
            // 
            // btnPreHome
            // 
            this.btnPreHome.BackColor = System.Drawing.Color.Yellow;
            this.btnPreHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreHome.Location = new System.Drawing.Point(342, 307);
            this.btnPreHome.Name = "btnPreHome";
            this.btnPreHome.Size = new System.Drawing.Size(153, 41);
            this.btnPreHome.TabIndex = 12;
            this.btnPreHome.Text = "Home";
            this.btnPreHome.UseVisualStyleBackColor = false;
            this.btnPreHome.Click += new System.EventHandler(this.btnPreHome_Click);
            // 
            // txtPreForce
            // 
            this.txtPreForce.Location = new System.Drawing.Point(188, 209);
            this.txtPreForce.Name = "txtPreForce";
            this.txtPreForce.Size = new System.Drawing.Size(100, 24);
            this.txtPreForce.TabIndex = 28;
            this.txtPreForce.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPreForce_KeyUp);
            this.txtPreForce.Leave += new System.EventHandler(this.txtPreForce_Leave);
            // 
            // btnPreStop
            // 
            this.btnPreStop.BackColor = System.Drawing.Color.Red;
            this.btnPreStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreStop.Location = new System.Drawing.Point(181, 307);
            this.btnPreStop.Name = "btnPreStop";
            this.btnPreStop.Size = new System.Drawing.Size(153, 41);
            this.btnPreStop.TabIndex = 11;
            this.btnPreStop.Text = "Stop Sequence";
            this.btnPreStop.UseVisualStyleBackColor = false;
            this.btnPreStop.Click += new System.EventHandler(this.btnPreStop_Click);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label120.Location = new System.Drawing.Point(24, 211);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(52, 18);
            this.label120.TabIndex = 27;
            this.label120.Text = "Force";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label124.Location = new System.Drawing.Point(294, 148);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(110, 18);
            this.label124.TabIndex = 26;
            this.label124.Text = "Counts/ Sec2";
            // 
            // btnPreStart
            // 
            this.btnPreStart.BackColor = System.Drawing.Color.Lime;
            this.btnPreStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPreStart.Location = new System.Drawing.Point(20, 307);
            this.btnPreStart.Name = "btnPreStart";
            this.btnPreStart.Size = new System.Drawing.Size(153, 41);
            this.btnPreStart.TabIndex = 10;
            this.btnPreStart.Text = "Start Sequence";
            this.btnPreStart.UseVisualStyleBackColor = false;
            this.btnPreStart.Click += new System.EventHandler(this.btnPreStart_Click);
            // 
            // comboBox8
            // 
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "Position move to A and B"});
            this.comboBox8.Location = new System.Drawing.Point(188, 10);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(307, 26);
            this.comboBox8.TabIndex = 0;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // txtPreAcceleration
            // 
            this.txtPreAcceleration.Location = new System.Drawing.Point(188, 145);
            this.txtPreAcceleration.Name = "txtPreAcceleration";
            this.txtPreAcceleration.Size = new System.Drawing.Size(100, 24);
            this.txtPreAcceleration.TabIndex = 9;
            this.txtPreAcceleration.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPreAcceleration_KeyUp);
            this.txtPreAcceleration.Leave += new System.EventHandler(this.txtPreAcceleration_Leave);
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label123.Location = new System.Drawing.Point(294, 116);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(75, 18);
            this.label123.TabIndex = 25;
            this.label123.Text = "mm/ Sec";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label114.Location = new System.Drawing.Point(24, 147);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(101, 18);
            this.label114.TabIndex = 8;
            this.label114.Text = "Acceleration";
            // 
            // txtPreSpeed
            // 
            this.txtPreSpeed.Location = new System.Drawing.Point(188, 112);
            this.txtPreSpeed.Name = "txtPreSpeed";
            this.txtPreSpeed.Size = new System.Drawing.Size(100, 24);
            this.txtPreSpeed.TabIndex = 7;
            this.txtPreSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPreSpeed_KeyUp);
            this.txtPreSpeed.Leave += new System.EventHandler(this.txtPreSpeed_Leave);
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label122.Location = new System.Drawing.Point(294, 84);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(36, 18);
            this.label122.TabIndex = 24;
            this.label122.Text = "mm";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label113.Location = new System.Drawing.Point(24, 112);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(55, 18);
            this.label113.TabIndex = 6;
            this.label113.Text = "Speed";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label86.Location = new System.Drawing.Point(24, 44);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(85, 18);
            this.label86.TabIndex = 2;
            this.label86.Text = "Position A";
            // 
            // txtPrePositionB
            // 
            this.txtPrePositionB.Location = new System.Drawing.Point(188, 78);
            this.txtPrePositionB.Name = "txtPrePositionB";
            this.txtPrePositionB.Size = new System.Drawing.Size(100, 24);
            this.txtPrePositionB.TabIndex = 5;
            this.txtPrePositionB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrePositionB_KeyUp);
            this.txtPrePositionB.Leave += new System.EventHandler(this.txtPrePositionB_Leave);
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label121.Location = new System.Drawing.Point(294, 50);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(36, 18);
            this.label121.TabIndex = 23;
            this.label121.Text = "mm";
            // 
            // txtPrePositionA
            // 
            this.txtPrePositionA.Location = new System.Drawing.Point(188, 44);
            this.txtPrePositionA.Name = "txtPrePositionA";
            this.txtPrePositionA.Size = new System.Drawing.Size(100, 24);
            this.txtPrePositionA.TabIndex = 4;
            this.txtPrePositionA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrePositionA_KeyUp);
            this.txtPrePositionA.Leave += new System.EventHandler(this.txtPrePositionA_Leave);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.NavajoWhite;
            this.label112.Location = new System.Drawing.Point(24, 80);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(86, 18);
            this.label112.TabIndex = 3;
            this.label112.Text = "Position B";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.Location = new System.Drawing.Point(244, 17);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(217, 36);
            this.label125.TabIndex = 27;
            this.label125.Text = "Position Move";
            // 
            // btnAutoscaleSecondary1
            // 
            this.btnAutoscaleSecondary1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAutoscaleSecondary1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutoscaleSecondary1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAutoscaleSecondary1.Location = new System.Drawing.Point(253, 5);
            this.btnAutoscaleSecondary1.Name = "btnAutoscaleSecondary1";
            this.btnAutoscaleSecondary1.Size = new System.Drawing.Size(15, 23);
            this.btnAutoscaleSecondary1.TabIndex = 507;
            this.btnAutoscaleSecondary1.UseVisualStyleBackColor = false;
            // 
            // btnAutoscalePrimary1
            // 
            this.btnAutoscalePrimary1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAutoscalePrimary1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAutoscalePrimary1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAutoscalePrimary1.Location = new System.Drawing.Point(232, 7);
            this.btnAutoscalePrimary1.Name = "btnAutoscalePrimary1";
            this.btnAutoscalePrimary1.Size = new System.Drawing.Size(15, 23);
            this.btnAutoscalePrimary1.TabIndex = 506;
            this.btnAutoscalePrimary1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAutoscalePrimary1.UseVisualStyleBackColor = false;
            this.btnAutoscalePrimary1.Click += new System.EventHandler(this.btnAutoscalePrimary1_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.btnConnect.Location = new System.Drawing.Point(1234, 1);
            this.btnConnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(116, 28);
            this.btnConnect.TabIndex = 113;
            this.btnConnect.Text = "Connect";
            this.btnConnect.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblPortName
            // 
            this.lblPortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortName.AutoSize = true;
            this.lblPortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblPortName.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblPortName.Location = new System.Drawing.Point(856, 7);
            this.lblPortName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPortName.Name = "lblPortName";
            this.lblPortName.Size = new System.Drawing.Size(95, 17);
            this.lblPortName.TabIndex = 111;
            this.lblPortName.Text = "Serial Port :";
            // 
            // txtConnectionStatus
            // 
            this.txtConnectionStatus.AutoSize = true;
            this.txtConnectionStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtConnectionStatus.ForeColor = System.Drawing.Color.Red;
            this.txtConnectionStatus.Location = new System.Drawing.Point(431, 7);
            this.txtConnectionStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtConnectionStatus.Name = "txtConnectionStatus";
            this.txtConnectionStatus.Size = new System.Drawing.Size(108, 19);
            this.txtConnectionStatus.TabIndex = 3;
            this.txtConnectionStatus.Text = "Disconnected";
            // 
            // btnFaultReset
            // 
            this.btnFaultReset.BackColor = System.Drawing.Color.Turquoise;
            this.btnFaultReset.Enabled = false;
            this.btnFaultReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFaultReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnFaultReset.Location = new System.Drawing.Point(126, 3);
            this.btnFaultReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnFaultReset.Name = "btnFaultReset";
            this.btnFaultReset.Size = new System.Drawing.Size(99, 26);
            this.btnFaultReset.TabIndex = 5;
            this.btnFaultReset.Text = "Fault Reset";
            this.btnFaultReset.UseVisualStyleBackColor = false;
            this.btnFaultReset.Visible = false;
            this.btnFaultReset.Click += new System.EventHandler(this.btnFaultReset_Click);
            // 
            // tmrStatus
            // 
            this.tmrStatus.Tick += new System.EventHandler(this.tmrStatus_Tick);
            // 
            // tmrLogging
            // 
            this.tmrLogging.Tick += new System.EventHandler(this.tmrLogging_Tick);
            // 
            // txtStatus
            // 
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtStatus.Location = new System.Drawing.Point(696, 4);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(144, 23);
            this.txtStatus.TabIndex = 95;
            this.txtStatus.Text = "STATUS";
            // 
            // colFunc
            // 
            this.colFunc.DisplayIndex = 0;
            this.colFunc.Text = "Function";
            this.colFunc.Width = 350;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnAutoscaleSecondary1);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.btnAutoscalePrimary1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblNode1);
            this.panel1.Controls.Add(this.cmbNode);
            this.panel1.Controls.Add(this.btnFaultQuery);
            this.panel1.Controls.Add(this.txtConnectionStatus);
            this.panel1.Controls.Add(this.txtStatus);
            this.panel1.Controls.Add(this.btnFaultReset);
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Controls.Add(this.cmbPortName);
            this.panel1.Controls.Add(this.lblPortName);
            this.panel1.Location = new System.Drawing.Point(4, 833);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1363, 33);
            this.panel1.TabIndex = 132;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label18.Location = new System.Drawing.Point(549, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(136, 17);
            this.label18.TabIndex = 507;
            this.label18.Text = "Operation Status:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label4.Location = new System.Drawing.Point(282, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 17);
            this.label4.TabIndex = 503;
            this.label4.Text = "Controller Status :";
            // 
            // lblNode1
            // 
            this.lblNode1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNode1.AutoSize = true;
            this.lblNode1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNode1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblNode1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblNode1.Location = new System.Drawing.Point(1086, 7);
            this.lblNode1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNode1.Name = "lblNode1";
            this.lblNode1.Size = new System.Drawing.Size(58, 19);
            this.lblNode1.TabIndex = 506;
            this.lblNode1.Text = "Node :";
            // 
            // cmbNode
            // 
            this.cmbNode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbNode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNode.FormattingEnabled = true;
            this.cmbNode.Location = new System.Drawing.Point(1156, 3);
            this.cmbNode.Margin = new System.Windows.Forms.Padding(4);
            this.cmbNode.Name = "cmbNode";
            this.cmbNode.Size = new System.Drawing.Size(63, 24);
            this.cmbNode.TabIndex = 505;
            this.cmbNode.SelectedIndexChanged += new System.EventHandler(this.cmbNode_SelectedIndexChanged);
            // 
            // btnFaultQuery
            // 
            this.btnFaultQuery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnFaultQuery.Enabled = false;
            this.btnFaultQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFaultQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.btnFaultQuery.Location = new System.Drawing.Point(4, 3);
            this.btnFaultQuery.Margin = new System.Windows.Forms.Padding(4);
            this.btnFaultQuery.Name = "btnFaultQuery";
            this.btnFaultQuery.Size = new System.Drawing.Size(113, 26);
            this.btnFaultQuery.TabIndex = 504;
            this.btnFaultQuery.Text = "Fault definition";
            this.btnFaultQuery.UseVisualStyleBackColor = false;
            this.btnFaultQuery.Visible = false;
            this.btnFaultQuery.Click += new System.EventHandler(this.btnFaultQuery_Click);
            // 
            // cmbPortName
            // 
            this.cmbPortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPortName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPortName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cmbPortName.FormattingEnabled = true;
            this.cmbPortName.Location = new System.Drawing.Point(966, 4);
            this.cmbPortName.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Size = new System.Drawing.Size(105, 24);
            this.cmbPortName.Sorted = true;
            this.cmbPortName.TabIndex = 110;
            this.cmbPortName.Click += new System.EventHandler(this.cmbPortName_Click);
            // 
            // tmrTuning
            // 
            this.tmrTuning.Enabled = true;
            this.tmrTuning.Interval = 10;
            this.tmrTuning.Tick += new System.EventHandler(this.tmrTuning_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(1024, 700);
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1372, 873);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabMeasure);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(998, 698);
            //this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseClick);
            this.tabMeasure.ResumeLayout(false);
            this.tabBuildandRun.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxMacro.ResumeLayout(false);
            this.groupBoxMacro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProgram)).EndInit();
            this.tabContLogging.ResumeLayout(false);
            this.tabContLogging.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartMeasure)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartSwitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.panel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.panel33.ResumeLayout(false);
            this.panel33.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel34.ResumeLayout(false);
            this.panel34.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMeasure;
        private System.Windows.Forms.TabPage tabBuildandRun;
        private System.Windows.Forms.Button btnOpenProgram;
        private System.Windows.Forms.Button btnDownloadMacro;
        private System.Windows.Forms.Label txtConnectionStatus;
        private System.Windows.Forms.Button btnFaultReset;
        private System.Windows.Forms.Timer tmrStatus;
        private System.Windows.Forms.Timer tmrLogging;
        private System.Windows.Forms.DataGridView dgvProgram;
        private System.Windows.Forms.Button btnInsertBelow;
        private System.Windows.Forms.Button btnInsertAbove;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnMoveDown;
        private System.Windows.Forms.Button btnMoveUp;
        private System.Windows.Forms.GroupBox groupBoxMacro;
        private System.Windows.Forms.Button btnDownloadAllMacros;
        private System.Windows.Forms.Button btnSaveProgram;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmbDownloadConfig;
        public System.Windows.Forms.TextBox txtComment;
        public System.Windows.Forms.Label lblComment;
        public System.Windows.Forms.Label lblUnits8;
        public System.Windows.Forms.Label lblUnits7;
        public System.Windows.Forms.TextBox txtParm8;
        public System.Windows.Forms.TextBox txtParm7;
        public System.Windows.Forms.Label lblName8;
        public System.Windows.Forms.Label lblName7;
        public System.Windows.Forms.Label lblName6;
        public System.Windows.Forms.Label lblName5;
        public System.Windows.Forms.Label lblName4;
        public System.Windows.Forms.Label lblName3;
        public System.Windows.Forms.Label lblName2;
        public System.Windows.Forms.Label lblName1;
        public System.Windows.Forms.ComboBox ComboBox4;
        public System.Windows.Forms.ComboBox ComboBox3;
        public System.Windows.Forms.ComboBox ComboBox2;
        public System.Windows.Forms.ComboBox ComboBox1;
        public System.Windows.Forms.Label lblUnits6;
        public System.Windows.Forms.TextBox txtParm6;
        public System.Windows.Forms.Label lblUnits5;
        public System.Windows.Forms.Label lblUnits4;
        public System.Windows.Forms.Label lblUnits3;
        public System.Windows.Forms.Label lblUnits2;
        public System.Windows.Forms.Label lblUnits1;
        public System.Windows.Forms.TextBox txtParm2;
        public System.Windows.Forms.TextBox txtParm3;
        public System.Windows.Forms.TextBox txtParm4;
        public System.Windows.Forms.TextBox txtParm5;
        public System.Windows.Forms.TextBox txtParm1;
        public System.Windows.Forms.ComboBox cmbFunction;
        private System.Windows.Forms.Label lblFunction;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lblPortName;
        private System.Windows.Forms.ComboBox cmbPortName;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ColumnHeader colFunc;
        public System.Windows.Forms.Label lblUnits9;
        public System.Windows.Forms.Label lblName9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbMacroDownload;
        private System.Windows.Forms.Button btnMotoroff;
        private System.Windows.Forms.Button btnFaultQuery;
        private System.Windows.Forms.Button btnContactInformation1;
        private System.Windows.Forms.Button btnNewFile;
        private System.Windows.Forms.Button btnEraseAllMacros;
        public System.Windows.Forms.Button btnSelect1;
        public System.Windows.Forms.Button btnSelect4;
        public System.Windows.Forms.Button btnSelect3;
        public System.Windows.Forms.Button btnSelect2;
        public System.Windows.Forms.ComboBox ComboBox5;
        public System.Windows.Forms.Label lblUnits10;
        public System.Windows.Forms.Label lblName10;
        public System.Windows.Forms.ComboBox ComboBox6;
        public System.Windows.Forms.Label lblUnits11;
        public System.Windows.Forms.Label lblName11;
        public System.Windows.Forms.Label lblUnits12;
        public System.Windows.Forms.Label lblName12;
        public System.Windows.Forms.Label lblUnits14;
        public System.Windows.Forms.Label lblUnits13;
        public System.Windows.Forms.Label lblName14;
        public System.Windows.Forms.Label lblName13;
        private System.Windows.Forms.Timer tmrTuning;
        public System.Windows.Forms.ComboBox ComboBox7;
        public System.Windows.Forms.Label lblName15;
        public System.Windows.Forms.Label lblUnits15;
        public System.Windows.Forms.Label lblUnits16;
        public System.Windows.Forms.Label lblName16;
        private System.Windows.Forms.Button btnEnable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Count;
        private System.Windows.Forms.DataGridViewTextBoxColumn Line;
        private System.Windows.Forms.DataGridViewTextBoxColumn Command;
        private System.Windows.Forms.DataGridViewTextBoxColumn Parameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn CommentedOut;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumberSave;
        private System.Windows.Forms.Label lblNode1;
        public System.Windows.Forms.ComboBox cmbNode;
        private System.Windows.Forms.CheckBox chkSetProtectedAccess;
        private System.Windows.Forms.CheckBox chkApplyToAllNodes;
        private System.Windows.Forms.CheckBox chkAutoBackup;
        private System.Windows.Forms.Button btnAutoBackupInfo;
        private System.Windows.Forms.TabPage tabContLogging;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnStartDemo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button btnAutoscaleSecondary;
        private System.Windows.Forms.TextBox txtMaxSecondaryRun;
        private System.Windows.Forms.TextBox txtMinSecondaryRun;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button btnSaveLogs;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button btnAutoscalePrimary;
        private System.Windows.Forms.TextBox txtMaxPrimaryRun;
        private System.Windows.Forms.TextBox txtMinPrimaryRun;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Button btnStartTraceLive;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkLogitem2secondary;
        private System.Windows.Forms.TextBox txtValue2;
        private System.Windows.Forms.TextBox txtValue1;
        private System.Windows.Forms.ComboBox cmbSamples;
        private System.Windows.Forms.CheckBox chkLogitem1primary;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtObjectindex1;
        private System.Windows.Forms.ComboBox cmbIntervalLive;
        private System.Windows.Forms.ComboBox cmbBackgroundobject2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbBackgroundobject1;
        private System.Windows.Forms.TextBox txtObjectindex2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtTargetPositionDemo;
        private System.Windows.Forms.Label lblTargetPositionDemo;
        private System.Windows.Forms.TextBox txtProfileVelocityDemo;
        private System.Windows.Forms.Label lblSpeedDemo;
        private System.Windows.Forms.TextBox txtSoftlandErrorDemo;
        private System.Windows.Forms.Label lblSoftlandSensitivityDemo;
        private System.Windows.Forms.Label lblForceDemo;
        private System.Windows.Forms.TextBox txtTargetForceDemo;
        private System.Windows.Forms.TextBox txtForceSlopeDemo;
        private System.Windows.Forms.Label lblForceSlopeDemo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnContactInfo2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbActuatorDemo;
        private System.Windows.Forms.Label lblActuator;
        private System.Windows.Forms.ComboBox cmbProgramTypeDemo;
        private System.Windows.Forms.Label lblProgramType;
        private System.Windows.Forms.ComboBox cmbController;
        private System.Windows.Forms.ComboBox cmbNode2;
        private System.Windows.Forms.ComboBox cmbNode1;
        private System.Windows.Forms.CheckBox chkLogitem2primary;
        private System.Windows.Forms.CheckBox chkLogitem1secondary;
        private System.Windows.Forms.Label labelctr2;
        private System.Windows.Forms.Label labelctr1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRun;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblInterval;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label lblFS1;
        private System.Windows.Forms.Label lblForce1;
        private System.Windows.Forms.Label lblSS1;
        private System.Windows.Forms.Label lblSpeed1;
        private System.Windows.Forms.Label lbltargetPosition1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPosition;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtSpeedRange;
        private System.Windows.Forms.TextBox txtForceSlopeRange;
        private System.Windows.Forms.TextBox txtForceRange;
        private System.Windows.Forms.TextBox txtSoftlandRange;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAutoSeq;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnAutoscaleSecondary1;
        private System.Windows.Forms.Button btnAutoscalePrimary1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cmbMeasureType;
        private System.Windows.Forms.Label lblMeasureType;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lblPosCloseToPart;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMeasureForce;
        private System.Windows.Forms.Label lblMeasureForce;
        private System.Windows.Forms.TextBox txtMeasureSensitivity;
        private System.Windows.Forms.Label lblMeasureSensitivity;
        private System.Windows.Forms.TextBox txtMeasureSpeed;
        private System.Windows.Forms.Label lblMeasureSpeed;
        private System.Windows.Forms.TextBox txtPosCloseToPart;
        private System.Windows.Forms.TextBox txtMsRangeF;
        private System.Windows.Forms.TextBox txtMsRangeSen;
        private System.Windows.Forms.TextBox txtMsRangeSpeed;
        private System.Windows.Forms.TextBox txtMsRangePos;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartMeasure;
        private System.Windows.Forms.CheckBox chkLP1Measure;
        private System.Windows.Forms.ComboBox cmbNode2Measure;
        private System.Windows.Forms.ComboBox cmbNode1Measure;
        private System.Windows.Forms.CheckBox chkLP2Measure;
        private System.Windows.Forms.CheckBox chkLS1Measure;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnMeasureStop;
        private System.Windows.Forms.Button btnASMeasure;
        private System.Windows.Forms.Button btnMeasureHome;
        private System.Windows.Forms.Button btnASP1;
        private System.Windows.Forms.Button btnTeachDatum;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnClearChart;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtMeasureResult;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtMeasureDT;
        private System.Windows.Forms.TextBox txtMsRangeDT;
        private System.Windows.Forms.Label lblMeasureDT;
        private System.Windows.Forms.TextBox txtMinSecondaryRun1;
        private System.Windows.Forms.TextBox txtMinPrimaryRun1;
        private System.Windows.Forms.TextBox txtMaxSecondaryRun1;
        private System.Windows.Forms.TextBox txtMaxPrimaryRun1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartSwitch;
        private System.Windows.Forms.TextBox txtSoftlandSpeedSwitch;
        private System.Windows.Forms.TextBox txtSwitchSpeed;
        private System.Windows.Forms.TextBox txtErrorSwitch;
        private System.Windows.Forms.TextBox txtPosCloseSwitch;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtPosSwitchRange;
        private System.Windows.Forms.TextBox txtErrorSwitchRange;
        private System.Windows.Forms.TextBox txtSwitchSpeedRange;
        private System.Windows.Forms.TextBox txtSSSRange;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblPositionSwitch;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cmbSwitchType;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox txtLastPosResult;
        private System.Windows.Forms.TextBox txtForceResult;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button btnStopSwitch;
        private System.Windows.Forms.Button btnStartSwitch;
        private System.Windows.Forms.Button btnHomeSwitch;
        private System.Windows.Forms.CheckBox chkLP2Switch;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btnSavelogSwitch;
        private System.Windows.Forms.CheckBox chkLP1Switch;
        private System.Windows.Forms.TextBox txtSwitchValue2;
        private System.Windows.Forms.TextBox txtSwitchValue1;
        private System.Windows.Forms.ComboBox cmbSwitchObject2;
        private System.Windows.Forms.ComboBox cmbSwitchObject1;
        private System.Windows.Forms.TextBox txtObject2Switch;
        private System.Windows.Forms.TextBox txtObject1Switch;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtIniPosResult;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ComboBox cmbLeakTestType;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label lblDwellLeak;
        private System.Windows.Forms.Label lblForceLeak;
        private System.Windows.Forms.Label lblSensitivityLeak;
        private System.Windows.Forms.Label lblSpeedLeak;
        private System.Windows.Forms.Label lblPostionLeak;
        private System.Windows.Forms.TextBox txtDwellLeak;
        private System.Windows.Forms.TextBox txtForceLeak;
        private System.Windows.Forms.TextBox txtSpeedLeak;
        private System.Windows.Forms.TextBox txtSensitivityLeak;
        private System.Windows.Forms.TextBox txtPositionLeak;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtDwellLeakRange;
        private System.Windows.Forms.TextBox txtForceLeakRange;
        private System.Windows.Forms.TextBox txtSpeedLeakRange;
        private System.Windows.Forms.TextBox txtSensitivityLeakRange;
        private System.Windows.Forms.TextBox txtPositionLeakRange;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Button btnActuatorStopLeak;
        private System.Windows.Forms.Button btnActuatorStartLeak;
        private System.Windows.Forms.Button btnHomeLeak;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtHighSpeedVelocity;
        private System.Windows.Forms.TextBox txtHighSpeedTarget2;
        private System.Windows.Forms.TextBox txtHighSpeedTarget1;
        private System.Windows.Forms.Button btnHighSpeedHome;
        private System.Windows.Forms.Button btnHighSpeedStop;
        private System.Windows.Forms.Button btnHighSpeedStart;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Button btnClearSwitch;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.ComboBox cmbHighSpeed;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox txtLeakResult;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox txtFinalLeak;
        private System.Windows.Forms.TextBox txtSoftlandLeak;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox txtPosition1StepForce;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox txtSoftlandStepForce;
        private System.Windows.Forms.Button btnHomeStepForce;
        private System.Windows.Forms.Button btnStopStepForce;
        private System.Windows.Forms.Button btnStartStepForce;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TextBox txtHighSpeedCycleFreq;
        private System.Windows.Forms.TextBox txtHighSpeedCycleCount;
        private System.Windows.Forms.TextBox txtHighSpeedCycleTime;
        private System.Windows.Forms.Button btnHighSpeedClear;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox txtPosition3StepForce;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox txtPosition2StepForce;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnSavelogMeasure;
        private System.Windows.Forms.ComboBox cmbSamMeasure;
        private System.Windows.Forms.ComboBox cmbILMeasure;
        private System.Windows.Forms.CheckBox chkLS2Measure;
        private System.Windows.Forms.TextBox txtMeasureValue2;
        private System.Windows.Forms.TextBox txtMeasureValue1;
        private System.Windows.Forms.ComboBox cmbBGobject2;
        private System.Windows.Forms.ComboBox cmbBGobject1;
        private System.Windows.Forms.TextBox txtObIn1Measure;
        private System.Windows.Forms.TextBox txtObIn2Measure;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.TextBox txtPreCycleRes;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button btnPreUpdateRes;
        private System.Windows.Forms.Button btnPreHome;
        private System.Windows.Forms.Button btnPreStop;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Button btnPreStart;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.TextBox txtPreAcceleration;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox txtPreSpeed;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox txtPrePositionB;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox txtPrePositionA;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.TextBox txtPreStroke;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox txtPreForce;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox txtPreDecel;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox txtSettleTime;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox30;
    }
}

