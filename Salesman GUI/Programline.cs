﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Salesman_GUI
{
    //
    // The first field in a line can be:
    // Blank - treated as a comment
    // ;    Starts a comment
    // Macro followed by a number, this is the start of a macro
    // A valid macro name followed by a optional list of parameters
    // R starts a read command
    // W 
    public class Programline
    {
    }

    public class BlankLine : Programline
    {
    }


    public class CommentLine : Programline
    {
    }


    public class DirectiveLine : Programline
    {
    }

    public class WriteCommandLine : Programline
    {
        public int iIndex;
        public int iSubindex;
        public Int64 i64Value;


        public WriteCommandLine(int index, int subindex, Int64 value)
        {
//            System.Diagnostics.Debug.Assert(false, "Assertion failed");

        }
    }

    public class ReadCommandLine : Programline
    {
    }
}
