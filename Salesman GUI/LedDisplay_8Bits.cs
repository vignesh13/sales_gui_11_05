﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Salesman_GUI
{
    public class LedDisplay_8Bits
    {
        private Label[] theLeds = new Label[8];
            
        public LedDisplay_8Bits(Label led7, Label led6, Label led5, Label led4, Label led3, Label led2, Label led1, Label led0)
        {
            theLeds[0] = led0;
            theLeds[1] = led1;
            theLeds[2] = led2;
            theLeds[3] = led3;
            theLeds[4] = led4;
            theLeds[5] = led5;
            theLeds[6] = led6;
            theLeds[7] = led7;
        }

        public void Refresh(int value)
        {
            for (int i = 0; i < 8; i++)
            {
                theLeds[i].ForeColor = ((value & (1 << i)) != 0) ? Color.Green : Color.DarkGray;
            }
        }

        public void SetVisible(bool visible)
        {
            foreach (Label label in theLeds)
            {
                label.Visible = visible;
            }
        }
    }
}
