﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Salesman_GUI
{
    // Class to perform logging of all Serial I/O
    class Logfile
    {
        private bool bStarted = false;
        private StreamWriter sw;

        public Logfile()
        {
        }


        private string TimeStamp()
        {
            DateTime now = DateTime.Now;
            return string.Format("{0:dd/MM/yyy hh:mm:ss.fff} ", now);
        }

        public void Start(string strFilename)
        {
            try
            {
                sw = new StreamWriter(strFilename);
                bStarted = true;
                sw.Write(TimeStamp() + "Logging started\n");
                sw.Flush();
            }
            catch
            {
                MessageBox.Show("Error Sequence and it affects continuos data logging.\n" +
                                "Please hit Stop button, home and then start the actuator to move to different position", "Error Sequence",MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        public void Stop()
        {
            if (bStarted)
            {
                sw.Write(TimeStamp() + "Logging stopped\n");
                sw.Flush();

                sw.Close();
                bStarted = false;
            }
        }

        public void Add(string strStringToAdd)
        {
            if (bStarted)
            {
                sw.Write(TimeStamp() + strStringToAdd);
                sw.Flush();
            }
        }

        public void AddWithoutTimestamp(string strStringToAdd)
        {
            if (bStarted)
            {
                sw.Write(strStringToAdd);
                sw.Flush();
            }
        }


    }
}
