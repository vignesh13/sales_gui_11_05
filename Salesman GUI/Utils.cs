﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Salesman_GUI
{
    public class Utils
    {
        public Int64 ConvertStringToInt64(string strInput)
        {
            Int64 i64Val;
            UInt64 u64Val;
            strInput = strInput.Trim().ToUpper();
            if ((strInput.Length >= 3) && (strInput.Substring(0,2)) == "0X")
            {
                if (Int64.TryParse(strInput.Substring(2), NumberStyles.HexNumber, null, out i64Val))
                {
                    return i64Val;
                }
            }
            else
            {
                if (Int64.TryParse(strInput, System.Globalization.NumberStyles.Number, null, out i64Val))
                {
                    return i64Val;
                }
                else if (UInt64.TryParse(strInput, System.Globalization.NumberStyles.Number, null, out u64Val))
                {
                    return (Int64)u64Val;
                }
            } 
//            MessageBox.Show("Illegal number format : " + strInput); // This error happens if the controller is switched off during a message
            return 0;
        }

        public int formatInt(int value, int size, bool signed)
        {
            int mask = 0;
            int signbit = 0;
            switch (size)
            {
                case 1:
                    signbit = unchecked((int)0x00000080);
                    mask =    unchecked((int)0x000000FF);
                    break;
                case 2:
                    signbit = unchecked((int)0x00008000);
                    mask =    unchecked((int)0x0000FFFF);
                    break;
                case 3:
                    signbit = unchecked((int)0x00800000);
                    mask =    unchecked((int)0x00FFFFFF);
                    break;
                default:
                    signbit = unchecked((int)0x80000000);
                    mask =    unchecked((int)0xFFFFFFFF);
                    break;
            }

            value = value & mask;
            if (signed)     // Then sign extend if sign bit is 1
            {
                if ((value & signbit) != 0)
                {
                    value |= ~mask;       // Sign extend
                }
            }
            return value;
        }

        public Int64 ConvertStringToInt64(string strInput, int size, bool signed)
        {
            Int64 mask = 0;
            Int64 signbit = 0;
            Int64 i64Val = ConvertStringToInt64(strInput);
            switch (size)
            {
                case 1:
                    signbit = unchecked ((Int64) 0x0000000000000080);
                    mask  =   unchecked ((Int64) 0x00000000000000FF);
                    break;
                case 2:
                    signbit = unchecked ((Int64) 0x0000000000008000);
                    mask  =   unchecked ((Int64) 0x000000000000FFFF);
                    break;
                case 3:
                    signbit = unchecked ((Int64) 0x0000000000800000);
                    mask  =   unchecked ((Int64) 0x0000000000FFFFFF);
                    break;
                case 4:
                    signbit = unchecked ((Int64) 0x0000000080000000);
                    mask  =   unchecked ((Int64) 0x00000000FFFFFFFF);
                    break;
                case 8:
                    signbit = unchecked ((Int64) 0x8000000000000000);
                    mask  =   unchecked ((Int64) 0xFFFFFFFFFFFFFFFF);
                    break;
            }

            i64Val = i64Val & mask;  
            if (signed)
            {
                if ((i64Val & signbit) != 0)
                {
                    i64Val |= ~mask;       // Sign extend
                }
            }
            return i64Val;
        }



        public string convertInt64ToVisibleString(Int64 intInput)
        {
            string strResult = "";
            string strTemp = intInput.ToString("X16");
 
            for (int i = 14; i >= 0; i = i - 2)     // The first character is at the end of the string
            {
                strResult += ((char)int.Parse(strTemp.Substring(i, 2), System.Globalization.NumberStyles.HexNumber, null)).ToString();
            }
            strResult = strResult.Trim('\0');
            strResult = strResult.Trim();
            return strResult;
        }

        public void FillComboBox(ComboBox box, string[] values)
        {
            box.Items.Clear();
            if (values.Count() > 0)
            {
                foreach (string str in values)
                {
                    box.Items.Add(str);
                }
                box.SelectedIndex = 0;  // select the first item
            }
        }


        public bool ControlHasValue(Control control)
        {
            return control.Text.Trim() != "";
        }

        public bool ControlHasValidValue(Control control, Int64 min, Int64 max, string name)
        {
            bool bRetval = true;

            Int64 value = 0;

            control.Text = control.Text.Trim();

            if (control.Text == "")
            {
                MessageBox.Show(name + " may not be empty");
                bRetval = false;
            }
            else
            {
                if (Int64.TryParse(control.Text, out value))
                {
                    if ((value < min) || (value > max))
                    {
                        MessageBox.Show("The value " + control.Text + " for [" + name + "] is out of limits (allowed range is " + min.ToString() + " to " + max.ToString() + ")");
                        bRetval = false;
                    }
                }
                else
                {
                    MessageBox.Show("The value " + control.Text + " for [" + name + "] is not in a valid numeric format");
                    bRetval = false;
                }
            }

            return bRetval;
        }

        public Int64 GetControlValue(Control control, Int64 min, Int64 max, string name)
        {
            Int64 value = 0;
            if (Int64.TryParse(control.Text.Trim(), out value))
            {
                if ((value < min) || (value > max))
                {
                    MessageBox.Show("The value " + control.Text + " for [" + name + "] is out of limits (allowed range is " + min.ToString() + " to " + max.ToString() + ")");  
                }
                return value;
            }
            else
            {
                MessageBox.Show("The value " + control.Text + " for [" + name + "] is not in a valid numeric format");
                return 0;
            }
        }

        public int GetNumParameters(string parameters)
        {
            parameters = parameters.Trim();
            if (parameters == "")
            {
                return 0;       // Empty parameter string, return 0
            }
            else
            {
                return (parameters.Split(',').Count());
            }
        }

        //
        // This function will return the name of a parameter with the _ replaced by a space character
        //
        public string GetParameterName(string parameters, int number)
        {
            string parameter = parameters.Split(',')[number];
//            if (parameters.IndexOf('=') >= 0)   // Then the parameter has a name
//            {
                return (parameter.Split('=')[0].Trim()).Replace('_', ' ');
//            }
//            return "";
        }

        public string GetParameterValue(string parameters, int number)
        {
            string parameter = parameters.Split(',')[number];
            if (parameter.IndexOf('=') >= 0)   // Then the parameter has a value
            {
                return parameter.Split('=')[1].Trim();
            }
            return "";
        }

        public string GetParameter(string parameters, int number)
        {
            return parameters.Split(',')[number];
        }

        public string GetSourceString(string mnemonic, string parameters, string comment)
        {
            string temp = "[" + mnemonic + " " + parameters + "]";
            if (comment.Trim() != "")
            {
                temp += "{" + comment.Trim() + "}";
            }
            return temp;
        }

        public double str2double(string in_str,double default_val=0.0)
        {
            double value = 0.0;

            if(in_str=="")
            {
                value = default_val;
            }
            else
            {
                double dummy = 5.0;
                if(dummy.ToString().Contains(","))
                {
                    in_str.Replace(",", ".");
                }
                if (!Double.TryParse(in_str, out value))
                {
                    value = default_val;
                }
            }

            return value;
        }
        public void HideAll()
        {
            Globals.mainform.lblName1.Visible = false;
            Globals.mainform.lblName2.Visible = false;
            Globals.mainform.lblName3.Visible = false;
            Globals.mainform.lblName4.Visible = false;
            Globals.mainform.lblName5.Visible = false;
            Globals.mainform.lblName6.Visible = false;
            Globals.mainform.lblName7.Visible = false;
            Globals.mainform.lblName8.Visible = false;
            Globals.mainform.lblName9.Visible = false;
            Globals.mainform.lblName10.Visible = false;
            Globals.mainform.lblName11.Visible = false;
            Globals.mainform.lblName12.Visible = false;
            Globals.mainform.lblName13.Visible = false;
            Globals.mainform.lblName14.Visible = false;
            Globals.mainform.lblName15.Visible = false;
            Globals.mainform.lblName16.Visible = false;


            Globals.mainform.lblUnits1.Visible = false;
            Globals.mainform.lblUnits2.Visible = false;
            Globals.mainform.lblUnits3.Visible = false;
            Globals.mainform.lblUnits4.Visible = false;
            Globals.mainform.lblUnits5.Visible = false;
            Globals.mainform.lblUnits6.Visible = false;
            Globals.mainform.lblUnits7.Visible = false;
            Globals.mainform.lblUnits8.Visible = false;
            Globals.mainform.lblUnits9.Visible = false;
            Globals.mainform.lblUnits10.Visible = false;
            Globals.mainform.lblUnits11.Visible = false;
            Globals.mainform.lblUnits12.Visible = false;
            Globals.mainform.lblUnits13.Visible = false;
            Globals.mainform.lblUnits14.Visible = false;
            Globals.mainform.lblUnits15.Visible = false;
            Globals.mainform.lblUnits16.Visible = false;

            Globals.mainform.ComboBox1.Visible = false;
            Globals.mainform.ComboBox2.Visible = false;
            Globals.mainform.ComboBox3.Visible = false;
            Globals.mainform.ComboBox4.Visible = false;
            Globals.mainform.ComboBox5.Visible = false;
            Globals.mainform.ComboBox6.Visible = false;
            Globals.mainform.ComboBox7.Visible = false;

            Globals.mainform.txtParm1.Visible = false;
            Globals.mainform.txtParm2.Visible = false;
            Globals.mainform.txtParm3.Visible = false;
            Globals.mainform.txtParm4.Visible = false;
            Globals.mainform.txtParm5.Visible = false;
            Globals.mainform.txtParm6.Visible = false;
            Globals.mainform.txtParm7.Visible = false;
            Globals.mainform.txtParm8.Visible = false;

            Globals.mainform.btnSelect1.Visible = false;
            Globals.mainform.btnSelect2.Visible = false;
            Globals.mainform.btnSelect3.Visible = false;
            Globals.mainform.btnSelect4.Visible = false;
        }


        public void ErrorMessageMultipleDefined(string message)
        {
            MessageBox.Show("Multiple definition of " + message);
        }

        public int GetIndex(int combinedindex)
        {
            return combinedindex & 0xFFFF;
        }

        public int GetSubindex(int combinedindex)
        {
            return (combinedindex >> 16) & 0xFF;
        }

        public string GetIndexString(int combinedindex)
        {
            string objectstring = "";
            if (combinedindex != 0)
            {
                objectstring = "0x" + (combinedindex & 0xFFFF).ToString("X4");
                if ((combinedindex >> 16) > 0)
                {
                    objectstring += " sub " + (combinedindex >> 16).ToString();
                }
            }
            return objectstring;
        }
    }
}
