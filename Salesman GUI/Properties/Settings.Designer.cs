﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Salesman_GUI.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.7.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("COM1")]
        public string Comport {
            get {
                return ((string)(this["Comport"]));
            }
            set {
                this["Comport"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("115200")]
        public string Baudrate {
            get {
                return ((string)(this["Baudrate"]));
            }
            set {
                this["Baudrate"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public string Samples {
            get {
                return ((string)(this["Samples"]));
            }
            set {
                this["Samples"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public string AxisMax {
            get {
                return ((string)(this["AxisMax"]));
            }
            set {
                this["AxisMax"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1000")]
        public string AxisMin {
            get {
                return ((string)(this["AxisMin"]));
            }
            set {
                this["AxisMin"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public string SampleInterval {
            get {
                return ((string)(this["SampleInterval"]));
            }
            set {
                this["SampleInterval"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("3")]
        public int LiveObject1 {
            get {
                return ((int)(this["LiveObject1"]));
            }
            set {
                this["LiveObject1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int LiveObject2 {
            get {
                return ((int)(this["LiveObject2"]));
            }
            set {
                this["LiveObject2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int LiveObject3 {
            get {
                return ((int)(this["LiveObject3"]));
            }
            set {
                this["LiveObject3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Inputfile {
            get {
                return ((string)(this["Inputfile"]));
            }
            set {
                this["Inputfile"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AutoUpdateIO {
            get {
                return ((bool)(this["AutoUpdateIO"]));
            }
            set {
                this["AutoUpdateIO"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool OutputWindowVisible {
            get {
                return ((bool)(this["OutputWindowVisible"]));
            }
            set {
                this["OutputWindowVisible"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AddTimestamp {
            get {
                return ((bool)(this["AddTimestamp"]));
            }
            set {
                this["AddTimestamp"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool AddName {
            get {
                return ((bool)(this["AddName"]));
            }
            set {
                this["AddName"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32")]
        public int Node {
            get {
                return ((int)(this["Node"]));
            }
            set {
                this["Node"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int WriteObject {
            get {
                return ((int)(this["WriteObject"]));
            }
            set {
                this["WriteObject"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int Target1 {
            get {
                return ((int)(this["Target1"]));
            }
            set {
                this["Target1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public int Target2 {
            get {
                return ((int)(this["Target2"]));
            }
            set {
                this["Target2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool chkPrimaryPositionActual {
            get {
                return ((bool)(this["chkPrimaryPositionActual"]));
            }
            set {
                this["chkPrimaryPositionActual"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool chkPrimaryPositionDemand {
            get {
                return ((bool)(this["chkPrimaryPositionDemand"]));
            }
            set {
                this["chkPrimaryPositionDemand"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool chkPrimaryPositionError {
            get {
                return ((bool)(this["chkPrimaryPositionError"]));
            }
            set {
                this["chkPrimaryPositionError"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool chkSecondaryForceActual {
            get {
                return ((bool)(this["chkSecondaryForceActual"]));
            }
            set {
                this["chkSecondaryForceActual"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool chkSecondaryForceDemand {
            get {
                return ((bool)(this["chkSecondaryForceDemand"]));
            }
            set {
                this["chkSecondaryForceDemand"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool chkSecondaryPositionError {
            get {
                return ((bool)(this["chkSecondaryPositionError"]));
            }
            set {
                this["chkSecondaryPositionError"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1000")]
        public int PrimaryAxisMinTuning {
            get {
                return ((int)(this["PrimaryAxisMinTuning"]));
            }
            set {
                this["PrimaryAxisMinTuning"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public int PrimaryAxisMaxTuning {
            get {
                return ((int)(this["PrimaryAxisMaxTuning"]));
            }
            set {
                this["PrimaryAxisMaxTuning"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1000")]
        public int SecondaryAxisMinTuning {
            get {
                return ((int)(this["SecondaryAxisMinTuning"]));
            }
            set {
                this["SecondaryAxisMinTuning"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public int SecondaryAxisMaxTuning {
            get {
                return ((int)(this["SecondaryAxisMaxTuning"]));
            }
            set {
                this["SecondaryAxisMaxTuning"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool PrimaryAxisAutoscale {
            get {
                return ((bool)(this["PrimaryAxisAutoscale"]));
            }
            set {
                this["PrimaryAxisAutoscale"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool SecondaryAxisAutoscale {
            get {
                return ((bool)(this["SecondaryAxisAutoscale"]));
            }
            set {
                this["SecondaryAxisAutoscale"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2")]
        public string CapturedataInterval {
            get {
                return ((string)(this["CapturedataInterval"]));
            }
            set {
                this["CapturedataInterval"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("100")]
        public string CapturedataSamples {
            get {
                return ((string)(this["CapturedataSamples"]));
            }
            set {
                this["CapturedataSamples"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Logitem1primary {
            get {
                return ((bool)(this["Logitem1primary"]));
            }
            set {
                this["Logitem1primary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Logitem2primary {
            get {
                return ((bool)(this["Logitem2primary"]));
            }
            set {
                this["Logitem2primary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Logitem3primary {
            get {
                return ((bool)(this["Logitem3primary"]));
            }
            set {
                this["Logitem3primary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Logitem4primary {
            get {
                return ((bool)(this["Logitem4primary"]));
            }
            set {
                this["Logitem4primary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Logitem1secondary {
            get {
                return ((bool)(this["Logitem1secondary"]));
            }
            set {
                this["Logitem1secondary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Logitem2secondary {
            get {
                return ((bool)(this["Logitem2secondary"]));
            }
            set {
                this["Logitem2secondary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Logitem3secondary {
            get {
                return ((bool)(this["Logitem3secondary"]));
            }
            set {
                this["Logitem3secondary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Logitem4secondary {
            get {
                return ((bool)(this["Logitem4secondary"]));
            }
            set {
                this["Logitem4secondary"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1000")]
        public int PrimaryAxisMinRun {
            get {
                return ((int)(this["PrimaryAxisMinRun"]));
            }
            set {
                this["PrimaryAxisMinRun"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public int PrimaryAxisMaxRun {
            get {
                return ((int)(this["PrimaryAxisMaxRun"]));
            }
            set {
                this["PrimaryAxisMaxRun"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1000")]
        public int SecondaryAxisMinRun {
            get {
                return ((int)(this["SecondaryAxisMinRun"]));
            }
            set {
                this["SecondaryAxisMinRun"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1000")]
        public int SecondaryAxisMaxRun {
            get {
                return ((int)(this["SecondaryAxisMaxRun"]));
            }
            set {
                this["SecondaryAxisMaxRun"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32")]
        public int Node1 {
            get {
                return ((int)(this["Node1"]));
            }
            set {
                this["Node1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32")]
        public int Node2 {
            get {
                return ((int)(this["Node2"]));
            }
            set {
                this["Node2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32")]
        public int Node3 {
            get {
                return ((int)(this["Node3"]));
            }
            set {
                this["Node3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("32")]
        public int Node4 {
            get {
                return ((int)(this["Node4"]));
            }
            set {
                this["Node4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeMacro1 {
            get {
                return ((int)(this["NodeMacro1"]));
            }
            set {
                this["NodeMacro1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeMacro2 {
            get {
                return ((int)(this["NodeMacro2"]));
            }
            set {
                this["NodeMacro2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeMacro3 {
            get {
                return ((int)(this["NodeMacro3"]));
            }
            set {
                this["NodeMacro3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeMacro4 {
            get {
                return ((int)(this["NodeMacro4"]));
            }
            set {
                this["NodeMacro4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int MacroNumber1 {
            get {
                return ((int)(this["MacroNumber1"]));
            }
            set {
                this["MacroNumber1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int MacroNumber2 {
            get {
                return ((int)(this["MacroNumber2"]));
            }
            set {
                this["MacroNumber2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int MacroNumber3 {
            get {
                return ((int)(this["MacroNumber3"]));
            }
            set {
                this["MacroNumber3"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int MacroNumber4 {
            get {
                return ((int)(this["MacroNumber4"]));
            }
            set {
                this["MacroNumber4"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeVariableWriter {
            get {
                return ((int)(this["NodeVariableWriter"]));
            }
            set {
                this["NodeVariableWriter"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeDebugInfo {
            get {
                return ((int)(this["NodeDebugInfo"]));
            }
            set {
                this["NodeDebugInfo"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeIo1 {
            get {
                return ((int)(this["NodeIo1"]));
            }
            set {
                this["NodeIo1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("-1")]
        public int NodeIo2 {
            get {
                return ((int)(this["NodeIo2"]));
            }
            set {
                this["NodeIo2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool SetProtectedAccess {
            get {
                return ((bool)(this["SetProtectedAccess"]));
            }
            set {
                this["SetProtectedAccess"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool ApplyToAllNodes {
            get {
                return ((bool)(this["ApplyToAllNodes"]));
            }
            set {
                this["ApplyToAllNodes"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool AutoBackup {
            get {
                return ((bool)(this["AutoBackup"]));
            }
            set {
                this["AutoBackup"] = value;
            }
        }
    }
}
