﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class MotorSequence : Sequence
    {


        private string parms;   // The parameter string

        public string[] motorStates    =  {Globals.STR_ON,
                                           Globals.STR_OFF };
        private string motorState = "";


        private UserCombobox UserComboboxMotorstate;


        public MotorSequence()    // The constructor
        {
            name = "Motor";
            mnemonic = "Motor";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, motorStates);

            UserComboboxMotorstate = new UserCombobox(Globals.mainform.lblName1, "", Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxMotorstate = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

 
            if (parms == "")       // Then use the default set of parameters
            {
                motorState = Globals.STR_ON;
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_ON:
                        case Globals.STR_OFF:
                            motorState = parameterName;
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserComboboxMotorstate.Show(0);
            Globals.mainform.ComboBox1.SelectedItem = motorState;
            Globals.mainform.txtComment.Text = strComment;

        }

        public override void UserInputHasChanged(Object sender)
        {
            motorState = Globals.mainform.ComboBox1.Text;
            UpdateParametersFromGui();
//
// This is not necessary for a position move
//            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
//            {
//                ShowParametersInGui();
//            }
        }


        public override void UpdateParametersFromGui()
        {
            motorState = Globals.mainform.ComboBox1.Text;
            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {
            string temp = motorState;
            return temp;            
        }

        private void CreateCode()
        {
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);
            
            instructions.Clear();

            switch (motorState)
            {
                case Globals.STR_ON:
                    CreateCodeForSystemMacro(Bios.callnumber.MOTOR_ON, strtemp);
                    break;
                case Globals.STR_OFF:
                    CreateCodeForSystemMacro(Bios.callnumber.MOTOR_OFF, strtemp);
                    break;
                default:
                    break;
            }

        }
    }
}
