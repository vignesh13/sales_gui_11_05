﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public class CanopenObject
    {
        public int index;
        public int subindex;
        public DataType datatype;
        public string groupname;
        public string objectname;
        public AccessType accesstype;
        public Int64 defaultvalue;
        public int combinedindex;
        
        public CanopenObject(int iIndex, int iSubindex, DataType eDatatype, string strGroupname, string strObjectname, AccessType eAccesstype, Int64 i64Defaultvalue)
        {
            index = iIndex;
            subindex = iSubindex;
            combinedindex = index + (subindex << 16);
            datatype = eDatatype;
            groupname = strGroupname;
            objectname = strObjectname;
            accesstype = eAccesstype;
            defaultvalue = i64Defaultvalue;
        }

        public bool IsWritable()
        {
            switch (accesstype)
            {
                case AccessType.RW:
                case AccessType.WO:
                    return true;
                default:
                    return false;
            }
        }

        public bool IsReadable()
        {
            switch (accesstype)
            {
//                case AccessType.CONST:
                case AccessType.RO:
                case AccessType.RW:
                    return true;
                default:
                    return false;
            }
        }
    }

    public class ObjectDictionary
    {
        public ObjectDictionary()
        {
//            MessageBox.Show(dictionary.Count().ToString() + " Objects");
        }
        public CanopenObject[] dictionary = {                                                           // Conform Firmware 2.0B1
new CanopenObject(0x1000,0x00,DataType.UINT32,"","Device Type",AccessType.RO,0x20192),
new CanopenObject(0x1001,0x00,DataType.UINT8,"","Error Register",AccessType.RO,0x0),
new CanopenObject(0x1018,0x00,DataType.UINT8,"Identity Object","number of Entries",AccessType.CONST,0x4),
new CanopenObject(0x1018,0x01,DataType.UINT32,"Identity Object","Vendor-ID",AccessType.RO,0x29C),
new CanopenObject(0x1018,0x02,DataType.UINT32,"Identity Object","Product code",AccessType.RO,0x116),
new CanopenObject(0x1018,0x03,DataType.UINT32,"Identity Object","Revision number",AccessType.RO,0x0),
new CanopenObject(0x1018,0x04,DataType.UINT32,"Identity Object","Serial number",AccessType.RO,0x0),
new CanopenObject(0x6040,0x00,DataType.UINT16,"","Controlword",AccessType.RW,0x0),
new CanopenObject(0x6041,0x00,DataType.UINT16,"","Statusword",AccessType.RO,0x0),
new CanopenObject(0x6060,0x00,DataType.INT8,"","Modes of operation",AccessType.RW,1),
new CanopenObject(0x6061,0x00,DataType.INT8,"","Modes of operation display",AccessType.RO,0),
new CanopenObject(0x606B,0x00,DataType.INT32,"","Velocity demand value",AccessType.RO,0),
new CanopenObject(0x606C,0x00,DataType.INT32,"","Velocity actual value",AccessType.RO,0),
new CanopenObject(0x6071,0x00,DataType.INT16,"","Target torque",AccessType.RW,0),
new CanopenObject(0x6087,0x00,DataType.UINT32,"","Torque slope",AccessType.RW,0x2710),
new CanopenObject(0x6088,0x00,DataType.INT16,"","Torque profile type",AccessType.RW,0),
new CanopenObject(0x60FF,0x00,DataType.INT32,"","Target velocity",AccessType.RW,0),
new CanopenObject(0x2000,0x00,DataType.UINT8,"Uart configuration","Number of entries",AccessType.CONST,0x5),
new CanopenObject(0x2000,0x01,DataType.UINT8,"Uart configuration","Node ID",AccessType.RW,0x20),
new CanopenObject(0x2000,0x02,DataType.UINT8,"Uart configuration","Baudrate",AccessType.RW,0x0),
new CanopenObject(0x2000,0x03,DataType.UINT8,"Uart configuration","Daisy chain mode",AccessType.RW,0x0),
new CanopenObject(0x2000,0x04,DataType.UINT8,"Uart configuration","Base format",AccessType.RW,0x0),
new CanopenObject(0x2000,0x05,DataType.UINT8,"Uart configuration","Statusword mode",AccessType.RW,0x1),
new CanopenObject(0x2100,0x00,DataType.UINT8,"Phasing","Number of entries",AccessType.CONST,0x6),
new CanopenObject(0x2100,0x01,DataType.UINT8,"Phasing","Phasing type",AccessType.RW,0x0),
new CanopenObject(0x2100,0x02,DataType.UINT32,"Phasing","Phasing time",AccessType.RW,0x3E8),
new CanopenObject(0x2100,0x03,DataType.UINT32,"Phasing","Phasing current",AccessType.RW,0x1F4),
new CanopenObject(0x2100,0x04,DataType.UINT8,"Phasing","Phasing tolerance",AccessType.RW,0x5),
new CanopenObject(0x2100,0x05,DataType.UINT16,"Phasing","Phasing initial rotor position",AccessType.RW,0x0),
new CanopenObject(0x2100,0x06,DataType.UINT16,"Phasing","Phasing actual rotor position",AccessType.RO,0x0),
new CanopenObject(0x2102,0x00,DataType.UINT8,"Homing extra parameters","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2102,0x01,DataType.UINT16,"Homing extra parameters","Total homing timeout",AccessType.RW,0x1F40),
new CanopenObject(0x2102,0x02,DataType.UINT16,"Homing extra parameters","Torque limit",AccessType.RW,0xC8),
new CanopenObject(0x2301,0x00,DataType.UINT8,"","Motor pair poles",AccessType.RW,0x4),
new CanopenObject(0x2311,0x00,DataType.UINT8,"","Position encoder swap mode",AccessType.RW,0x1),
new CanopenObject(0x2312,0x00,DataType.UINT8,"","Position encoder type",AccessType.RW,0x2),
new CanopenObject(0x2400,0x00,DataType.UINT8,"","System polarity",AccessType.RW,0x0),
new CanopenObject(0x2430,0x00,DataType.UINT8,"","Command reference source",AccessType.RW,0x0),
new CanopenObject(0x2433,0x00,DataType.UINT8,"Step and Direction cmd source","Number of entries",AccessType.CONST,0x1),                 // Manual edit: replace & by the word and
new CanopenObject(0x2433,0x01,DataType.UINT32,"Step and Direction cmd source","Step value",AccessType.RW,0x1),                 // Manual edit: replace & by the word and
new CanopenObject(0x2434,0x00,DataType.UINT8,"Analog input cmd source","Number of entries",AccessType.CONST,0x3),
new CanopenObject(0x2434,0x01,DataType.UINT8,"Analog input cmd source","Analog input used",AccessType.RW,0x1),
new CanopenObject(0x2434,0x02,DataType.UINT16,"Analog input cmd source","Analog input offset",AccessType.RW,0x800),
new CanopenObject(0x2434,0x03,DataType.UINT8,"Analog input cmd source","Velocity deadband",AccessType.RW,0x1),
new CanopenObject(0x2500,0x00,DataType.UINT8,"Position control parameter set","Number of entries",AccessType.CONST,0x7),
new CanopenObject(0x2500,0x01,DataType.UINT32,"Position control parameter set","Proportional constant",AccessType.RW,0xFA0),
new CanopenObject(0x2500,0x02,DataType.UINT32,"Position control parameter set","Integral constant",AccessType.RW,0xA),
new CanopenObject(0x2500,0x03,DataType.UINT32,"Position control parameter set","Derivative constant",AccessType.RW,0x61A80),
new CanopenObject(0x2500,0x04,DataType.UINT32,"Position control parameter set","Reserved",AccessType.CONST,0x0),                        // Manual edit: AccessTuype CONST
new CanopenObject(0x2500,0x05,DataType.UINT32,"Position control parameter set","Velocity feedforward constant",AccessType.RW,0x0),
new CanopenObject(0x2500,0x06,DataType.UINT32,"Position control parameter set","Acceleration feedforward constant",AccessType.RW,0x0),
new CanopenObject(0x2500,0x07,DataType.UINT32,"Position control parameter set","Integral limit",AccessType.RW,0xA),
new CanopenObject(0x2502,0x00,DataType.UINT8,"Flux control parameter set","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2502,0x01,DataType.UINT16,"Flux control parameter set","Proportional constant",AccessType.RW,0xBB8),
new CanopenObject(0x2502,0x02,DataType.UINT16,"Flux control parameter set","Integral constant",AccessType.RW,0x12C),
new CanopenObject(0x2503,0x00,DataType.UINT8,"Torque control parameter set","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2503,0x01,DataType.UINT16,"Torque control parameter set","Proportional constant",AccessType.RW,0xBB8),
new CanopenObject(0x2503,0x02,DataType.UINT16,"Torque control parameter set","Integral constant",AccessType.RW,0x12C),
new CanopenObject(0x2504,0x00,DataType.UINT8,"Position control monitor","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2504,0x01,DataType.INT32,"Position control monitor","Integral part",AccessType.RO,0),
new CanopenObject(0x2504,0x02,DataType.INT32,"Position control monitor","Integral contribution",AccessType.RO,0),
new CanopenObject(0x2508,0x00,DataType.UINT8,"Spring torque compensation","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2508,0x01,DataType.INT16,"Spring torque compensation","Spring torque offset",AccessType.RW,0),
new CanopenObject(0x2508,0x02,DataType.UINT32,"Spring torque compensation","Spring offset position",AccessType.RW,0x0),
new CanopenObject(0x2601,0x00,DataType.INT16,"","Current A",AccessType.RO,0),
new CanopenObject(0x2602,0x00,DataType.INT16,"","Current B",AccessType.RO,0),
new CanopenObject(0x2603,0x00,DataType.INT16,"","Current C",AccessType.RO,0),
new CanopenObject(0x2604,0x00,DataType.INT16,"","Current Direct",AccessType.RO,0),
new CanopenObject(0x2605,0x00,DataType.INT16,"","Current Quadrature",AccessType.RO,0),
new CanopenObject(0x2702,0x00,DataType.UINT8,"I2T parameters","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2702,0x01,DataType.UINT16,"I2T parameters","Peak current",AccessType.RW,0x3E8),
new CanopenObject(0x2702,0x02,DataType.UINT16,"I2T parameters","Peak time",AccessType.RW,0x3E8),
new CanopenObject(0x2A01,0x00,DataType.UINT8,"Dedicated digital inputs","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2A01,0x01,DataType.UINT16,"Dedicated digital inputs","Polarity",AccessType.RW,0x0),
new CanopenObject(0x2A01,0x02,DataType.UINT16,"Dedicated digital inputs","Value",AccessType.RO,0x0),
new CanopenObject(0x2A02,0x00,DataType.UINT8,"General digital input / output","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2A02,0x01,DataType.UINT16,"General digital input / output","Polarity",AccessType.RW,0x0),
new CanopenObject(0x2A02,0x02,DataType.UINT16,"General digital input / output","Value",AccessType.RW,0x0),
new CanopenObject(0x2A03,0x00,DataType.UINT8,"Analog inputs","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2A03,0x01,DataType.UINT32,"Analog inputs","Analog input 1 value",AccessType.RO,0x0),
new CanopenObject(0x2A03,0x02,DataType.UINT32,"Analog inputs","Analog input 2 value",AccessType.RO,0x0),
new CanopenObject(0x2A04,0x00,DataType.UINT8,"Analog output","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2A04,0x01,DataType.UINT32,"Analog output","Analog output 1 value",AccessType.RW,0x0),
new CanopenObject(0x2A04,0x02,DataType.UINT32,"Analog output","Analog output 2 value",AccessType.RW,0x0),
new CanopenObject(0x2A08,0x00,DataType.UINT8,"Analog output automatic","Number of entries",AccessType.CONST,0x4),
new CanopenObject(0x2A08,0x01,DataType.UINT8,"Analog output automatic","Mode enabled",AccessType.RW,0x0),
new CanopenObject(0x2A08,0x02,DataType.INT32,"Analog output automatic","Source register",AccessType.RW,24692),
new CanopenObject(0x2A08,0x03,DataType.UINT8,"Analog output automatic","Destination output",AccessType.RW,0x1),
new CanopenObject(0x2A08,0x04,DataType.UINT32,"Analog output automatic","Max represented value",AccessType.RW,0x7FFFFFFF),
new CanopenObject(0x2C00,0x00,DataType.UINT8,"General purpouse registers","Number of entries",AccessType.CONST,0x64),
new CanopenObject(0x2C00,0x01,DataType.INT32,"General purpouse registers","ACCUM",AccessType.RW,0),
new CanopenObject(0x2C00,0x02,DataType.INT32,"General purpouse registers","W2",AccessType.RW,0),
new CanopenObject(0x2C00,0x03,DataType.INT32,"General purpouse registers","W3",AccessType.RW,0),
new CanopenObject(0x2C00,0x04,DataType.INT32,"General purpouse registers","W4",AccessType.RW,0),
new CanopenObject(0x2C00,0x05,DataType.INT32,"General purpouse registers","W5",AccessType.RW,0),
new CanopenObject(0x2C00,0x06,DataType.INT32,"General purpouse registers","W6",AccessType.RW,0),
new CanopenObject(0x2C00,0x07,DataType.INT32,"General purpouse registers","W7",AccessType.RW,0),
new CanopenObject(0x2C00,0x08,DataType.INT32,"General purpouse registers","W8",AccessType.RW,0),
new CanopenObject(0x2C00,0x09,DataType.INT32,"General purpouse registers","W9",AccessType.RW,0),
new CanopenObject(0x2C00,0x0A,DataType.INT32,"General purpouse registers","W10",AccessType.RW,0),
new CanopenObject(0x2C00,0x0B,DataType.INT32,"General purpouse registers","W11",AccessType.RW,0),
new CanopenObject(0x2C00,0x0C,DataType.INT32,"General purpouse registers","W12",AccessType.RW,0),
new CanopenObject(0x2C00,0x0D,DataType.INT32,"General purpouse registers","W13",AccessType.RW,0),
new CanopenObject(0x2C00,0x0E,DataType.INT32,"General purpouse registers","W14",AccessType.RW,0),
new CanopenObject(0x2C00,0x0F,DataType.INT32,"General purpouse registers","W15",AccessType.RW,0),
new CanopenObject(0x2C00,0x10,DataType.INT32,"General purpouse registers","W16",AccessType.RW,0),
new CanopenObject(0x2C00,0x11,DataType.INT32,"General purpouse registers","W17",AccessType.RW,0),
new CanopenObject(0x2C00,0x12,DataType.INT32,"General purpouse registers","W18",AccessType.RW,0),
new CanopenObject(0x2C00,0x13,DataType.INT32,"General purpouse registers","W19",AccessType.RW,0),
new CanopenObject(0x2C00,0x14,DataType.INT32,"General purpouse registers","W20",AccessType.RW,0),
new CanopenObject(0x2C00,0x15,DataType.INT32,"General purpouse registers","W21",AccessType.RW,0),
new CanopenObject(0x2C00,0x16,DataType.INT32,"General purpouse registers","W22",AccessType.RW,0),
new CanopenObject(0x2C00,0x17,DataType.INT32,"General purpouse registers","W23",AccessType.RW,0),
new CanopenObject(0x2C00,0x18,DataType.INT32,"General purpouse registers","W24",AccessType.RW,0),
new CanopenObject(0x2C00,0x19,DataType.INT32,"General purpouse registers","W25",AccessType.RW,0),
new CanopenObject(0x2C00,0x1A,DataType.INT32,"General purpouse registers","W26",AccessType.RW,0),
new CanopenObject(0x2C00,0x1B,DataType.INT32,"General purpouse registers","W27",AccessType.RW,0),
new CanopenObject(0x2C00,0x1C,DataType.INT32,"General purpouse registers","W28",AccessType.RW,0),
new CanopenObject(0x2C00,0x1D,DataType.INT32,"General purpouse registers","W29",AccessType.RW,0),
new CanopenObject(0x2C00,0x1E,DataType.INT32,"General purpouse registers","W30",AccessType.RW,0),
new CanopenObject(0x2C00,0x1F,DataType.INT32,"General purpouse registers","W31",AccessType.RW,0),
new CanopenObject(0x2C00,0x20,DataType.INT32,"General purpouse registers","W32",AccessType.RW,0),
new CanopenObject(0x2C00,0x21,DataType.INT32,"General purpouse registers","W33",AccessType.RW,0),
new CanopenObject(0x2C00,0x22,DataType.INT32,"General purpouse registers","W34",AccessType.RW,0),
new CanopenObject(0x2C00,0x23,DataType.INT32,"General purpouse registers","W35",AccessType.RW,0),
new CanopenObject(0x2C00,0x24,DataType.INT32,"General purpouse registers","W36",AccessType.RW,0),
new CanopenObject(0x2C00,0x25,DataType.INT32,"General purpouse registers","W37",AccessType.RW,0),
new CanopenObject(0x2C00,0x26,DataType.INT32,"General purpouse registers","W38",AccessType.RW,0),
new CanopenObject(0x2C00,0x27,DataType.INT32,"General purpouse registers","W39",AccessType.RW,0),
new CanopenObject(0x2C00,0x28,DataType.INT32,"General purpouse registers","W40",AccessType.RW,0),
new CanopenObject(0x2C00,0x29,DataType.INT32,"General purpouse registers","W41",AccessType.RW,0),
new CanopenObject(0x2C00,0x2A,DataType.INT32,"General purpouse registers","W42",AccessType.RW,0),
new CanopenObject(0x2C00,0x2B,DataType.INT32,"General purpouse registers","W43",AccessType.RW,0),
new CanopenObject(0x2C00,0x2C,DataType.INT32,"General purpouse registers","W44",AccessType.RW,0),
new CanopenObject(0x2C00,0x2D,DataType.INT32,"General purpouse registers","W45",AccessType.RW,0),
new CanopenObject(0x2C00,0x2E,DataType.INT32,"General purpouse registers","W46",AccessType.RW,0),
new CanopenObject(0x2C00,0x2F,DataType.INT32,"General purpouse registers","W47",AccessType.RW,0),
new CanopenObject(0x2C00,0x30,DataType.INT32,"General purpouse registers","W48",AccessType.RW,0),
new CanopenObject(0x2C00,0x31,DataType.INT32,"General purpouse registers","W49",AccessType.RW,0),
new CanopenObject(0x2C00,0x32,DataType.INT32,"General purpouse registers","W50",AccessType.RW,0),
new CanopenObject(0x2C00,0x33,DataType.INT32,"General purpouse registers","W51",AccessType.RW,0),
new CanopenObject(0x2C00,0x34,DataType.INT32,"General purpouse registers","W52",AccessType.RW,0),
new CanopenObject(0x2C00,0x35,DataType.INT32,"General purpouse registers","W53",AccessType.RW,0),
new CanopenObject(0x2C00,0x36,DataType.INT32,"General purpouse registers","W54",AccessType.RW,0),
new CanopenObject(0x2C00,0x37,DataType.INT32,"General purpouse registers","W55",AccessType.RW,0),
new CanopenObject(0x2C00,0x38,DataType.INT32,"General purpouse registers","W56",AccessType.RW,0),
new CanopenObject(0x2C00,0x39,DataType.INT32,"General purpouse registers","W57",AccessType.RW,0),
new CanopenObject(0x2C00,0x3A,DataType.INT32,"General purpouse registers","W58",AccessType.RW,0),
new CanopenObject(0x2C00,0x3B,DataType.INT32,"General purpouse registers","W59",AccessType.RW,0),
new CanopenObject(0x2C00,0x3C,DataType.INT32,"General purpouse registers","W60",AccessType.RW,0),
new CanopenObject(0x2C00,0x3D,DataType.INT32,"General purpouse registers","W61",AccessType.RW,0),
new CanopenObject(0x2C00,0x3E,DataType.INT32,"General purpouse registers","W62",AccessType.RW,0),
new CanopenObject(0x2C00,0x3F,DataType.INT32,"General purpouse registers","W63",AccessType.RW,0),
new CanopenObject(0x2C00,0x40,DataType.INT32,"General purpouse registers","W64",AccessType.RW,0),
new CanopenObject(0x2C00,0x41,DataType.INT32,"General purpouse registers","W65",AccessType.RW,0),
new CanopenObject(0x2C00,0x42,DataType.INT32,"General purpouse registers","W66",AccessType.RW,0),
new CanopenObject(0x2C00,0x43,DataType.INT32,"General purpouse registers","W67",AccessType.RW,0),
new CanopenObject(0x2C00,0x44,DataType.INT32,"General purpouse registers","W68",AccessType.RW,0),
new CanopenObject(0x2C00,0x45,DataType.INT32,"General purpouse registers","W69",AccessType.RW,0),
new CanopenObject(0x2C00,0x46,DataType.INT32,"General purpouse registers","W70",AccessType.RW,0),
new CanopenObject(0x2C00,0x47,DataType.INT32,"General purpouse registers","W71",AccessType.RW,0),
new CanopenObject(0x2C00,0x48,DataType.INT32,"General purpouse registers","W72",AccessType.RW,0),
new CanopenObject(0x2C00,0x49,DataType.INT32,"General purpouse registers","W73",AccessType.RW,0),
new CanopenObject(0x2C00,0x4A,DataType.INT32,"General purpouse registers","W74",AccessType.RW,0),
new CanopenObject(0x2C00,0x4B,DataType.INT32,"General purpouse registers","W75",AccessType.RW,0),
new CanopenObject(0x2C00,0x4C,DataType.INT32,"General purpouse registers","W76",AccessType.RW,0),
new CanopenObject(0x2C00,0x4D,DataType.INT32,"General purpouse registers","W77",AccessType.RW,0),
new CanopenObject(0x2C00,0x4E,DataType.INT32,"General purpouse registers","W78",AccessType.RW,0),
new CanopenObject(0x2C00,0x4F,DataType.INT32,"General purpouse registers","W79",AccessType.RW,0),
new CanopenObject(0x2C00,0x50,DataType.INT32,"General purpouse registers","W80",AccessType.RW,0),
new CanopenObject(0x2C00,0x51,DataType.INT32,"General purpouse registers","W81",AccessType.RW,0),
new CanopenObject(0x2C00,0x52,DataType.INT32,"General purpouse registers","W82",AccessType.RW,0),
new CanopenObject(0x2C00,0x53,DataType.INT32,"General purpouse registers","W83",AccessType.RW,0),
new CanopenObject(0x2C00,0x54,DataType.INT32,"General purpouse registers","W84",AccessType.RW,0),
new CanopenObject(0x2C00,0x55,DataType.INT32,"General purpouse registers","W85",AccessType.RW,0),
new CanopenObject(0x2C00,0x56,DataType.INT32,"General purpouse registers","W86",AccessType.RW,0),
new CanopenObject(0x2C00,0x57,DataType.INT32,"General purpouse registers","W87",AccessType.RW,0),
new CanopenObject(0x2C00,0x58,DataType.INT32,"General purpouse registers","W88",AccessType.RW,0),
new CanopenObject(0x2C00,0x59,DataType.INT32,"General purpouse registers","W89",AccessType.RW,0),
new CanopenObject(0x2C00,0x5A,DataType.INT32,"General purpouse registers","W90",AccessType.RW,0),
new CanopenObject(0x2C00,0x5B,DataType.INT32,"General purpouse registers","W91",AccessType.RW,0),
new CanopenObject(0x2C00,0x5C,DataType.INT32,"General purpouse registers","W92",AccessType.RW,0),
new CanopenObject(0x2C00,0x5D,DataType.INT32,"General purpouse registers","W93",AccessType.RW,0),
new CanopenObject(0x2C00,0x5E,DataType.INT32,"General purpouse registers","W94",AccessType.RW,0),
new CanopenObject(0x2C00,0x5F,DataType.INT32,"General purpouse registers","W95",AccessType.RW,0),
new CanopenObject(0x2C00,0x60,DataType.INT32,"General purpouse registers","W96",AccessType.RW,0),
new CanopenObject(0x2C00,0x61,DataType.INT32,"General purpouse registers","W97",AccessType.RW,0),
new CanopenObject(0x2C00,0x62,DataType.INT32,"General purpouse registers","W98",AccessType.RW,0),
new CanopenObject(0x2C00,0x63,DataType.INT32,"General purpouse registers","W99",AccessType.RW,0),
new CanopenObject(0x2C00,0x64,DataType.INT32,"General purpouse registers","W100",AccessType.RW,0),
new CanopenObject(0x2C01,0x00,DataType.UINT8,"Register commands","Number of entries",AccessType.CONST,0x17),
new CanopenObject(0x2C01,0x01,DataType.INT32,"Register commands","Add constant to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x02,DataType.INT32,"Register commands","Accumulator divide constant",AccessType.WO,0),
new CanopenObject(0x2C01,0x03,DataType.INT32,"Register commands","Xor constant to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x04,DataType.INT32,"Register commands","Accumulator multiply constant",AccessType.WO,0),
new CanopenObject(0x2C01,0x05,DataType.INT32,"Register commands","And constant to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x06,DataType.INT32,"Register commands","Or constant to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x07,DataType.INT32,"Register commands","Subtract constant from accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x08,DataType.INT32,"Register commands","Shift left accumulator by constant",AccessType.WO,0),
new CanopenObject(0x2C01,0x09,DataType.INT32,"Register commands","Shift right accumulator by constant",AccessType.WO,0),
new CanopenObject(0x2C01,0x0A,DataType.INT32,"Register commands","Add register to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x0B,DataType.INT32,"Register commands","Accumulator divide register",AccessType.WO,0),
new CanopenObject(0x2C01,0x0C,DataType.INT32,"Register commands","Xor register to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x0D,DataType.INT32,"Register commands","Accumulator multiply register",AccessType.WO,0),
new CanopenObject(0x2C01,0x0E,DataType.INT32,"Register commands","And register to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x0F,DataType.INT32,"Register commands","Or register to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x10,DataType.INT32,"Register commands","Subtract register from accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x11,DataType.INT32,"Register commands","Shift left accumulator by register",AccessType.WO,0),
new CanopenObject(0x2C01,0x12,DataType.INT32,"Register commands","Shift right accumulator by register",AccessType.WO,0),
new CanopenObject(0x2C01,0x13,DataType.INT32,"Register commands","Absolute accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x14,DataType.INT32,"Register commands","Accumulator complement",AccessType.WO,0),
new CanopenObject(0x2C01,0x15,DataType.INT32,"Register commands","Write accumulator to register",AccessType.WO,0),
new CanopenObject(0x2C01,0x16,DataType.INT32,"Register commands","Write register32 to accumulator",AccessType.WO,0),
new CanopenObject(0x2C01,0x17,DataType.INT32,"Register commands","Write register16 to accumulator",AccessType.WO,0),
new CanopenObject(0x2C02,0x00,DataType.UINT8,"Sequence commands","Number of entries",AccessType.CONST,0x16),
new CanopenObject(0x2C02,0x01,DataType.INT32,"Sequence commands","Do if i/o is \"Off\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x02,DataType.INT32,"Sequence commands","Do if i/o is \"On\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x03,DataType.INT32,"Sequence commands","End program",AccessType.WO,0),
new CanopenObject(0x2C02,0x04,DataType.INT32,"Sequence commands","If accumulator is below value",AccessType.WO,0),
new CanopenObject(0x2C02,0x05,DataType.INT32,"Sequence commands","If accumulator is higher value",AccessType.WO,0),
new CanopenObject(0x2C02,0x06,DataType.INT32,"Sequence commands","If accumulator is equal value",AccessType.WO,0),
new CanopenObject(0x2C02,0x07,DataType.INT32,"Sequence commands","If accumulator is unequal value",AccessType.WO,0),
new CanopenObject(0x2C02,0x08,DataType.INT32,"Sequence commands","If i/o is \"Off\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x09,DataType.INT32,"Sequence commands","If i/o is \"On\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x0A,DataType.INT32,"Sequence commands","If bit of accumulator is set",AccessType.WO,0),
new CanopenObject(0x2C02,0x0B,DataType.INT32,"Sequence commands","If bit of accumulator is clear",AccessType.WO,0),
new CanopenObject(0x2C02,0x0C,DataType.INT32,"Sequence commands","Repeat",AccessType.WO,0),
new CanopenObject(0x2C02,0x0D,DataType.INT32,"Sequence commands","Wait (miliseconds)",AccessType.WO,0),
new CanopenObject(0x2C02,0x0E,DataType.INT32,"Sequence commands","Wait for Index",AccessType.WO,0),
new CanopenObject(0x2C02,0x0F,DataType.INT32,"Sequence commands","Wait for i/o to be \"Off\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x10,DataType.INT32,"Sequence commands","Wait for i/o to be \"On\"",AccessType.WO,0),                   // Manual edit: preceed " with \                   
new CanopenObject(0x2C02,0x11,DataType.INT32,"Sequence commands","If analog is below",AccessType.WO,0),
new CanopenObject(0x2C02,0x12,DataType.INT32,"Sequence commands","If analog is higher",AccessType.WO,0),
new CanopenObject(0x2C02,0x13,DataType.INT32,"Sequence commands","If accumulator is below register",AccessType.WO,0),
new CanopenObject(0x2C02,0x14,DataType.INT32,"Sequence commands","If accumulator is higher register",AccessType.WO,0),
new CanopenObject(0x2C02,0x15,DataType.INT32,"Sequence commands","If accumulator is equal register",AccessType.WO,0),
new CanopenObject(0x2C02,0x16,DataType.INT32,"Sequence commands","If accumulator is unequal register",AccessType.WO,0),
new CanopenObject(0x2C03,0x00,DataType.UINT8,"Learned position","Number of entries",AccessType.CONST,0x3),
new CanopenObject(0x2C03,0x01,DataType.UINT8,"Learned position","Learn current position",AccessType.WO,0x0),
new CanopenObject(0x2C03,0x02,DataType.UINT8,"Learned position","Learn target position",AccessType.WO,0x0),
new CanopenObject(0x2C03,0x03,DataType.UINT8,"Learned position","Move index table position",AccessType.WO,0x0),
new CanopenObject(0x2C04,0x00,DataType.UINT8,"Macro commands","Number of entries",AccessType.CONST,0x7),
new CanopenObject(0x2C04,0x01,DataType.INT32,"Macro commands","Macro call",AccessType.WO,0),
new CanopenObject(0x2C04,0x02,DataType.INT32,"Macro commands","Return from macro call",AccessType.WO,0),
new CanopenObject(0x2C04,0x03,DataType.INT32,"Macro commands","Macro jump",AccessType.WO,0),
new CanopenObject(0x2C04,0x04,DataType.INT32,"Macro commands","Reset macros",AccessType.WO,0),
new CanopenObject(0x2C04,0x05,DataType.INT32,"Macro commands","Jump absolute",AccessType.WO,0),
new CanopenObject(0x2C04,0x06,DataType.INT32,"Macro commands","Jump relative",AccessType.WO,0),
new CanopenObject(0x2C04,0x07,DataType.INT32,"Macro commands","Unpush macro",AccessType.WO,0),
new CanopenObject(0x2C05,0x00,DataType.UINT8,"Macro access","Number of entries",AccessType.CONST,0x4),
new CanopenObject(0x2C05,0x01,DataType.UINT8,"Macro access","Macro number",AccessType.RW,0x0),
new CanopenObject(0x2C05,0x02,DataType.UINT8,"Macro access","Macro command",AccessType.RW,0x0),
new CanopenObject(0x2C05,0x03,DataType.UINT64,"Macro access","Command",AccessType.RW,0x0),
new CanopenObject(0x2C05,0x04,DataType.UINT8,"Macro access","Protected access",AccessType.RW,0x0),
new CanopenObject(0x2C06,0x00,DataType.UINT8,"Timer access","Number of entries",AccessType.CONST,0x4),
new CanopenObject(0x2C06,0x01,DataType.UINT32,"Timer access","Timer 1 (count up) value",AccessType.RW,0x0),
new CanopenObject(0x2C06,0x02,DataType.UINT32,"Timer access","Timer 2 (count up) value",AccessType.RW,0x0),
new CanopenObject(0x2C06,0x03,DataType.UINT32,"Timer access","Timer 3 (count down) value",AccessType.RW,0x0),
new CanopenObject(0x2C06,0x04,DataType.UINT32,"Timer access","Timer 4 (count down) value",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x00,DataType.UINT8,"Interrupt vector","Number of entries",AccessType.CONST,0xA),
new CanopenObject(0x2C07,0x01,DataType.UINT8,"Interrupt vector","Interrupt 1 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x02,DataType.UINT8,"Interrupt vector","Interrupt 2 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x03,DataType.UINT8,"Interrupt vector","Interrupt 3 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x04,DataType.UINT8,"Interrupt vector","Interrupt 4 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x05,DataType.UINT8,"Interrupt vector","Interrupt 5 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x06,DataType.UINT8,"Interrupt vector","Interrupt 6 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x07,DataType.UINT8,"Interrupt vector","Interrupt 7 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x08,DataType.UINT8,"Interrupt vector","Interrupt 8 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x09,DataType.UINT8,"Interrupt vector","Interrupt 9 vector",AccessType.RW,0x0),
new CanopenObject(0x2C07,0x0A,DataType.UINT8,"Interrupt vector","Interrupt 10 vector",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x00,DataType.UINT8,"Interrupt enable","Number of entries",AccessType.CONST,0xA),
new CanopenObject(0x2C08,0x01,DataType.UINT8,"Interrupt enable","Interrupt 1 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x02,DataType.UINT8,"Interrupt enable","Interrupt 2 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x03,DataType.UINT8,"Interrupt enable","Interrupt 3 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x04,DataType.UINT8,"Interrupt enable","Interrupt 4 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x05,DataType.UINT8,"Interrupt enable","Interrupt 5 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x06,DataType.UINT8,"Interrupt enable","Interrupt 6 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x07,DataType.UINT8,"Interrupt enable","Interrupt 7 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x08,DataType.UINT8,"Interrupt enable","Interrupt 8 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x09,DataType.UINT8,"Interrupt enable","Interrupt 9 enabled",AccessType.RW,0x0),
new CanopenObject(0x2C08,0x0A,DataType.UINT8,"Interrupt enable","Interrupt A enabled",AccessType.RW,0x0),
new CanopenObject(0x2C09,0x00,DataType.UINT8,"Pointer access","Number of entries",AccessType.CONST,0x3),
new CanopenObject(0x2C09,0x01,DataType.INT32,"Pointer access","Pointer to register",AccessType.RW,0),
new CanopenObject(0x2C09,0x02,DataType.INT32,"Pointer access","Content of register",AccessType.RW,0),
new CanopenObject(0x2C09,0x03,DataType.INT32,"Pointer access","Write content to register",AccessType.RW,0),
new CanopenObject(0x2C0A,0x00,DataType.UINT8,"Macro debug","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2C0A,0x01,DataType.UINT8,"Macro debug","Actual macro number",AccessType.RO,0x0),
new CanopenObject(0x2C0A,0x02,DataType.UINT8,"Macro debug","Actual command number",AccessType.RO,0x0),
new CanopenObject(0x2C50,0x00,DataType.UINT8,"Monitor config","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x2C50,0x01,DataType.UINT16,"Monitor config","Sampling rate",AccessType.RW,0x0),
new CanopenObject(0x2C50,0x02,DataType.UINT8,"Monitor config","Enable mode",AccessType.RW,0x0),
new CanopenObject(0x2C51,0x00,DataType.UINT8,"Monitor result","Number of entries",AccessType.CONST,0x7),
new CanopenObject(0x2C51,0x01,DataType.UINT16,"Monitor result","Max entry number",AccessType.RO,0xFA),
new CanopenObject(0x2C51,0x02,DataType.UINT8,"Monitor result","Filled entry values",AccessType.RO,0x0),
new CanopenObject(0x2C51,0x03,DataType.UINT16,"Monitor result","Entry number",AccessType.RW,0x0),
new CanopenObject(0x2C51,0x04,DataType.INT32,"Monitor result","Actual entry table 1",AccessType.RO,0),
new CanopenObject(0x2C51,0x05,DataType.INT32,"Monitor result","Actual entry table 2",AccessType.RO,0),
new CanopenObject(0x2C51,0x06,DataType.INT32,"Monitor result","Actual entry table 3",AccessType.RO,0),
new CanopenObject(0x2C51,0x07,DataType.INT32,"Monitor result","Actual entry table 4",AccessType.RO,0),
new CanopenObject(0x2F01,0x00,DataType.INT16,"","Target duty",AccessType.RW,0),
new CanopenObject(0x2F02,0x00,DataType.UINT16,"","Dedicated digital value",AccessType.RO,0x0),
new CanopenObject(0x2FF0,0x00,DataType.UINT8,"Hardware configuration","Number of entries",AccessType.CONST,0xF),
new CanopenObject(0x2FF0,0x01,DataType.UINT16,"Hardware configuration","Current sensing resistor value",AccessType.RW,0x64),
new CanopenObject(0x2FF0,0x02,DataType.UINT16,"Hardware configuration","Preamplifier gain",AccessType.RW,0xBD6),
new CanopenObject(0x2FF0,0x03,DataType.UINT8,"Hardware configuration","VGA available",AccessType.RW,0x0),
new CanopenObject(0x2FF0,0x04,DataType.UINT8,"Hardware configuration","Temperature sensor available",AccessType.RW,0x0),
new CanopenObject(0x2FF0,0x05,DataType.INT32,"Hardware configuration","Max absolute temperature",AccessType.RW,72000),
new CanopenObject(0x2FF0,0x06,DataType.INT32,"Hardware configuration","Min absolute temperature",AccessType.RW,-20000),
new CanopenObject(0x2FF0,0x07,DataType.UINT8,"Hardware configuration","Vbus sensor available",AccessType.RW,0x0),
new CanopenObject(0x2FF0,0x08,DataType.UINT32,"Hardware configuration","Max absolute voltage",AccessType.RW,0x11170),
new CanopenObject(0x2FF0,0x09,DataType.UINT32,"Hardware configuration","Min absolute voltage",AccessType.RW,0x2710),
new CanopenObject(0x2FF0,0x0A,DataType.UINT32,"Hardware configuration","Nominal current (peak value)",AccessType.RW,0x7D0),
new CanopenObject(0x2FF0,0x0B,DataType.UINT32,"Hardware configuration","Peak current (peak value)",AccessType.RW,0xFA0),
new CanopenObject(0x2FF0,0x0C,DataType.UINT32,"Hardware configuration","Maximum peak time",AccessType.RW,0x2710),
new CanopenObject(0x2FF0,0x0D,DataType.UINT32,"Hardware configuration","Maximum current (peak value)",AccessType.RW,0x125C),
new CanopenObject(0x2FF0,0x0E,DataType.UINT8,"Hardware configuration","Stepper in 3phases available",AccessType.RW,0x0),
new CanopenObject(0x2FF0,0x0F,DataType.UINT8,"Hardware configuration","NVM available",AccessType.RW,0x0),
new CanopenObject(0x2FF1,0x00,DataType.INT32,"","Hardware configuration pass",AccessType.WO,0),
new CanopenObject(0x2FFF,0x00,DataType.UINT32,"","Reset device",AccessType.RW,0x0),
new CanopenObject(0x1003,0x00,DataType.UINT8,"Pre-defined Error Field","number of errors",AccessType.RW,0x0),
new CanopenObject(0x1003,0x01,DataType.UINT32,"Pre-defined Error Field","standard error field",AccessType.RO,0x0),
new CanopenObject(0x1003,0x02,DataType.UINT32,"Pre-defined Error Field","standard error field",AccessType.RO,0x0),
new CanopenObject(0x1003,0x03,DataType.UINT32,"Pre-defined Error Field","standard error field",AccessType.RO,0x0),
new CanopenObject(0x1003,0x04,DataType.UINT32,"Pre-defined Error Field","standard error field",AccessType.RO,0x0),
new CanopenObject(0x1005,0x00,DataType.UINT32,"","COB-ID SYNC",AccessType.RW,0x80),
new CanopenObject(0x1006,0x00,DataType.UINT32,"","Communication Cycle Period",AccessType.RW,0x0),
new CanopenObject(0x1007,0x00,DataType.UINT32,"","Sync Windows Length",AccessType.RW,0x0),
new CanopenObject(0x1008,0x00,DataType.VISIBLE_STRING,"","Device name",AccessType.CONST,0),                                  // Manual edit: replace string by 0
new CanopenObject(0x1009,0x00,DataType.VISIBLE_STRING,"","Hardware version",AccessType.CONST,0),                                  // Manual edit: replace string by 0
new CanopenObject(0x100A,0x00,DataType.VISIBLE_STRING,"","Software version",AccessType.CONST,0),                                  // Manual edit: replace string by 0
new CanopenObject(0x100C,0x00,DataType.UINT16,"","Guard Time",AccessType.RW,0x0),
new CanopenObject(0x100D,0x00,DataType.UINT8,"","Life Time Factor",AccessType.RW,0x0),
new CanopenObject(0x1010,0x00,DataType.UINT8,"Store Parameters","largest supported Sub-Index",AccessType.CONST,0x3),
new CanopenObject(0x1010,0x01,DataType.UINT32,"Store Parameters","save all parameters",AccessType.RW,0x0),
new CanopenObject(0x1010,0x02,DataType.UINT32,"Store Parameters","save communication parameters",AccessType.RW,0x0),
new CanopenObject(0x1010,0x03,DataType.UINT32,"Store Parameters","save application parameters",AccessType.RW,0x0),
new CanopenObject(0x1011,0x00,DataType.UINT8,"Restore default parameters","largest supported Sub-Index",AccessType.CONST,0x3),
new CanopenObject(0x1011,0x01,DataType.UINT32,"Restore default parameters","restore all default para.",AccessType.RW,0x0),
new CanopenObject(0x1011,0x02,DataType.UINT32,"Restore default parameters","restore comm. default para.",AccessType.RW,0x0),
new CanopenObject(0x1011,0x03,DataType.UINT32,"Restore default parameters","restore app. default para.",AccessType.RW,0x0),
new CanopenObject(0x1014,0x00,DataType.UINT32,"","COB-ID Emergency message",AccessType.RW,0xA0),
new CanopenObject(0x1017,0x00,DataType.UINT16,"","Producer Heartbeat Time",AccessType.RW,0x0),
new CanopenObject(0x1200,0x00,DataType.UINT8,"SSDO","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x1200,0x01,DataType.UINT32,"SSDO","COB-ID Client->Server",AccessType.RO,0x620),
new CanopenObject(0x1200,0x02,DataType.UINT32,"SSDO","COB-ID Server->Client",AccessType.RO,0x5A0),
new CanopenObject(0x1400,0x00,DataType.UINT8,"RPDO 1","largest subindex supported",AccessType.CONST,0x3),
new CanopenObject(0x1400,0x01,DataType.UINT32,"RPDO 1","COB-Id used",AccessType.RW,0x80000220),
new CanopenObject(0x1400,0x02,DataType.UINT8,"RPDO 1","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1400,0x03,DataType.UINT16,"RPDO 1","inhibit time",AccessType.RW,0x0),
new CanopenObject(0x1401,0x00,DataType.UINT8,"RPDO 2","largest subindex supported",AccessType.CONST,0x3),
new CanopenObject(0x1401,0x01,DataType.UINT32,"RPDO 2","COB-ID used",AccessType.RW,0x80000320),
new CanopenObject(0x1401,0x02,DataType.UINT8,"RPDO 2","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1401,0x03,DataType.UINT16,"RPDO 2","Inhibit Time",AccessType.RW,0x0),
new CanopenObject(0x1402,0x00,DataType.UINT8,"RPDO 3","largest subindex supported",AccessType.CONST,0x3),
new CanopenObject(0x1402,0x01,DataType.UINT32,"RPDO 3","COB-ID used",AccessType.RW,0x8000420),
new CanopenObject(0x1402,0x02,DataType.UINT8,"RPDO 3","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1402,0x03,DataType.UINT16,"RPDO 3","Inhibit Time",AccessType.RW,0x0),
new CanopenObject(0x1403,0x00,DataType.UINT8,"RPDO 4","largest subindex supported",AccessType.CONST,0x3),
new CanopenObject(0x1403,0x01,DataType.UINT32,"RPDO 4","COB-ID used",AccessType.RW,0x80000520),
new CanopenObject(0x1403,0x02,DataType.UINT8,"RPDO 4","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1403,0x03,DataType.UINT16,"RPDO 4","Inhibit Time",AccessType.RW,0x0),
new CanopenObject(0x1600,0x00,DataType.UINT8,"RPDO 1 mapping parameter","number of mapped objects",AccessType.RW,0x1),
new CanopenObject(0x1600,0x01,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60400010),
new CanopenObject(0x1600,0x02,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x03,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x04,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x05,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x06,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x07,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1600,0x08,DataType.UINT32,"RPDO 1 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x00,DataType.UINT8,"RPDO 2 mapping parameter","number of mapped objects",AccessType.RW,0x2),
new CanopenObject(0x1601,0x01,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60400010),
new CanopenObject(0x1601,0x02,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x60600008),
new CanopenObject(0x1601,0x03,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x04,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x05,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x06,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x07,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1601,0x08,DataType.UINT32,"RPDO 2 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x00,DataType.UINT8,"RPDO 3 mapping parameter","number of mapped objects",AccessType.RW,0x2),
new CanopenObject(0x1602,0x01,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60400010),
new CanopenObject(0x1602,0x02,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x607A0020),
new CanopenObject(0x1602,0x03,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x04,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x05,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x06,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x07,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1602,0x08,DataType.UINT32,"RPDO 3 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x00,DataType.UINT8,"RPDO 4 mapping parameter","number of mapped objects",AccessType.RW,0x2),
new CanopenObject(0x1603,0x01,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60400010),
new CanopenObject(0x1603,0x02,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x60FF0020),
new CanopenObject(0x1603,0x03,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x04,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x05,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x06,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x07,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1603,0x08,DataType.UINT32,"RPDO 4 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1800,0x00,DataType.UINT8,"TPDO 1","largest subindex supported",AccessType.CONST,0x5),
new CanopenObject(0x1800,0x01,DataType.UINT32,"TPDO 1","COB-ID used",AccessType.RW,0x400001A0),
new CanopenObject(0x1800,0x02,DataType.UINT8,"TPDO 1","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1800,0x03,DataType.UINT16,"TPDO 1","inhibit time",AccessType.RW,0x3E8),
new CanopenObject(0x1800,0x05,DataType.UINT16,"TPDO 1","event timer",AccessType.RW,0x0),
new CanopenObject(0x1801,0x00,DataType.UINT8,"TPDO 2","largest subindex supported",AccessType.CONST,0x5),
new CanopenObject(0x1801,0x01,DataType.UINT32,"TPDO 2","COB-ID used",AccessType.RW,0x400002A0),
new CanopenObject(0x1801,0x02,DataType.UINT8,"TPDO 2","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1801,0x03,DataType.UINT16,"TPDO 2","inhibit time",AccessType.RW,0x3E8),
new CanopenObject(0x1801,0x05,DataType.UINT16,"TPDO 2","event timer",AccessType.RW,0x0),
new CanopenObject(0x1802,0x00,DataType.UINT8,"TPDO 3","largest subindex supported",AccessType.CONST,0x5),
new CanopenObject(0x1802,0x01,DataType.UINT32,"TPDO 3","COB-ID used",AccessType.RW,0x400003A0),
new CanopenObject(0x1802,0x02,DataType.UINT8,"TPDO 3","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1802,0x03,DataType.UINT16,"TPDO 3","inhibit time",AccessType.RW,0x3E8),
new CanopenObject(0x1802,0x05,DataType.UINT16,"TPDO 3","event timer",AccessType.RW,0x0),
new CanopenObject(0x1803,0x00,DataType.UINT8,"TPDO 4","largest subindex supported",AccessType.CONST,0x5),
new CanopenObject(0x1803,0x01,DataType.UINT32,"TPDO 4","COB-ID used",AccessType.RW,0x400004A0),
new CanopenObject(0x1803,0x02,DataType.UINT8,"TPDO 4","transmission type",AccessType.RW,0xFF),
new CanopenObject(0x1803,0x03,DataType.UINT16,"TPDO 4","inhibit time",AccessType.RW,0x3E8),
new CanopenObject(0x1803,0x05,DataType.UINT16,"TPDO 4","event timer",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x00,DataType.UINT8,"TPDO 1 mapping parameter","number of mapped objects",AccessType.RW,0x1),
new CanopenObject(0x1A00,0x01,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60410010),
new CanopenObject(0x1A00,0x02,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x03,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x04,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x05,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x06,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x07,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A00,0x08,DataType.UINT32,"TPDO 1 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x00,DataType.UINT8,"TPDO 2 mapping parameter","number of mapped app. objects",AccessType.RW,0x2),
new CanopenObject(0x1A01,0x01,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60410010),
new CanopenObject(0x1A01,0x02,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x60610008),
new CanopenObject(0x1A01,0x03,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x04,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x05,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x06,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x07,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A01,0x08,DataType.UINT32,"TPDO 2 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x00,DataType.UINT8,"TPDO 3 mapping parameter","number of mapped app. objects",AccessType.RW,0x2),
new CanopenObject(0x1A02,0x01,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60410010),
new CanopenObject(0x1A02,0x02,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x60640020),
new CanopenObject(0x1A02,0x03,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x04,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x05,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x06,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x07,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A02,0x08,DataType.UINT32,"TPDO 3 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x00,DataType.UINT8,"TPDO 4 mapping parameter","number of mapped app. objects",AccessType.RW,0x2),
new CanopenObject(0x1A03,0x01,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 1. app. object",AccessType.RW,0x60410010),
new CanopenObject(0x1A03,0x02,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 2. app. object",AccessType.RW,0x606C0020),
new CanopenObject(0x1A03,0x03,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 3. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x04,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 4. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x05,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 5. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x06,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 6. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x07,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 7. app. object",AccessType.RW,0x0),
new CanopenObject(0x1A03,0x08,DataType.UINT32,"TPDO 4 mapping parameter","PDO mapping 8. app. object",AccessType.RW,0x0),
new CanopenObject(0x603F,0x00,DataType.UINT16,"","Error code",AccessType.RO,0x0),
new CanopenObject(0x6062,0x00,DataType.INT32,"","Position demand value",AccessType.RO,0),
new CanopenObject(0x6063,0x00,DataType.INT32,"","Position actual internal value",AccessType.RO,0),
new CanopenObject(0x6064,0x00,DataType.INT32,"","Position actual value",AccessType.RO,0),
new CanopenObject(0x6065,0x00,DataType.UINT32,"","Following error window",AccessType.RW,0xFFFFFFFF),
new CanopenObject(0x6066,0x00,DataType.UINT16,"","Following error time out",AccessType.RW,0x64),
new CanopenObject(0x6067,0x00,DataType.UINT32,"","Position window",AccessType.RW,0x64),
new CanopenObject(0x6068,0x00,DataType.UINT16,"","Position window time",AccessType.RW,0xA),
new CanopenObject(0x606D,0x00,DataType.UINT16,"","Velocity window",AccessType.RW,0x3E8),
new CanopenObject(0x606E,0x00,DataType.UINT16,"","Velocity window time",AccessType.RW,0xA),
new CanopenObject(0x606F,0x00,DataType.UINT16,"","Velocity threshold",AccessType.RW,0x3E8),
new CanopenObject(0x6070,0x00,DataType.UINT16,"","Velocity threshold time",AccessType.RW,0x64),
new CanopenObject(0x6072,0x00,DataType.UINT16,"","Max torque",AccessType.RW,0x3E8),
new CanopenObject(0x6073,0x00,DataType.UINT16,"","Max current",AccessType.RW,0x190),
new CanopenObject(0x6074,0x00,DataType.INT16,"","Torque demand",AccessType.RO,0),
new CanopenObject(0x6075,0x00,DataType.UINT32,"","Motor rated current",AccessType.RW,0x3E8),
new CanopenObject(0x6076,0x00,DataType.UINT32,"","Motor rated torque",AccessType.RW,0x136),
new CanopenObject(0x6077,0x00,DataType.INT16,"","Torque actual value",AccessType.RO,0),
new CanopenObject(0x6078,0x00,DataType.INT16,"","Current actual value",AccessType.RO,0),
new CanopenObject(0x607A,0x00,DataType.INT32,"","Target position",AccessType.RW,0),
new CanopenObject(0x607C,0x00,DataType.INT32,"","Home offset",AccessType.RW,0),
new CanopenObject(0x607D,0x00,DataType.UINT8,"Software position limit","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x607D,0x01,DataType.INT32,"Software position limit","Min position limit",AccessType.RW,-2147483648),
new CanopenObject(0x607D,0x02,DataType.INT32,"Software position limit","Max position limit",AccessType.RW,2147483647),
new CanopenObject(0x607E,0x00,DataType.UINT8,"","Polarity",AccessType.RW,0x0),
new CanopenObject(0x607F,0x00,DataType.UINT32,"","Max profile velocity",AccessType.RW,0x1E8480),
new CanopenObject(0x6080,0x00,DataType.UINT32,"","Max motor speed",AccessType.RW,0xC350),
new CanopenObject(0x6081,0x00,DataType.UINT32,"","Profile velocity",AccessType.RW,0x1E8480),
new CanopenObject(0x6082,0x00,DataType.UINT32,"","End velocity",AccessType.RW,0x0),
new CanopenObject(0x6083,0x00,DataType.UINT32,"","Profile acceleration",AccessType.RW,0x6ACFC0),
new CanopenObject(0x6084,0x00,DataType.UINT32,"","Profile deceleration",AccessType.RW,0x6ACFC0),
new CanopenObject(0x6085,0x00,DataType.UINT32,"","Quick stop deceleration",AccessType.RW,0x6ACFC0),
new CanopenObject(0x6086,0x00,DataType.INT16,"","Motion profile type",AccessType.RW,0),
new CanopenObject(0x608F,0x00,DataType.UINT32,"Position encoder resolution","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x608F,0x01,DataType.UINT32,"Position encoder resolution","Encoder increments",AccessType.RW,0x4E20),
new CanopenObject(0x608F,0x02,DataType.UINT32,"Position encoder resolution","Motor revolutions",AccessType.RW,0x1),
new CanopenObject(0x6098,0x00,DataType.INT8,"","Homing method",AccessType.RW,-2),
new CanopenObject(0x6099,0x00,DataType.UINT8,"Homing speeds","Number of entries",AccessType.CONST,0x2),
new CanopenObject(0x6099,0x01,DataType.UINT32,"Homing speeds","Speed for switch search",AccessType.RW,0xC350),
new CanopenObject(0x6099,0x02,DataType.UINT32,"Homing speeds","Speed for zero search",AccessType.RW,0x1388),
new CanopenObject(0x609A,0x00,DataType.UINT32,"","Homing acceleration",AccessType.RW,0x186A0),
new CanopenObject(0x60B2,0x00,DataType.INT16,"","Torque offset",AccessType.RW,0),
new CanopenObject(0x60C5,0x00,DataType.UINT32,"","Max acceleration",AccessType.RW,0x6ACFC0),
new CanopenObject(0x60C6,0x00,DataType.UINT32,"","Max deceleration",AccessType.RW,0x6ACFC0),
new CanopenObject(0x60E0,0x00,DataType.INT16,"","Positive torque limit value",AccessType.RW,1000),
new CanopenObject(0x60E1,0x00,DataType.INT16,"","Negative torque limit value",AccessType.RW,-1000),
new CanopenObject(0x60F4,0x00,DataType.INT32,"","Following error actual value",AccessType.RO,0),
new CanopenObject(0x60FA,0x00,DataType.INT32,"","Control effort",AccessType.RO,0),
new CanopenObject(0x60FC,0x00,DataType.INT32,"","Position demand internal value",AccessType.RO,0),
new CanopenObject(0x6402,0x00,DataType.UINT16,"","Motor type",AccessType.RW,0xD),
new CanopenObject(0x6502,0x00,DataType.UINT32,"","Supported drive modes",AccessType.CONST,0xAD)
// new CanopenObject(0x6505,0x00,DataType.VISIBLE_STRING,"","Http drive catalog address",AccessType.CONST,http://www.ingeniamc.com),                  Manual edit: removed object 0x6505

                                             };

        public int Count()
        {
            return dictionary.Count();
        }

        public CanopenObject GetObject(int index)
        
        {
            return GetObject(index & 0xFFFF, index >> 16);

        }

        public CanopenObject GetObject(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    return dictionary[i];
                }
            }
            return null;
        }

        public bool ObjectExists(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    return true;
                }
            }
            return false;
        }

        public bool ObjectHasWriteAccess(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    switch (dictionary[i].accesstype)
                    {
                        case AccessType.RW:
                        case AccessType.WO:
                            return true;
                        default:
                            return false;
                    }
                }
            }
            return false;   // The object does not exist: return false
        }

        public bool ObjectHasReadAccess(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    switch (dictionary[i].accesstype)
                    {
//                        case AccessType.CONST:
                        case AccessType.RW:
                        case AccessType.RO:
                            return true;
                        default:
                            return false;
                    }
                }
            }
            return false;   // The object does not exist: return false
        }

        public int ObjectSize(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    switch (dictionary[i].datatype)
                    {
                        case DataType.INT8:
                        case DataType.UINT8:
                            return 1;
                        case DataType.INT16:
                        case DataType.UINT16:
                            return 2;
                        case DataType.INT32:
                        case DataType.UINT32:
                            return 4;
                        case DataType.INT64:
                        case DataType.UINT64:
                        case DataType.VISIBLE_STRING:
                            return 8;
                        default:
                            return 0;
                    }
                }
            }
            return 0;   // The object does not exist: return size zero to indicate error
        }

        public int ObjectSize(int index)
        {
            return ObjectSize(index & 0xFFFF, (index >> 16) & 0xFFFF);
        }

        public bool ObjecIsSignedvalue(int index, int subindex)
        {
            for (int i = 0; i < this.dictionary.Count(); i++)
            {
                if ((dictionary[i].index == index) && (dictionary[i].subindex == subindex))
                {
                    switch (dictionary[i].datatype)
                    {
                        case DataType.INT8:
                        case DataType.INT16:
                        case DataType.INT32:
                        case DataType.INT64:
                            return true;
                        case DataType.UINT8:
                        case DataType.UINT16:
                        case DataType.UINT32:
                        case DataType.UINT64:
                            return false;
                        default:
                            return false;
                    }
                }
            }
            return false;
        }

        public bool ObjecIsSignedvalue(int index)
        {
            return ObjecIsSignedvalue(index & 0xFFFF, (index >> 16) & 0xFFFF);
        }


        public string ObjectName(int index)
        {
            CanopenObject temp = GetObject(index);
            if (temp == null)
            {
                return "";
            }
            else
            {
                return temp.objectname;
            }
        }

        public string ObjectLongName(int index)
        {

            string longname = "";
            CanopenObject temp = GetObject(index);
            if (temp == null)
            {
                longname = "Object name not found in dictionary";
            }
            else
            {
                if (temp.groupname != "")
                {
                    longname = temp.groupname + "-";
                }
                longname += temp.objectname;
            }
            return longname;
        }


        public bool IsWithinLimits(Int64 value, int index, bool showMessage)
        {
            bool bOk = true;
            Int64 min = 0;
            Int64 max = 0;

            CanopenObject temp = GetObject(index);
            if (temp != null)
            {
                switch (temp.datatype)
                {
                    case DataType.INT8:
                        min = sbyte.MinValue;
                        max = sbyte.MaxValue;
                        break;
                    case DataType.INT16:
                        min = Int16.MinValue;
                        max = Int16.MaxValue;
                        break;
                    case DataType.INT32:
                        min = Int32.MinValue;
                        max = Int32.MaxValue;
                        break;

                    case DataType.UINT8:
                        min = byte.MinValue;
                        max = byte.MaxValue;
                        break;
                    case DataType.UINT16:
                        min = UInt16.MinValue;
                        max = UInt16.MaxValue;
                        break;
                    case DataType.UINT32:
                        min = UInt32.MinValue;
                        max = UInt32.MaxValue;
                        break;
                }
                bOk = (value >= min) && (value <= max);
                if (!bOk && showMessage)
                {
                    MessageBox.Show("The value is out of limits\n The allowed range is " + min.ToString() + " to " + max.ToString());  
                }
            }



            return bOk;
        }
    }
}
