﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    //
    // Possible format of the Set variable syntax:
    // variable = constant 
    // variable = variable1
    // variable = expression variable1 operation constant
    // variable = expression var1 operation var2
    //
    public class SetvariableSequence : Sequence
    {

        private string parms;   // The parameter string

        private string operation = "";
        private string operand = "";

        private Int32 constant = 0;

        private string variable = "";
        private string variable1 = "";
        private string variable2 = "";

        bool bCheck = true;

        private const string STR_OPERATION = "Operation";
        private const string STR_VARIABLE_TO_SET = "Variable to set";
        private const string STR_VARIABLE_1 = "Variable 1";
        private const string STR_VARIABLE_2 = "Variable 2";


        private const string STR_PREFIX_VAR      = "Var";
        private const string STR_PREFIX_VAR1     = "Var1";
        private const string STR_PREFIX_VAR2     = "Var2";
        private const string STR_PREFIX_CONSTANT = "Const";



        private const string STR_CONST =      "Constant";
        private const string STR_VARIABLE =   "Variable";
        private const string STR_ADD =        "Add";
        private const string STR_SUBTRACT =   "Subtract";
        private const string STR_MULTIPLY =   "Multiply";
        private const string STR_DIVIDE =     "Divide";
        private const string STR_ABSOLUTE =   "Absolute";
        private const string STR_COMPLEMENT = "Complement";

        private const string STR_XOR =         "Xor";
        private const string STR_AND =         "And";
        private const string STR_OR =          "Or";
        private const string STR_SHIFT_LEFT =  "Shift left";
        private const string STR_SHIFT_RIGHT = "Shift right";


        private const string STR_OPERANDS = "Operands";


        private const string STR_VARIABLE_AND_CONSTANT = "Variable and constant";
        private const string STR_VARIABLE_AND_VARIABLE = "Variable and variable";
        private const string STR_CONSTANT_AND_VARIABLE = "Constant and variable";

        public string[] operations = { STR_CONST,
                                       STR_VARIABLE,
                                       STR_ADD,
                                       STR_SUBTRACT,
                                       STR_DIVIDE,
                                       STR_MULTIPLY,
                                       STR_ABSOLUTE,
                                       STR_COMPLEMENT,
                                       STR_AND,
                                       STR_OR,
                                       STR_XOR,
                                       STR_SHIFT_LEFT,
                                       STR_SHIFT_RIGHT};


        private string[] operands = {STR_VARIABLE_AND_CONSTANT,
                                     STR_VARIABLE_AND_VARIABLE,
                                     STR_CONSTANT_AND_VARIABLE};
                                    

        private UserCombobox UserComboboxOperation;
        private UserCombobox UserComboboxOperands;
        private UserObjectselector UserObjectselectorVariable;
        private UserObjectselector UserObjectselectorVariable1;
        private UserObjectselector UserObjectselectorVariable2;
        private UserTextbox UserTextboxConstant;








        public SetvariableSequence()    // The constructor
        {
            name = "Set variable";
            mnemonic = "SetVariable";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, operations);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, operands);


            UserComboboxOperation = new UserCombobox(Globals.mainform.lblName1, STR_OPERATION, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxOperands = new UserCombobox(Globals.mainform.lblName2, STR_OPERANDS, Globals.mainform.ComboBox2, Globals.mainform.lblUnits2, "");
            UserObjectselectorVariable =  new UserObjectselector(Globals.mainform.lblName3, STR_VARIABLE_TO_SET, Globals.mainform.btnSelect1, Globals.mainform.lblUnits3, "", false, true);
            UserObjectselectorVariable1 = new UserObjectselector(Globals.mainform.lblName4, STR_VARIABLE_1, Globals.mainform.btnSelect2, Globals.mainform.lblUnits4, "", true, false);
            UserObjectselectorVariable2 = new UserObjectselector(Globals.mainform.lblName5, STR_VARIABLE_2, Globals.mainform.btnSelect3, Globals.mainform.lblUnits5, "", true, false);
            UserTextboxConstant = new UserTextbox(Globals.mainform.lblName6, Globals.STR_CONSTANT, Globals.mainform.txtParm1, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits6, "", true);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxOperation = null;
            UserComboboxOperands = null;
            UserObjectselectorVariable = null;
            UserObjectselectorVariable1 = null;
            UserObjectselectorVariable2 = null;
            UserTextboxConstant = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            //
            // First set the default parameters
            //

            constant = 0;
            variable = "";
            variable1 = "";
            variable2 = "";
            operation = STR_CONST;
            operand = STR_VARIABLE_AND_CONSTANT;

            if (parms != "")       // Then parse the parameter string
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case STR_CONST:
                        case STR_VARIABLE:
                        case STR_ADD:
                        case STR_SUBTRACT:
                        case STR_MULTIPLY:
                        case STR_DIVIDE:
                        case STR_ABSOLUTE:
                        case STR_COMPLEMENT:
                        case STR_AND:
                        case STR_OR:
                        case STR_XOR:
                        case STR_SHIFT_LEFT:
                        case STR_SHIFT_RIGHT:
                            operation = parameterName;
                            break;

                        case STR_VARIABLE_AND_CONSTANT:
                        case STR_VARIABLE_AND_VARIABLE:
                        case STR_CONSTANT_AND_VARIABLE:
                            operand = parameterName;
                            break;

                        case STR_PREFIX_VAR:

                            variable = parameterValue;
                            
                            break;
                        case STR_PREFIX_VAR1:
                            variable1 = parameterValue;
                            break;
                        case STR_PREFIX_VAR2:
                            variable2 = parameterValue;
                            break;

                        case STR_PREFIX_CONSTANT:
                            constant = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;
                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            UserObjectselectorVariable.SetSelection(variable);
            UserObjectselectorVariable1.SetSelection(variable1);
            UserObjectselectorVariable2.SetSelection(variable2);
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserComboboxOperation.Hide();
            UserComboboxOperands.Hide();

            UserObjectselectorVariable.Hide();
            UserObjectselectorVariable1.Hide();
            UserObjectselectorVariable2.Hide();

            UserTextboxConstant.Hide();


            Globals.mainform.ComboBox1.SelectedItem = operation;
            Globals.mainform.ComboBox2.SelectedItem = operand;

            //
            // Now switch the position and visibility of all selections dependant on  mode
            //
            UserObjectselectorVariable.Show(0); // And show the affected variable
            UserComboboxOperation.Show(1);   // Allways show the selected operation
            switch (operation)
            {
                case STR_CONST:
                    UserTextboxConstant.Show(2);
                    break;

                case STR_VARIABLE:
                    UserObjectselectorVariable1.Show(2);
                    break;

                case STR_ADD:
                case STR_SUBTRACT:
                case STR_MULTIPLY:
                case STR_DIVIDE:
                case STR_AND:
                case STR_OR:
                case STR_XOR:
                case STR_SHIFT_LEFT:
                case STR_SHIFT_RIGHT:
                    UserComboboxOperands.Show(2);
                    switch (operand)
                    {
                        case STR_VARIABLE_AND_CONSTANT:
                            UserObjectselectorVariable1.Show(3);
                            UserTextboxConstant.Show(4);
                            break;
                        case STR_VARIABLE_AND_VARIABLE:
                            UserObjectselectorVariable1.Show(3);
                            UserObjectselectorVariable2.Show(4);
                            break;
                        case STR_CONSTANT_AND_VARIABLE:
                            UserTextboxConstant.Show(3);
                            UserObjectselectorVariable1.Show(4);
                            break;
                    }
                    break;
                case STR_ABSOLUTE:
                case STR_COMPLEMENT:
                    UserObjectselectorVariable1.Show(2);
                    break;
 
                default:
                    break;
            }


            UserTextboxConstant.SetValue(constant.ToString());
            UserObjectselectorVariable.SetSelection(variable);
            UserObjectselectorVariable1.SetSelection(variable1);
            UserObjectselectorVariable2.SetSelection(variable2);
            Globals.mainform.txtComment.Text = strComment;

        }

        public override void ButtonClick(Object sender)
        {
            if (sender == Globals.mainform.btnSelect1)
            {
                UserObjectselectorVariable.SelectObject();
            }
            if (sender == Globals.mainform.btnSelect2)
            {
                UserObjectselectorVariable1.SelectObject();
            }
            if (sender == Globals.mainform.btnSelect3)
            {
                UserObjectselectorVariable2.SelectObject();
            }
        }



        public override void UserInputHasChanged(Object sender)
        {
            operation = Globals.mainform.ComboBox1.Text;
            operand = Globals.mainform.ComboBox2.Text;

            bCheck = false;
            UpdateParametersFromGui();
            bCheck = true;
            if ((sender == Globals.mainform.ComboBox1) || (sender == Globals.mainform.ComboBox2))       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {

            operation = UserComboboxOperation.Text();
            operand = UserComboboxOperands.Text();
            variable = UserObjectselectorVariable.selectedIndexString;
            variable1 = UserObjectselectorVariable1.selectedIndexString;
            variable2 = UserObjectselectorVariable2.selectedIndexString;

            UserObjectselectorVariable.ShowMessageIfNotSelected(bCheck);


            switch (operation)
            {
                case STR_CONST:
                    constant = (Int32)UserTextboxConstant.Value();
                    break;
                case STR_VARIABLE:

                    UserObjectselectorVariable1.ShowMessageIfNotSelected(bCheck);
                    break;
                case STR_ADD:
                case STR_SUBTRACT:
                case STR_MULTIPLY:
                case STR_DIVIDE:
                case STR_AND:
                case STR_OR:
                case STR_XOR:
                case STR_SHIFT_LEFT:
                case STR_SHIFT_RIGHT:
                    UserObjectselectorVariable1.ShowMessageIfNotSelected(bCheck);
                    switch (operand)
                    {
                        case STR_VARIABLE_AND_CONSTANT:
                            constant = (Int32)UserTextboxConstant.Value();
                            break;
                        case STR_VARIABLE_AND_VARIABLE:
                            UserObjectselectorVariable2.ShowMessageIfNotSelected(bCheck);
                            break;
                        case STR_CONSTANT_AND_VARIABLE:
                            constant = (Int32)UserTextboxConstant.Value();
                            break;
                    }
                    break;
                case STR_ABSOLUTE:
                case STR_COMPLEMENT:
                    UserObjectselectorVariable1.ShowMessageIfNotSelected(bCheck);
                    break;
            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {

            string temp = STR_PREFIX_VAR + "=" + variable;
            temp += Globals.STR_COMMA + operation.Replace(' ', '_');

            switch (operation)
            {

                case STR_VARIABLE:
                    temp += Globals.STR_COMMA + STR_PREFIX_VAR1 + "=" + variable1;
                    break;

                case STR_CONST:
                    temp += Globals.STR_COMMA + STR_PREFIX_CONSTANT + "=" + constant.ToString();
                    break;
                case STR_ADD:
                case STR_SUBTRACT:
                case STR_MULTIPLY:
                case STR_DIVIDE:
                case STR_AND:
                case STR_OR:
                case STR_XOR:
                case STR_SHIFT_LEFT:
                case STR_SHIFT_RIGHT:
                    temp += Globals.STR_COMMA + operand.Replace(' ', '_');
                    switch (operand)
                    {
                        case STR_VARIABLE_AND_CONSTANT:
                            temp += Globals.STR_COMMA + STR_PREFIX_VAR1 + "=" + variable1;
                            temp += Globals.STR_COMMA + STR_PREFIX_CONSTANT + "=" + constant.ToString();
                            break;
                        case STR_VARIABLE_AND_VARIABLE:
                            temp += Globals.STR_COMMA + STR_PREFIX_VAR1 + "=" + variable1;
                            temp += Globals.STR_COMMA + STR_PREFIX_VAR2 + "=" + variable2;
                            break;
                        case STR_CONSTANT_AND_VARIABLE:
                            temp += Globals.STR_COMMA + STR_PREFIX_CONSTANT + "=" + constant.ToString();
                            temp += Globals.STR_COMMA + STR_PREFIX_VAR1 + "=" + variable1;
                            break;
                    }
                    break;
                case STR_ABSOLUTE:
                case STR_COMPLEMENT:
                    temp += Globals.STR_COMMA + STR_PREFIX_VAR1 + "=" + variable1;
                    break;
            }
            return temp;            
        }



        private void CreateCode()
        {
            int sourcevariable1 = 0;
            int sourcevariable2 = 0;

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            instructions.Clear();
            int targetVariable = UserObjectselectorVariable.GetIndex(variable);
            sourcevariable1 = UserObjectselectorVariable1.GetIndex(variable1);

            switch (operation)
            {
                case STR_CONST:
                        instructions.Add(new WriteInstruction(Globals.utils.GetIndex(targetVariable), Globals.utils.GetSubindex(targetVariable), constant, strtemp));
                        return;
                        
                case STR_VARIABLE:
                        sourcevariable1 = UserObjectselectorVariable1.GetIndex(variable1);
                        instructions.Add(new WriteInstruction(0x2c01, 22, sourcevariable1, strtemp + "Get " + variable1 + " in ACCUM"));
                        break;

                case STR_ABSOLUTE:
                        instructions.Add(new WriteInstruction(0x2c01, 22, sourcevariable1, strtemp + "Get " + variable1 + " in ACCUM"));
                        instructions.Add(new WriteInstruction(0x2c01, 0x13, 0));        // Absolute ACCUM
                        break;

                case STR_COMPLEMENT:
                        instructions.Add(new WriteInstruction(0x2c01, 22, sourcevariable1, strtemp + "Get " + variable1 + " in ACCUM"));
                        instructions.Add(new WriteInstruction(0x2c01, 0x14, 0));        // Complement ACCUM
                        break;


                case STR_ADD:
                case STR_SUBTRACT:
                case STR_MULTIPLY:
                case STR_DIVIDE:
                case STR_AND:
                case STR_OR:
                case STR_XOR:
                case STR_SHIFT_LEFT:
                case STR_SHIFT_RIGHT:
                        switch (operand)
                        {
                            case STR_VARIABLE_AND_CONSTANT:
                                instructions.Add(new WriteInstruction(0x2c01, 22, sourcevariable1, strtemp + "Get " + variable1 + " in ACCUM"));
                                break;
                            case STR_VARIABLE_AND_VARIABLE:
                                instructions.Add(new WriteInstruction(0x2c01, 22, sourcevariable1, strtemp + "Get " + variable1 + " in ACCUM"));
                                sourcevariable2 = UserObjectselectorVariable2.GetIndex(variable2);
                                break;
                            case STR_CONSTANT_AND_VARIABLE:
                                instructions.Add(new WriteInstruction(0x2c00, 1, constant, strtemp + "Set ACCUM to " + constant.ToString()));
                                break;
                        }
                        switch (operation)
                        {
                            case STR_ADD:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 1, constant, "Add " + constant.ToString() + " to ACCUM"));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 10, sourcevariable2, "Add " + variable2 + " to ACCUM"));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 10, sourcevariable1, "Add " + variable1 + " to ACCUM"));
                                        break;
                                }
                                break;

                            case STR_SUBTRACT:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 7, constant, "Subtract " + constant.ToString() + " from ACCUM"));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 16, sourcevariable2, "Subtract " + variable2 + " from ACCUM"));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 16, sourcevariable1, "Subtract " + variable1 + " from ACCUM"));
                                        break;
                                }
                                break;
                            case STR_MULTIPLY:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 4, constant, "Multiply ACCUM by " + constant.ToString()));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 13, sourcevariable2, "Multiply ACCUM by " + variable2));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 13, sourcevariable1, "Multiply ACCUM by " + variable1));
                                        break;
                                }
                                break;
                            case STR_DIVIDE:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 2, constant, "Divide ACCUM by " + constant.ToString()));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 11, sourcevariable2, "Divide ACCUM by " + variable2));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 11, sourcevariable1, "Divide ACCUM by " + variable1));
                                        break;
                                }
                                break;
                            case STR_AND:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 5, constant, "And ACCUM with " + constant.ToString()));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 14, sourcevariable2, "And ACCUM with " + variable2));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 14, sourcevariable1, "And ACCUM with " + variable1));
                                        break;
                                }
                                break;
                            case STR_OR:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 6, constant, "Or ACCUM with " + constant.ToString()));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 15, sourcevariable2, "Or ACCUM with " + variable2));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 15, sourcevariable1, "Or ACCUM with " + variable1));
                                        break;
                                }
                                break;
                            case STR_XOR:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 3, constant, "Xor ACCUM with " + constant.ToString()));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 12, sourcevariable2, "Xor ACCUM with " + variable2));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 12, sourcevariable1, "Xor ACCUM with " + variable1));
                                        break;
                                }
                                break;
                            case STR_SHIFT_LEFT:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 8, constant, "Shift left ACCUM " + constant.ToString() + " bits"));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 17, sourcevariable2, "Shift left ACCUM " + variable2 + " bits"));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 17, sourcevariable1, "Shift left ACCUM " + variable1 + " bits"));
                                        break;
                                }
                                break;

                            case STR_SHIFT_RIGHT:
                                switch (operand)
                                {
                                    case STR_VARIABLE_AND_CONSTANT:
                                        instructions.Add(new WriteInstruction(0x2c01, 9, constant, "Shift right ACCUM " + constant.ToString() + " bits"));
                                        break;
                                    case STR_VARIABLE_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 18, sourcevariable2, "Shift right ACCUM " + variable2 + " bits"));
                                        break;
                                    case STR_CONSTANT_AND_VARIABLE:
                                        instructions.Add(new WriteInstruction(0x2c01, 18, sourcevariable1, "Shift right ACCUM " + variable1 + " bits"));
                                        break;
                                }
                                break;
                        }
                        break;
            }
            instructions.Add(new WriteInstruction(0x2c01, 21, targetVariable, "Write ACCUM to " + variable));
        }
    }
}
