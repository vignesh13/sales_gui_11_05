﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    //
    // Class Sequences holds a list of all available sequences
    //
    public class Sequences
    {
        public List<Sequence> sequencelist = new List<Sequence>();
        public Sequences()
        {
        }
        public void Initialize()        // Must be called from the main application
        {
            sequencelist.Add(new MacronumberSequence());
            sequencelist.Add(new MacroSequence());
            sequencelist.Add(new HomingSequence());
            sequencelist.Add(new SoftlandSequence());     // Leave out the softland until implemented
            sequencelist.Add(new PhasedetectSequence());
            sequencelist.Add(new PositionmoveSequence());
            sequencelist.Add(new VelocitymoveSequence());
            sequencelist.Add(new ForcemoveSequence());
            sequencelist.Add(new WaitSequence());
            sequencelist.Add(new IfSequence());
            sequencelist.Add(new SetoutputSequence());
            sequencelist.Add(new MotorSequence());
            sequencelist.Add(new GetvariableSequence());
            sequencelist.Add(new SetvariableSequence());
        }
    }

    //
    // Base class for a sequence definition
    //
    // All derived classes must override the following functions:
    //
    // void SetParameters(string parameters)    // The parameters are in the form "(A=10000,XP=2000,V2=4500)"
    // void ShowParametersInGui()               // Shows the parameters in the general purpose input controls
    // void UpdateParametersFromGui()              // Gets the parameters from the Gui
    // void UserInputHasChanged()               // Is called by the main application after changing a selection
    // void GetParameterString()                     // Shows the command line in the gridview
    // void int GetSize()                       // Returns the size in instructions of the sequence
    //

    public class Sequence
    {
        protected string name = "";         // This is the name that is used in the GUI and also the key to lookup the derived class
        protected string mnemonic = "";     // This is the name of the sequence used in the macro file
        protected string group = "";        // Not used until now
        protected string strError = "";
        protected string strComment = "";

        public List<Instruction> instructions = new List<Instruction>();

        public string GetName()
        {
            return name;
        }

        public string GetMnemonic()
        {
            return mnemonic;
        }


        public string GetGroup()
        {
            return group;
        }

        public string GetError()
        {
            return strError;
        }

        public void SetComment(string comment)
        {
            strComment = comment;
        }

        public string GetComment()
        {
            return strComment;
        }

        //
        // SetParameters() is called from the main application after initial selection of a sequence.
        // This can be after initial selection of the function combo box. In that case the parameter string will be ""
        // Or after the user clicks a line in the macro window, in that case the parameter string will be passed
        // The function copies all parameters to the objects local storage
        //

        public virtual void SetParameters(string parameters)        // This function must be defined in a derived class
        {
        }

        //
        // ShowParametersInGui() is called to show all parameters in the GUI
        //

        public virtual void ShowParametersInGui()
        {
        }

        public virtual void Init()
        {
        }

        public virtual void Exit()
        {
        }

        // 
        // UpdateParametersFromGui() is called to read the parameters from the GUI and to store the
        // read values into the sequences local storage.
        //

        public virtual void UpdateParametersFromGui()
        {
        }

        //
        // UserInputHasChanged() is called by the main application after the user has changed
        // a value in a parameter control
        //

        public virtual void UserInputHasChanged(Object sender)       // This function is called after the user has changed something in the comboboxes
        {
        }

        public virtual void ButtonClick(Object sender)       // This function is called after the user has clicked a button in the function editor
        {
        }

        public virtual string GetParameterString() 
        {
            return "";
        }

        public virtual int GetSize()        // Returns the size of the sequence in instructions
        {
            return instructions.Count;
        }
        //
        // Create code to wait for a statusbit
        //
        protected void CreateCodeWaitForStatusbit(int statusbit, bool condition, string bitname, string commandline)
        {
            instructions.Add(new WriteInstruction(0x2C01, 23, 0x6041, commandline + "Wait until " + bitname + " is "+ (condition ? "set" : "clear") + ": Get statusword in ACCUM"));
            instructions.Add(new WriteInstruction(0x2C02, (condition ? 11 : 10), statusbit, "Bit " + statusbit.ToString() + "= " + bitname));
            instructions.Add(new WriteInstruction(0x2C04, 6, -2, "Loop back until bit " + statusbit.ToString() + " is " + (condition ? "set" : "clear")));   // Jump relative -2
        }

        protected void CreateCodeWaitForStatusbit(int statusbit, bool condition, string bitname)
        {
            CreateCodeWaitForStatusbit(statusbit, condition, bitname, "");

        }
        protected void CreateCodeForSystemMacro(Bios.callnumber callnumber, string comment)
        {
            instructions.Add(new WriteInstruction(0x2c00, 1, (int)callnumber, comment + "Prepare ACCUM to call system macro #" + ((int)callnumber).ToString() + " : " + callnumber.ToString()));
            instructions.Add(new WriteInstruction(0x2c04, 1, 63, "Call system macro"));
        }

        protected void CreateCodeForSystemMacro(Bios.callnumber callnumber)
        {
            CreateCodeForSystemMacro(callnumber, "");
        }
    }
}
