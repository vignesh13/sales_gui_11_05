﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class PhasedetectSequence : Sequence
    {

        // private enums go here



        // constant strings that go into comboboxes go here


        const string STR_FORCED = "Forced";
        const string STR_INITIAL_POSITION_ALLWAYS_KNOWN = "Initial position allways known";


        // constant strings that are parameter prefixes go here 

        const string STR_PREFIX_TIME = "Time";
        const string STR_PREFIX_CURRENT = "Current";
        const string STR_PREFIX_TOLERANCE = "Tolerance";
        const string STR_PREFIX_ROTOR_POS = "Rotorpos";



        // Strings that are used as labels:

        const string STR_TYPE = "Type";
        const string STR_TIME = "Time";
        const string STR_CURRENT = "Current";
        const string STR_TOLERANCE = "Tolerance";
        const string STR_INITIAL_ROTOR_POSITION = "Initial rotor position";

        //
        // private variables
        //

        private string phasingType;
        private UInt32 time;
        private bool timeDefined;
        private UInt32 current;
        private bool currentDefined;
        private byte tolerance;
        private bool toleranceDefined;
        private UInt16 initialrotorposition;
        private bool initialrotorpositionDefined;

        private  string[] phasingTypes = {STR_FORCED,
                                          STR_INITIAL_POSITION_ALLWAYS_KNOWN};

        private string parms;   // The parameter string

        private UserCombobox UserComboboxType;
        private UserTextbox UserTextboxTime;
        private UserTextbox UserTextboxCurrent;
        private UserTextbox UserTextboxTolerance;
        private UserTextbox UserTextboxRotorposition;





        //        private string[] homingDirections = {STR_POSITIVE,
        //                                             STR_NEGATIVE};

        public PhasedetectSequence()    // The constructor
        {
            name = "Phase detect";
            mnemonic = "PhaseDetect";
        }

        public override void  Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, phasingTypes);

            UserComboboxType = new UserCombobox(Globals.mainform.lblName1, STR_TYPE, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserTextboxTime = new UserTextbox(Globals.mainform.lblName2, STR_TIME, Globals.mainform.txtParm1, 0, UInt32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits2, Globals.STR_MSEC, false);
            UserTextboxCurrent = new UserTextbox(Globals.mainform.lblName3, STR_CURRENT, Globals.mainform.txtParm2, 0, UInt32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits3, Globals.STR_PROMILLE_OF_RATED_CURRENT, false);
            UserTextboxTolerance = new UserTextbox(Globals.mainform.lblName4, STR_TOLERANCE, Globals.mainform.txtParm3, 0, byte.MinValue, byte.MaxValue, Globals.mainform.lblUnits4, Globals.STR_PERCENT, false);
            UserTextboxRotorposition = new UserTextbox(Globals.mainform.lblName5, STR_INITIAL_ROTOR_POSITION, Globals.mainform.txtParm4, 0, UInt16.MinValue, UInt16.MaxValue, Globals.mainform.lblUnits5, Globals.STR_COUNTS, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxType = null;
            UserTextboxTime = null;
            UserTextboxCurrent = null;
            UserTextboxTolerance = null;
            UserTextboxRotorposition = null;
        }
        
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;


            phasingType = "";
            timeDefined = false;
            currentDefined = false;
            toleranceDefined = false;
            initialrotorpositionDefined = false;

            if (parms == "")       // Then use the default set of parameters
            {
                phasingType = STR_FORCED;
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case STR_PREFIX_TIME:
                            if (parameterValue != "")
                            {
                                time = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                timeDefined = true;
                            }
                            break;

                        case STR_INITIAL_POSITION_ALLWAYS_KNOWN:
                        case STR_FORCED:
                            phasingType = parameterName;
                            break;

                        case STR_PREFIX_TOLERANCE:
                            if (parameterValue != "")
                            {
                                tolerance = (byte)Globals.utils.ConvertStringToInt64(parameterValue);
                                toleranceDefined = true;
                            }
                            break;
                        case STR_PREFIX_CURRENT:
                            if (parameterValue != "")
                            {
                                current = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                currentDefined = true;
                            }
                            break;
                        case STR_PREFIX_ROTOR_POS:
                            if (parameterValue != "")
                            {
                                initialrotorposition = (UInt16)Globals.utils.ConvertStringToInt64(parameterValue);
                                initialrotorpositionDefined = true;
                            }
                            break;
                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {
            Globals.utils.HideAll();

            //
            // Set the layout of the labels and comboboxes, textboxes
            // and display the values

            UserTextboxTime.Hide();
            UserTextboxCurrent.Hide();
            UserTextboxTolerance.Hide();
            UserTextboxRotorposition.Hide();

            UserComboboxType.Show(0);

            switch (phasingType)
            {
                case STR_FORCED:
                    Globals.mainform.ComboBox1.SelectedItem = STR_FORCED;
                    UserTextboxTime.Show(1);
                    UserTextboxCurrent.Show(2);
                    UserTextboxTolerance.Show(3);
                    break;

                case STR_INITIAL_POSITION_ALLWAYS_KNOWN:
                    Globals.mainform.ComboBox1.SelectedItem = STR_INITIAL_POSITION_ALLWAYS_KNOWN;
                    UserTextboxRotorposition.Show(1);
                    break;
            }

            UserTextboxTime.SetValue(timeDefined ? time.ToString() : "");
            UserTextboxCurrent.SetValue(currentDefined ? current.ToString() : "");
            UserTextboxTolerance.SetValue(toleranceDefined ? tolerance.ToString() : "");
            UserTextboxRotorposition.SetValue(initialrotorpositionDefined ? initialrotorposition.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;
        }

        public override void UserInputHasChanged(Object sender)
        {
            phasingType = UserComboboxType.Text();
            UpdateParametersFromGui();
            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {
            switch (phasingType)
            {
            
                case STR_FORCED:
            
                    timeDefined = UserTextboxTime.HasValue();
                    if (timeDefined)
                    {
                        time =  (UInt32)UserTextboxTime.Value();
                    }
                    currentDefined = UserTextboxCurrent.HasValue();
                    if (currentDefined)
                    {
                        current = (UInt32) UserTextboxCurrent.Value();
                    }
                    toleranceDefined = UserTextboxTolerance.HasValue();
                    if (toleranceDefined)
                    {
                        tolerance = (byte)UserTextboxTolerance.Value();
                    }
                    break;

                case STR_INITIAL_POSITION_ALLWAYS_KNOWN:

                    initialrotorpositionDefined = UserTextboxRotorposition.HasValue();
                    if (initialrotorpositionDefined)
                    {
                        initialrotorposition = (UInt16) UserTextboxRotorposition.Value();
                    }
                    break;
            }
            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }

        private  void CreateCode()
        {
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            instructions.Clear();

            int type = 0;

            switch(phasingType)
            {
                case STR_FORCED:
                    type = 0;
                    break;
                case STR_INITIAL_POSITION_ALLWAYS_KNOWN:
                    type = 2;
                    break;
            }


            instructions.Add(new WriteInstruction(0x2100, 1, (int)type, strtemp + phasingType));           // Phasing type
            CreateCodeForSystemMacro(Bios.callnumber.MOTOR_OFF);
            instructions.Add(new WriteInstruction(0x2c01, 23, 0x6402, "Get motor type in ACCUM"));
            instructions.Add(new WriteInstruction(0x2c01, 21, 0x6402, "Rewrite Motortype to force reset of phasing"));
            switch (phasingType)
            {
                case STR_INITIAL_POSITION_ALLWAYS_KNOWN:
                    if (initialrotorpositionDefined)
                    {
                        instructions.Add(new WriteInstruction(0x2100, 5, initialrotorposition)); // Initial rotor position
                    }
                    break;

                case STR_FORCED:
                    if (timeDefined)
                    {
                        instructions.Add(new WriteInstruction(0x2100, 2, (int) time));                 // Phasing time
                    }
                    if (currentDefined)
                    {
                        instructions.Add(new WriteInstruction(0x2100, 3, (int) current));
                    }
                    if (toleranceDefined)
                    {
                        instructions.Add(new WriteInstruction(0x2100, 4, (int) tolerance));
                    }
                    break;
            }
            CreateCodeForSystemMacro(Bios.callnumber.MOTOR_ON);     // This will start the phasing
            //
            // Now wait until phasing is reached
            //
            instructions.Add(new WriteInstruction(0x2c01, 23, 0x6041, "Get status in ACCUM"));
            instructions.Add(new WriteInstruction(0x2c02, 11, 14, "Check phasing reached bit"));
            instructions.Add(new WriteInstruction(0x2c04, 6, -2, "Loop until phasing is reached"));

        }


        public override string GetParameterString()
        {
            string temp = phasingType.Replace(' ', '_');
            switch (phasingType)
            {
                case STR_INITIAL_POSITION_ALLWAYS_KNOWN:
                    temp = temp + Globals.STR_COMMA + STR_PREFIX_ROTOR_POS + "=";
                    if (initialrotorpositionDefined)
                    {
                        temp = temp + initialrotorposition.ToString();
                    }
                    break;
                case STR_FORCED:
                    temp = temp + Globals.STR_COMMA + STR_PREFIX_TIME + "=";
                    if (timeDefined)
                    {
                        temp = temp + time.ToString();
                    }

                    temp = temp + Globals.STR_COMMA + STR_PREFIX_CURRENT + "=";
                    if (currentDefined)
                    {
                        temp = temp + current.ToString();
                    }

                    temp = temp + Globals.STR_COMMA + STR_PREFIX_TOLERANCE + "=";
                    if (toleranceDefined)
                    {
                        temp = temp + tolerance.ToString();
                    }
                    break;
            }
            return temp;
        }
    }
}
