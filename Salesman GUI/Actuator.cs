﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public class Actuator
    {
        public string strNameDemo = ""; // holds ini file name for checking


        public string fullPath = ""; // holds path to file


        public void Load(string filename)
        {
           
            filename = filename.Replace(';', ';');

            // searches each file in the actuator config directory until chosen one is found
            foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
            {
                if (Path.GetFileNameWithoutExtension(s) == filename)
                {
                    strNameDemo = filename;
                    fullPath = s;

                   
                    configurationFileDemo config = new configurationFileDemo(strNameDemo);
                    





                    config.ReadfromFile(strNameDemo);
                                                           

                }
            }
        }



        public class configurationFileDemo
        {
            private List<long> indexes = new List<long>();
            private List<long> values = new List<long>();

            private string header = "[MotionLab;1.8.1.0R]"; // This is the default header, will be overridden when a readfile is done



            public configurationFileDemo(string fullPath)    // The constructor
            {


            }


            public void Init()
            {
                indexes.Clear();
                values.Clear();
                header = "";

            }

            public bool ReadfromFile(string strNameDemo)
            {
                bool bOk = true;
                Init();
                //foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
                //{
                foreach (string s in File.ReadAllLines(strNameDemo + ".mlc"))
                {
                    // searches each file in the actuator config directory until chosen one is found

                    if (s.Length != 0)
                    {
                        if (s[0] == '[')
                        {
                            header = s;
                        }
                        else
                        {
                            switch (s.Split(';').Count())
                            {
                                case 3:
                                    long index = Globals.utils.ConvertStringToInt64(s.Split(';')[0].Trim());
                                    long subindex = Globals.utils.ConvertStringToInt64(s.Split(';')[1].Trim());
                                    long value = Globals.utils.ConvertStringToInt64(s.Split(';')[2].Trim());
                                    Add(index + (subindex << 16), value);
                                    break;
                                default:
                                    bOk = false;
                                    break;
                            }
                        }
                    }

                }

                if (!bOk || (indexes.Count < 20))
                {
                    MessageBox.Show("File " + strNameDemo + " is not a valid configuration file");
                    return false;
                }
                return true;
            }




            public void Add(long index, long value)
            {
                int i;
                bool bFound = false;
                for (i = 0; i < indexes.Count; i++)
                {
                    if (indexes[i] == index)
                    {
                        values[i] = value;
                        bFound = true;
                    }
                }
                if (!bFound)
                {
                    indexes.Add(index);
                    values.Add(value);
                }
            }
            public int Count()
            {
                return indexes.Count;
            }
        }
              

        public string Name()
        {
            return strNameDemo;
        }

        public string[] Info()
        {
            List<string> info = new List<string>();

            info.Add("Actuator: " + strNameDemo);
            info.Add("");
            return info.ToArray();
        }

    }
}
