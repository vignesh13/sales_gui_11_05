﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{


    public class ForcemoveSequence : Sequence
    {

        const string STR_PREFIX_TARGET =       "Target";
        const string STR_PREFIX_SLOPE =        "Slope";

        const string STR_TARGET        = "Target force";
        const string STR_SLOPE         = "Profile slope";


        private string parms;   // The parameter string


        private bool targetforceDefined;
        private Int32 targetforce;

        private bool slopeDefined = false;
        private UInt32 slope;

        private UserTextbox UserTextboxTargetforce;
        private UserTextbox UserTextboxSlope;



        public ForcemoveSequence()    // The constructor
        {
            name = "Force move";
            mnemonic = "ForceMove";
        }

        public override void Init()
        {

            UserTextboxTargetforce = new UserTextbox(Globals.mainform.lblName1, STR_TARGET, Globals.mainform.txtParm1, 0, Int32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits1, Globals.STR_PROMILLE_OF_RATED_FORCE, false);
            UserTextboxSlope = new UserTextbox(Globals.mainform.lblName2, STR_SLOPE, Globals.mainform.txtParm2, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits2, Globals.STR_PROMILLE_OF_RATED_FORCE_PER_SEC, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserTextboxTargetforce = null;
            UserTextboxSlope = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            targetforceDefined = false;
            slopeDefined = false;

            if (parms == "")       // Then use the default set of parameters
            {
                targetforceDefined = false;
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {

                        case STR_PREFIX_TARGET:
                            if (parameterValue != "")
                            {
                                targetforce = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                                targetforceDefined = true;
                            }
                            break;


                        case STR_PREFIX_SLOPE:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                slope = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                slopeDefined = true;
                            }
                            break;


                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserTextboxTargetforce.Show(0);
            UserTextboxSlope.Show(1);

            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //

            UserTextboxTargetforce.SetValue(targetforceDefined ? targetforce.ToString() : "");
            UserTextboxSlope.SetValue(slopeDefined ? slope.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;
        }

        public override void UserInputHasChanged(Object sender)
        {

//
// This is not necessary for a position move
//            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
//            {
//                ShowParametersInGui();
//            }
        }


        public override void UpdateParametersFromGui()
        {

            targetforceDefined = UserTextboxTargetforce.HasValue();
            if (targetforceDefined)
            {
                targetforce = (Int32)UserTextboxTargetforce.Value();
            }

            slopeDefined = UserTextboxSlope.HasValue();
            if (slopeDefined)
            {
                slope = (UInt32)UserTextboxSlope.Value();
            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {

            string temp = STR_PREFIX_TARGET + "=";

            if (targetforceDefined)
            {
                temp = temp + targetforce.ToString();
            }

            temp = temp + Globals.STR_COMMA + STR_PREFIX_SLOPE + "=";
            if (slopeDefined)
            {
                temp = temp + slope.ToString();
            }
            return temp;            
        }

        
        private void CreateCode()
        {
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            instructions.Clear();


            if (slopeDefined)
            {
                instructions.Add(new WriteInstruction(0x6087, 0, (int) slope, strtemp));
                strtemp = "";
            }

            CreateCodeForSystemMacro(Bios.callnumber.FORCE_MOVE, strtemp + "Set profile force mode first. ");                           // Need to set operation mode first
            if (targetforceDefined)
            {
                instructions.Add(new WriteInstruction(0x6071, 0, (int)targetforce, "Apply target force"));   // Writing target torque will activate torque mode
            }
            else
            {
                instructions.Add(new WriteInstruction(0x2c01, 23, 0x6071, "Force not defined: Get target force in ACCUM"));
                instructions.Add(new WriteInstruction(0x2c01, 21, 0x6071, "Apply target force"));
            }
        }
    }
}
