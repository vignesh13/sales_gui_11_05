﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    class UserInputField
    {
        protected Label lblName;
        protected Control control;
        protected Label lblUnits;

        public UserInputField()             // The constructor
        {
        }
        
        protected virtual void SetVisibility(bool visibility)
        {
            lblName.Visible = visibility;
            control.Visible = visibility;
            lblUnits.Visible = visibility;
        }

        public void Hide()
        {
            SetVisibility(false);
        }

        public void Show()
        {
            SetVisibility(true);
        }

        public void Show(int row)
        {
            SetVisibility(true);
            setposition(row);
        }

        public virtual void setposition(int row)
        {
            int topOfRow0 = Globals.mainform.cmbFunction.Top + 23;
            lblName.Left = 8;
            lblName.Top = topOfRow0 + 23 * row;
            control.Top = topOfRow0 + 23 * row;
            control.TabIndex = 500 + row;

            lblUnits.Left = control.Right + 8;
            lblUnits.Top = topOfRow0 + 23 * row;
        }


        public string Text()
        {
            if (control.Text != null)
            {
                return control.Text;
            }
            else
            {
                return "";
            }
        }

    }

    class UserTextbox : UserInputField
    {
        private Int64 min;
        private Int64 max;

        public UserTextbox(Label namelabel, string name, TextBox txtValue, Int64 value, Int64 minvalue, Int64 maxvalue, Label unitslabel, string units, bool mandatory)
        {
            lblName = namelabel;
            namelabel.Text = (mandatory ? "*" : "") + name;     // Mark mandatory items with a *
//            namelabel.Text = name;      // Do not mark mandatory items as they are obsolete
            control = txtValue;
            lblUnits = unitslabel;
            unitslabel.Text = units;
            min = minvalue;
            max = maxvalue;
        }

        public bool HasValue()
        {
            return control.Text.Trim() != "";
        }

        public Int64 Value()
        {
            Int64 value = 0;
            if (Int64.TryParse(control.Text.Trim(), out value))
            {
                if ((value < min) || (value > max))
                {
                    MessageBox.Show("The value " + control.Text + " for [" + lblName.Text + "] is out of limits (allowed range is " + min.ToString() + " to " + max.ToString() + ")");
                    value = min;
                }
            }
            else
            {
                MessageBox.Show("The value " + control.Text + " for [" + lblName.Text + "] is not in a valid numeric format");
                value = min;
            }
            return value;
        }


        public void SetValue(string value)
        {
            control.Text = value;
        }
    }

    class UserCombobox : UserInputField
    {
        public UserCombobox(Label namelabel, string name, ComboBox cmbValue, Label unitslabel, string units)
        {
            lblName = namelabel;
            namelabel.Text = name;
            control = cmbValue;
            lblUnits = unitslabel;
            unitslabel.Text = units;
        }
    }

    class UserObjectselector : UserInputField
    {
        private Button selectButton = null;

        public string selectedIndexString = "";

        private string currentSelection = "";

        private ObjectSelector selector = new ObjectSelector();




        public UserObjectselector(Label namelabel, string name, Button btnValue, Label unitslabel, string units, bool readable, bool writable)
        {
            lblName = namelabel;
            namelabel.Text = name;
            control = btnValue;
            lblUnits = unitslabel;
            unitslabel.Text = units;
            selectButton = btnValue;
            selector.Init(readable, writable);
        }

        private void SetButtonText()
        {
            if (selector.selectedIndex == 0)
            {
                selectButton.Text = "Click to select variable";
            }
            else
            {
                selectButton.Text = selector.groupname + selector.objectname;
            }
        }

        public void SelectObject()
        {
            selector.SetSelection(selectedIndexString);
            if (selector.ShowDialog() == DialogResult.OK)
            {
                selectedIndexString = selector.fullname;
            }
            SetButtonText();
        }

        public void SetSelection(string selection)
        {
            currentSelection = selection;
            selector.SetSelection(currentSelection);
            selectedIndexString = selector.fullname;
            SetButtonText();
        }

        public void SetSelection(int index)
        {
            string temp = "(0x" + index.ToString("X6") + ")";
            SetSelection(temp);
        }

        public int GetIndex(string fullstring)
        {
            return selector.getindex(fullstring);
        }

        public int GetSelectedIndex()
        {
            return selector.selectedIndex;
        }

        public void ShowMessageIfNotSelected(bool bCheck)
        {
            if (selectedIndexString == "" && bCheck)
            {
                MessageBox.Show("No variable selected for " + lblName.Text + "\n\nClick in the input box to select a variable", "Incomplete function", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
