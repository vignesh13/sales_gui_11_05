﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class WaitSequence : Sequence
    {

        private string parms;   // The parameter string

        private string waitType = "";
        private string comparison = "";
        private string activeState = "";

        private UInt32 timeout;
        private Int32 threshold;

        public string[] waitTypes = {Globals.STR_TIME,
                                    Globals.STR_TARGET_REACHED,
                                    Globals.STR_TRAJECTORY_GENERATOR_READY,
                                    Globals.STR_INDEX_PULSE,
                                    Globals.STR_ACTUAL_POSITION,
                                    Globals.STR_ACTUAL_VELOCITY,
                                    Globals.STR_ACTUAL_FORCE,
                                    Globals.STR_DIGITAL_INPUT_0,
                                    Globals.STR_DIGITAL_INPUT_1,
                                    Globals.STR_DIGITAL_INPUT_2,
                                    Globals.STR_DIGITAL_INPUT_3,
                                    Globals.STR_ANALOG_INPUT_1,
                                    Globals.STR_ANALOG_INPUT_2 };

        private string[] comparisons = {Globals.STR_ABOVE,
                                        Globals.STR_BELOW};

        private string[] activeStates = {Globals.STR_ON,
                                         Globals.STR_OFF};

        private UserCombobox UserComboboxWaittype;
        private UserCombobox UserComboboxComparisons;
        private UserCombobox UserComboboxActivestates;

        private UserTextbox UserTextboxTimeout;
        private UserTextbox UserTextboxPositionthreshold;
        private UserTextbox UserTextboxVelocitythreshold;
        private UserTextbox UserTextboxForcethreshold;
        private UserTextbox UserTextboxAnalogvaluethreshold;




        public WaitSequence()    // The constructor
        {
            name = "Wait";
            mnemonic = "Wait";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, waitTypes);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, comparisons);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox3, activeStates);

            UserComboboxWaittype = new UserCombobox(Globals.mainform.lblName1, Globals.STR_CONDITION, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxComparisons = new UserCombobox(Globals.mainform.lblName2, "", Globals.mainform.ComboBox2, Globals.mainform.lblUnits2, "");
            UserComboboxActivestates = new UserCombobox(Globals.mainform.lblName3, "", Globals.mainform.ComboBox3, Globals.mainform.lblUnits3, "");

            UserTextboxTimeout = new UserTextbox(Globals.mainform.lblName4, Globals.STR_TIME, Globals.mainform.txtParm1, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits4, Globals.STR_MSEC, true);
            UserTextboxPositionthreshold = new UserTextbox(Globals.mainform.lblName5, Globals.STR_THRESHOLD, Globals.mainform.txtParm2, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits5, Globals.STR_COUNTS, true);
            UserTextboxVelocitythreshold = new UserTextbox(Globals.mainform.lblName6, Globals.STR_THRESHOLD, Globals.mainform.txtParm3, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits6, Globals.STR_COUNTS_PER_SEC, true);
            UserTextboxForcethreshold = new UserTextbox(Globals.mainform.lblName7, Globals.STR_THRESHOLD, Globals.mainform.txtParm4, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits7, Globals.STR_PROMILLE_OF_RATED_FORCE, true);
            UserTextboxAnalogvaluethreshold = new UserTextbox(Globals.mainform.lblName8, Globals.STR_THRESHOLD, Globals.mainform.txtParm5, 0, 0, 1023, Globals.mainform.lblUnits8, "", true);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxWaittype = null;
            UserComboboxComparisons = null;
            UserComboboxActivestates = null;

            UserTextboxTimeout = null;
            UserTextboxPositionthreshold = null;
            UserTextboxVelocitythreshold = null;
            UserTextboxForcethreshold = null;
            UserTextboxAnalogvaluethreshold = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;


            // Set the defaults

            timeout = 0;
            threshold = 0;
            waitType = Globals.STR_TIME;

            if (parms != "")       // Then use the default set of parameters
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_TIME:
                        case Globals.STR_TARGET_REACHED:
                        case Globals.STR_TRAJECTORY_GENERATOR_READY:
                        case Globals.STR_INDEX_PULSE:
                        case Globals.STR_ACTUAL_POSITION:
                        case Globals.STR_ACTUAL_VELOCITY:
                        case Globals.STR_ACTUAL_FORCE:
                        case Globals.STR_DIGITAL_INPUT_0:
                        case Globals.STR_DIGITAL_INPUT_1:
                        case Globals.STR_DIGITAL_INPUT_2:
                        case Globals.STR_DIGITAL_INPUT_3:
                        case Globals.STR_ANALOG_INPUT_1:
                        case Globals.STR_ANALOG_INPUT_2:
                            waitType = parameterName;
                            break;

                        case Globals.STR_ABOVE:
                        case Globals.STR_BELOW:
                            comparison = parameterName;
                            break;

                        case Globals.STR_ON:
                        case Globals.STR_OFF:
                            activeState = parameterName;
                            break;

                        case Globals.STR_PREFIX_TIMEOUT:   // This is an optional parameter
                            timeout = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_THRESHOLD:      // This is an optional parameter, may be empty
                            threshold = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserComboboxWaittype.Hide();
            UserComboboxComparisons.Hide();
            UserComboboxActivestates.Hide();

            UserTextboxPositionthreshold.Hide();
            UserTextboxVelocitythreshold.Hide();
            UserTextboxForcethreshold.Hide();
            UserTextboxTimeout.Hide();
            UserTextboxAnalogvaluethreshold.Hide();

            Globals.mainform.ComboBox1.SelectedItem = waitType;
            Globals.mainform.ComboBox2.SelectedItem = comparison;
            Globals.mainform.ComboBox3.SelectedItem = activeState;
            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //
            UserComboboxWaittype.Show(0);       // Allways show the selected wait type
            switch (waitType)
            {
                case Globals.STR_TIME:
                    UserTextboxTimeout.Show(1);
                    break;

                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                case Globals.STR_INDEX_PULSE:
                            // Nothing to show
                    break;

                case Globals.STR_ACTUAL_POSITION:
                    UserComboboxComparisons.Show(1);
                    UserTextboxPositionthreshold.Show(2);
                    break;

                case Globals.STR_ACTUAL_VELOCITY:
                    UserComboboxComparisons.Show(1);
                    UserTextboxVelocitythreshold.Show(2);
                    break;

                case Globals.STR_ACTUAL_FORCE:
                    UserComboboxComparisons.Show(1);
                    UserTextboxForcethreshold.Show(2);
                    break;

                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    UserComboboxActivestates.Show(1);

                    break;

                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    UserComboboxComparisons.Show(1);
                    UserTextboxAnalogvaluethreshold.Show(2);
                    break;
 
                default:
                    break;
            }


            UserTextboxTimeout.SetValue(timeout.ToString());
            UserTextboxPositionthreshold.SetValue(threshold.ToString());
            UserTextboxVelocitythreshold.SetValue(threshold.ToString());
            UserTextboxForcethreshold.SetValue(threshold.ToString());
            UserTextboxAnalogvaluethreshold.SetValue(threshold.ToString());


            Globals.mainform.txtComment.Text = strComment;

        }


        public override void UserInputHasChanged(Object sender)
        {
            waitType = Globals.mainform.ComboBox1.Text;
            UpdateParametersFromGui();
            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {


            waitType = UserComboboxWaittype.Text();
            activeState = UserComboboxActivestates.Text();
            comparison = UserComboboxComparisons.Text();

            switch (waitType)
            {
                case Globals.STR_TIME:
                    timeout = (UInt32)UserTextboxTimeout.Value();
                    break;

                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                case Globals.STR_INDEX_PULSE:
                    break;

                case Globals.STR_ACTUAL_POSITION:
                    threshold = (Int32)UserTextboxPositionthreshold.Value();
                    break;

                case Globals.STR_ACTUAL_VELOCITY:
                    threshold = (Int32)UserTextboxVelocitythreshold.Value();
                    break;

                case Globals.STR_ACTUAL_FORCE:
                    threshold = (Int32)UserTextboxForcethreshold.Value();
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    break;

                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    threshold = (Int32)UserTextboxAnalogvaluethreshold.Value();
                    break;

            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {
            string temp = waitType.Replace(' ', '_');

            switch (waitType)
            {

                case Globals.STR_TIME:
                    temp += Globals.STR_COMMA + Globals.STR_PREFIX_TIMEOUT + "=" + timeout.ToString();
                    break;
                case Globals.STR_TARGET_REACHED:
                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                case Globals.STR_INDEX_PULSE:
                    break;

                case Globals.STR_ACTUAL_POSITION:
                case Globals.STR_ACTUAL_VELOCITY:
                case Globals.STR_ACTUAL_FORCE:
                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    temp += Globals.STR_COMMA + comparison + Globals.STR_COMMA + Globals.STR_PREFIX_THRESHOLD + "=" + threshold.ToString();
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    {
                        temp += Globals.STR_COMMA + activeState;
                    }
                    break;
            }
            return temp;            
        }

        private void CreateCode()
        {

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            int registerToRead = 0;
            int inputBit = 0;
            bool Is32Bits   = true;    // assume that this is a 32 bits value
            bool extendSign = false; // assume that sign extension is not necessary

            instructions.Clear();

            // First case is to do some preparations

            switch (waitType)
            {
                case Globals.STR_ACTUAL_POSITION:
                    registerToRead = 0x6064;
                    break;
                case Globals.STR_ACTUAL_VELOCITY:
                    registerToRead = 0x606c;
                    break;
                case Globals.STR_ACTUAL_FORCE:
                    registerToRead = 0x6077;
                    Is32Bits = false;
                    extendSign = true;
                    break;
                case Globals.STR_ANALOG_INPUT_1:
                    registerToRead = 0x12a03;
                    break;
                case Globals.STR_ANALOG_INPUT_2:
                    registerToRead = 0x22a03;
                    break;
                case Globals.STR_DIGITAL_INPUT_0:
                    inputBit = 4;
                    break;
                case Globals.STR_DIGITAL_INPUT_1:
                    inputBit = 5;
                    break;
                case Globals.STR_DIGITAL_INPUT_2:
                    inputBit = 6;
                    break;
                case Globals.STR_DIGITAL_INPUT_3:
                    inputBit = 7;
                    break;
            }

            switch (waitType)
            {
                case Globals.STR_TIME:
                    instructions.Add(new WriteInstruction(0x2c02, 13, (int)timeout, strtemp));
                    break;
                case Globals.STR_TARGET_REACHED:
                    CreateCodeForSystemMacro(Bios.callnumber.WAIT_FOR_TARGET_REACHED, strtemp);
                    break;

                case Globals.STR_TRAJECTORY_GENERATOR_READY:
                    instructions.Add(new WriteInstruction(0x2c01, 22, 0x6062, strtemp + "Get position demand value in ACCUM"));
                    instructions.Add(new WriteInstruction(0x2c02, 22, 0x607a, "Compare with target position"));
                    instructions.Add(new WriteInstruction(0x2c04, 6, -2, "Loop until position demand value = target position"));
                    break;

                case Globals.STR_INDEX_PULSE:
                    instructions.Add(new WriteInstruction(0x2c02, 14, 0, strtemp));
                    break;

                case Globals.STR_ACTUAL_POSITION:
                case Globals.STR_ACTUAL_VELOCITY:
                case Globals.STR_ACTUAL_FORCE:
                case Globals.STR_ANALOG_INPUT_1:
                case Globals.STR_ANALOG_INPUT_2:
                    int jumpOffset = -2;
                    instructions.Add(new WriteInstruction(0x2c01, Is32Bits ? 22 : 23, registerToRead, strtemp + "Get value in ACCUM"));
                    if (extendSign)
                    {
                        instructions.Add(new WriteInstruction(0x2c02, 10, 15, "If 16 bits value is negative"));
                        instructions.Add(new WriteInstruction(0x2c01, 6, -65536, "Sign extend"));       // -65536 equals 0xFFFF0000
                        jumpOffset += -2;
                    }
                    instructions.Add(new WriteInstruction(0x2c02, comparison == Globals.STR_ABOVE ? 4 : 5, threshold, "Compare against threshold"));
                    instructions.Add(new WriteInstruction(0x2c04, 6, jumpOffset, "Repeat until condition is reached"));
                    break;

                case Globals.STR_DIGITAL_INPUT_0:
                case Globals.STR_DIGITAL_INPUT_1:
                case Globals.STR_DIGITAL_INPUT_2:
                case Globals.STR_DIGITAL_INPUT_3:
                    instructions.Add(new WriteInstruction(0x2c02, activeState == Globals.STR_ON ? 16 : 15, inputBit, strtemp));
                    break;
            }
        }
    }
}
