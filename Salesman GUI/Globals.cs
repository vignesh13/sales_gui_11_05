﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Salesman_GUI
{
    public enum DataType
    {
        UINT8,
        UINT16,
        UINT32,
        UINT64,
        INT8,
        INT16,
        INT32,
        INT64,
        VISIBLE_STRING
    };

    public enum AccessType
    {
        RO,
        RW,
        WO,
        CONST
    };


    static class Globals
    {
        public const string STR_COUNTS          = "Counts";
        public const string STR_COUNTS_PER_SEC  = "Counts/sec";
        public const string STR_COUNTS_PER_SEC2 = "Counts/sec² ";
        public const string STR_MSEC =            "msec";
        public const string STR_PERCENT =         "%";

        public static string POS_UNITS = "";
        public static string VEL_UNITS = POS_UNITS + "/s";
        public static string ACC_UNITS = "";
        public static string FORCE_UNITS = "N";
        public static string TORQ_UNITS = "mNm";
        public static bool pos_unit_assigned = false;

        public static double force_constant = 1;
        public static bool force_constant_asssigned = false;
        public static double mrc = 1;
        public static bool mrc_assigned = false;
        public static double Encoder_resoultion = 0;
        public static bool Encoder_res_asigned = false;
        public const string STR_COMMA = ",";

        public const string STR_DIRECTION = "Direction";

        public const string STR_POSITIVE = "Positive";
        public const string STR_NEGATIVE = "Negative";



        // Common strings for Wait and If sequences

        // The combobox for the primary selection:

        public const string STR_TIME = "Time";
        public const string STR_TARGET_REACHED = "Target reached";
        public const string STR_TRAJECTORY_GENERATOR_READY = "Trajectory generator ready";
        public const string STR_ACTUAL_POSITION = "Actual position";
        public const string STR_INDEX_PULSE = "Index pulse";
        public const string STR_ACTUAL_VELOCITY = "Actual velocity";
        public const string STR_ACTUAL_FORCE = "Actual force";
        public const string STR_DIGITAL_INPUT_0 = "Digital input 0";
        public const string STR_DIGITAL_INPUT_1 = "Digital input 1";
        public const string STR_DIGITAL_INPUT_2 = "Digital input 2";
        public const string STR_DIGITAL_INPUT_3 = "Digital input 3";
        public const string STR_ANALOG_INPUT_1 = "Analog input 1";
        public const string STR_ANALOG_INPUT_2 = "Analog input 2";
        public const string STR_VARIABLE = "Variable";
        public const string STR_CONSTANT = "Constant";

        public const string STR_DIGITAL_OUTPUT_0 = "Digital output 0";
        public const string STR_DIGITAL_OUTPUT_1 = "Digital output 1";
        public const string STR_DIGITAL_OUTPUT_2 = "Digital output 2";
        public const string STR_DIGITAL_OUTPUT_3 = "Digital output 3";
        public const string STR_ANALOG_OUTPUT_10_BITS = "Analog output 10 bits";
        public const string STR_ANALOG_OUTPUT_16_BITS = "Analog output 16 bits";
        public const string STR_ANALOG_OUTPUT_AUTOMATIC = "Analog output automatic";

        public const string STR_SOURCE_REGISTER = "Source register";
        public const string STR_ENABLE = "Enable";
        public const string STR_DISABLE = "Disable";
        public const string STR_DESTINATION_OUTPUT = "Destination output";
        public const string STR_MAX_REPRESENTED_VALUE = "Max represented value";




        public const string STR_OUTPUT = "Output";

        // The combobox selections for the analog inputs and actual profile values

        public const string STR_ABOVE = "Higher";
        public const string STR_BELOW = "Lower";
        public const string STR_EQUAL = "Equal";
        public const string STR_UNEQUAL = "Unequal";


        // The combobox for the Digital inputs

        public const string STR_ON = "On";
        public const string STR_OFF = "Off";

        public const string STR_PREFIX_TIMEOUT = "Timeout";               // Caution, do not use Time here, this conflicts with the Time parameter
        public const string STR_PREFIX_THRESHOLD = "Threshold";
        public const string STR_PREFIX_VALUE = "Value";


        public const string STR_PREFIX_MAX_REPRESENTED_VALUE = "Max";
        public const string STR_PREFIX_DESTINATION_OUTPUT = "Dest";
        public const string STR_PREFIX_SOURCE_REGISTER = "Source";





        public const string STR_CONDITION = "Condition";
        public const string STR_VALUE =     "Value";
        public const string STR_THRESHOLD = "Threshold";



        public const string STR_MACRO_CONTINUE = "Macro continue";
        public const string STR_MACRO_JUMP = "Macro jump";
        public const string STR_MACRO_CALL = "Macro call";
        public const string STR_MACRO_RETURN = "Macro return";

        public const string STR_JUMP = "Jump";
        public const string STR_CALL = "Call";
        public const string STR_RETURN = "Return";
        public const string STR_REPEAT = "Repeat";
        public const string STR_UNPUSH = "Unpush";
        public const string STR_END_PROGRAM = "End program";

        public const string STR_ACTION = "Action";


        public const string STR_MACRO_NUMBER = "Macro number";
        public const string STR_PREFIX_MACRO_NUMBER = "MacroNumber";

        public const string STR_REPEAT_COUNT = "Repeat count";
        public const string STR_PREFIX_REPEAT_COUNT = "RepeatCount";

        public const string STR_THEN = "Then";
        public const string STR_ELSE = "Else";

        public const string STR_PREFIX_THEN_MACRO_JUMP =     "Then Macro Jump";
        public const string STR_PREFIX_THEN_MACRO_CALL =     "Then Macro Call";
        public const string STR_PREFIX_THEN_MACRO_RETURN =   "Then Macro Return";
        public const string STR_PREFIX_THEN_MACRO_CONTINUE = "Then Macro Continue";
        public const string STR_PREFIX_THEN_END_PROGRAM =    "Then End Program";

        public const string STR_PREFIX_ELSE_MACRO_JUMP =     "Else Macro Jump";
        public const string STR_PREFIX_ELSE_MACRO_CALL =     "Else Macro Call";
        public const string STR_PREFIX_ELSE_MACRO_RETURN =   "Else Macro Return";
        public const string STR_PREFIX_ELSE_MACRO_CONTINUE = "Else Macro Continue";
        public const string STR_PREFIX_ELSE_END_PROGRAM =    "Else End Program";

        public const string STR_PROMILLE_OF_RATED_FORCE = "‰ of rated force";
        public const string STR_PROMILLE_OF_RATED_FORCE_PER_SEC = "‰ of rated force/sec";
        public const string STR_PROMILLE_OF_RATED_CURRENT = "‰ of rated current";
        public static Logfile logSerial = new Logfile();
        public static Logfile loglive = new Logfile();
        public static ObjectDictionary dictionary = new ObjectDictionary();
        public static Sequences sequences = new Sequences();

        public static MainForm mainform;
        public static Utils utils = new Utils();
        
    }
}
