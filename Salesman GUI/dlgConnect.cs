﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public partial class dlgConnect : Form
    {
        public dlgConnect()
        {
            InitializeComponent();
        }

        public void Init(int node)
        {
            cmbNode.Items.Clear();
            for (int i = 0; i < Globals.mainform.Network.Count; i++)
            {
                cmbNode.Items.Add(Globals.mainform.Network[i].ID.ToString());
            }
            cmbNode.SelectedItem = node.ToString();
            if (cmbNode.SelectedIndex == -1)    // Then the requested node is not in the list
            {
                cmbNode.SelectedIndex = 0;
            }
        }
        public int getSelectedNode()
        {
            return int.Parse(cmbNode.SelectedItem.ToString());
        }
    }
}
