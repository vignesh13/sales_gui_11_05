﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Salesman_GUI
{
    public partial class ContactInfo : Form
    {
        public ContactInfo()
        {
            InitializeComponent();
            
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:support@smac-mca.nl");
            DialogResult = DialogResult.OK;
        }

        private void linkSmacInternational_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.smac-mca.com");
            DialogResult = DialogResult.OK;
        }

        private void linkSmacDutch_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.smac-mca.nl");
            DialogResult = DialogResult.OK;
        }
    }
}
