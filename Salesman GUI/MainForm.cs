﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;
using System.Reflection;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms.DataVisualization.Charting;
using System.Diagnostics;




namespace Salesman_GUI
{

    public partial class MainForm : Form
    {
        //
        // Texts that go in the header
        //

        private const string strName = "SMAC_Salesman_GUI";
        private const string strVersion = "0.26";
        //        private const string strRemark = " *** This version is for test and review only ***";
        private const string strRemark = "";

        //public InputBoxDouble ibdTargetPositionDemo = null;
        private const string COL_COMMAND = "Command";
        private const string COL_PARAMETER = "Parameter";
        private const string COL_COMMENT = "Comment";
        private const string COL_COUNT = "Count";
        private const string COL_COUNT_SAVE = "NumberSave";

        private const string COL_LINE = "Line";
        private const string COL_COMMENTED_OUT = "CommentedOut";

        private const string STR_COMMENTED_OUT = "X";

        private const string STR_START = "Start Logging";
        private const string STR_STOP = "Stop Logging";

        private bool loggingLive = false;
        private bool loggingLiveMeasure = false;
        private string logfileName;
        
        private bool bReconnect = false;

        private bool bTuningmode = false;
        private bool bCapture = false;

        public bool bSilentmode = false;
        private bool bLock = true;

        private bool bMacroNumberRequested = false;
        private bool bMacroLineRequested = false;

        private const int WRITE_DELAY_PER_NODE = 20;    // Necessary for daisy chain mode Ingenia states 12 msec, 20 msec to make sure

        private int tracesamples;
        private int tracesamplesMeasure;
        private int traceinterval;
        private int traceintervalMeasure;

        private int sendobject1counter = 0;
        private int sendobject2counter = 0;

        private int statuscounter = 0;

        private int numPorts;

        private int lastTab = 0;

        //
        // Selection strings for the Trace intervals
        //

       

        private string[] intervalSelections = new string[]{
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000"};

        private string[] intervalSelectionsMeasure = new string[]{
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000"};

        private string[] samplesSelections = new string[]{
            "50",
            "100",
            "250"};

        private string[] samplesSelectionsMeasure = new string[]{
            "50",
            "100",
            "250"};

        private const string STR_DEFAULT_FILETYPE_READ_LCC = "LCC Control Center files (*.lcc)|*.lcc|Legacy SMAC Control Center files (*.scc)|*.scc";
        private const string STR_DEFAULT_FILETYPE_WRITE_LCC = "LCC Control Center files (*.lcc)|*.lcc";
        private const string STR_DEFAULT_FILETYPE_MLM = "LCC macro files (*.mlm)|*.mlm";
        private const string STR_DEFAULT_FILETYPE_MLC = "LCC config files (*.mlc)|*.mlc";
        private string inputFile = "";
        private string outputFile = "";     // Used in silent mode
        private string errorFile = "";      // Used in silent mode


        private string backupFolderName = "";


        public bool bConnected = false;
        public int activeNode = 32;
        public Node currentNode = null;
        public List<int> nodes = new List<int>();
        private int iWatchdog = 0;          // Used to check controller activity
        private Bios bios = new Bios();

        private bool bUnsavedChanges = false;       //  This bool is set if there are unsaved changes
        private bool bUndownloadedMacros = true;

        public bool bNetworkChanged = false;

        bool bConfigurationDownloaded = false;

        private int protectionLevel = 0;

        private Stopwatch stopWatch = new Stopwatch();

        private Stopwatch stopwatchMeasure = new Stopwatch();

        private class Message
        {
            public int index;
            public long value;
            public Message(int i, long v)
            {
                index = i;
                value = v;
            }
        }

        private List<Message> received = new List<Message>();

        private struct MacroData
        {
            public int hits;
            public int instructions;
        };

        private UserObjectselector selectorWritevariable;

        private LedDisplay_8Bits IoLeds_1;
        private LedDisplay_8Bits IoLeds_2;

        private dlgOutputWarning dlgoutputwarning = new dlgOutputWarning();
        private dlgTuningWarning dlgtuningwarning = new dlgTuningWarning();
        private dlgSaveComposer dlgsavecomposer = new dlgSaveComposer();

        private NodeSelector nodeSelectorActivenode = null;

        //  private NodeSelector nodeSelectorVariableWriter = null;

        private NodeSelector nodeSelector1 = null;
        private NodeSelector nodeSelector2 = null;

        private NodeSelector nodeSelector3 = null;
        private NodeSelector nodeSelector4 = null;



        private List<NodeSelector> nodeselectors = new List<NodeSelector>();

        private int errorCode = 0;

        private dlgOutputWindow outputWindow = null;

        public class ErrorDescription
        {
            public int errorNumber;
            public string ErrorString;

            public ErrorDescription(int number, string description)
            {
                errorNumber = number;
                ErrorString = description;
            }
        }

        public class NodeSelector
        {
            public ComboBox cmbNode = null;
            private bool zeroallowed = false;
            private int initialselection = -1;

            public NodeSelector(ComboBox combobox, bool ZeroAllowed)
            {
                cmbNode = combobox;
                zeroallowed = ZeroAllowed;
            }

            public void SetInitialselection(int selection)
            {
                initialselection = selection;
            }
            //
            // Refresh must be called after connection to the network
            //
            // Will rebuild the combobox nodes and try to re-select the node
            // Will disable the control if there is only one node on the network
            //

            public void Refresh()
            {
                cmbNode.Items.Clear();
                if (zeroallowed && (Globals.mainform.Network.Count > 1))
                {
                    cmbNode.Items.Add(0);
                }

                foreach (Node n in Globals.mainform.Network)
                {
                    cmbNode.Items.Add(n.ID);
                }

                switch (initialselection)
                {
                    case -1:
                        cmbNode.SelectedIndex = 0;
                        if ((int)cmbNode.SelectedItem == 0)
                        {
                            cmbNode.SelectedIndex++;
                        }
                        break;

                    case 0:
                        if (zeroallowed)
                        {
                            cmbNode.SelectedIndex = 0;
                        }
                        break;

                    default:
                        bool bFound = false;
                        for (int i = 0; i < cmbNode.Items.Count && !bFound; i++)
                        {
                            if (((int)cmbNode.Items[i]) == initialselection)
                            {
                                cmbNode.SelectedIndex = i;
                                bFound = true;
                            }
                        }
                        if (!bFound)
                        {
                            cmbNode.SelectedIndex = 0;
                            if ((int)cmbNode.SelectedItem == 0)
                            {
                                cmbNode.SelectedIndex++;
                            }
                            Globals.mainform.bNetworkChanged = true;
                        }
                        break;
                }
                cmbNode.Enabled = (Globals.mainform.Network.Count >= 2);
            }

            public int GetSelectedNode()
            {
                if (cmbNode.SelectedIndex == -1)
                {
                    return -1;
                }
                else
                {
                    return int.Parse(cmbNode.SelectedItem.ToString());
                }
            }
        }

        public int NetworkDelay()
        {
            return WRITE_DELAY_PER_NODE * Network.Count;
        }

        public class Node
        {
            public int ID;
            public bool systemMacrosLoaded = false;
            bool bWarningIncorrectFirmwareShown = false;

            private string strDeviceName = "";
            private string strHardwareVersion = "";
            public string strFirmwareVersion = "";

            private long iSerialNumber;

            private long iNominalCurrent = 0;        // Not supported on all firmware versions
            private long iPreamplifierGain = 0;      // Not supported on all firmware versions

            private long iProtectedAccess = 0;

            public Node(int nodeID)     // The constructor
            {
                ID = nodeID;
            }

            public long GetProtectedAccess()        // May only be called if serial async communication is disabled

            {
                Globals.mainform.port.GetObjectValue(ID, 0x42C05, ref iProtectedAccess);  // Get the protected access bit
                return iProtectedAccess;
            }


            public void GetConnectionInfo(bool bSilentmode = true)
            {
                long iValue1 = 0;
                long iValue2 = 0;
                long iValue3 = 0;


                string message = "";
                if (Globals.mainform.bConnected)
                {
                    bool bAsyncSave = Globals.mainform.bAsyncCommunicationEnabled;
                    Globals.mainform.bAsyncCommunicationEnabled = false;

                    bool bDataReceived = false;




                    for (int trycounter = 1; (trycounter <= 2) && !bDataReceived; trycounter++)
                    {
                        Thread.Sleep(100);  // Some delay before sending data to the serial port

                        if (Globals.mainform.port.GetObjectValue(ID, 0x1008, ref iValue1) &&            // Get the device name
                            Globals.mainform.port.GetObjectValue(ID, 0x1009, ref iValue2) &&            // Get the hardware version
                            Globals.mainform.port.GetObjectValue(ID, 0x100A, ref iValue3) &&            // Get the software version       
                            Globals.mainform.port.GetObjectValue(ID, 0x41018, ref iSerialNumber))       // Get the serial number
                        {
                            bDataReceived = true;
                            iNominalCurrent = 0;        // Not supported on all firmware versions
                            iPreamplifierGain = 0;      // Not supported on all firmware versions

                            Globals.mainform.port.GetObjectValue(ID, 0xA2FF0, ref iNominalCurrent);

                            if (iNominalCurrent != 0)
                            {
                                Globals.mainform.port.GetObjectValue(ID, 0x22FF0, ref iPreamplifierGain);  // Get the preamplifier gain
                            }

                            strDeviceName = Globals.utils.convertInt64ToVisibleString(iValue1);
                            strHardwareVersion = Globals.utils.convertInt64ToVisibleString(iValue2);
                            strFirmwareVersion = Globals.utils.convertInt64ToVisibleString(iValue3);

                            switch (iPreamplifierGain)
                            {
                                case 3030:
                                    strHardwareVersion = "1.1";
                                    break;
                                case 3485:
                                    strHardwareVersion = "1.0";
                                    break;
                                default:
                                    strHardwareVersion = "Unknown";
                                    break;
                            }
                            iProtectedAccess = GetProtectedAccess();
                        }
                    }
                    Globals.mainform.bAsyncCommunicationEnabled = bAsyncSave;

                    string strCurrent = "";

                    if (iNominalCurrent != 0)
                    {
                        strCurrent = ((double)iNominalCurrent / 1000.0).ToString().Replace(',', '.') + " Amp";
                    }
                    else
                    {
                        strCurrent = "Unknown";
                    }

                    message = "You are connected to the controller with node number " + ID.ToString() + ".\n\n" +
                              "Device name = " + strDeviceName + "\n" +
                              "Hardware version = " + strHardwareVersion + "\n" +
                              "Firmware version = " + strFirmwareVersion + "\n" +
                              "Nominal current = " + strCurrent + "\n" +
                              "Serial number = " + iSerialNumber.ToString() + "\n" +
                              "Protected access bit = ";
                    message += (strFirmwareVersion[0] >= '2') ? iProtectedAccess.ToString() : "not implemented";

                    if (Globals.mainform.Network.Count > 1)
                    {
                        message += "\n\nNodes on network = ";
                        for (int i = 0; i < Globals.mainform.Network.Count; i++)
                        {
                            if (i > 0)
                            {
                                message += ", ";
                            }
                            message += Globals.mainform.Network[i].ID.ToString();
                        }
                    }
                }
                else
                {
                    message = "You are not connected to any controller";

                }
                if (!bSilentmode)
                {
                    MessageBox.Show(message, "Connection status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            public void CheckConfiguration()
            {
                Int64 polarity = 0;
                Int64 daisychain = 0;

                string message = "";

                bool asyncSave;
                asyncSave = Globals.mainform.bAsyncCommunicationEnabled;
                Globals.mainform.bAsyncCommunicationEnabled = false;

                  Thread.Sleep(100);

                  if (Globals.mainform.port.GetObjectValue(ID, 0x607e, ref polarity) &&
                    Globals.mainform.port.GetObjectValue(ID, 0x2000, 3, ref daisychain))
                  {
                    if (strFirmwareVersion[0] < '2')            // Check if the firmware version in the controller is at least 0.92B5
                    {
                        message += "\n- The controller firmware (" + strFirmwareVersion + ") must be upgraded to version 2.0 or higher";
                    }

                    if ((byte)(polarity) != 0)
                    {
                        message += "\n- Object 0x607E: The polarity for Position and Velocity must be set to Positive (0)";
                    }

                    if (daisychain != 1)
                    {
                        message += "\n- Object 0x2000 sub 3: Daisy chain mode must be set to daisy chain (1)";
                    }

                    if (!bWarningIncorrectFirmwareShown && message != "")
                    {
                        message = "The controller with node ID " + ID.ToString() + " is not configured properly to work with LCC Control Center.\n\n" +
                                  "Please correct the following:\n" +
                                  message +
                                  "\n\nIgnoring this message will result in unpredictable moves or behaviour";
                        MessageBox.Show(message, "Configuration error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        bWarningIncorrectFirmwareShown = true;
                    }
                  }
                Globals.mainform.bAsyncCommunicationEnabled = asyncSave;
            }

        }
        //
        // Function used for sorting the network list.
        // Compare nodes based on their node id's
        //
        public static int CompareNodesByID(Node x, Node y)
        {
            return x.ID.CompareTo(y.ID);
        }

        public List<Node> Network = new List<Node>();      // The actual network

        private void SetCurrentNode(int nodeID)
        {
            for (int i = 0; i < Network.Count; i++)
            {
                if (Network[i].ID == nodeID)
                {
                    currentNode = Network[i];
                    activeNode = Network[i].ID;

                    if (!chkSetProtectedAccess.Visible)
                    {
                        chkSetProtectedAccess.Checked = false;
                    }
                }
            }
            EnableButtons(bConnected);
            if (bTuningmode)
            {
                btnUpload_Click(null, null);    // Force an opdate of the configuration data
            }
        }

        public void PopulateTooltips()
        {
            const string tiptextActuator =
                "Select the right actuator you got for demo";
            DefineToolTip(new ToolTip(), cmbActuatorDemo, tiptextActuator);
            DefineToolTip(new ToolTip(), lblActuator, tiptextActuator);

            const string tiptextProgramTypeDemo =
                "Select the type of program sequence that you wanted to run";
            DefineToolTip(new ToolTip(), cmbProgramTypeDemo, tiptextProgramTypeDemo);
            DefineToolTip(new ToolTip(), lblProgramType, tiptextProgramTypeDemo);

            const string tiptextPositionRange =
                "Type the final position value in the right side box, for the actuator to move from any current position. \n" +
                "Note: This value will change depending upon the stroke and encoder value of actuator selected.";
            DefineToolTip(new ToolTip(), txtPosition, tiptextPositionRange);

            const string tiptextPositionRangelabel =
                "Type the final position value for position move";
            DefineToolTip(new ToolTip(), lblTargetPositionDemo, tiptextPositionRangelabel);
            DefineToolTip(new ToolTip(), txtTargetPositionDemo, tiptextPositionRangelabel);


            const string tiptextSpeedRange =
                "This value is in counts/sec and will vary depending upon encoder resolution of the actuator";
            DefineToolTip(new ToolTip(), txtSpeedRange, tiptextSpeedRange);


            const string tiptextSpeed =
                "Enter the universal speed value for any program sequence(Position and softland)";
            DefineToolTip(new ToolTip(), lblSpeedDemo, tiptextSpeed);
            DefineToolTip(new ToolTip(), txtProfileVelocityDemo, tiptextSpeed);


            const string tiptextSoftlandRange =
                "This value is in counts and alters the softland capability for the actuator.\n" +
                "Try to use intermediate value between 150-250 for better performance";
            DefineToolTip(new ToolTip(), txtSoftlandRange, tiptextSoftlandRange);

            const string tiptextSoftland =
                "Used to enter the thread pitch.\n" +
                "Please Note: If entered incorrectly this will cause errors.";
            DefineToolTip(new ToolTip(), txtSoftlandRange, tiptextSoftland);

            const string tiptextForceRange =
                "This is the value in percentage of rated current. Please verify the catalog for rated current definitions.\n" +
                "This value will change with different actuator selection";
            DefineToolTip(new ToolTip(), txtForceRange, tiptextForceRange);

            const string tiptextForce =
                "This force value is related to only force mode";
            DefineToolTip(new ToolTip(), txtTargetForceDemo, tiptextForce);
            DefineToolTip(new ToolTip(), lblForceDemo, tiptextForce);

            const string tiptextForceSlopeRange =

            "Enter the force slope value on the right based on the range mentioned";
            DefineToolTip(new ToolTip(), txtForceSlopeRange, tiptextForceSlopeRange);

            const string tiptextForceSlope =
                "This force slope value affects the time to reach the target force in force mode.\n" +
                "Higher the slope value = Lesser the time it takes to reach the target force.";
            DefineToolTip(new ToolTip(), txtForceSlopeDemo, tiptextForceSlope);
            DefineToolTip(new ToolTip(), lblForceSlopeDemo, tiptextForceSlope);

            const string tiptextSavelogs =
                "Saves the chart data into .csv file format";

            DefineToolTip(new ToolTip(), btnSaveLogs, tiptextSavelogs);

            const string tiptextHome =
               "Moves the actuator to 0 position (2mm from the rear end bumper of actuator.\n" +
               "Note: Always do homing before starting the next sequence";

            DefineToolTip(new ToolTip(), btnHome, tiptextHome);

            const string tiptextStartDemo =
               "Moves the actuator from home position to the programmed destination as per selected program sequence.\n" +
               "Note: The data will be logged automatically while this button is clicked";

            DefineToolTip(new ToolTip(), btnStartDemo, tiptextStartDemo);

            const string tiptextMotorOff =
               "Interrupts the actuator at any part of the program and stops the actuator.\n" +
               "Note: The actuator needs to be homed before clicking the actuator start button";

            DefineToolTip(new ToolTip(), btnMotoroff, tiptextMotorOff);

            const string tiptextchartRun =
              "Monitors the actual position of the actuator in Y axis left- Monitors Actual force value in the right Y axis (If selected)\n" +
              "Monitors the time in the X axis.\n" +
              "The colours for the chart represents specific paramaters selected in the program sequence.\n" +
              "Note: Refer to help document to understand more about the chart data points and colours.";

            DefineToolTip(new ToolTip(), chartRun, tiptextchartRun);

        }

        private class numericField
        {
            private TextBox txtBox;
            private long value;
            private long min;
            private long max;

            public numericField(TextBox textBox, long initialValue, long minValue, long maxValue)      // The constructor
            {
                txtBox = textBox;
                value = initialValue;
                min = minValue;
                max = maxValue;
                textBox.Text = value.ToString();
            }

            public long Value()
            {
                Checkvalue(true);
                return value;
            }

            public void Setvalue(long newValue)
            {
                value = newValue;
                txtBox.Text = value.ToString();
            }

            public bool Checkvalue(bool bShowmessage)
            {
                bool bRetval = true;
                long temp = 0;
                if (long.TryParse(txtBox.Text, out temp))
                {
                    if ((temp >= min) && (temp <= max))
                    {
                        value = temp;
                    }
                    else
                    {
                        bRetval = false;
                    }
                }
                else
                {
                    bRetval = false;
                }
                if (!bRetval && bShowmessage)
                {
                    MessageBox.Show("The value you entered (" + txtBox.Text + ") is not valid\n\n" +
                                    "The value must be in the range " + min.ToString() + " to " + max.ToString());
                }
                txtBox.Text = value.ToString();

                return bRetval;
            }
        }

        public class ConfigurationObject
        {
            int index;
            long value;
            TextBox txtBox;


            public ConfigurationObject(TextBox textBox, int objectIndex)
            {
                index = objectIndex;
                txtBox = textBox;
            }

            public void Read()
            {
                if (Globals.mainform.getObjectFromActiveNode(index, ref value))
                {
                    txtBox.Text = value.ToString();
                }
                else
                {
                    txtBox.Text = "0";

                }
            }
            public void Write()
            {
                Globals.mainform.port.WriteObject(Globals.mainform.activeNode, index, value);
                Thread.Sleep(Globals.mainform.NetworkDelay());           // This deley is necessary in daisy chain mode to avoid buffer overflows
            }

            public bool Isreadonly()
            {
                return txtBox.ReadOnly;
            }

            public void Checkvalue()
            {
                long temp = 0;
                if (long.TryParse(txtBox.Text, out temp))
                {
                    if (Globals.dictionary.IsWithinLimits(temp, index, true))
                    {
                        value = temp;
                    }
                }
                else
                {
                    MessageBox.Show("The value you entered is not valid");
                }
                txtBox.Text = value.ToString();
            }

            public long Value()
            {
                return value;
            }

            public long Index()
            {
                return index;
            }
        }

        public string strNameDemo = ""; // holds ini file name for checking


        public string fullPath = ""; // holds path to file

        //
        // Construct an actuator and read the settings from an ini file
        // Input ini file containing stroke, rotary, and PID settings
        //
        public class ActuatorDemo
        {
            public string strNameDemo = "LAL35-050-71S"; // holds ini file name for checking


            public string fullPath = ""; // holds path to file



            public void Load(string filename)
            {
                // MessageBox.Show(Environment.CurrentDirectory);
                // replaces exclamation mark with colon mark in file name
                filename = filename.Replace(';', ';');
                string infile = "";

                // searches each file in the actuator config directory until chosen one is found
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
                {
                    if (Path.GetFileNameWithoutExtension(s) == filename)
                    {
                        strNameDemo = filename;
                        string parameters = strNameDemo;
                        parameters.Split('-').Count();

                        string Encoder = "";
                        string Stroke = "";

                        Stroke = (parameters.Split('-')[1].Trim());
                        Encoder = (parameters.Split('-')[2].Trim());
                        long StrokeFinal = Globals.utils.ConvertStringToInt64(parameters.Split('-')[1].Trim());
                        long EncoderFinal = Globals.utils.ConvertStringToInt64(Encoder[1].ToString());

                        //EncoderFinal = Encoder[1].ToString();
                        /*if (EncoderFinal == 1)
                        {
                            int x = 0;

                            x = (int)StrokeFinal * 1000;
                        }*/

                        if (EncoderFinal == 1)
                        {


                            //X  = Convert.ToString(((int)StrokeFinal) * 1000); 
                        }


                        fullPath = s;

                        configurationFileDemo1 config = new configurationFileDemo1();
                        infile = fullPath; //VIGNESH:: 05/29:: This is to make sure the file path is transfered to the variable(infile) so the file gets picked up from Actuator folder in the setup destination
                        if (infile != "")
                        {
                            if (config.ReadfromFile(infile))
                            {
                                config.WriteToController();


                            }
                        }

                    }
                }
            }
        }

        public class configurationFileDemo1
        {
            private List<long> indexes = new List<long>();
            private List<long> values = new List<long>();

            private string header = "[MotionLab;1.8.1.0R]"; // This is the default header, will be overridden when a readfile is done

            public configurationFileDemo1()    // The constructor
            {


            }

            public void Init()
            {
                indexes.Clear();
                values.Clear();
                header = "";

            }


            public bool ReadfromFile(string strNameDemo)
            {
                bool bOk = true;
                Init();

                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
                {
                    foreach (string m in File.ReadAllLines(strNameDemo))
                    {
                        // searches each file in the actuator config directory until chosen one is found

                        if (m.Length != 0)
                        {
                            if (m[0] == '[')
                            {
                                header = m;
                            }
                            else
                            {
                                switch (m.Split(';').Count())
                                {
                                    case 3:
                                        long index = Globals.utils.ConvertStringToInt64(m.Split(';')[0].Trim());
                                        long subindex = Globals.utils.ConvertStringToInt64(m.Split(';')[1].Trim());
                                        long value = Globals.utils.ConvertStringToInt64(m.Split(';')[2].Trim());
                                        Add(index + (subindex << 16), value);
                                        break;
                                    default:
                                        bOk = false;
                                        break;
                                }
                            }
                        }

                    }

                }

                if (!bOk || (indexes.Count < 20))
                {
                    MessageBox.Show("File " + strNameDemo + " is not a valid configuration file");
                    return false;
                }
                return true;


            }
            public bool WriteToController()
            {
                int i;
                for (i = 0; i < indexes.Count; i++)
                {
                    Globals.mainform.port.WriteObject(Globals.mainform.activeNode, (int)indexes[i], (int)values[i]);
                    Thread.Sleep(Globals.mainform.NetworkDelay());       // This deley is necessary in daisy chain mode
                }
                return true;
            }

            public void Add(long index, long value)
            {
                int i;
                bool bFound = false;
                for (i = 0; i < indexes.Count; i++)
                {
                    if (indexes[i] == index)
                    {
                        values[i] = value;
                        bFound = true;
                    }
                }
                if (!bFound)
                {
                    indexes.Add(index);
                    values.Add(value);
                }
            }
            public int Count()
            {
                return indexes.Count;
            }
        }


        public string Name()
        {
            return strNameDemo;
        }

        public string[] Info()
        {
            List<string> info = new List<string>();

            info.Add("Actuator: " + strNameDemo);
            info.Add("");
            return info.ToArray();
        }

        private class configurationFile
        {
            private List<long> indexes = new List<long>();
            private List<long> values = new List<long>();

            private string header = "[MotionLab;1.8.1.0R]"; // This is the default header, will be overridden when a readfile is done


            public configurationFile(string fullpath)      // The constructor
            {

            }

            public void Init()
            {
                indexes.Clear();
                values.Clear();
                header = "";
            }

            public bool ReadfromFile(string path)
            {
                bool bOk = true;
                Init();

                foreach (string s in File.ReadAllLines(path))
                {

                    if (s.Length != 0)
                    {
                        if (s[0] == '[')
                        {
                            header = s;
                        }
                        else
                        {
                            switch (s.Split(';').Count())
                            {
                                case 3:
                                    long index = Globals.utils.ConvertStringToInt64(s.Split(';')[0].Trim());
                                    long subindex = Globals.utils.ConvertStringToInt64(s.Split(';')[1].Trim());
                                    long value = Globals.utils.ConvertStringToInt64(s.Split(';')[2].Trim());
                                    Add(index + (subindex << 16), value);
                                    break;
                                default:
                                    bOk = false;
                                    break;
                            }
                        }
                    }
                }

                if (!bOk || (indexes.Count < 20))
                {
                    MessageBox.Show("File " + path + " is not a valid configuration file");
                    return false;
                }

                return true;
            }

            public bool WritetoFile(string path)
            {
                StreamWriter file = null;
                file = new System.IO.StreamWriter(path);
                file.WriteLine(header);
                for (int i = 0; i < indexes.Count; i++)
                {
                    file.WriteLine("0x" + (indexes[i] & 0xFFFF).ToString("X4") + ";0x" + (indexes[i] >> 16).ToString("X2") + ";" + values[i].ToString());
                }
                file.Close();
                return true;
            }

            public bool WriteToController()
            {
                int i;
                for (i = 0; i < indexes.Count; i++)
                {
                    Globals.mainform.port.WriteObject(Globals.mainform.activeNode, (int)indexes[i], (int)values[i]);
                    Thread.Sleep(Globals.mainform.NetworkDelay());       // This deley is necessary in daisy chain mode
                }
                return true;
            }

            public void Add(long index, long value)
            {
                int i;
                bool bFound = false;
                for (i = 0; i < indexes.Count; i++)
                {
                    if (indexes[i] == index)
                    {
                        values[i] = value;
                        bFound = true;
                    }
                }
                if (!bFound)
                {
                    indexes.Add(index);
                    values.Add(value);
                }
            }
            public int Count()
            {
                return indexes.Count;
            }
        }

        int numPrePositionA;
        int numPrePositionB;
        int numPreVelocity;
        private numericField numPreAcceleration;
        private numericField numPreForce;
        private numericField numPreDecel;
        private numericField numPreSettlingtime;

        int numPositionA;
       int numPositionB;
        int numVelocityHighSpeed;

        private numericField numPosition1;
        private numericField numPosition2;
        private numericField numTarget1;
        private numericField numTarget2;

        int numPositionSwitch;
        int numSwitchSpeed;

        private numericField numSwitchSensitivity;
        int numSwitchSoftlandSpeed;

        int numPositionLeak;
        int numLeakSpeed;
        private numericField numLeakSensitivity;
        private numericField numLeakForce;
        private numericField numLeakDwellTime;

        private numericField numHighSpeedTime;
  
        int numPosCloseToPart;
        int numMeasureSpeed;
        private numericField numMeasureSLError;
        private numericField numMeasureTF;
        private numericField numMeasureDT;
        private numericField numMeasureResult; // this is to display measurment value

        private numericField numPosition;
        private numericField numSoftlandError;
        private numericField numForceSlope;
        private numericField numVelocity;
        private numericField numTargetForce;

        private numericField numFollowingerror;
        private numericField numModeofop;

        private numericField numPrimaryaxisMaxTuning;
        private numericField numPrimaryaxisMinTuning;
        private numericField numSecondaryaxisMaxTuning;
        private numericField numSecondaryaxisMinTuning;

        private numericField numPrimaryaxisMaxRun;
        private numericField numPrimaryaxisMinRun;
        private numericField numSecondaryaxisMaxRun;
        private numericField numSecondaryaxisMinRun;

        private numericField numPrimaryaxisMaxRun1;
        private numericField numPrimaryaxisMinRun1;
        private numericField numSecondaryaxisMaxRun1;
        private numericField numSecondaryaxisMinRun1;

        private List<ConfigurationObject> configurationObjects = new List<ConfigurationObject>();

        private ConfigurationObject cfgAcceleration;
        private ConfigurationObject cfgDecelleration;
        private ConfigurationObject cfgVelocity;
        private ConfigurationObject cfgTargetPosition;

        private int[,] capturedData = new int[4, 250];

        private ErrorDescription[] ErrorList = {
            new ErrorDescription(0x0000, "No error"),
            new ErrorDescription(0x2200, "Hardware peak over-current detected (system protection)"),
            new ErrorDescription(0x2201, "Hardware I2T over-current detected (system protection)"),
            new ErrorDescription(0x7305, "Error in incremental encoder feedback detected"),
            new ErrorDescription(0x7306, "Differential encoder broken wire detected"),
            new ErrorDescription(0x8110, "CAN bus overrun"),
            new ErrorDescription(0x8120, "CAN in error passive mode"),
            new ErrorDescription(0x8130, "Lifeguard or heartbeat error"),
            new ErrorDescription(0x8140, "Recovered from bus off"),
            new ErrorDescription(0x8141, "Bus off occurred"),
            new ErrorDescription(0x8150, "Transmit COBID collision"),
            new ErrorDescription(0x8210, "PDO not processed due to length error"),
            new ErrorDescription(0x8220, "PDO length exceeded"),
            new ErrorDescription(0x8613, "Timeout during homing process"),
            new ErrorDescription(0xFF04, "Phasing process out of tolerance detected"),
            new ErrorDescription(0xFF10, "Divide by zero instruction detected"),
            new ErrorDescription(0xFF20, "Uart reception overflow"),
            new ErrorDescription(0xFF30, "An out of valid range Macro or command address has been executed"),
            new ErrorDescription(0xFF31, "Macro stack is full"),
            new ErrorDescription(0xFF33, "Detected interrupt without associated macro function"),
            new ErrorDescription(0xFF34, "Saving or restoring out of learned position space")};

        private string GetErrorString(int errornumber)
        {
            foreach (ErrorDescription description in ErrorList)
            {
                if (description.errorNumber == errornumber)
                {
                    return description.ErrorString;
                }
            }
            return "Unknown error 0x" + errornumber.ToString("X4");
        }


        private List<string> Compiled = new List<string>();

        private MacroData[] macroData = new MacroData[64];

        // Variables for the capture state machine

        private enum CaptureState
        {
            Idle,
            WaitForMacroReady,
            PrepareFirstMove,
            PrepareSecondMove,
            Capture,
            CaptureDone
        }

        private CaptureState captureState = CaptureState.Idle;
        private CaptureState nextCapturestate = CaptureState.Idle;

        public class logitem
        {
            //
            //  Selection strings for the traceable parameters
            //

            private const string STR_OFF = "Off";
            private const string STR_ACTUAL_POSITION = "Actual position";
            private const string STR_ACTUAL_VELOCITY = "Actual velocity";
            private const string STR_ACTUAL_FORCE = "Actual force";
            private const string STR_MODE_OF_OP = "Mode of Operation";
            private const string STR_FOLLOWING_ERROR = "Following error";
            private const string STR_OTHER = "Other";

            private string[] traceSelections = new string[] {
                STR_ACTUAL_POSITION,
                STR_ACTUAL_FORCE};

            private ComboBox cmbSelect;
            // private CheckBox ChkPositionActual;
            private TextBox txtObject;
            private TextBox txtValue;
            private CheckBox chkShowPrimaryAxis;
            private CheckBox chkShowSecondaryAxis;
            public Series series;
            private ComboBox cmbNode;

            private int combinedindex;
            private int lastSelection = 0;
            private int lastOtherObject = 0;

            private string name;
            private int intValue;
            private bool valueValid;
            private long timeStamp;
            private int size;
            private bool issigned;

            public logitem(ComboBox comboSelect, TextBox textObject, TextBox textValue, CheckBox ckkShowOnPrimaryAxis, CheckBox chkShowOnSecondaryAxis, Series plotseries, ComboBox comboNode)
            {
                cmbSelect = comboSelect;
                txtObject = textObject;
                txtValue = textValue;
                chkShowPrimaryAxis = ckkShowOnPrimaryAxis;
                chkShowSecondaryAxis = chkShowOnSecondaryAxis;
                series = plotseries;
                cmbNode = comboNode;

                valueValid = false;
                Globals.mainform.utils.FillComboBox(cmbSelect, traceSelections);
                SelectionChanged(false);
            }

            private void ShowIndexInGui()
            {
                string objectstring = Globals.utils.GetIndexString(combinedindex);
                txtObject.Text = objectstring;
                txtValue.Text = "";
            }

            public int getLastNode()
            {
                return int.Parse(cmbNode.SelectedItem.ToString());
            }

            //
            // Check if node exists must be called only when connected
            // Will read all present nodes from the node list, update the combo boxes with the actual nodes
            // that are present on the network
            // If only 1 node is present then the node selection box will be disabled and the node is set to the
            // only node in the system
            // An error message can be shown if the node number has changed.
            //

            public void init(int index)
            {
                combinedindex = index;
                //  cmbSelect.SelectedIndex = 0x6064;  

                switch (combinedindex)
                {
                    case 0x6077:
                        name = STR_ACTUAL_FORCE;
                        break;

                    case 0x6064:
                        name = STR_ACTUAL_POSITION;
                        break;
                    default:
                        name = STR_ACTUAL_VELOCITY;
                        // cmbSelect.Items[cmbSelect.Items.Count - 1] = name;
                        break;
                }

                size = Globals.dictionary.ObjectSize(index);
                issigned = Globals.dictionary.ObjecIsSignedvalue(index);
                cmbSelect.SelectedItem = name;
                lastSelection = cmbSelect.SelectedIndex;
                ShowIndexInGui();
            }

            public void SelectionChanged(bool query)
            {
                string currentSelection = cmbSelect.SelectedItem.ToString();

                switch (currentSelection)
                {
                    case STR_ACTUAL_POSITION:
                        combinedindex = 0x6064;
                        break;

                    case STR_ACTUAL_FORCE:
                        combinedindex = 0x6077;
                        break;

                    default:
                        // it must be another selected item
                        combinedindex = 0x6064;
                        break;

                }
                ShowIndexInGui();
                name = cmbSelect.SelectedItem.ToString();
                size = Globals.dictionary.ObjectSize(combinedindex);
                issigned = Globals.dictionary.ObjecIsSignedvalue(combinedindex);
                lastSelection = cmbSelect.SelectedIndex;
            }

            public int GetCombinedIndex()
            {
                return (combinedindex);
            }

            public bool IsObjectSelected()
            {
                return (combinedindex != 0);
            }

            public string Name()
            {
                return name;
            }

            public void Enable(bool enable)
            {
                cmbSelect.Enabled = enable;
                cmbNode.Enabled = enable;
            }

            public void SetValue(int value, long timestamp)
            {
                intValue = Globals.utils.formatInt(value, size, issigned);
                valueValid = true;
                timeStamp = timestamp;
            }

            public void ShowValue()
            {
                txtValue.Text = intValue.ToString();
            }

            

            public bool ValueIsValid()
            {
                return valueValid;
            }

            public int GetValue()
            {
                valueValid = false;
                return intValue;
            }

            public int PeekValue()
            {
                return intValue;
            }

            public long GetTimestamp()
            {
                return timeStamp;
            }
        }

        public class logitem1
        {

            //
            //  Selection strings for the traceable parameters
            //

            private const string STR_OFF = "Off";
            private const string STR_ACTUAL_POSITION = "Actual position";
            private const string STR_ACTUAL_VELOCITY = "Actual velocity";
            private const string STR_ACTUAL_FORCE = "Actual force";
            private const string STR_MODE_OF_OP = "Mode of Operation";
            private const string STR_FOLLOWING_ERROR = "Following error";
            private const string STR_OTHER = "Other";

            private string[] traceSelections1 = new string[] {
                STR_ACTUAL_POSITION,
                STR_ACTUAL_FORCE};

            private ComboBox cmbSelectMeasure;
            // private CheckBox ChkPositionActual;
            // private TextBox txtObject;
            private TextBox txtMeasureObject;
            // private TextBox txtValue;
            private TextBox txtMeasureValue;
            private CheckBox chkShowPrimaryAxisMeasure;
            private CheckBox chkShowSecondaryAxisMeasure;
            public Series seriesMeasure;
            private ComboBox cmbNodeMeasure;

            private int combinedindexMeasure;
            private int lastSelectionMeasure = 0;
            private int lastOtherObjectMeasure = 0;

            private string nameMeasure;
            private int intValueMeasure;
            private bool valueValidMeasure;
            private long timeStampMeasure;
            private int sizeMeasure;
            private bool issignedMeasure;

            public logitem1(ComboBox comboSelect1, TextBox textObject1, TextBox textValue1, CheckBox ckkShowOnPrimaryAxis1, CheckBox chkShowOnSecondaryAxis1, Series plotseries1, ComboBox comboNode1)
            {
                cmbSelectMeasure = comboSelect1;
                txtMeasureObject = textObject1;
                txtMeasureValue = textValue1;
                chkShowPrimaryAxisMeasure = ckkShowOnPrimaryAxis1;
                chkShowSecondaryAxisMeasure = chkShowOnSecondaryAxis1;
                seriesMeasure = plotseries1;
                cmbNodeMeasure = comboNode1;

                valueValidMeasure = false;
                Globals.mainform.utils.FillComboBox(cmbSelectMeasure, traceSelections1);
                SelectionChangedMeasure(false);
            }



            private void ShowIndexInGuiMeasure()
            {
                string objectstring = Globals.utils.GetIndexString(combinedindexMeasure);
                txtMeasureObject.Text = objectstring;
                txtMeasureValue.Text = "";
            }


            public int getLastNodeMeasure()
            {
                return int.Parse(cmbNodeMeasure.SelectedItem.ToString());
            }

            //
            // Check if node exists must be called only when connected
            // Will read all present nodes from the node list, update the combo boxes with the actual nodes
            // that are present on the network
            // If only 1 node is present then the node selection box will be disabled and the node is set to the
            // only node in the system
            // An error message can be shown if the node number has changed.
            //

            public void init(int indexMeasure)
            {
                combinedindexMeasure = indexMeasure;
                //  cmbSelect.SelectedIndex = 0x6064;  

                switch (combinedindexMeasure)
                {
                    case 0x6077:
                        nameMeasure = STR_ACTUAL_FORCE;
                        break;

                    case 0x6064:
                        nameMeasure = STR_ACTUAL_POSITION;
                        break;
                    default:
                        nameMeasure = STR_ACTUAL_VELOCITY;
                        // cmbSelect.Items[cmbSelect.Items.Count - 1] = name;
                        break;
                }

                sizeMeasure = Globals.dictionary.ObjectSize(indexMeasure);
                issignedMeasure = Globals.dictionary.ObjecIsSignedvalue(indexMeasure);
                cmbSelectMeasure.SelectedItem = nameMeasure;
                lastSelectionMeasure = cmbSelectMeasure.SelectedIndex;
                ShowIndexInGuiMeasure();
            }

            public void SelectionChangedMeasure(bool query)
            {
                string currentSelectionMeasure = cmbSelectMeasure.SelectedItem.ToString();
                switch (currentSelectionMeasure)
                {
                    case STR_ACTUAL_POSITION:
                        combinedindexMeasure = 0x6064;
                        break;

                    case STR_ACTUAL_FORCE:
                        combinedindexMeasure = 0x6077;
                        break;

                    default:
                        // it must be another selected item
                        combinedindexMeasure = 0x6064;
                        break;
                }
                ShowIndexInGuiMeasure();
                nameMeasure = cmbSelectMeasure.SelectedItem.ToString();
                sizeMeasure = Globals.dictionary.ObjectSize(combinedindexMeasure);
                issignedMeasure = Globals.dictionary.ObjecIsSignedvalue(combinedindexMeasure);
                lastSelectionMeasure = cmbSelectMeasure.SelectedIndex;
            }

            public int GetCombinedIndexMeasure()
            {
                return (combinedindexMeasure);
            }

            public bool IsObjectSelectedMeasure()
            {
                return (combinedindexMeasure != 0);
            }

            public string NameMeasure()
            {
                return nameMeasure;
            }

            public void Enable(bool enable)
            {
                cmbSelectMeasure.Enabled = enable;
                cmbNodeMeasure.Enabled = enable;
            }

            public void SetValueMeasure(int value, long timestamp)
            {
                intValueMeasure = Globals.utils.formatInt(value, sizeMeasure, issignedMeasure);
                valueValidMeasure = true;
                timeStampMeasure = timestamp;
            }

            public void ShowValueMeasure()
            {
                txtMeasureValue.Text = intValueMeasure.ToString();
            }

            public bool ValueIsValidMeasure()
            {
                return valueValidMeasure;
            }

            public int GetValueMeasure()
            {
                valueValidMeasure = false;
                return intValueMeasure;
            }

            public int PeekValueMeasure()
            {
                return intValueMeasure;
            }

            public long GetTimestampMeasure()
            {
                return timeStampMeasure;
            }
        }

        public List<logitem> logitems = new List<logitem>();
        public List<logitem1> logitems1 = new List<logitem1>();

        private Serial port = new Serial();

        public MainForm()
        {
            InitializeComponent();
            //            Globals.logSerial.Start(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\RS232 Logging.txt");
            //            MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\RS232 Logging.txt");
        }


        #region Global & Initializing
        private delegate void SetTextDeleg(string text);
        int[] uploadParms = new int[31];
        Utils utils = new Utils();
        public bool bAsyncCommunicationEnabled = true;

        private Sequence actualSequence = null;

        private List<string> theProgram = new List<string>();




        private class SequenceSaver
        {
            private int sequenceToRestore;
            private string parameters = "";
            private string comment = "";
            public SequenceSaver()       // The constructor
            {

                sequenceToRestore = Globals.mainform.cmbFunction.SelectedIndex;    // Get the actual sequence
                parameters = Globals.mainform.actualSequence.GetParameterString();
                comment = Globals.mainform.actualSequence.GetComment();
            }

            public void Restore()
            {
                Globals.mainform.actualSequence = Globals.sequences.sequencelist[sequenceToRestore];
            }

            public string GetParameters()
            {
                return parameters;
            }
            public string GetComment()
            {
                return comment;
            }
        }
        private void UpdateActualSequence(SequenceSaver saver)
        {
            saver.Restore();
            Protect();
            actualSequence.Exit();
            actualSequence.Init();
            //actualSequence.Init1();
            actualSequence.SetParameters(saver.GetParameters());
            actualSequence.SetComment(saver.GetComment());
            actualSequence.ShowParametersInGui();
            Unprotect();
        }

        public bool getObjectFromActiveNode(int index, ref long value)
        {
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            bool bRetval = port.GetObjectValue(activeNode, index, ref value); ;
            bAsyncCommunicationEnabled = bSave;
            return bRetval;
        }

       



        //
        // The application can be called with command line arguments to invoke "Silent compile" mode
        // For silent compile mode the 2 arguments must be:
        //
        // -S - To invoke silent mode
        // <Name of the input file containing the full path>
        //
        // The input file must be in .scc format
        //
        // The output file will be written in the same directory as the input file and will have a .mlc suffix
        // Compilation errors will be written to a file with .err suffix
        //



        private void MainForm_Shown(object sender, EventArgs e)     // Original this was the MainForm_Load event
                                                                    // Changed to Form_Shown to solve problem with dialogs starting in the background
        {
            PopulateTooltips();
            backupFolderName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            backupFolderName = Path.Combine(backupFolderName, "SMAC");
            backupFolderName = Path.Combine(backupFolderName, "Backup");
            backupFolderName = Path.Combine(backupFolderName, "LCC Control Center");

            //ibdTargetPositionDemo = new InputBoxDouble(txtTargetPositionDemo, 0, 160, 0, "Target Position");

            bSilentmode = (Environment.GetCommandLineArgs().Count() == 3) &&
                          (Environment.GetCommandLineArgs()[1].ToUpper() == "-S");
            if (bSilentmode)
            {
                inputFile = Environment.GetCommandLineArgs()[2];
                outputFile = Path.ChangeExtension(inputFile, ".mlc");
                errorFile = Path.ChangeExtension(inputFile, ".err");
                try
                {
                    File.Delete(outputFile);        // Delete if this file exists
                    File.Delete(errorFile);         // Delete if this file existd

                }
                catch
                {
                }
                WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;

                tmrTuning.Enabled = false;
                //                parseProgramfile();
                //                Compile();
                //                Environment.Exit(0);
            }


            Globals.mainform = this;
            cmbProgramTypeDemo.SelectedIndex = 0; // Default selection of program type 
            cmbActuatorDemo.Items.Clear();
            //  cmbMeasureType.Items.Clear(); 
            tabMeasure.TabPages.Remove(tabBuildandRun);
            btnAutoscalePrimary1.PerformClick();
            //  tabControl1.TabPages.

            tabMeasure.Enabled = true;
            


            // btnAutoscalePrimary1.Visible = false;
            try
            {
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
                {
                    cmbActuatorDemo.Items.Add(Path.GetFileNameWithoutExtension(s));
                    configurationFile config = new configurationFile(fullpath);
                    bConfigurationDownloaded = false;
                    config.ReadfromFile(s);
                }
            }


            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                               "Re-install the application and then try again.",
                               "Fatal error",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
                Application.Exit();



            }
            /////////////////////////////




            // cmbActuator.SelectedIndex = 0;


            //cmbActuator::04/29::VIGNESH:: Added to include actuator folder for loading from .ini file.
            /*  try
               {
                      foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
                      {
                      configurationFile config = new configurationFile();
                      bConfigurationDownloaded = false;
                      if (config.ReadfromFile(s))
                          {
                              config.WriteToController();
                          }

                          // And finaly write to non volatile memory
                          port.WriteString(activeNode.ToString() + " W 0x11010 1702257011");  // Object 0x1010, sub1 - Store all parameters with signature "save" 



                          // TODO: Maybe a check if all parameters are written correctly
                          MessageBox.Show("Configuration Successfully Downloaded.");
                          currentNode.CheckConfiguration();
                          bConfigurationDownloaded = true;

                      }

               }
               catch
               {
                   MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                   "Re-install the application and then try again.",
                                   "Fatal error",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error);
                   Application.Exit();
               }

              //            MessageBox.Show("Actuators " + cmbActuator.Items.Count.ToString());
              // cmbActuator.SelectedIndex = 0;
              //  load_values_from_actuator_ini();
              */

            if (!bSilentmode)
            {
                outputWindow = new dlgOutputWindow();
            }

           


            


            


            //
            // Do some alignments of the general purpose controls.
            // All are aligned on the top Function select combo box
            // In vertical the alignment is on the name labels
            // Only the vertical unit labels are aligned here, the rest is done in the sequence functions
            //
            PopulateTooltips();

            txtParm1.Width = cmbFunction.Width;
            txtParm2.Width = cmbFunction.Width;
            txtParm3.Width = cmbFunction.Width;
            txtParm4.Width = cmbFunction.Width;
            txtParm5.Width = cmbFunction.Width;
            txtParm6.Width = cmbFunction.Width;
            txtParm7.Width = cmbFunction.Width;
            txtParm8.Width = cmbFunction.Width;

            ComboBox1.Width = cmbFunction.Width;
            ComboBox2.Width = cmbFunction.Width;
            ComboBox3.Width = cmbFunction.Width;
            ComboBox4.Width = cmbFunction.Width;
            ComboBox5.Width = cmbFunction.Width;
            ComboBox6.Width = cmbFunction.Width;
            ComboBox7.Width = cmbFunction.Width;

            btnSelect1.Width = cmbFunction.Width;
            btnSelect2.Width = cmbFunction.Width;
            btnSelect3.Width = cmbFunction.Width;
            btnSelect4.Width = cmbFunction.Width;

            ComboBox1.Left = cmbFunction.Left;
            ComboBox2.Left = cmbFunction.Left;
            ComboBox3.Left = cmbFunction.Left;
            ComboBox4.Left = cmbFunction.Left;
            ComboBox5.Left = cmbFunction.Left;
            ComboBox6.Left = cmbFunction.Left;
            ComboBox7.Left = cmbFunction.Left;

            txtParm1.Left = cmbFunction.Left;
            txtParm2.Left = cmbFunction.Left;
            txtParm3.Left = cmbFunction.Left;
            txtParm4.Left = cmbFunction.Left;
            txtParm5.Left = cmbFunction.Left;
            txtParm6.Left = cmbFunction.Left;
            txtParm7.Left = cmbFunction.Left;
            txtParm8.Left = cmbFunction.Left;

            btnSelect1.Left = cmbFunction.Left;
            btnSelect2.Left = cmbFunction.Left;
            btnSelect3.Left = cmbFunction.Left;
            btnSelect4.Left = cmbFunction.Left;

            utils.HideAll();

            chartRun.BackColor = Color.FromArgb(60, 60, 60);

            chartMeasure.BackColor = Color.FromArgb(60, 60, 60);
            chartSwitch.BackColor = Color.FromArgb(60, 60, 60);

            txtObjectindex1.Visible = false;
            txtObjectindex2.Visible = false;

            


            //  txtVariablewriterIndex.Visible = false;


            Globals.sequences.Initialize();
            cmbFunction.Items.Clear();
            for (int i = 0; i < Globals.sequences.sequencelist.Count; i++)
            {
                //                MessageBox.Show(Globals.sequences.sequencelist[i].GetName());
                cmbFunction.Items.Add(Globals.sequences.sequencelist[i].GetName());
            }
            cmbFunction.SelectedIndex = 0;


            actualSequence = Globals.sequences.sequencelist[0];
            actualSequence.Init();

            actualSequence.SetParameters("");       // Set the parameters to default
            actualSequence.ShowParametersInGui();
            //            cmbFunction_SelectedIndexChanged(null, null);
            //cmbProgramType.Items.Clear();
            // cmbProgramType.SelectedIndex = 0;

            if (!bSilentmode)
            {
                cmbPortName_Click(null, null);
            }
            //  selectorWritevariable = new UserObjectselector(Globals.mainform.lblObject, "Object", Globals.mainform.btnVariableWriter, Globals.mainform.lblDummy, "", false, true);
            //  selectorWritevariable.SetSelection("");

            utils.FillComboBox(cmbIntervalLive, intervalSelections);
            utils.FillComboBox(cmbILMeasure, intervalSelectionsMeasure);

            utils.FillComboBox(cmbSamples, samplesSelections);
            utils.FillComboBox(cmbSamples, samplesSelectionsMeasure);

            cmbSamples.SelectedItem = "100";
            cmbSamMeasure.SelectedItem = "100";

            cmbIntervalLive.SelectedItem = "100";
            cmbILMeasure.SelectedItem = "100"; 

            cmbMacroDownload.SelectedItem = "0";
            // cmbActuatorDemo.SelectedIndex = 0;

            SetEditButtons();
            //tabControl1.TabPages.Remove(tabHelp);

            logfileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\LiveLogging.csv";

            logitems.Add(new logitem(cmbBackgroundobject1, txtObjectindex1, txtValue1, chkLogitem1primary, chkLogitem1secondary, chartRun.Series[0], cmbNode1));
            logitems.Add(new logitem(cmbBackgroundobject2, txtObjectindex2, txtValue2, chkLogitem2primary, chkLogitem2secondary, chartRun.Series[1], cmbNode2));

            logitems1.Add(new logitem1(cmbBGobject1, txtObIn1Measure, txtMeasureValue1, chkLP1Measure, chkLS1Measure, chartMeasure.Series[0], cmbNode1Measure));
            logitems1.Add(new logitem1(cmbBGobject2, txtObIn2Measure, txtMeasureValue2, chkLP2Measure, chkLS2Measure, chartMeasure.Series[1], cmbNode2Measure));


            //   logitems.Add(new logitem(cmbBackgroundobject3, txtObjectindex3, txtValue3, chkLogitem3primary, chkLogitem3secondary, chartRun.Series[2], cmbNode3));
            //  logitems.Add(new logitem(cmbBackgroundobject4, txtObjectindex4, txtValue4, chkLogitem4primary, chkLogitem4secondary, chartDemo.Series[3], cmbNode4));

            nodeSelectorActivenode = new NodeSelector(Globals.mainform.cmbNode, false);




            //  nodeSelectorVariableWriter = new NodeSelector(Globals.mainform.cmbNodeMacro1, true);





            nodeSelector1 = new NodeSelector(Globals.mainform.cmbNode1, false);
            nodeSelector2 = new NodeSelector(Globals.mainform.cmbNode2, false);

            nodeSelector3 = new NodeSelector(Globals.mainform.cmbNode1Measure, false);
            nodeSelector4 = new NodeSelector(Globals.mainform.cmbNode2Measure, false);



            nodeselectors.Add(nodeSelectorActivenode);

            // nodeselectors.Add(nodeSelectorVariableWriter);




            nodeselectors.Add(nodeSelector1);
            nodeselectors.Add(nodeSelector2);

            nodeselectors.Add(nodeSelector3);
            nodeselectors.Add(nodeSelector4);


            //For Position move
          //  numPrePositionA = new numericField(txtPrePositionA, 0, int.MinValue, int.MaxValue);
            //numPrePositionB = new numericField(txtPrePositionB, 0, int.MinValue, int.MaxValue);
            //numPreVelocity = new numericField(txtPreSpeed, 0, int.MinValue, int.MaxValue);
            numPreAcceleration = new numericField(txtPreAcceleration, 0, int.MinValue, int.MaxValue);
            numPreDecel = new numericField(txtPreDecel, 0, int.MinValue, int.MaxValue);
            numPreForce = new numericField(txtPreForce, 0, int.MinValue, int.MaxValue);
            numPreSettlingtime = new numericField(txtSettleTime, 0, int.MinValue, int.MaxValue);

            //For High Speed move
         //   numPositionA = new numericField(txtHighSpeedTarget1, 0, int.MinValue, int.MaxValue);
          //  numPositionB = new numericField(txtHighSpeedTarget2, 0, int.MinValue, int.MaxValue);
          //  numVelocityHighSpeed = new numericField(txtHighSpeedVelocity, 0, int.MinValue, int.MaxValue);



            //For Switch tab
            //numPositionSwitch = new numericField(txtPosCloseSwitch, 0, int.MinValue, int.MaxValue);
           // numSwitchSpeed = new numericField(txtSwitchSpeed, 0, int.MinValue, int.MaxValue);
            numSwitchSensitivity = new numericField(txtErrorSwitch, 0, int.MinValue, int.MaxValue);

            //   numSwitchSoftlandSpeed = new numericField(txtSoftlandSpeedSwitch, 0, int.MinValue, int.MaxValue);

            //For Leak Test
            //  numPositionLeak = new numericField(txtPositionLeak, 0, int.MinValue, int.MaxValue);
            //  numLeakSpeed = new numericField(txtSpeedLeak, 0, int.MinValue, int.MaxValue);
            numLeakSensitivity = new numericField(txtSensitivityLeak, 0, int.MinValue, int.MaxValue);
            numLeakForce = new numericField(txtForceLeak, 0, int.MinValue, int.MaxValue);
            numLeakDwellTime = new numericField(txtDwellLeak, 0, int.MinValue, int.MaxValue);


           



            // For measurement tab

            // numPosCloseToPart = new numericField(test, 0, int.MinValue, int.MaxValue);
           // numMeasureSpeed = new numericField(txtMeasureSpeed, 0, int.MinValue, int.MaxValue);
            numMeasureSLError = new numericField(txtMeasureSensitivity, 0, int.MinValue, int.MaxValue);
            numMeasureTF = new numericField(txtMeasureForce, 0, int.MinValue, int.MaxValue);
            numMeasureDT = new numericField(txtMeasureDT, 0, int.MinValue, int.MaxValue);

            // For basic tab

            numTarget1 = new numericField(txtTargetPositionDemo, 0, int.MinValue, int.MaxValue);
            numVelocity = new numericField(txtProfileVelocityDemo, 0, int.MinValue, int.MaxValue);
            numTargetForce = new numericField(txtTargetForceDemo, 0, int.MinValue, int.MaxValue);
            numSoftlandError = new numericField(txtSoftlandErrorDemo, 0, int.MinValue, int.MaxValue);
            numForceSlope = new numericField(txtForceSlopeDemo, 0, int.MinValue, int.MaxValue);



            numPrimaryaxisMaxRun = new numericField(txtMaxPrimaryRun, 50000, int.MinValue, int.MaxValue);
            numPrimaryaxisMinRun = new numericField(txtMinPrimaryRun, -50000, int.MinValue, int.MaxValue);
            numSecondaryaxisMaxRun = new numericField(txtMaxSecondaryRun, 50000, int.MinValue, int.MaxValue);
            numSecondaryaxisMinRun = new numericField(txtMinSecondaryRun, -50000, int.MinValue, int.MaxValue);

            numPrimaryaxisMaxRun1 = new numericField(txtMaxPrimaryRun1, 50000, int.MinValue, int.MaxValue);
            numPrimaryaxisMinRun1 = new numericField(txtMinPrimaryRun1, -50000, int.MinValue, int.MaxValue);
            numSecondaryaxisMaxRun1 = new numericField(txtMaxSecondaryRun1, 50000, int.MinValue, int.MaxValue);
            numSecondaryaxisMinRun1 = new numericField(txtMinSecondaryRun1, -50000, int.MinValue, int.MaxValue);

            cfgTargetPosition = new ConfigurationObject(txtTargetPositionDemo, 0x607A);

            configurationObjects.Add(cfgAcceleration);
            configurationObjects.Add(cfgDecelleration);
            configurationObjects.Add(cfgVelocity);
            configurationObjects.Add(cfgTargetPosition);

            if (!bSilentmode)
            {
                try
                {


                    if (numPorts > 0)
                    {
                        cmbPortName.SelectedItem = Properties.Settings.Default.Comport;
                    }


                    cmbSamples.SelectedItem = Properties.Settings.Default.Samples;
                    cmbIntervalLive.SelectedItem = Properties.Settings.Default.SampleInterval;

                    // logitems[0].init(Properties.Settings.Default.LiveObject1);
                    //logitems[1].init(Properties.Settings.Default.LiveObject2);
                    //logitems[2].init(Properties.Settings.Default.LiveObject3);
                    //   logitems[3].init(Properties.Settings.Default.LiveObject4);


                    selectorWritevariable.SetSelection(Properties.Settings.Default.WriteObject);
                    ShowWriterindex();


                    inputFile = Properties.Settings.Default.Inputfile;

                    outputWindow.chkAddtimestamp.Checked = Properties.Settings.Default.AddTimestamp;
                    outputWindow.chkAddname.Checked = Properties.Settings.Default.AddName;
                    outputWindow.Show();
                    outputWindow.WindowState = FormWindowState.Minimized;

                    if (Properties.Settings.Default.OutputWindowVisible)
                    {
                        // This will trigger the changed event
                    }

                    activeNode = Properties.Settings.Default.Node;

                    // Retrieve the tuning data from the default settings

                    numPosition1.Setvalue(Properties.Settings.Default.Target1);
                    numPosition2.Setvalue(Properties.Settings.Default.Target2);



                    numPrimaryaxisMinTuning.Setvalue(Properties.Settings.Default.PrimaryAxisMinTuning);
                    numPrimaryaxisMaxTuning.Setvalue(Properties.Settings.Default.PrimaryAxisMaxTuning);
                    numSecondaryaxisMinTuning.Setvalue(Properties.Settings.Default.SecondaryAxisMinTuning);
                    numSecondaryaxisMaxTuning.Setvalue(Properties.Settings.Default.SecondaryAxisMaxTuning);

                    numPrimaryaxisMinRun.Setvalue(Properties.Settings.Default.PrimaryAxisMinRun);
                    numPrimaryaxisMaxRun.Setvalue(Properties.Settings.Default.PrimaryAxisMaxRun);
                    numSecondaryaxisMinRun.Setvalue(Properties.Settings.Default.SecondaryAxisMinRun);
                    numSecondaryaxisMaxRun.Setvalue(Properties.Settings.Default.SecondaryAxisMaxRun);

                    chkLogitem1primary.Checked = Properties.Settings.Default.Logitem1primary;
                    chkLP1Measure.Checked = Properties.Settings.Default.Logitem1primary;
                    chkLogitem1secondary.Checked = Properties.Settings.Default.Logitem1secondary;
                    chkLS1Measure.Checked = Properties.Settings.Default.Logitem1secondary;
                    chkLogitem2primary.Checked = Properties.Settings.Default.Logitem2primary;
                    chkLP2Measure.Checked = Properties.Settings.Default.Logitem2primary;
                    chkLogitem2secondary.Checked = Properties.Settings.Default.Logitem2secondary;
                    chkLS2Measure.Checked = Properties.Settings.Default.Logitem2secondary;

                    // chkLogitem4primary.Checked = Properties.Settings.Default.Logitem4primary;
                    //chkLogitem4secondary.Checked = Properties.Settings.Default.Logitem4secondary;

                    nodeSelector1.SetInitialselection(Properties.Settings.Default.Node1);
                    nodeSelector2.SetInitialselection(Properties.Settings.Default.Node2);

                    nodeSelector3.SetInitialselection(Properties.Settings.Default.Node3);
                    nodeSelector4.SetInitialselection(Properties.Settings.Default.Node4);


                    //   nodeSelectorVariableWriter.SetInitialselection(Properties.Settings.Default.NodeVariableWriter);

                    chkSetProtectedAccess.Checked = Properties.Settings.Default.SetProtectedAccess;
                    chkApplyToAllNodes.Checked = Properties.Settings.Default.ApplyToAllNodes;
                    chkAutoBackup.Checked = Properties.Settings.Default.AutoBackup;

                }

                catch
                {

                }
            }

            parseProgramfile();



            if (!bSilentmode)
            {

                this.Visible = true;
                if (numPorts > 0)
                {
                    btnConnect_Click(null, null);
                }

                outputWindow.init();
            }
            if (bSilentmode)
            {
                Compile();
                StreamWriter file = new System.IO.StreamWriter(outputFile);
                string strTemp = "";
                int commentPos;
                foreach (string s in Compiled)
                {
                    strTemp = s;
                    commentPos = strTemp.IndexOf("//");
                    switch (commentPos)
                    {
                        case 0:
                            strTemp = "";
                            break;
                        case -1:
                            break;
                        default:
                            strTemp = strTemp.Substring(0, commentPos);
                            break;
                    }
                    strTemp = strTemp.Trim();
                    if (strTemp != "")
                    {
                        file.WriteLine(strTemp);
                    }
                }
                file.Close();

                Environment.Exit(0);
            }
            Title();



            bLock = false;
        }
        #endregion

        //
        // DisplayErrors will show the errors in a message box.
        // In silent mode the errors will be logged in the error file

        private void DisplayErrors(string strErrors)
        {
            if (bSilentmode)
            {
                StreamWriter sw = File.AppendText(errorFile);
                sw.WriteLine(strErrors);
                sw.Close();
            }
            else
            {
                MessageBox.Show(strErrors);
            }
        }

        private void Protect()
        {
            protectionLevel++;
            if (protectionLevel >= 1)
            {
                ComboBox1.SelectedIndexChanged -= UserInputChanged;
                ComboBox2.SelectedIndexChanged -= UserInputChanged;
                ComboBox3.SelectedIndexChanged -= UserInputChanged;
                ComboBox4.SelectedIndexChanged -= UserInputChanged;
                ComboBox5.SelectedIndexChanged -= UserInputChanged;
                ComboBox6.SelectedIndexChanged -= UserInputChanged;
                ComboBox7.SelectedIndexChanged -= UserInputChanged;
                txtParm1.Leave -= UserInputChanged;
                txtParm2.Leave -= UserInputChanged;
                txtParm3.Leave -= UserInputChanged;
                txtParm4.Leave -= UserInputChanged;
                txtParm5.Leave -= UserInputChanged;
                txtParm6.Leave -= UserInputChanged;
                txtParm7.Leave -= UserInputChanged;
                txtParm8.Leave -= UserInputChanged;
            }
        }


        private void Unprotect()
        {
            protectionLevel--;
            {
                if (protectionLevel < 0)
                {
                    MessageBox.Show("protectionLevel error");      // This should never happen
                    protectionLevel = 0;
                }
            }
            if (protectionLevel == 0)
            {
                ComboBox1.SelectedIndexChanged += UserInputChanged;
                ComboBox2.SelectedIndexChanged += UserInputChanged;
                ComboBox3.SelectedIndexChanged += UserInputChanged;
                ComboBox4.SelectedIndexChanged += UserInputChanged;
                ComboBox5.SelectedIndexChanged += UserInputChanged;
                ComboBox6.SelectedIndexChanged += UserInputChanged;
                ComboBox7.SelectedIndexChanged += UserInputChanged;

                txtParm1.Leave += UserInputChanged;
                txtParm2.Leave += UserInputChanged;
                txtParm3.Leave += UserInputChanged;
                txtParm4.Leave += UserInputChanged;
                txtParm5.Leave += UserInputChanged;
                txtParm6.Leave += UserInputChanged;
                txtParm7.Leave += UserInputChanged;
                txtParm8.Leave += UserInputChanged;
            }
        }


        #region Serial Port
        //This button controls the connect and disconnect actions based on it state


        // Node detection algorithm on connect:
        // - Open serial port
        // - Write a read status command for all nodes (node 0) twice.
        // - Read the node ID's of all received messages during the next 500 msec.
        // - Store the ID's of all received nodes in the nodes array

        public void btnConnect_Click(object sender, EventArgs e)
        {
            if (!bConnected)
            {
                bAsyncCommunicationEnabled = false;
                port.Open(cmbPortName.Text, 115200, sp_DataReceived);

                Thread.Sleep(100);      // Some delay before sending the first data

                port.WriteString("0 R 0x6041");     // Read status command on all nodes
                port.WriteString("0 R 0x6041");     // Read status command on all nodes a second time (not all nodeds respond on first call
                Stopwatch watch = new Stopwatch();
                watch.Restart();
                nodes.Clear();
                Network.Clear();

                while (watch.ElapsedMilliseconds < 500)
                {
                    if (port.CompleteLineReceived())
                    {
                        string response = port.ReadLine();
                        if (response.Split().Count() == 4)
                        {

                            int node = (int)utils.ConvertStringToInt64(response.Split()[0]);
                            if (node < 0 || node > 255)
                            {
                                MessageBox.Show("Received illegal node number " + node.ToString());
                            }
                            else
                            {

                                if (!nodes.Contains(node))
                                {
                                    nodes.Add(node);
                                    nodes.Sort();
                                    Network.Add(new Node(node));
                                }
                            }
                        }
                    }
                }

                Network.Sort(CompareNodesByID);
                if (Network.Count > 0)
                {
                    bConnected = true;


                    if (Network.Count > 1 && !bReconnect)
                    {
                        Globals.mainform.cmbNode.Items.Clear();
                        dlgConnect dlgconnect = new dlgConnect();
                        dlgconnect.Init(activeNode);
                        dlgconnect.ShowDialog();
                        activeNode = dlgconnect.getSelectedNode();
                    }

                    if (Network.Count == 1)
                    {
                        chkApplyToAllNodes.Checked = false;
                    }

                    nodeSelectorActivenode.SetInitialselection(activeNode);

                    for (int i = 0; i < Network.Count; i++)
                    {
                        Network[i].GetConnectionInfo(true);
                        Network[i].CheckConfiguration();
                    }


                    SetCurrentNode(activeNode);



                    bNetworkChanged = false;

                    foreach (NodeSelector ns in nodeselectors)
                    {
                        ns.Refresh();
                    }

                    chkApplyToAllNodes.Enabled =
                    lblNode1.Enabled = (Network.Count >= 2);










                    if (bNetworkChanged && !bReconnect)
                    {
                        MessageBox.Show("The network configuration has changed since last session\n\n" +
                                        "One or more nodes has changed automatic",
                                        "Change in network configuration detected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


                    if (!bReconnect)
                    {
                        btnInfo_Click(null, null);
                    }

                    btnConnect.Text = "Disconnect";
                    tmrStatus.Enabled = true;
                    tmrLogging.Enabled = true;
                    txtStatus.Visible = true;
                    btnFaultReset.Visible = true;
                    btnFaultQuery.Visible = true;
                    bAsyncCommunicationEnabled = true;
                }


                if (!bConnected)
                {
                    MessageBox.Show("You are not properly connected.\nPlease check all connections then re-connect.\n\n" +
                                    "Note that the controller baud rate must be set at 115200.", "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    DisconnectFromAdapter();
                }
            }
            else // Then disconnect from the controller
            {
                if (!bReconnect)
                {
                    foreach (NodeSelector ns in nodeselectors)
                    {
                        ns.SetInitialselection(ns.GetSelectedNode());
                    }
                }
                btnConnect.Text = "Connect";
                tmrStatus.Enabled = false;
                txtStatus.Visible = false;
                btnFaultReset.Visible = false;
                btnFaultQuery.Visible = false;
                bAsyncCommunicationEnabled = false;
                bConnected = false;
                if (loggingLive)                // Stop live logging
                {
                    btnStartDemo_Click(null, null);
                }
                 if (loggingLiveMeasure)
                 {
                     btnASMeasure_Click(null, null);
                 }
                Thread.Sleep(10);              // Some delay before closing the serial port
                DisconnectFromAdapter();
            }
            EnableButtons(bConnected);
        }

        //Disconnects from serial port
        private void DisconnectFromAdapter()
        {
            if (port.IsOpen())
            {
                port.Close();
            }
            else
            {
                //                MessageBox.Show("Port Already Closed");
            }
        }


        //Some buttons require access to serial port.  If these are enabled while disconnected, the program will be unstable
        //This method was implemented as a security measure
        //
        private void EnableButtons(bool enabled)
        {
            btnDownloadMacro.Enabled = enabled;
            btnDownloadAllMacros.Enabled = enabled;
            btnEraseAllMacros.Enabled = enabled;
            cmbDownloadConfig.Enabled = enabled;
            btnHome.Enabled = enabled;
            cmbActuatorDemo.Enabled = enabled;




            cmbPortName.Enabled = !enabled;
            btnStartTraceLive.Enabled = enabled;


            btnMotoroff.Enabled = enabled;
            //  btnVariablewriterWrite.Enabled = enabled;
            txtConnectionStatus.Text = enabled ? "Node " + activeNode.ToString() : "Disconnected";
            txtConnectionStatus.ForeColor = enabled ? Color.Green : Color.Red;


            


            foreach (NodeSelector ns in nodeselectors)
            {
                ns.cmbNode.Enabled = (enabled && (Network.Count > 1));
            }
        }



        //These methods write to the serial port.  You can either pass a single string or an array
        #region Write to Serial Port
        private void SerialWriter(string[] messages)
        {
            foreach (string message in messages)
            {
                port.WriteString(message);
                Thread.Sleep(25);
            }
        }

        private void SerialWriter(string message)
        {
            port.WriteString(message);
        }
        #endregion

        //These Methods will receive messages and process them based on content
        #region Reading Methods
        void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

            if (bAsyncCommunicationEnabled && port.IsOpen())
            {
                while (port.CompleteLineReceived())
                {
                    string data = port.ReadLine();
                    //                    this.BeginInvoke(new SetTextDeleg(si_DataReceived), new object[] { data });
                    BeginInvoke(new SetTextDeleg(si_DataReceived), data);
                }
            }
        }

        private void si_DataReceived(string data)
        {
            int sourceNode = 0; // node that did send the message
            int index = 0;
            Int64 value = 0;
            int iValue = 0;
            bool bHandled = false;
            iWatchdog = 0;                          // Received something: restart the watchdog

            bool bOk = (data.Split().Count() == 4);

            if (bOk)
            {
                string direction = data.Split()[1].ToUpper();
                bOk = (direction == "W");
            }

            if (bOk)
            {

                sourceNode = (int)utils.ConvertStringToInt64(data.Split()[0]);
                index = (int)utils.ConvertStringToInt64(data.Split()[2]);
                value = utils.ConvertStringToInt64(data.Split()[3]);
                iValue = (int)value;

                if (sourceNode == 0)
                {
                    return;     // Ignore messages addressed to all nodes in daisy chain mode
                }

                if (sourceNode == activeNode)
                {
                    switch (index)
                    {
                        case 0x6041:        // Status word
                            bHandled = true;
                            statuscounter = 0;
                            switch (value & 0x6F)
                            {
                                case 0:
                                case 32:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Not Ready to Switch On";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 64:
                                case 96:
                                    txtStatus.BackColor = Color.Orange;
                                    txtStatus.Text = "Switch On Disabled";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 33:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Ready to Swicth On";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 35:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Switched On";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 39:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Operation Enabled";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 7:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Quick Stop Active";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 15:
                                case 47:
                                    txtStatus.BackColor = Color.Green;
                                    txtStatus.Text = "Fault Reaction Active";
                                    btnFaultReset.Enabled = false;
                                    btnFaultQuery.Enabled = false;
                                    break;
                                case 8:
                                case 40:    // Fault: Get fault code
                                    SerialWriter(activeNode.ToString() + " R 0x603F");
                                    break;
                                default:
                                    txtStatus.BackColor = Color.Red;
                                    txtStatus.Text = "Undef" + value.ToString();
                                    break;
                            }
                            txtStatus.DeselectAll();

                            break;

                        case 0x603F:
                            errorCode = (int)value & 0xFFFF;
                            txtStatus.BackColor = Color.Red;
                            txtStatus.Text = "Error 0x" + errorCode.ToString("X4");
                            btnFaultReset.Enabled = true;
                            btnFaultQuery.Enabled = true;

                            bHandled = true;
                            break;

                        default:
                            break;
                    }

                } //


                if (!bHandled && loggingLive)
                {
                    long now = stopWatch.ElapsedMilliseconds;

                    foreach (logitem item in logitems)
                    {
                        if ((item.GetCombinedIndex() == index) && (item.getLastNode() == sourceNode))
                        {
                            bHandled = true;
                            item.SetValue(iValue, now);
                            item.ShowValue();
                            sendobject1counter--;
                            item.series.Points.Add(new DataPoint(now, item.PeekValue()));

                            if (now > chartRun.ChartAreas[0].AxisX.Maximum)
                            {
                                chartRun.ChartAreas[0].AxisX.Maximum = now;
                                chartRun.ChartAreas[0].AxisX.Minimum = now - tracesamples * traceinterval;
                            }

                            if (item.series.Points.Count > tracesamples)
                            {
                                item.series.Points.RemoveAt(0);
                            }
                        }
                    }

                    ShowCounters();
                }

                 if (!bHandled && loggingLive)
                 {
                     long nowMeasure = stopwatchMeasure.ElapsedMilliseconds;

                    Int64 Currentposition = 1;
                    port.GetObjectValue(activeNode, 0x6064, ref Currentposition);

                    foreach (logitem1 item in logitems1)
                     {
                         if ((item.GetCombinedIndexMeasure() == index) && (item.getLastNodeMeasure() == sourceNode))
                         {
                             bHandled = true;
                             item.SetValueMeasure(iValue, Currentposition);
                             item.ShowValueMeasure();
                             sendobject2counter--;
                             item.seriesMeasure.Points.Add(new DataPoint(Currentposition, Currentposition));

                             if (nowMeasure > chartMeasure.ChartAreas[0].AxisX.Maximum)
                             {
                                 chartMeasure.ChartAreas[0].AxisX.Maximum = nowMeasure;
                                 chartMeasure.ChartAreas[0].AxisX.Minimum = nowMeasure - tracesamplesMeasure * traceintervalMeasure;
                             }

                             if (item.seriesMeasure.Points.Count > tracesamplesMeasure)
                             {
                                 item.seriesMeasure.Points.RemoveAt(0);
                             }
                         }

                    }

                    ShowCounters();
                 }
                 
            }

          if (!bHandled)  // Then this is the response on a get command coming from the controller.
          {
              if (bTuningmode)
              {
                  received.Add(new Message(index, value));
              }
              else
              {
                  outputWindow.AddMessage(data, index);
              }
          }
      }
      #endregion

      #endregion

      #region macro building

      //This loads a saved macro from a file

      private void Title()
      {
          Text = strName + " " + strVersion + strRemark;
          if (inputFile != "")
          {

              Text = inputFile + " - " + Text;
          }
      }

      private void parseProgramfile()
      {
          if (inputFile == "")
          {
              Title();
              return;
          }

          if (!File.Exists(inputFile))
          {
              MessageBox.Show("Cannot open last file " + inputFile, "File open error", MessageBoxButtons.OK, MessageBoxIcon.Error);
              inputFile = "";
              Title();
              return;
          }

          {
              SequenceSaver saver = new SequenceSaver();

              Title();
              string strtemp;
              string mnemonic;
              string parameters;
              string comment;
              int foundIndex = -1;
              bool bOk;
              string strReason = "";
              theProgram.Clear();
              dgvProgram.Rows.Clear();

              foreach (string s in System.IO.File.ReadAllLines(inputFile))
              {
                  if (s.Trim() != "") // Skip blank lines on input.
                  {
                      if (s[0] != '[')        // Skip lines that start with [
                      {
                          theProgram.Add(s);
                      }
                  }
              }

              for (int i = 0; i < theProgram.Count; i++)
              {
                  bool bCommentedOut = false;
                  bOk = true;
                  mnemonic = "";
                  parameters = "";
                  comment = "";
                  int index;
                  strtemp = theProgram[i].Trim();
                  if (strtemp.IndexOf(@"//") == 0) // Then this line has been commented out
                  {
                      strtemp = strtemp.Substring(2); // Remove the commented out marker
                      bCommentedOut = true;           // And process the line as any other program line
                  }

                  index = strtemp.IndexOf(@"//");               // Check if a comment is present on this line
                                                                // @ sign means ignore the special meaning of the \ characher

                  if (index >= 0)
                  {
                      comment = strtemp.Substring(index);       // This includes the //
                      comment = comment.Remove(0, 2).Trim();           // Remove the //
                      strtemp = strtemp.Remove(index).Trim();          // Remove the comment from the original string
                  }

                  strtemp = strtemp.Replace('\t', ' ');           // replace tab characters by spaces

                  if (strtemp.Length > 0)
                  {
                      index = strtemp.IndexOf(" ");             // Check if a command and parameters are present
                      if (index >= 0)                           // Then a command and a parameter list are present
                      {
                          parameters = strtemp.Substring(index); // This includes the terminating space
                          parameters = parameters.Trim();
                          strtemp = strtemp.Remove(index).Trim();  // Remove the parameters from the string
                      }
                  }
                  else
                  {
                      bOk = false;
                      strReason = "No command field (empty comment lines are not allowed)";
                  }
                  if (bOk)
                  {
                      foundIndex = -1;
                      mnemonic = strtemp.Trim();
                      for (int j = 0; j < Globals.sequences.sequencelist.Count; j++)
                      {
                          if (Globals.sequences.sequencelist[j].GetMnemonic() == mnemonic)
                          {
                              foundIndex = j;
                          }
                      }
                      if (foundIndex < 0)     // Then the command is not known
                      {
                          bOk = false;
                          strReason = "The command \"" + strtemp + "\" Does not exist";
                      }
                  }
                  if (bOk)        // Let the command check if the parameters are ok
                  {
                      Protect();
                      actualSequence = Globals.sequences.sequencelist[foundIndex];
                      actualSequence.Init();
                      actualSequence.SetComment(comment);
                      actualSequence.SetParameters(parameters);

                      Unprotect();
                      strReason = actualSequence.GetError();
                      if (strReason != "")
                      {
                          parameters = actualSequence.GetParameterString();       // Get corrected parameter string from the actual sequence

                          DisplayErrors("Parameter error in File " + inputFile + " Line " + (i + 1).ToString() + " :\n" + theProgram[i] + "\n" + strReason + "\n\n" +
                                          "Parameterstring automatic replaced by: \n" + parameters);
                          bOk = true;     // Do not delete lines that have invalid parameters, bOk still true
                      }
                      else
                      {
                          string newParameters = actualSequence.GetParameterString();
                          if (parameters != newParameters)
                          {
                              DisplayErrors("Parameters in File " + inputFile + " Line " + (i + 1).ToString() + "\n" +
                                            parameters + "\n" +
                                            "Changed in:\n" +
                                            newParameters);
                              parameters = newParameters;
                          }
                      }
                  }
                  if (bOk)    // Then add this line to the datagrid view
                  {
                      int n = dgvProgram.Rows.Add();      // Add this line to the program
                      dgvProgram.Rows[n].Cells[COL_COUNT].Value = actualSequence.GetSize();
                      dgvProgram.Rows[n].Cells[COL_COMMAND].Value = mnemonic;
                      dgvProgram.Rows[n].Cells[COL_PARAMETER].Value = parameters;
                      dgvProgram.Rows[n].Cells[COL_COMMENT].Value = comment;
                      dgvProgram.Rows[n].Cells[COL_LINE].Value = "";
                      dgvProgram.Rows[n].Cells[COL_COMMENTED_OUT].Value = bCommentedOut ? STR_COMMENTED_OUT : "";
                      dgvProgram.Rows[n].Cells[COL_COUNT_SAVE].Value = dgvProgram.Rows[n].Cells[COL_COUNT].Value;
                  }
                  else
                  {
                      DisplayErrors("Syntax error in File " + inputFile + " Line " + (i + 1).ToString() + " :\n" + theProgram[i] + "\n" + strReason);
                  }
              }

              ProcessMacroLines(true);
              dgvProgram.ClearSelection();
              UpdateActualSequence(saver);
              SetEditButtons();
                
          }
      }

      private void btnOpenMacro_Click(object sender, EventArgs e)
      {
          if (!UnsavedChanges("Are you sure to open another file ?"))
          {
              OpenFileDialog browseFile = new OpenFileDialog();
              browseFile.Filter = STR_DEFAULT_FILETYPE_READ_LCC;
              browseFile.DefaultExt = "scc";
              if (inputFile != "")
              {
                  browseFile.InitialDirectory = Path.GetDirectoryName(inputFile);
              }
              browseFile.ShowDialog();
              inputFile = browseFile.FileName;
              //            MessageBox.Show(inputFile);         // TODO: Remove
              parseProgramfile();
              bUndownloadedMacros = true;
              bUnsavedChanges = false;

          }
      }


      //This function is used to change the visible labels basesd on selected function
      private void cmbFunction_SelectedIndexChanged(object sender, EventArgs e)
      {

      }


      #endregion

      #region General

      private void btnFaultReset_Click(object sender, EventArgs e)
      {
          string[] resetCommands = new string[3];
          resetCommands[0] = activeNode.ToString() + " W 0x6040 0";
          resetCommands[1] = activeNode.ToString() + " W 0x6040 128";
          resetCommands[2] = activeNode.ToString() + " W 0x6040 6";
          SerialWriter(resetCommands);
      }

      private void tmrStatus_Tick(object sender, EventArgs e)
      {
          statuscounter++;                    // Increment the status counter
          if (bAsyncCommunicationEnabled)
          {
              if (statuscounter >= 10)
              {
                  SerialWriter(activeNode.ToString() + " R 0x6041");     // Force a read status every 10 ticks
                  statuscounter = 0;
              }


              iWatchdog++;
              if (iWatchdog == 30)        // This is 3 seconds
              {
                  if (MessageBox.Show("Communication with the controller is lost.\n\nCheck the serial port connection", "Connection Lost", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                  {
                      iWatchdog = 0;
                  }
                  else
                  {
                      btnConnect_Click(null, null);
                  }
              }
          }
      }
      #endregion

      #region tuning

      //Loads a saved tuning configuration from a file




      //Saves the current tuning configuration to a file
      private void btnSaveConfigtoFile_Click(object sender, EventArgs e)
      {
      }






      //Downloads a tuning configuration to the controller
      private void cmbDownloadConfig_Click(object sender, EventArgs e)
      {
          configurationFile config = new configurationFile(fullpath);
          bConfigurationDownloaded = false;

          foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.mlc*", SearchOption.AllDirectories))
          {
              if (config.ReadfromFile(s))
              {
                  config.WriteToController();
              }

              // And finaly write to non volatile memory
              port.WriteString(activeNode.ToString() + " W 0x11010 1702257011");  // Object 0x1010, sub1 - Store all parameters with signature "save" 

              // TODO: Maybe a check if all parameters are written correctly
              MessageBox.Show("Configuration Successfully Downloaded.");
              currentNode.CheckConfiguration();
              bConfigurationDownloaded = true;
          }
          if (DialogResult.Yes == MessageBox.Show("Are you sure you want to download a configuration file into the controllers non-volatile memory ?", "Download configuration", MessageBoxButtons.YesNo, MessageBoxIcon.Question))

          {
              string infile = "";
              OpenFileDialog browseFile = new OpenFileDialog();
              browseFile.Filter = STR_DEFAULT_FILETYPE_MLC;
              browseFile.DefaultExt = "mlc";
              browseFile.ShowDialog();
              infile = browseFile.FileName;
              if (infile != "")
              {
                  if (config.ReadfromFile(infile))
                  {
                      config.WriteToController();
                  }

                  // And finaly write to non volatile memory
                  port.WriteString(activeNode.ToString() + " W 0x11010 1702257011");  // Object 0x1010, sub1 - Store all parameters with signature "save" 

                  // TODO: Maybe a check if all parameters are written correctly
                  MessageBox.Show("Configuration Successfully Downloaded.");
                  currentNode.CheckConfiguration();
                  bConfigurationDownloaded = true;
              }
          }
      }




      #endregion

      #region Run Macro

      //Runs a macro of specified number
      private void btnRunMacro_Click(ComboBox cmbNode, ComboBox cmbMacro)
      {
          if (bUndownloadedMacros)
          {
              MessageBox.Show("There are changed macros that are not downloaded in the controller" + "\n\n" + "Download these macros in the controller first\n\n" + "If you choose to ignore this message you can hit the run button again.", "Macros not downloaded in controller", MessageBoxButtons.OK, MessageBoxIcon.Error);
              bUndownloadedMacros = false;
          }
          else
          {
              int node = int.Parse(cmbNode.SelectedItem.ToString());
              int macro = int.Parse(cmbMacro.SelectedItem.ToString());

              port.WriteObject(node, 0x2c02, 3, 0); // End program
              port.WriteObject(node, 0x2c04, 1, macro); // Macro call
          }
      }

      #endregion
      /*private void PopulateTooltips()
      {
          ToolTip toolTip = new ToolTip();
          toolTip.AutoPopDelay = 5000;
          toolTip.InitialDelay = 1000;
          toolTip.ReshowDelay = 500;
          toolTip.ShowAlways = true;

          toolTip.SetToolTip(this.btnConnect, "Open or close a serial connection with the LCC.");
          toolTip.SetToolTip(this.btnDownloadMacro, "Downloads the designed macro to a specified macro number in the controller.");
          toolTip.SetToolTip(this.btnOpenProgram, "Loads a saved macro from a file into the macro design area.");

          toolTip.SetToolTip(this.btnSaveLogs, "Saves the currently displayed logs to a text file.");
          toolTip.SetToolTip(this.btnStartTraceLive, "Starts logging all data in logs that have been selected by checking the corresponding box.");
          toolTip.SetToolTip(this.btnFaultReset, "Resets any fault that may have occured.  Only available when a fault is encountered.");

          toolTip.SetToolTip(this.cmbActuatorDemo, "Select the right actuator you got for demo.");
          toolTip.SetToolTip(this.btnHome, "Moves the actuator to 0 position(2mm from the rear end bumper).");


      }*/
                public void DefineToolTip(ToolTip tooltip, Control control, String text)
        {
            tooltip.ShowAlways = true;
            tooltip.UseFading = true;
            tooltip.UseAnimation = true;
            tooltip.ShowAlways = true;
            int autoPopdelay = 50 * text.Length;
            if (autoPopdelay < 5000)
            {
                autoPopdelay = 5000;
            }
            if (autoPopdelay > 30000)
            {
                autoPopdelay = 30000;
            }
            tooltip.AutoPopDelay = autoPopdelay;
            tooltip.InitialDelay = 500;
            tooltip.IsBalloon = false;
            tooltip.SetToolTip(control, text);
        }

        #region Logging
        private void EnableLogItems(bool enable)
        {
            foreach (logitem item in logitems)
            {
                item.Enable(enable);
            }
            cmbSamples.Enabled = enable;
            cmbIntervalLive.Enabled = enable;
            btnSaveLogs.Enabled = enable;


        }

        private void EnableLogItemsMeasure(bool enable)
        {
            foreach (logitem1 item in logitems1)
            {
                item.Enable(enable);
            }
            cmbSamMeasure.Enabled = enable;
            cmbILMeasure.Enabled = enable;

            btnSavelogMeasure.Enabled = enable;

        }

        private void btnStartLogging_Click(object sender, EventArgs e)
        // private void EnableCharting()
        {
        }

        private void ShowCounters()
        {
            labelctr1.Text = sendobject1counter.ToString();
            labelctr2.Text = sendobject2counter.ToString();
        }

        private void tmrLogging_Tick(object sender, EventArgs e)
        {

            if (loggingLive && bAsyncCommunicationEnabled)
            {
                //
                // First handle the responses of the last call
                //

                foreach (logitem item in logitems)      // First check if all responses are received
                {
                    if (item.IsObjectSelected())
                    {
                        if (item.ValueIsValid())
                        {
                            Globals.loglive.AddWithoutTimestamp(item.GetTimestamp().ToString() + ";" + item.GetValue().ToString() + ";");
                        }
                        else
                        {
                            Globals.loglive.AddWithoutTimestamp(";;");
                        }
                    }
                }
                Globals.loglive.AddWithoutTimestamp("\n");

                //
                // And then do requests for the next scan
                //

                foreach (logitem item in logitems)
                {
                    if (item.IsObjectSelected())
                    {
                        port.RequestObject(item.getLastNode(), item.GetCombinedIndex());
                        sendobject1counter++;
                    }
                }
                ShowCounters();
            }
        }

        private void tmrLogging_Tick1(object sender, EventArgs e)
        {

            if (loggingLiveMeasure && bAsyncCommunicationEnabled)
            {
                //
                // First handle the responses of the last call
                //

                foreach (logitem1 item in logitems1)      // First check if all responses are received
                {
                    if (item.IsObjectSelectedMeasure())
                    {
                        if (item.ValueIsValidMeasure())
                        {
                            Globals.loglive.AddWithoutTimestamp(item.GetTimestampMeasure().ToString() + ";" + item.GetValueMeasure().ToString() + ";");
                        }
                        else
                        {
                            Globals.loglive.AddWithoutTimestamp(";;");
                        }
                    }
                }
                Globals.loglive.AddWithoutTimestamp("\n");

                //
                // And then do requests for the next scan
                //

                foreach (logitem1 item in logitems1)
                {
                    if (item.IsObjectSelectedMeasure())
                    {
                        port.RequestObject(item.getLastNodeMeasure(), item.GetCombinedIndexMeasure());
                        sendobject1counter++;
                    }
                }
                ShowCounters();
            }
        }

        private void btClearLogging_Click(object sender, EventArgs e)
        {
            chartRun.Series[0].Points.Clear();



        }

        private void btClearLogging1_Click(object sender, EventArgs e)
        {


             chartMeasure.Series[0].Points.Clear();

        }

        private void btnSaveLogs_Click(object sender, EventArgs e)
        {
            if (!File.Exists(logfileName))
            {
                MessageBox.Show("Can not find file " + logfileName, "Can not open file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            SaveFileDialog browseFile = new SaveFileDialog();
            string path = System.IO.Directory.GetCurrentDirectory();
            browseFile.Filter = "csv files (*.csv)|*.csv";
            browseFile.DefaultExt = "csv";
            browseFile.ShowDialog();
            string chosenFile = browseFile.FileName;
            if (chosenFile != "")
            {

                System.IO.StreamWriter outputfile = new System.IO.StreamWriter(chosenFile);

                foreach (string s in File.ReadAllLines(logfileName))
                {
                    outputfile.WriteLine(s);
                }
                outputfile.Close();
            }
        }
        #endregion       

        private void UserInputChanged(object sender, EventArgs e)
        {
            if (actualSequence != null)
            {
                Protect();
                actualSequence.UserInputHasChanged(sender);
                Unprotect();
            }
        }

        private void btnAutoSeq_Click(object sender, EventArgs e)
        {

            int destination = activeNode;

            if ((Network.Count > 1))
            {
                if (MessageBox.Show("Step 2: Erase all content\n\n" + "The controller content should be erased before loading auto sequence program file, Please press OK", "Step 2 : Erase contents from controller", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
                else // Count macros in the controller first
                {
                    destination = 0;
                    foreach (Node n in Network)
                    {
                        n.systemMacrosLoaded = false;
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Step 2: The controller content should be erased before loading auto sequence program file, Please press OK ", "Erase contents from controller", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                else
                {
                    currentNode.systemMacrosLoaded = false;       // Will force downloading system macros again
                }
            }

            dlgDownloadMacroProgress progress = new dlgDownloadMacroProgress();
            progress.Text = "Erase progress";
            progress.lblProgress.Text = "Halting program execution";
            progress.Refresh();
            progress.Show();
            progress.BringToFront();
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Enabled = false;
            Thread.Sleep(500);
            port.WriteObject(destination, 0x32c02, 0);   // End program
            Thread.Sleep(500 + NetworkDelay());

            progress.Refresh();
            for (int macronumber = 0; macronumber < 64; macronumber++)
            {
                progress.lblProgress.Text = "Erasing macro " + (macronumber + 1).ToString() + " of 64";
                progress.BringToFront();
                progress.Refresh();
                port.WriteObject(destination, 0x42c04, macronumber); // Reset macro
                Thread.Sleep(500 + NetworkDelay());
            }
            progress.Hide();
            BringToFront();
            MessageBox.Show("All program contents are erased from the controllers memory.", "Erase successfully finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Enabled = true;
            BringToFront();
            bAsyncCommunicationEnabled = bSave;
            progress = null;

            //Sequence 3 = Load the demo program after erasing everything in the controller :: VIGNESH:: 05/31
            if (DialogResult.Yes == MessageBox.Show("Download the auto sequence program file automatically, Press OK", "Step 3 : Download Program file", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory, "Auto Sequence.lcc*", SearchOption.AllDirectories))
                {
                    inputFile = s;
                    parseProgramfile();
                    bUndownloadedMacros = true;
                    bUnsavedChanges = false;

                }
                parseProgramfile();
                bUndownloadedMacros = true;
                bUnsavedChanges = false;
            }
            DownloadMacros(-1);
            MessageBox.Show("The auto sequence program file has been downloaded to the controller", "Program file downloaded", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void cmbMeasureType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMeasureType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
            {
                txtPosCloseToPart.Text = "15";
                txtMeasureSpeed.Text = "10000";
                txtMeasureSensitivity.Text = "250";
                txtMeasureForce.Text = "1000";
                txtMeasureDT.Text = "20";                      
            }
        }

        private void cmbSwitchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSwitchType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
            {
                txtPosCloseSwitch.Text = "18";
                txtSwitchSpeed.Text = "10000";
                txtErrorSwitch.Text = "200";
                txtSoftlandSpeedSwitch.Text = "4";            
            }
        }

        private void cmbLeakTestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbLeakTestType.SelectedIndex == 0)
            {
                txtPositionLeak.Text = "4";
                txtSpeedLeak.Text = "55";
                txtSensitivityLeak.Text = "70";
                txtForceLeak.Text = "6000";
                txtDwellLeak.Text = "300";                 
            }
        }

        private void cmbActuatorDemo_SelectedIndexChanged(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x6040, 0, 7);
            bConfigurationDownloaded = false;
            actuatorDemo.Load(cmbActuatorDemo.SelectedItem.ToString());

            strNameDemo = cmbActuatorDemo.SelectedItem.ToString();
            string parameters = strNameDemo;
            parameters.Split('-').Count();


            string Encoder = "";
            string Stroke = "";


            Stroke = (parameters.Split('-')[1].Trim());

            Encoder = (parameters.Split('-')[2].Trim());
            long StrokeFinal = Globals.utils.ConvertStringToInt64(parameters.Split('-')[1].Trim());
            long EncoderFinal = Globals.utils.ConvertStringToInt64(Encoder[1].ToString());

            //EncoderFinal = Encoder[1].ToString();
            /*if (EncoderFinal == 1)
            {
                int x = 0;

                x = (int)StrokeFinal * 1000;
            }*/

            if (EncoderFinal == 1)
            {
                //Demo tab
                txtPosition.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtSpeedRange.Text = "0-20,000,000";
                txtSoftlandRange.Text = "0-500";
                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(100);
                long MaxForce = 0;
                Globals.mainform.port.GetObjectValue(activeNode, 0x6072, ref MaxForce);
                txtForceRange.Text = "0" + "-" + Convert.ToString(MaxForce);
                


                // Measure tab
                txtMsRangePos.Text = "0" + "-" + Convert.ToString(StrokeFinal);
                txtMsRangeSpeed.Text = "0-20,000";
                txtMsRangeSen.Text = "0-500";
                txtMsRangeF.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtMsRangeDT.Text = "0-2000";
                bAsyncCommunicationEnabled = bsave;


                //Switch Test tab
                txtPosSwitchRange.Text = "0" + "-" + Convert.ToString(StrokeFinal);
                txtSwitchSpeedRange.Text = "0-20,000";
                txtSSSRange.Text = "0-20";
                txtErrorSwitchRange.Text = "0-500";


                // leak Test tab
                txtPositionLeakRange.Text = "0-12";
                txtSpeedLeakRange.Text = "0-80,000";
                txtSensitivityLeakRange.Text = "0-500";
                txtForceLeakRange.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtDwellLeakRange.Text = "0-2000";
                
                


            }

            if (EncoderFinal == 5)
            {
                txtPosition.Text = "0" + "-" + Convert.ToString((StrokeFinal * 1000) / 5);
                txtSpeedRange.Text = "0-20,000,000";
                txtSoftlandRange.Text = "0-500";
                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(100);
                long MaxForce = 0;
                txtForceRange.Text = "0" + "-" + port.GetObjectValue(activeNode, 0x6072, ref MaxForce);
                bAsyncCommunicationEnabled = bsave;
                txtForceSlopeRange.Text = "0-20000";


                // Measure tab
                txtMsRangePos.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtMsRangeSpeed.Text = "0-20,000,000";
                txtMsRangeSen.Text = "0-500";
                txtMsRangeF.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtMsRangeDT.Text = "0-2000";
              


                //Switch Test tab
                txtPosSwitchRange.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtSwitchSpeedRange.Text = "0-20,000,000";
                txtSSSRange.Text = "0-20,000";
                txtErrorSwitchRange.Text = "0-500";


                // leak Test tab
                txtPositionLeakRange.Text = "0-12,000";
                txtSpeedLeakRange.Text = "0-80,000,000";
                txtSensitivityLeakRange.Text = "0-500";
                txtForceLeakRange.Text = "0" + "-" + Convert.ToString(MaxForce); 
                txtDwellLeakRange.Text = "0-2000";

                bAsyncCommunicationEnabled = bsave;
            }

            if (EncoderFinal == 0)
            {
                txtPosition.Text = "0" + "-" + Convert.ToString((StrokeFinal * 10000) / 5);
                txtSpeedRange.Text = "0-20,000,000";
                txtSoftlandRange.Text = "0-500";
                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(100);
                long MaxForce = 0;
                txtForceRange.Text = "0" + "-" + port.GetObjectValue(activeNode, 0x6072, ref MaxForce);
                bAsyncCommunicationEnabled = bsave;
                txtForceSlopeRange.Text = "0-20000";

                // Measure tab
                txtMsRangePos.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtMsRangeSpeed.Text = "0-20,000,000";
                txtMsRangeSen.Text = "0-500";
                txtMsRangeF.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtMsRangeDT.Text = "0-2000";



                //Switch Test tab
                txtPosSwitchRange.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtSwitchSpeedRange.Text = "0-20,000,000";
                txtSSSRange.Text = "0-20,000";
                txtErrorSwitchRange.Text = "0-500";


                // leak Test tab
                txtPositionLeakRange.Text = "0-12,000";
                txtSpeedLeakRange.Text = "0-80,000,000";
                txtSensitivityLeakRange.Text = "0-500";
                txtForceLeakRange.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtDwellLeakRange.Text = "0-2000";

                bAsyncCommunicationEnabled = bsave;

            }

            if (EncoderFinal == 2)
            {
                txtPosition.Text = "0" + "-" + Convert.ToString((StrokeFinal * 10000));
                txtSpeedRange.Text = "0-20,000,000";
                txtSoftlandRange.Text = "0-500";
                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(100);
                long MaxForce = 0;
                txtForceRange.Text = "0" + "-" + port.GetObjectValue(activeNode, 0x6072, ref MaxForce);
                bAsyncCommunicationEnabled = bsave;
                txtForceSlopeRange.Text = "0-20000";

                // Measure tab
                txtMsRangePos.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtMsRangeSpeed.Text = "0-20,000,000";
                txtMsRangeSen.Text = "0-500";
                txtMsRangeF.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtMsRangeDT.Text = "0-2000";



                //Switch Test tab
                txtPosSwitchRange.Text = "0" + "-" + Convert.ToString(StrokeFinal * 1000);
                txtSwitchSpeedRange.Text = "0-20,000,000";
                txtSSSRange.Text = "0-20,000";
                txtErrorSwitchRange.Text = "0-500";


                // leak Test tab
                txtPositionLeakRange.Text = "0-12,000";
                txtSpeedLeakRange.Text = "0-80,000,000";
                txtSensitivityLeakRange.Text = "0-500";
                txtForceLeakRange.Text = "0" + "-" + Convert.ToString(MaxForce);
                txtDwellLeakRange.Text = "0-2000";

                bAsyncCommunicationEnabled = bsave;
            }

            //Sequence 1 - Download the configuration file for right actuator and display configuration downloaded message at the end
            port.WriteString(activeNode.ToString() + " W 0x11010 1702257011");  // Object 0x1010, sub1 - Store all parameters with signature "save" 

            MessageBox.Show("Configuration Successfully Downloaded.");
            currentNode.CheckConfiguration();
            bConfigurationDownloaded = true;


            //Sequence 2- Erase all the program in the controller before loading the program file
            int destination = activeNode;

            if ((Network.Count > 1))
            {
                if (MessageBox.Show("Step 2: Erase all content\n\n" + "The controller content should be erased before loading a program file, Please press OK", "Step 2 : Erase contents from controller", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
                else // Count macros in the controller first
                {
                    destination = 0;
                    foreach (Node n in Network)
                    {
                        n.systemMacrosLoaded = false;
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Step 2: The controller content should be erased before loading a program file, Please press OK ", "Erase contents from controller", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                else
                {
                    currentNode.systemMacrosLoaded = false;       // Will force downloading system macros again
                }
            }

            dlgDownloadMacroProgress progress = new dlgDownloadMacroProgress();
            progress.Text = "Erase progress";
            progress.lblProgress.Text = "Halting program execution";
            progress.Refresh();
            progress.Show();
            progress.BringToFront();
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Enabled = false;
            Thread.Sleep(500);
            port.WriteObject(destination, 0x32c02, 0);   // End program
            Thread.Sleep(500 + NetworkDelay());

            progress.Refresh();
            for (int macronumber = 0; macronumber < 64; macronumber++)
            {
                progress.lblProgress.Text = "Erasing macro " + (macronumber + 1).ToString() + " of 64";
                progress.BringToFront();
                progress.Refresh();
                port.WriteObject(destination, 0x42c04, macronumber); // Reset macro
                Thread.Sleep(500 + NetworkDelay());
            }
            progress.Hide();
            BringToFront();
            MessageBox.Show("All program contents are erased from the controllers memory.", "Erase successfully finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Enabled = true;
            BringToFront();
            bAsyncCommunicationEnabled = bSave;
            progress = null;

            //Sequence 3 = Load the demo program after erasing everything in the controller :: VIGNESH:: 05/31
            if (DialogResult.Yes == MessageBox.Show("Download the correct program file automatically, Press OK", "Step 3 : Download Program file", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory, "*Program file.lcc*", SearchOption.AllDirectories))
                {
                    inputFile = s;
                    parseProgramfile();
                    bUndownloadedMacros = true;
                    bUnsavedChanges = false;

                }
                parseProgramfile();
                bUndownloadedMacros = true;
                bUnsavedChanges = false;
            }
            DownloadMacros(-1);
            MessageBox.Show("The correct program file has been downloaded to the controller", "Program file downloaded", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }


        private void cmbController_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbActuatorDemo.Items.Clear();

            try
            {
                if (Globals.mainform.cmbController.Text == "LAC-1")
                {
                    foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators\\LAL35-50-71S", "*.mlc*", SearchOption.AllDirectories))
                    {
                        cmbActuatorDemo.Items.Add(Path.GetFileNameWithoutExtension(s));


                    }
                }

            }
            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Re-install the application and then try again.",
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Application.Exit();
            }

            cmbActuatorDemo.SelectedIndex = 0;
        }

        private void AddTheBios()
        {
            for (int i = 0; i < bios.Lines(); i++)
            {
                Compiled.Add(bios.Line(i));
            }
        }

        private void Compile()
        {

            Compiled.Clear();

            int macroLineCounter = 0;
            int lines = 0;
            int skipped = 0;
            string strErrors = "";
            int linenumber = 0;
            bool newMacro;
            Sequence sequence = null;

            Protect();


            SequenceSaver saver = new SequenceSaver();

            foreach (DataGridViewRow row in dgvProgram.Rows)
            {
                if (row.Cells[COL_COMMENTED_OUT].Value.ToString() != STR_COMMENTED_OUT)
                {
                    newMacro = false;
                    linenumber++;
                    lines++;
                    string number = row.Cells[COL_LINE].Value.ToString().Trim(); ;
                    if (number.IndexOf("??") == 0)
                    {
                        strErrors += "Skip line " + linenumber.ToString() + " (No macronumber defined)\n";
                        lines--;
                        skipped++;
                    }
                    else
                    {
                        if (row.Cells[COL_COMMAND].Value.ToString().ToUpper() == "MACRONUMBER")
                        {
                            newMacro = true;
                        }
                        foreach (Sequence s in Globals.sequences.sequencelist)
                        {
                            if (s.GetMnemonic().ToUpper() == row.Cells[COL_COMMAND].Value.ToString().ToUpper())
                            {
                                sequence = s;
                            }

                        }
                        if (sequence == null)
                        {
                            // TODO: Some error message here
                        }
                        else
                        {
                            sequence.Exit();
                            sequence.Init();
                            sequence.SetComment(row.Cells[COL_COMMENT].Value.ToString());       // Set the comment first
                            sequence.SetParameters(row.Cells[COL_PARAMETER].Value.ToString());
                            foreach (Instruction instruction in sequence.instructions)
                            {
                                if (newMacro)
                                {
                                    string strTemp = "[" + row.Cells[COL_COMMAND].Value.ToString() + " " + row.Cells[COL_PARAMETER].Value.ToString() + "]";
                                    if (row.Cells[COL_COMMENT].Value.ToString().Trim() != "")
                                    {
                                        strTemp += "{" + row.Cells[COL_COMMENT].Value.ToString().Trim() + "}";
                                    }

                                    macroLineCounter = 0;
                                    Instruction macroNumberInstruction = new WriteInstruction(0x2C05, 1, int.Parse(row.Cells[COL_PARAMETER].Value.ToString()), strTemp);
                                    Compiled.Add(macroNumberInstruction.GetString());
                                }
                                else
                                {
                                    Instruction macroLineInstruction = new WriteInstruction(0x2C05, 2, macroLineCounter);
                                    Compiled.Add(macroLineInstruction.GetString());
                                    Compiled.Add(instruction.GetMacroCommandString());
                                    macroLineCounter++;
                                }
                            }
                        }
                    }
                }
            }

            //
            // Now add the Bios
            //

            AddTheBios();

            if (strErrors != "")
            {
                DisplayErrors("Compilation errors:\n" + strErrors);
            }
            Unprotect();
            UpdateActualSequence(saver);
            //   actuator.Load(cmbActuatorDemo.SelectedItem.ToString());
        }


        private void SetEditButtons()
        {
            if (dgvProgram.Rows.Count > 0)
            {
                if (SelectedRow() >= 0)   // Then there is a row selected in the view
                {
                    if (dgvProgram.Rows[SelectedRow()].Cells[COL_COMMENTED_OUT].Value.ToString() == STR_COMMENTED_OUT)
                    {
                        btnEnable.Text = "Enable";
                    }
                    else
                    {
                        btnEnable.Text = "Disable";
                    }

                    btnEdit.Enabled = true;
                    btnApply.Enabled = true;
                    btnInsertAbove.Enabled = true;
                    btnInsertBelow.Enabled = true;
                    btnMoveUp.Enabled = (SelectedRow() == 0) ? false : true;
                    btnMoveDown.Enabled = (SelectedRow() == (dgvProgram.Rows.Count - 1)) ? false : true;
                    btnDelete.Enabled = true;
                    btnEnable.Enabled = true;
                    dgvProgram.CurrentCell = dgvProgram.Rows[SelectedRow()].Cells[0]; // Make sure that this row is visible in the datagrid view
                }
                else
                {
                    btnEdit.Enabled = false;
                    btnApply.Enabled = false;
                    btnInsertAbove.Enabled = false;
                    btnInsertBelow.Enabled = false;
                    btnMoveUp.Enabled = false;
                    btnMoveDown.Enabled = false;
                    btnDelete.Enabled = false;
                    btnEnable.Enabled = false;
                }
            }
            else
            {
                btnEdit.Enabled = false;
                btnApply.Enabled = false;
                btnInsertAbove.Enabled = false;
                btnInsertBelow.Enabled = true;
                btnMoveUp.Enabled = false;
                btnMoveDown.Enabled = false;
                btnDelete.Enabled = false;
                btnEnable.Enabled = false;
            }
        }

        private int SelectedRow()
        {
            return dgvProgram.SelectedRows.Count == 1 ? dgvProgram.SelectedRows[0].Index : -1;
        }

        private void dgvProgram_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SetEditButtons();
            ProcessMacroLines(false);

        }

        private void MoveCurrentLine(int increment)         // Increment can be +1 or -1
        {
            for (int i = 0; i < dgvProgram.ColumnCount; i++)
            {
                string strTemp = dgvProgram.Rows[SelectedRow() + increment].Cells[i].Value.ToString();                     // Temporary save value of destination cell
                dgvProgram.Rows[SelectedRow() + increment].Cells[i].Value = dgvProgram.Rows[SelectedRow()].Cells[i].Value; // Copy contents of cell
                dgvProgram.Rows[SelectedRow()].Cells[i].Value = strTemp;                                                   // Restore contents of cell
            }
            dgvProgram.Rows[SelectedRow() + increment].Selected = true;
            SetEditButtons();
            ProcessMacroLines(true);
            bUnsavedChanges = true;
            bUndownloadedMacros = true;
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            MoveCurrentLine(-1);
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            MoveCurrentLine(+1);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int actualRow = SelectedRow();

            if (actualRow == dgvProgram.Rows.Count - 1) // Then we are going to delete the last row
            {
                dgvProgram.Rows.Remove(dgvProgram.Rows[actualRow]);
            }
            else
            {
                dgvProgram.Rows.Remove(dgvProgram.Rows[actualRow]);
                if (dgvProgram.Rows.Count > 1)
                {
                    dgvProgram.Rows[actualRow].Selected = true;
                }
            }
            SetEditButtons();
            ProcessMacroLines(true);
            bUnsavedChanges = true;
            bUndownloadedMacros = true;
        }

        private void dgvProgram_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && SelectedRow() >= 0)
            {
                btnDelete_Click(null, null);
            }
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            // set the current sequence
            string strMnemonic = dgvProgram.Rows[SelectedRow()].Cells[COL_COMMAND].Value.ToString(); // get the mnemonic
            int selectedSequence = -1;

            for (int i = 0; i < Globals.sequences.sequencelist.Count; i++)
            {
                if (Globals.sequences.sequencelist[i].GetMnemonic().ToUpper() == strMnemonic.ToUpper())
                {
                    selectedSequence = i;
                }
            }
            cmbFunction.SelectedIndexChanged -= cmbFunction_SelectedIndexChanged;   // Disable changed events from this combo box
            if (actualSequence != null)
            {
                actualSequence.Exit();
            }
            Protect();
            actualSequence = Globals.sequences.sequencelist[selectedSequence];
            cmbFunction.SelectedItem = actualSequence.GetName();

            actualSequence.Init();
            actualSequence.SetComment(dgvProgram.Rows[SelectedRow()].Cells[COL_COMMENT].Value.ToString());       // Copy content of the comment field
            actualSequence.SetParameters(dgvProgram.Rows[SelectedRow()].Cells[COL_PARAMETER].Value.ToString());  // Set the parameters to default
            actualSequence.ShowParametersInGui();
            Unprotect();
            cmbFunction.SelectedIndexChanged += cmbFunction_SelectedIndexChanged;                                // Enable changed events from this combo box again
        }

        public void SetRowColor(DataGridViewRow row)
        {
            row.DefaultCellStyle.SelectionBackColor = Color.FromArgb(50, 50, 50);   // Selection back color is allways the same
            if (row.Cells[COL_COMMENTED_OUT].Value.ToString() == STR_COMMENTED_OUT)
            {
                row.DefaultCellStyle.ForeColor = Color.FromArgb(140, 140, 140);
                row.DefaultCellStyle.BackColor = Color.Beige;
                row.DefaultCellStyle.SelectionForeColor = Color.FromArgb(140, 140, 140);
            }
            else
            {
                row.DefaultCellStyle.ForeColor = Color.Blue;
                row.DefaultCellStyle.SelectionForeColor = Color.Yellow;
                if (row.Cells[COL_COMMAND].Value.ToString().ToUpper() == "MacroNumber".ToUpper())
                {
                    row.DefaultCellStyle.BackColor = Color.DarkSalmon;
                }
                else
                {
                    row.DefaultCellStyle.BackColor = Color.Beige;
                }
            }
        }

        private void ProcessMacroLines(bool showerrors)
        {
            int currentMacro = -1;  // Set to undefined
            int selectedMacro = -1; // This is the macro that is currently selected (highlighted by the user)
            int macroLine = 0;      // Number of macros in the file

            Color backcolor = Color.Beige;
            string strError = "";

            for (int i = 0; i < macroData.Count(); i++)
            {
                macroData[i].hits = 0;
                macroData[i].instructions = 0;
            }
            foreach (DataGridViewRow row in dgvProgram.Rows)
            {
                SetRowColor(row);

                if (row.Cells[COL_COMMENTED_OUT].Value.ToString() == STR_COMMENTED_OUT)
                {
                    row.Cells[COL_LINE].Value = "";
                    row.Cells[COL_COUNT].Value = "";
                }
                else
                {
                    row.Cells[COL_COUNT].Value = row.Cells[COL_COUNT_SAVE].Value;
                    if (row.Cells[COL_COMMAND].Value.ToString().ToUpper() == "MacroNumber".ToUpper())
                    {
                        currentMacro = Int32.Parse(row.Cells[COL_PARAMETER].Value.ToString());
                        if (currentMacro < 0 || currentMacro > 59)      // Ignore illegal macro numbers
                        {
                            return;
                        }
                        macroData[currentMacro].hits++;
                        macroData[currentMacro].instructions = 0;
                        macroLine = 0;
                    }
                    else
                    {
                        if (currentMacro != -1)
                        {
                            macroLine++;
                            row.Cells[COL_LINE].Value = currentMacro.ToString() + "-" + macroLine.ToString();
                            macroData[currentMacro].instructions += int.Parse(row.Cells[COL_COUNT].Value.ToString());
                        }
                        else
                        {
                            row.Cells[COL_LINE].Value = "??";
                            strError += "Error: No macronumber defined in first line\n";
                        }
                    }
                    //                    if (row.Index == SelectedRow())
                    //                    {
                    //                        selectedMacro = currentMacro;       // Then this macro number is selected
                    //                    }
                }
                if (row.Selected)
                {
                    selectedMacro = currentMacro;       // Then this macro number is selected
                }

            }
            if (selectedMacro != -1)
            {
                cmbMacroDownload.SelectedItem = selectedMacro.ToString();
            }

            int definedmacros = 0;
            int currentmacrosize = 0;
            int memoryusage = 0;
            for (int i = 0; i < 64; i++)
            {
                if (i == selectedMacro)
                {
                    currentmacrosize = macroData[i].instructions;
                }
                memoryusage += macroData[i].instructions;

                switch (macroData[i].hits)
                {
                    case 0:         // Macro not defined
                        break;
                    case 1:         // Defined just once: this is a real good macro
                        if (macroData[i].instructions > 64)
                        {
                            strError += "Error: Macro number " + i.ToString() + " has to many instructions (" + macroData[i].instructions.ToString() + " and 64 is the maximum).\n";
                        }
                        definedmacros++;
                        break;
                    default:        // This is a multiple defined macro
                        strError += "Error: Macro number " + i.ToString() + " is defined " + macroData[i].hits.ToString() + " times.\n";
                        definedmacros++;
                        break;
                }
            }


            if (showerrors && strError != "")
            {
                DisplayErrors(strError);
            }
        }

        //
        // UpdateGui() lets the current sequence update the values from the GUI to local variables and
        // also checks if there are errors in the user input.
        // Further assures that the row is visible for the user
        //

        private void UpdateFromGui(int destinationRow)
        {
            Protect();
            actualSequence.UpdateParametersFromGui();               // This can generate some error messages
            dgvProgram.Rows[destinationRow].Cells[COL_LINE].Value = "";
            dgvProgram.Rows[destinationRow].Cells[COL_COUNT].Value = actualSequence.GetSize().ToString();
            dgvProgram.Rows[destinationRow].Cells[COL_COMMAND].Value = actualSequence.GetMnemonic();
            dgvProgram.Rows[destinationRow].Cells[COL_PARAMETER].Value = actualSequence.GetParameterString();
            dgvProgram.Rows[destinationRow].Cells[COL_COMMENT].Value = actualSequence.GetComment();
            dgvProgram.Rows[destinationRow].Cells[COL_COMMENTED_OUT].Value = "";        // Not commented out
            dgvProgram.Rows[destinationRow].Cells[COL_COUNT_SAVE].Value = dgvProgram.Rows[destinationRow].Cells[COL_COUNT].Value;

            SetEditButtons();
            ProcessMacroLines(true);

            dgvProgram.CurrentCell = dgvProgram.Rows[destinationRow].Cells[0]; // Make sure that this row is visible in the datagrid view
            Unprotect();
            bUnsavedChanges = true;
            bUndownloadedMacros = true;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            UpdateFromGui(SelectedRow());                             // Update data on the current row
        }

        private void btnInsertAbove_Click(object sender, EventArgs e)
        {
            int currentRow = SelectedRow();
            dgvProgram.Rows.Insert(currentRow, 1);
            dgvProgram.Rows[currentRow].Selected = true;
            UpdateFromGui(currentRow);
        }

        private void btnInsertBelow_Click(object sender, EventArgs e)
        {
            if (dgvProgram.Rows.Count == 0)
            {
                dgvProgram.Rows.Add();
                dgvProgram.Rows[0].Selected = true;
                UpdateFromGui(0);
            }
            else
            {
                dgvProgram.Rows.Insert(SelectedRow() + 1, 1);
                dgvProgram.Rows[SelectedRow() + 1].Selected = true;
                UpdateFromGui(SelectedRow());
            }
        }

        private void dgvProgram_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnEdit_Click(null, null);
        }

        private string cleanupFilename(string filename)
        {
            string forbidden = "\\/:*?\"<>";
            //            MessageBox.Show(forbidden);

            for (int i = 0; i < forbidden.Length; i++)
            {
                filename = filename.Replace(forbidden[i], '.');
            }
            return filename;
        }

        private void saveProgram(string fileName)
        {
            //            MessageBox.Show(fileName);
            try
            {
                StreamWriter file = new System.IO.StreamWriter(fileName);
                file.WriteLine("[File created by " + strName + " " + strVersion + "]");
                foreach (DataGridViewRow row in dgvProgram.Rows)
                {
                    if (row.Cells[COL_COMMENTED_OUT].Value.ToString() == STR_COMMENTED_OUT)
                    {
                        file.Write(@"//");  // Commented out
                    }
                    file.Write(row.Cells[COL_COMMAND].Value.ToString().PadRight(16) + " " + row.Cells[COL_PARAMETER].Value.ToString());
                    if (row.Cells[COL_COMMENT].Value.ToString().Trim() != "")
                    {
                        file.Write(" // " + row.Cells[COL_COMMENT].Value.ToString());
                    }
                    file.WriteLine("");
                }
                file.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Can not save to file " + fileName + Environment.NewLine +
                                "Reason: " + e.ToString(), "File write error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnSaveProgram_Click(object sender, EventArgs e)
        {
            string chosenFile;
            StreamWriter file = null;

            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = STR_DEFAULT_FILETYPE_WRITE_LCC;
            browseFile.DefaultExt = "lcc";
            browseFile.FileName = Path.GetFileName(inputFile);
            if (inputFile != "")
            {
                browseFile.InitialDirectory = Path.GetDirectoryName(inputFile);
            }
            if (browseFile.ShowDialog() == DialogResult.OK)
            {
                chosenFile = browseFile.FileName;
                if (chosenFile != "")
                {
                    inputFile = chosenFile;
                    Title();
                    saveProgram(chosenFile);
                    bUnsavedChanges = false;
                }
            }
            if (!dlgsavecomposer.DonotShowAgain())
            {
                dlgsavecomposer.ShowDialog();
            }
            if (dlgsavecomposer.LastAnswer == "Yes")
            {
                Compile();
                browseFile.Filter = STR_DEFAULT_FILETYPE_MLM;
                browseFile.FileName = Path.GetFileNameWithoutExtension(inputFile);
                browseFile.DefaultExt = "mlm";
                if (browseFile.ShowDialog() == DialogResult.OK)
                {
                    chosenFile = browseFile.FileName;
                    if (chosenFile != "")
                    {
                        file = new System.IO.StreamWriter(chosenFile);
                        file.WriteLine("// File created by " + strName + " " + strVersion);
                        file.WriteLine("// Manual edits will NOT be imported in the original " + strName + " file.");
                        file.WriteLine("//");
                        foreach (string s in Compiled)
                        {
                            file.WriteLine(s);
                        }
                        file.Close();
                    }
                }
            }
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            if (bConnected)
            {
                currentNode.GetConnectionInfo(false);
                currentNode.CheckConfiguration();
            }
            else
            {
                MessageBox.Show("You are not connected to any controller");
            }
        }

        private bool UnsavedChanges(string question)
        {
            bool retval = false;
            if (bUnsavedChanges)
            {
                if (MessageBox.Show(question + "\n\nAny unsaved changes will be lost", "Confirm ignore unsave changes", MessageBoxButtons.YesNo, MessageBoxIcon.Error) != DialogResult.Yes)
                {
                    retval = true;
                }
                else
                {
                    bUnsavedChanges = false;
                }
            }
            return retval;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //            MessageBox.Show("Form Closing Event");

            if (numPorts > 0)
            {
                Properties.Settings.Default.Comport = cmbPortName.SelectedItem.ToString();
            }
            // Properties.Settings.Default.Actuator = int.Parse(cmbActuator.SelectedItem.ToString());    
            Properties.Settings.Default.Samples = cmbSamples.SelectedItem.ToString();
            Properties.Settings.Default.SampleInterval = cmbIntervalLive.SelectedItem.ToString();

            //    Properties.Settings.Default.LiveObject1 = logitems[0].GetCombinedIndex();
            //  Properties.Settings.Default.LiveObject2 = logitems[1].GetCombinedIndex();
            // Properties.Settings.Default.LiveObject3 = logitems[2].GetCombinedIndex();
            // Properties.Settings.Default.LiveObject4 = logitems[3].GetCombinedIndex();

            Properties.Settings.Default.Node1 = nodeSelector1.GetSelectedNode();
            Properties.Settings.Default.Node2 = nodeSelector2.GetSelectedNode();

            Properties.Settings.Default.Node3 = nodeSelector3.GetSelectedNode();
            Properties.Settings.Default.Node4 = nodeSelector4.GetSelectedNode();






            //Properties.Settings.Default.MacroNumber1 = int.Parse(cmbMacroNumber1.SelectedItem.ToString());
            //  Properties.Settings.Default.MacroNumber2 = int.Parse(cmbMacroNumber2.SelectedItem.ToString());
            //  Properties.Settings.Default.MacroNumber3 = int.Parse(cmbMacroNumber3.SelectedItem.ToString());
            //  Properties.Settings.Default.MacroNumber4 = int.Parse(cmbMacroNumber4.SelectedItem.ToString());


            //  Properties.Settings.Default.NodeVariableWriter = nodeSelectorVariableWriter.GetSelectedNode();


            Properties.Settings.Default.ApplyToAllNodes = chkApplyToAllNodes.Checked;


            //   Properties.Settings.Default.WriteObject = selectorWritevariable.GetSelectedIndex();

            Properties.Settings.Default.Inputfile = inputFile;

            Properties.Settings.Default.AddTimestamp = outputWindow.chkAddtimestamp.Checked;
            Properties.Settings.Default.AddName = outputWindow.chkAddname.Checked;

            Properties.Settings.Default.OutputWindowVisible = (outputWindow.WindowState == FormWindowState.Normal);

            Properties.Settings.Default.Node = activeNode;








            Properties.Settings.Default.PrimaryAxisMinRun = (int)numPrimaryaxisMinRun.Value();
            Properties.Settings.Default.PrimaryAxisMaxRun = (int)numPrimaryaxisMaxRun.Value();
            Properties.Settings.Default.SecondaryAxisMinRun = (int)numSecondaryaxisMinRun.Value();
            Properties.Settings.Default.SecondaryAxisMaxRun = (int)numSecondaryaxisMaxRun.Value();

            Properties.Settings.Default.Logitem1primary = chkLogitem1primary.Checked;
            Properties.Settings.Default.Logitem1primary = chkLP1Measure.Checked;
            Properties.Settings.Default.Logitem1secondary = chkLogitem1secondary.Checked;
            Properties.Settings.Default.Logitem1secondary = chkLS1Measure.Checked;
            Properties.Settings.Default.Logitem2primary = chkLogitem2primary.Checked;
            Properties.Settings.Default.Logitem2primary = chkLP2Measure.Checked;
            Properties.Settings.Default.Logitem2secondary = chkLogitem2secondary.Checked;
            Properties.Settings.Default.Logitem2secondary = chkLS2Measure.Checked;



            Properties.Settings.Default.SetProtectedAccess = chkSetProtectedAccess.Checked;
            Properties.Settings.Default.AutoBackup = chkAutoBackup.Checked;


            Properties.Settings.Default.Save();

            if (!UnsavedChanges("Are you sure you want to exit?"))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        //
        // DownloadMacro will download the requested macro to the controller.
        // If the macronumber is -1 the all macros will be downloaded.
        // If the macronumber is 100 then only the system macros will be downloaded
        //

        private void DownloadMacros(int macronumber)
        {
            //            MessageBox.Show("DownloadMacros(" + macronumber.ToString() + ")");

            int macros = 0;
            int instructions = 0;
            bool downloadSystemmacros = false;



            int destination = activeNode;

            if (chkApplyToAllNodes.Checked)
            {
                destination = 0;        // Address all nodes
            }

            tmrStatus.Enabled = false;

            if (chkAutoBackup.Checked && macronumber != 100)
            {

                saveProgram(Path.Combine(backupFolderName, cleanupFilename(DateTime.Now.ToString()) + ".lcc"));

            }

            bool bSave = bAsyncCommunicationEnabled;
            bool downloadthismacro = false;

            SequenceSaver saver = new SequenceSaver();
            BringToFront();
            dlgDownloadMacroProgress dialog = new dlgDownloadMacroProgress();

            if (destination == 0)       // Then all nodes must be programmed
            {
                foreach (Node n in Network)
                {
                    if (!n.systemMacrosLoaded)
                    {
                        downloadSystemmacros = true;
                    }
                }
            }
            else
            {
                if (!currentNode.systemMacrosLoaded)
                {
                    downloadSystemmacros = true;
                }
            }

            if (macronumber == 100)
            {
                Compiled.Clear();
                AddTheBios();
            }
            else
            {
                dialog.lblProgress.Text = "Compiling ...";
                dialog.Show();
                dialog.Refresh();
                dialog.BringToFront();
                Compile();
            }

            bAsyncCommunicationEnabled = false;
            Enabled = false;                    // Disable all buttons during download
            Thread.Sleep(100);                  //

            port.WriteObject(destination, 0x2c02, 3, 0);     // End program

            // DIT STUK GOED CHECKEN **************

            port.WriteObject(destination, 0x2C05, 4, chkSetProtectedAccess.Checked ? 1 : 0);

            List<Node> nodesToCheck = new List<Node>();
            List<Node> nodesFailing = new List<Node>();

            if (destination == 0)
            {
                foreach (Node n in Network)
                {
                    nodesToCheck.Add(n);
                }
            }
            else
            {
                nodesToCheck.Add(currentNode);
            }

            foreach (Node n in nodesToCheck)
            {
                bool isProtected = (n.GetProtectedAccess() == 1);
                if (isProtected != chkSetProtectedAccess.Checked)
                {
                    nodesFailing.Add(n);
                }
            }

            if (nodesFailing.Count > 0)
            {
                string strNodes = "";
                if (destination == 0)
                {
                    strNodes = nodesFailing[0].ID.ToString();
                    if (nodesFailing.Count == 1)
                    {
                        strNodes = "in node " + strNodes;
                    }
                    else
                    {
                        // More than 1 nod is failing
                        strNodes = "in nodes " + strNodes;
                        for (int i = 1; i < nodesFailing.Count; i++)
                        {
                            strNodes += (", " + nodesFailing[i].ID.ToString());
                        }
                    }
                }



                if (chkSetProtectedAccess.Checked)
                {
                    MessageBox.Show("Could not set protected mode access bit " + strNodes + "\n" +
                                    "The code in the controller is not copy protected\n" +
                                    "Upgrade controller firmware to 2.0 or higher",
                                    "Warning",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Could not reset protected mode access bit " + strNodes + "\n" +
                                    "To reset this bit first erase all macros in the controller",
                                    "Warning",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Warning);
                }

            }



            // TOT HIER ***************

            foreach (string instruction in Compiled)
            {
                string writecommand = instruction.ToUpper();
                if (writecommand.IndexOf("//") > -1)                // Delete the comment
                {
                    writecommand = writecommand.Substring(0, writecommand.IndexOf("//"));
                }
                writecommand = writecommand.Trim().ToUpper();
                //                MessageBox.Show(command);
                if (writecommand != "") // Skip blank or comment only lines
                {
                    writecommand = destination.ToString() + " " + writecommand.Split()[1] + " " + writecommand.Split()[2] + " " + writecommand.Split()[3]; // replace node 0 by the destination node number
                    if (writecommand.Split()[2] == "0X012C05")    // Then this is the start of a new macro
                    {
                        //                    int number = int.Parse(writecommand.Split()[3]);
                        int number = (int)utils.ConvertStringToInt64(writecommand.Split()[3]);
                        downloadthismacro = ((number == macronumber) || ((number < 60) && (macronumber == -1)) || ((number >= 60) && downloadSystemmacros)); // Download the bios only once
                        if (downloadthismacro)
                        {
                            //                        MessageBox.Show(writecommand);
                            Enabled = false;    // Disable buttons during download
                            dialog.Show();
                            dialog.lblProgress.Text = "Writing macro " + number.ToString();
                            dialog.Refresh();
                            dialog.BringToFront();


                            string resetmacro = writecommand.Split()[0] + " " +
                                                writecommand.Split()[1] + " " +
                                                "0X042C04" + " " +                       // This is a reset macro command
                                                writecommand.Split()[3];
                            port.WriteString(resetmacro);
                            Thread.Sleep(500);                      // Just because reverse engineering showed that Ingenia does this also
                            port.WriteString(writecommand);
                            macros++;
                        }
                    }
                    else
                    {
                        if (downloadthismacro)
                        {
                            port.WriteString(writecommand);
                            instructions++;

                            if (writecommand.Split()[2] == "0X032C05")
                            {
                                Thread.Sleep(NetworkDelay());
                            }
                        }
                    }
                }
            }
            dialog.Hide();
            dialog = null;
            Activate();
            UpdateActualSequence(saver);
            Enabled = true;
            if (destination == 0)
            {
                foreach (Node n in Network)
                {
                    n.systemMacrosLoaded = true;
                }
            }
            else
            {
                currentNode.systemMacrosLoaded = true;
            }
            bAsyncCommunicationEnabled = bSave;
            bUndownloadedMacros = false;
            tmrStatus.Enabled = true;
        }

        private void btnDownloadMacro_Click(object sender, EventArgs e)
        {
            DownloadMacros(int.Parse(cmbMacroDownload.SelectedItem.ToString()));   // Get the macronumber from the combobox
        }

        private void btnDownloadAllMacros_Click(object sender, EventArgs e)
        {
            DownloadMacros(-1);     // -1 means all macros
        }

        //
        // Tracing
        //


        private void cmbBackgroundobject1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLock)
            {
                logitems[0].SelectionChanged(true);
            }
        }

        private void cmbBGobject1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLock)
            {
                logitems[0].SelectionChanged(true);
            }
        }

        private void cmbBGobject2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLock)
            {
                logitems[1].SelectionChanged(true);
            }
        }

        private void cmbBackgroundobject2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLock)
            {
                logitems[1].SelectionChanged(true);
            }
        }

        private void cmbBackgroundobject3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!bLock)
            {
                logitems[2].SelectionChanged(true);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            bool bSave = bAsyncCommunicationEnabled;
            long value = 0;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            foreach (logitem item in logitems)
            {
                if (item.IsObjectSelected())
                {
                    port.GetObjectValue(item.getLastNode(), item.GetCombinedIndex(), ref value);
                    item.SetValue((int)value, 0);
                    item.ShowValue();
                }
            }


            bAsyncCommunicationEnabled = bSave;

        }

        private void btnUpdate1_Click(object sender, EventArgs e)
        {
            bool bSave = bAsyncCommunicationEnabled;
            long value = 0;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            foreach (logitem1 item in logitems1)
            {
                if (item.IsObjectSelectedMeasure())
                {
                    port.GetObjectValue(item.getLastNodeMeasure(), item.GetCombinedIndexMeasure(), ref value);
                    item.SetValueMeasure((int)value, 0);
                    item.ShowValueMeasure();
                }
            }


            bAsyncCommunicationEnabled = bSave;

        }

        private void btnMotoroff_Click(object sender, EventArgs e)
        {
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);

            tmrLogging.Stop();
            tmrLogging.Enabled = false;
            Globals.loglive.Stop();
            // btnStartTraceLive.Text = STR_START;
            EnableLogItems(true);




            port.WriteObject(0, 0x6040, 0, 7); // Control word 7

            bAsyncCommunicationEnabled = bSave;
        }

        private void btnFaultQuery_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Error code 0x" + errorCode.ToString("X4") + "\n\n" + GetErrorString(errorCode), "Error description", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ShowContactInformation()
        {

            ContactInfo info = new ContactInfo();
            info.ShowDialog();
        }

        private void btnContactInformation1_Click(object sender, EventArgs e)
        {
            ShowContactInformation();
        }

        private void btnContactInfo2_Click(object sender, EventArgs e)
        {
            ShowContactInformation();
        }

        private void btnEraseAllMacros_Click(object sender, EventArgs e)
        {
            int destination = activeNode;

            if ((Network.Count > 1) && chkApplyToAllNodes.Checked)
            {
                if (MessageBox.Show("Erase all macros from all nodes on the network\n\n" + "Are you sure that you want to erase all macros from all controllers in the network ?", "Confirm erase", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
                else // Count macros in the controller first
                {
                    destination = 0;
                    foreach (Node n in Network)
                    {
                        n.systemMacrosLoaded = false;
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Are you sure that you want to erase all macros from the controllers memory ?", "Confirm erase", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                else
                {
                    currentNode.systemMacrosLoaded = false;       // Will force downloading system macros again
                }
            }

            dlgDownloadMacroProgress progress = new dlgDownloadMacroProgress();
            progress.Text = "Erase progress";
            progress.lblProgress.Text = "Halting program execution";
            progress.Refresh();
            progress.Show();
            progress.BringToFront();
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Enabled = false;
            Thread.Sleep(500);
            port.WriteObject(destination, 0x32c02, 0);   // End program
            Thread.Sleep(500 + NetworkDelay());

            progress.Refresh();
            for (int macronumber = 0; macronumber < 64; macronumber++)
            {
                progress.lblProgress.Text = "Erasing macro " + (macronumber + 1).ToString() + " of 64";
                progress.BringToFront();
                progress.Refresh();
                port.WriteObject(destination, 0x42c04, macronumber); // Reset macro
                Thread.Sleep(500 + NetworkDelay());
            }
            progress.Hide();
            BringToFront();
            MessageBox.Show("All macros are erased from the controllers memory.", "Erase finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Enabled = true;
            BringToFront();
            bAsyncCommunicationEnabled = bSave;
            progress = null;
        }

        private void btnNewFile_Click(object sender, EventArgs e)
        {
            if (!UnsavedChanges("Are you sure to create a new file ?"))
            {
                inputFile = "";
                Title();
                theProgram.Clear();
                dgvProgram.Rows.Clear();
                ProcessMacroLines(true);
                dgvProgram.ClearSelection();
                SetEditButtons();
            }
        }

        private void cmbPortName_Click(object sender, EventArgs e)
        {
            numPorts = SerialPort.GetPortNames().Count();

            if (numPorts == 0)
            {
                MessageBox.Show("There are no serial ports detected on your system.\n\n" +
                                "It will not be possible to connect to a controller",
                                "No serial ports found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConnect.Enabled = false;
                EnableButtons(false);
            }
            else
            {
                btnConnect.Enabled = true;
            }
            utils.FillComboBox(cmbPortName, SerialPort.GetPortNames());
        }

        private void ObjectSelectorClick(object sender, EventArgs e)
        {
            if (actualSequence != null)
            {
                Protect();
                actualSequence.ButtonClick(sender);
                Unprotect();
            }
        }



        private void ToggleOutput(int node, int bit)
        {
            if (!dlgoutputwarning.DonotShowAgain())
            {
                dlgoutputwarning.ShowDialog();
            }
            if (dlgoutputwarning.LastAnswer == "Yes")
            {
                long value = 0;
                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(100);
                port.GetObjectValue(node, 0x22A02, ref value);        // Get current value of all outputs
                value = value ^ (1 << bit);                     // Toggle the requested bit (EXOR)
                port.WriteObject(node, 0x22A02, value);               // And write back again
                bAsyncCommunicationEnabled = bsave;
            }
        }

        private void lblOut0_Click(object sender, EventArgs e)
        {

        }

        private void lblOut1_Click(object sender, EventArgs e)
        {

        }

        private void lblOut2_Click(object sender, EventArgs e)
        {

        }

        private void lblOut3_Click(object sender, EventArgs e)
        {

        }

        private void lblOut0b_Click(object sender, EventArgs e)
        {

        }

        private void lblOut1b_Click(object sender, EventArgs e)
        {

        }

        private void lblOut2b_Click(object sender, EventArgs e)
        {

        }

        private void lblOut3b_Click(object sender, EventArgs e)
        {

        }


        private void ShowWriterindex()
        {
            int index;
            index = selectorWritevariable.GetIndex(selectorWritevariable.selectedIndexString);
            // txtVariablewriterIndex.Text = Globals.utils.GetIndexString(index);
        }

        private void btnVariableWriter_Click(object sender, EventArgs e)
        {
            selectorWritevariable.SelectObject();
            ShowWriterindex();
        }

        private void btnVariablewriterWrite_Click(object sender, EventArgs e)
        {
            bool bOk = true;
            Int64 value = 0;
            int index;

            //   bOk = Globals.utils.ControlHasValidValue(txtVariablewriterValue, Int64.MinValue, Int64.MaxValue, "Value");

            /*  if (bOk)
              {
                  value = Int64.Parse(txtVariablewriterValue.Text);
              }*/
            selectorWritevariable.ShowMessageIfNotSelected(true);

            index = selectorWritevariable.GetSelectedIndex();

            /* if (bOk && (index != 0))
             {
                 if (Globals.dictionary.IsWithinLimits(value, index, true))
                 {
                     port.WriteObject(nodeSelectorVariableWriter.GetSelectedNode(), index, value);
                 }
             }*/
        }

        // 04/27::VIGNESH Changed the unit settings tab
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabMeasure.SelectedIndex == 4)

            {
                
            }
            /*  
              if (tabMeasure.SelectedIndex == 0)
              {


                  string strError = "";

                  if (!loggingLive)
                  {

                      chartMeasure.Series[0].Points.Clear();
                      chartMeasure.Series[1].Points.Clear();

                      // chartDemo.Series[3].Points.Clear();
                      tracesamples = int.Parse(cmbSamples.SelectedItem.ToString());
                      traceinterval = int.Parse(cmbIntervalLive.SelectedItem.ToString());

                      chartMeasure.ChartAreas[0].AxisX.Minimum = 0;
                      chartMeasure.ChartAreas[0].AxisX.Maximum = traceinterval * tracesamples;

                     // SetPrimaryRunAxis1(true);
                      SetPrimaryRunAxis1(true);

                    //  SetSecondaryRunAxis1(true);
                      SetSecondaryRunAxis1(true);

                     // EnableChartAxesRun();

                      EnableChartAxesRun1();
                      // btnAutoscalePrimary1.PerformClick();
                      btnASP1.PerformClick();

                      //
                      // Check if there is at least 1 object selected
                      //
                      bool bObjectselected = false;

                      foreach (logitem item in logitems)
                      {

                          item.GetValue();    // This is to reset de valueValid flag
                          if (item.IsObjectSelected())
                          {
                              bObjectselected = true;
                          }
                      }

                      if (!bObjectselected)
                      {
                          strError = "No trace objects selected";
                      }

                      if (strError == "")
                      {
                          foreach (logitem item1 in logitems)
                          {
                              foreach (logitem item2 in logitems)
                              {
                                  if ((item1 != item2) && item1.IsObjectSelected() && item2.IsObjectSelected() && ((item1.GetCombinedIndex() == item2.GetCombinedIndex()) && (item1.getLastNode() == item2.getLastNode())))
                                  {
                                      strError = "Log items must be different";
                                  }

                              }
                          }
                      }

                      if (strError != "")
                      {
                          MessageBox.Show(strError, "Can not start Logging", MessageBoxButtons.OK, MessageBoxIcon.Error);
                      }

                      if (strError == "")
                      {
                          Globals.loglive.Start(logfileName);
                          foreach (logitem item in logitems)
                          {
                              if (item.IsObjectSelected())
                              {
                                  string name = item.Name();
                                  if (Network.Count > 1)
                                  {
                                      name = "Node " + item.getLastNode().ToString() + ": " + name;
                                  }
                                  Globals.loglive.AddWithoutTimestamp("Time(msec);" + name + ";");
                                  item.series.LegendText = name;
                              }
                          }
                          Globals.loglive.AddWithoutTimestamp("\n");

                          stopWatch.Restart();

                          //              MessageBox.Show("Tracing object 0X" + liveObject.ToString("X") + " and 0X" + traceobject2.ToString("X"));
                          // btnStartTraceLive.Text = STR_STOP;
                          tmrLogging.Interval = traceinterval;
                          tmrLogging.Enabled = true;

                          EnableLogItems(false);

                          tmrLogging.Start();

                      }

                  }

                  else
                  {
                      tmrLogging.Stop();
                      tmrLogging.Enabled = false;
                      Globals.loglive.Stop();
                      btnStartTraceLive.Text = STR_START;
                      EnableLogItems(true);
                  }
                  loggingLive = !loggingLive;

              }


              else   // The Tuning tab is not selected
              {
                  bTuningmode = false;
              }
              lastTab = tabMeasure.SelectedIndex;
  */
        }

        private void CallSystemMacro(int macronumber, int accumulator, bool bWaitdone)
        {
            port.WriteObject(activeNode, 0x2c02, 3, 0);             // Do a end program first
            port.WriteObject(activeNode, 0x2c00, 1, accumulator);   // First write the accumulator
            port.WriteObject(activeNode, 0x2c04, 1, macronumber);   // Then call the macro
            if (bWaitdone)
            {

            }
        }

        long target1;
        long target2;









        private void btnUpload_Click(object sender, EventArgs e)
        {
            foreach (ConfigurationObject configurationObject in configurationObjects)
            {
                configurationObject.Read();
            }
        }

        private void DownloadConfigobjects()
        {
            foreach (ConfigurationObject configurationObject in configurationObjects)
            {
                if (!configurationObject.Isreadonly())
                {
                    configurationObject.Write();
                }
            }
        }

        private int samplesRead = 0;
        private int samplesAvailable = 0;
        private int pointer = 0;

        private bool bReceived1 = false;
        private bool bReceived2 = false;
        private bool bReceived3 = false;
        private bool bReceived4 = false;

        private bool bRequestnext = false;

        private int captureSamples = 100; // Number of samples to capture
        private int captureSamplerate = 1;  // The sample rate
        public Actuator actuator = new Actuator();
        public ActuatorDemo actuatorDemo = new ActuatorDemo();
        private string fullpath;
        private bool btnHighSpeedStopWasClicked;

        private void tmrTuning_Tick(object sender, EventArgs e)
        {
            tmrTuning.Enabled = false;
            if ((captureState != CaptureState.Idle) && !bCapture) // The the user has pressed the capture button
            {
                CallSystemMacro(61, 2, false);  // Motor off
                captureState = CaptureState.Idle;
                MessageBox.Show("Capture interrupted by user");
            }
            else
            {
                switch (captureState)
                {
                    case CaptureState.Idle:
                        if (bCapture)
                        {

                            received.Clear();
                            CallSystemMacro(61, 1, false);                      // Motor on
                            captureState = CaptureState.WaitForMacroReady;
                            nextCapturestate = CaptureState.PrepareFirstMove;
                        }
                        else
                        {

                        }
                        break;

                    case CaptureState.WaitForMacroReady:
                        bool bMacroReady = false;
                        while (received.Count >= 1)
                        {
                            if (received[0].index == 0x12c00)   // Then this is the ACCUM object
                            {
                                bMacroReady = true;
                            }
                            received.RemoveAt(0);
                        }
                        if (bMacroReady)
                        {
                            //                            MessageBox.Show("Macro Ready");
                            captureState = nextCapturestate;    // Switch to the next state
                        }
                        break;

                    case CaptureState.PrepareFirstMove:

                        DownloadConfigobjects();
                        port.WriteObject(activeNode, 0x607a, 0, (target1));
                        port.WriteObject(activeNode, 0x6083, 0, cfgAcceleration.Value() / 2);     // Go to target 1 at half acceleration
                        port.WriteObject(activeNode, 0x6084, 0, cfgDecelleration.Value() / 2);    // Go to target 1 at half deceleration
                        port.WriteObject(activeNode, 0x6081, 0, cfgVelocity.Value() / 2);         // Go to target 1 at half speed
                        port.WriteObject(activeNode, 0x2c50, 2, 0);                               // Monitor mode off during next move
                        nextCapturestate = CaptureState.PrepareSecondMove;
                        captureState = CaptureState.WaitForMacroReady;

                        CallSystemMacro(61, 3, false);                                            // Move, wait for trajectory ready and sleep 500 msec
                        break;

                    case CaptureState.PrepareSecondMove:
                        port.WriteObject(activeNode, 0x607a, 0, (target2));
                        port.WriteObject(activeNode, 0x6083, 0, cfgAcceleration.Value());         // Go to target 2 at full acceleration
                        port.WriteObject(activeNode, 0x6084, 0, cfgDecelleration.Value());        // Go to target 2 at full deceleration
                        port.WriteObject(activeNode, 0x6081, 0, cfgVelocity.Value());             // Go to target 2 at full speed
                        port.WriteObject(activeNode, 0x2c50, 1, captureSamplerate);               // Set sampling rate
                        port.WriteObject(activeNode, 0x2c50, 2, 1);                               // Monitor mode on during next move
                        CallSystemMacro(61, 4, false);                                            // Start the move

                        samplesRead = 0;
                        pointer = 0;
                        samplesAvailable = 0;
                        bRequestnext = true;
                        bReceived1 = false;
                        bReceived2 = false;
                        bReceived3 = false;
                        bReceived4 = false;
                        captureState = CaptureState.Capture;
                        break;

                    case CaptureState.Capture:
                        if (received.Count == 0)    // No messages received
                        {
                            if (samplesAvailable < captureSamples)
                            {
                                port.RequestObject(activeNode, 0x22c51);                                  // Get the number of filled entries
                            }
                        }
                        else
                        {
                            while (received.Count > 0)
                            {
                                int index = received[0].index;
                                long value = received[0].value;
                                received.RemoveAt(0);

                                switch (index)
                                {
                                    case 0x22c51:           // Filled entry values
                                        samplesAvailable = (int)(value & 0xff);                        // Due to a bug in the firmware the mask is necessary
                                        break;
                                    case 0x42c51:           // Actual entry 1
                                        capturedData[0, pointer] = (int)value;
                                        bReceived1 = true;
                                        break;
                                    case 0x52c51:           // Actual entry 2
                                        capturedData[1, pointer] = (int)value;
                                        bReceived2 = true;
                                        break;
                                    case 0x62c51:           // Actual entry 3
                                        capturedData[2, pointer] = (int)value;
                                        bReceived3 = true;
                                        break;
                                    case 0x72c51:           // Actual entry 4
                                        capturedData[3, pointer] = (int)value;
                                        bReceived4 = true;
                                        break;
                                }
                                if (bReceived1 && bReceived2 && bReceived3 && bReceived4)
                                {
                                    bReceived1 = false;
                                    bReceived2 = false;
                                    bReceived3 = false;
                                    bReceived4 = false;
                                    bRequestnext = true;
                                    samplesRead++;
                                    pointer++;
                                    if (samplesRead == captureSamples)
                                    {
                                        captureState = CaptureState.CaptureDone;
                                    }

                                }
                                if (bRequestnext && (samplesRead != captureSamples) && (samplesAvailable > samplesRead))
                                {
                                    bRequestnext = false;
                                    port.WriteObject(activeNode, 0x32c51, samplesRead);      // Set the entry number
                                    port.RequestObject(activeNode, 0x42c51);                // Get actual entry 1
                                    port.RequestObject(activeNode, 0x52c51);                // Get actual entry 2
                                    port.RequestObject(activeNode, 0x62c51);                // Get actual entry 3
                                    port.RequestObject(activeNode, 0x72c51);                // Get actual entry 4
                                }
                            }
                        }
                        break;
                    case CaptureState.CaptureDone:

                        // UpdateCaptureChartDemo();
                        bCapture = false;
                        UpdateHighSpeed();
                        UpdatePrecisionPosition();
                        //                        btnStartTest_Click(null, null);
                        captureState = CaptureState.Idle;
                        break;
                }
            }
            tmrTuning.Enabled = true;
        }

        private void UpdateHighSpeed()
        {
            int i = 0;
                   
                for (i = 0; i < captureSamples; i++)
                {
                    Random n = new Random();

                    for (int j = 0; j < n.Next(1, 5); j++)
                    {
                        label2.Text = "UI Id" + ((Thread.CurrentThread.ManagedThreadId).ToString());
                        Thread th1 = new Thread(produser);
                        th1.Start();
                    }
                }
            
        }

        private void UpdatePrecisionPosition()
        {
            int i = 0;

            for (i = 0; i < captureSamples; i++)
            {
                Random n = new Random();

                for (int j = 0; j < n.Next(1, 5); j++)
                {
                    label2.Text = "UI Id" + ((Thread.CurrentThread.ManagedThreadId).ToString());
                    Thread th2 = new Thread(produser2);
                    th2.Start();
                }
            }

        }

        public void chartRun_Customize(object sender, EventArgs e)
        {

            Int64 GPR10 = 1; //handshaking Register for Position move
            Int64 GPR11 = 1; // Handshaking register for Position move in macro 41
            Int64 GPR12 = 1;  // Handshaking Register for Softland in Macro 41
            Int64 GPR13 = 1;  // Handshaking Register for Softland in Macro 42
            Int64 GPR14 = 1;  // Handshaking register for position move in Macro 42
            Int64 GPR15 = 1; //  Handshaking register for Softland move in Macro 42
            Int64 GPR16 = 1;  //  Handshaking register for Force move in Macro 42
            Int64 GPR17 = 1;  //  Handshaking register for Force move in Macro 42
            Int64 GPR18 = 1;  //  Handshaking register for Force move in Macro 42
            Int64 GPR19 = 1;  //  Handshaking register for Force move in Macro 42
          //  Int64 GPR20 = 0;  //  Handshaking register for Force move in Macro 42
            Int64 GPR21 = 0;  //  Handshaking register for Force move in Macro 42
          //  Int64 GPR22 = 1;  //  Handshaking register for Force move in Macro 42
          //  Int64 GPR23 = 0;  //  Handshaking register for Force move in Macro 42
          //  Int64 GPR24 = 0;  //  Handshaking register for Force move in Macro 42
            Int64 Currentposition = 1;
            int value = 0;
            int value2 = 0;
            int value3 = 0;


           int TargetPosition = (int)numTarget1.Value();

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(20);
            port.GetObjectValue(activeNode, 0xA2C00, ref GPR10);
            port.GetObjectValue(activeNode, 0xB2C00, ref GPR11);
            port.GetObjectValue(activeNode, 0xC2C00, ref GPR12);
            port.GetObjectValue(activeNode, 0xD2C00, ref GPR13);
            port.GetObjectValue(activeNode, 0xE2C00, ref GPR14);
            port.GetObjectValue(activeNode, 0xF2C00, ref GPR15);
            port.GetObjectValue(activeNode, 0x102C00, ref GPR16);
            port.GetObjectValue(activeNode, 0x112C00, ref GPR17);
            port.GetObjectValue(activeNode, 0x122C00, ref GPR18);
            port.GetObjectValue(activeNode, 0x132C00, ref GPR19);
           // port.GetObjectValue(activeNode, 0x142C00, ref GPR20);
            port.GetObjectValue(activeNode, 0x152C00, ref GPR21);
            //port.GetObjectValue(activeNode, 0x162C00, ref GPR22);
            //port.GetObjectValue(activeNode, 0x172C00, ref GPR23);
            //port.GetObjectValue(activeNode, 0x182C00, ref GPR24);

            port.GetObjectValue(activeNode, 0x6064, ref Currentposition);

            // btnAutoscalePrimary1.PerformClick();
            for (int i = 0; i < chartRun.Series[0].Points.Count; i++)
            {
                btnAutoscalePrimary1.PerformClick();
                //int[] value2;
                //value2 = (int)chartRun.Series[0].Points.Count;
                value = (int)chartRun.Series[0].Points[i].YValues[0];
                // chartRun.Series[0].Points[i].Color = Color.Blue;

                // chartRun.Series[0].Points[i].Color = Color.Blue;
                if (GPR10 == 1)
                {
                    chartRun.Series[0].Points[i].Color = Color.Blue;
                   
                }

                if (GPR11 == 1)
                {
                    chartRun.Series[0].Points[i].Color = Color.Blue;
                }

                if ((value > TargetPosition) && (GPR11 == 1))
                {
                    chartRun.Series[0].Points[i].Color = Color.Purple;
                }

                /*if ((value < GPR13) && (GPR11 == 1)&&( value>TargetPosition))
                {
                  chartRun.Series[0].Points[i].Color = Color.Purple;
                  
                }

                if ((value < GPR13) && (GPR12 == 1)&&(GPR19==0))
                {
                    chartRun.Series[0].Points[i].Color = Color.Purple;

                }*/

                if ((GPR14 == 1))
                {

                    if ((value < TargetPosition))
                    {
                        chartRun.Series[0].Points[i].Color = Color.Blue;
                    }

                    /*if ((value < TargetPosition) && (GPR18 == 1) && (GPR15 == 2))
                    {
                        chartRun.Series[0].Points[i].Color = Color.LimeGreen;
                    }*/

                    if ((value > TargetPosition))
                    {
                        chartRun.Series[0].Points[i].Color = Color.Purple;

                    }

                    if ((GPR15 == 1) && (value > GPR13) && (value > TargetPosition))
                    {
                        chartRun.Series[0].Points[i].Color = Color.LimeGreen;
                    }

                    if ((GPR16 == 1) && (GPR15 == 2) && (value > GPR13) && (value > TargetPosition))
                    {
                        chartRun.Series[0].Points[i].Color = Color.LimeGreen;


                        i = 100000000;
                    }
                }

                if (GPR19 == 1)
                {
                    chartRun.Series[0].Points[i].Color = Color.Blue;
                }
            }
            bAsyncCommunicationEnabled = bsave;
            // Globals.mainform.port.GetObjectValue(activeNode, 0xB2C00, ref GPR11);
            //Globals.mainform.port.GetObjectValue(activeNode, 0xC2C00, ref GPR12);       
        }

        // Calculation for Measurement tab and graph for plot
         public void chartMeasure_Customize(object sender, EventArgs e)
         {
             Int64 GPR30 = 1; //handshaking Register for Position move
             Int64 GPR31 = 1; // Handshaking register for Position move in macro 41
             Int64 GPR33 = 1; // Handshaking register for Position move in macro 41
            // double PosCloseToPart = (int)numPosCloseToPart.Value();

             bool bsave = bAsyncCommunicationEnabled;
             bAsyncCommunicationEnabled = false;
             Thread.Sleep(20);
             port.GetObjectValue(activeNode, 0x1E2C00, ref GPR30);
             port.GetObjectValue(activeNode, 0x1F2C00, ref GPR31);
             port.GetObjectValue(activeNode, 0x212C00, ref GPR33);
         
             for (int i = 0; i < chartMeasure.Series[0].Points.Count; i++)
             {
                 chartMeasure.Series[0].Points[i].Color = Color.Yellow;
                 btnAutoscalePrimary1.PerformClick();                
             }
             bAsyncCommunicationEnabled = bsave;
         }

        // Switch tab conditions and graph plotting parameters
        public void chartSwitch_Customize(object sender, EventArgs e)
        {
            /*
            Int64 GPR30 = 1; //handshaking Register for Position move
            Int64 GPR31 = 1; // Handshaking register for Position move in macro 41
            Int64 GPR33 = 1; // Handshaking register for Position move in macro 41

            int PosCloseToPart = (int)numPosCloseToPart.Value();

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(20);
            port.GetObjectValue(activeNode, 0x1E2C00, ref GPR30);
            port.GetObjectValue(activeNode, 0x1F2C00, ref GPR31);
            port.GetObjectValue(activeNode, 0x212C00, ref GPR33);
          

            for (int i = 0; i < chartMeasure.Series[0].Points.Count; i++)
            {
                chartMeasure.Series[0].Points[i].Color = Color.Yellow;
                btnAutoscalePrimary1.PerformClick();
            }
            bAsyncCommunicationEnabled = bsave;
            */
        }

        private void UpdateCaptureChartDemo()
        {
            int i = 0;

            if (samplesAvailable >= 50)
            {
                for (i = 0; i < 3; i++)
                {
                    chartRun.Series[i].Points.Clear();
                    //   chartMeasure.Series[i].Points.Clear();
                }
                /*for (i = 0; i < captureSamples; i++)
                {
                    chartMeasure.Series[0].Points.Add(new DataPoint(i * captureSamplerate, capturedData[0, i]));                          // Position actual
                      
                    chartMeasure.Series[1].Points.Add(new DataPoint(i * captureSamplerate, capturedData[1, i]));                          // Torque actual
                }*/
            }

        }

        /* private void UpdateCaptureChartMeasure()
         {
             int i = 0;

             if (samplesAvailable >= 50)
             {
                 for (i = 0; i < 3; i++)
                 {

                     chartMeasure.Series[i].Points.Clear();
                 }
                 for (i = 0; i < captureSamples; i++)
                 {
                     chartMeasure.Series[0].Points.Add(new DataPoint(i * captureSamplerate, capturedData[0, i]));                          // Position actual

                     chartMeasure.Series[1].Points.Add(new DataPoint(i * captureSamplerate, capturedData[1, i]));                          // Torque actual
                 }
             }

         }*/
        private void SetChartAxis(numericField numMin, numericField numMax, CheckBox chkAuto, Axis axis, bool bCorrectmax)
        {
            if (chkAuto.Checked)
            {
                axis.Minimum = double.NaN;  // This will force auto scale of the axis
                axis.Maximum = double.NaN;
            }
            else
            {
                SetChartAxis(numMin, numMax, axis, bCorrectmax);
            }
        }

        private void SetChartAxis(numericField numMin, numericField numMax, Axis axis, bool bCorrectmax)
        {
            if (numMin.Value() >= numMax.Value())
            {
                if (bCorrectmax)
                {
                    numMax.Setvalue(numMin.Value() + 1);
                }
                else
                {
                    numMin.Setvalue(numMax.Value() - 1);
                }
            }
            int grid = ((int)numMax.Value() - (int)numMin.Value()) / 12;
            if (grid == 0)
            {
                grid = 1;
            }

            //            MessageBox.Show(grid.ToString());
            axis.Interval = double.NaN;
            axis.MajorGrid.Interval = double.NaN;
            axis.MajorTickMark.Interval = double.NaN;

            axis.Minimum = numMin.Value();
            axis.Maximum = numMax.Value();
        }

        private void SetChartAxisMeasure(numericField numMin, numericField numMax, CheckBox chkAuto, Axis axis, bool bCorrectmax)
        {
            if (chkAuto.Checked)
            {
                axis.Minimum = double.NaN;  // This will force auto scale of the axis
                axis.Maximum = double.NaN;
            }
            else
            {
                SetChartAxisMeasure(numMin, numMax, axis, bCorrectmax);
            }
        }

        private void SetChartAxisMeasure(numericField numMin, numericField numMax, Axis axis, bool bCorrectmax)
        {
            if (numMin.Value() >= numMax.Value())
            {
                if (bCorrectmax)
                {
                    numMax.Setvalue(numMin.Value() + 1);
                }
                else
                {
                    numMin.Setvalue(numMax.Value() - 1);
                }
            }
            int grid = ((int)numMax.Value() - (int)numMin.Value()) / 12;
            if (grid == 0)
            {
                grid = 1;
            }

            //            MessageBox.Show(grid.ToString());
            axis.Interval = double.NaN;
            axis.MajorGrid.Interval = double.NaN;
            axis.MajorTickMark.Interval = double.NaN;

            axis.Minimum = numMin.Value();
            axis.Maximum = numMax.Value();
        }

        private void SetPrimaryRunAxis(bool bCorrectmax)
        {
            SetChartAxis(numPrimaryaxisMinRun, numPrimaryaxisMaxRun, chartRun.ChartAreas[0].AxisY, bCorrectmax);
            // SetChartAxis(numPrimaryaxisMinRun, numPrimaryaxisMaxRun, chartMeasure.ChartAreas[0].AxisY, bCorrectmax);
        }

         private void SetPrimaryRunAxis1(bool bCorrectmax)
         {
             SetChartAxisMeasure(numPrimaryaxisMinRun1, numPrimaryaxisMaxRun1, chartMeasure.ChartAreas[0].AxisY, bCorrectmax);
         }

        private void SetSecondaryRunAxis(bool bCorrectmax)
        {
            SetChartAxis(numSecondaryaxisMinRun, numSecondaryaxisMaxRun, chartRun.ChartAreas[0].AxisY2, bCorrectmax);

        }

        private void SetSecondaryRunAxis1(bool bCorrectmax)
         {
           SetChartAxisMeasure(numSecondaryaxisMinRun1, numSecondaryaxisMaxRun1, chartMeasure.ChartAreas[0].AxisY2, bCorrectmax);
         }



        private void ConfigObject_Keyup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                foreach (ConfigurationObject configurationOject in configurationObjects)
                {
                    configurationOject.Checkvalue();
                }
            }

        }

        private void ConfigObject_Leave(object sender, EventArgs e)
        {
            foreach (ConfigurationObject configurationOject in configurationObjects)
            {
                configurationOject.Checkvalue();
            }
        }


        private void txtPosition_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosition.Checkvalue(true);
            }

        }

        private void txtPosition_Leave(object sender, EventArgs e)
        {
            numPosition.Checkvalue(true);
        }

        private void txtPrePositionA_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrePositionA.ToString();
            }

        }

        private void txtPrePositionA_Leave(object sender, EventArgs e)
        {
            numPrePositionA.ToString();
        }

        private void txtSettleTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPreSettlingtime.Checkvalue(true);
            }

        }

        private void txtSettleTime_Leave(object sender, EventArgs e)
        {
            numPreSettlingtime.Checkvalue(true);
        }


        private void txtPreForce_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPreForce.Checkvalue(true);
            }

        }

        private void txtPreForce_Leave(object sender, EventArgs e)
        {
            numPreForce.Checkvalue(true);
        }


        private void txtPrePositionB_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrePositionB.ToString();
            }

        }

        private void txtPrePositionB_Leave(object sender, EventArgs e)
        {
            numPrePositionB.ToString() ;
        }

        private void txtPreSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPreVelocity.ToString() ;
            }

        }

        private void txtPreSpeed_Leave(object sender, EventArgs e)
        {
            numPreVelocity.ToString();
        }

        private void txtPreAcceleration_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPreAcceleration.Checkvalue(true);
            }

        }

        private void txtPreAcceleration_Leave(object sender, EventArgs e)
        {
            numPreAcceleration.Checkvalue(true);
        }

        private void txtPreDecel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPreDecel.Checkvalue(true);
            }

        }

        private void txtPreDecel_Leave(object sender, EventArgs e)
        {
            numPreDecel.Checkvalue(true);
        }





        private void txtHighSpeedTarget1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPositionA.ToString() ;
            }

        }

        private void txtHighSpeedTarget1_Leave(object sender, EventArgs e)
        {
            numPositionA.ToString() ;
        }

        private void txtHighSpeedTarget2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPositionB.ToString();
            }

        }

        private void txtHighSpeedTarget2_Leave(object sender, EventArgs e)
        {
            numPositionB.ToString();
        }

        private void txtHighSpeedVelocity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numVelocityHighSpeed.ToString();
            }

        }

        private void txtHighSpeedVelocity_Leave(object sender, EventArgs e)
        {
            numVelocityHighSpeed.ToString();
        }


        private void txtTarget1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosition1.Checkvalue(true);
            }
        }

        private void txtTarget1_Leave(object sender, EventArgs e)
        {
            numPosition1.Checkvalue(true);
        }

        private void txtProfileVelocityDemo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numVelocity.Checkvalue(true);
            }

        }

        private void txtProfileVelocityDemo_Leave(object sender, EventArgs e)
        {
            numVelocity.Checkvalue(true);
        }

        private void txtTargetForceDemo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numTargetForce.Checkvalue(true);
            }

        }

        private void txtTargetForceDemo_Leave(object sender, EventArgs e)
        {
            numTargetForce.Checkvalue(true);
        }

        private void txtTargetPositionDemo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numTarget1.Checkvalue(true);
            }

        }

        private void txtTargetPositionDemo_Leave(object sender, EventArgs e)
        {
            numTarget1.Checkvalue(true);
        }

        private void txtTargetPosition2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numTarget2.Checkvalue(true);
            }

        }

        private void txtTargetPosition2_Leave(object sender, EventArgs e)
        {
            numTarget2.Checkvalue(true);
        }

        private void txtPosCloseToPart_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosCloseToPart.ToString();
            }

        }

        private void txtPosCloseToPart_Leave(object sender, EventArgs e)
        {
            numPosCloseToPart.ToString();
        }
        

        private void txtPosCloseSwitch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPositionSwitch.ToString() ;
            }

        }

        private void txtPosCloseSwitch_Leave(object sender, EventArgs e)
        {
            numPositionSwitch.ToString() ;
        }

        private void txtSoftlandSpeedSwitch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSwitchSoftlandSpeed.ToString() ;
            }

        }

        private void txtSoftlandSpeedSwitch_Leave(object sender, EventArgs e)
        {
            numSwitchSoftlandSpeed.ToString() ;
        }

        private void txtPositionLeak_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPositionLeak.ToString();
            }

        }

        private void txtPositionLeak_Leave(object sender, EventArgs e)
        {
            numPositionLeak.ToString();
        }

        private void txtLeakSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numLeakSpeed.ToString() ;
            }

        }

        private void txtLeakspeed_Leave(object sender, EventArgs e)
        {
            numLeakSpeed.ToString();
        }

        private void txtLeakSensitivity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numLeakSensitivity.Checkvalue(true);
            }

        }

        private void txtLeakSensitivity_Leave(object sender, EventArgs e)
        {
            numLeakSensitivity.Checkvalue(true);
        }

        private void txtLeakForce_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numLeakForce.Checkvalue(true);
            }

        }

        private void txtLeakForce_Leave(object sender, EventArgs e)
        {
            numLeakForce.Checkvalue(true);
        }
     

        private void txtDwellLeak_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numLeakDwellTime.Checkvalue(true);
            }

        }

        private void txtDwellLeak_Leave(object sender, EventArgs e)
        {
            numLeakDwellTime.Checkvalue(true);
        }
       

        private void txtErrorSwitch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSwitchSensitivity.Checkvalue(true); 
            }
            
        }

        private void txtErrorSwitch_Leave(object sender, EventArgs e)
        {
            numSwitchSensitivity.Checkvalue(true);
        }

        private void txtSwitchSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSwitchSpeed.ToString() ;
            }

        }

        private void txtSwitchSpeed_Leave(object sender, EventArgs e)
        {
            numSwitchSpeed.ToString();
        }

       



        private void txtSoftlandErrorDemo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSoftlandError.Checkvalue(true);
            }

        }

        private void txtSoftlandErrorDemo_Leave(object sender, EventArgs e)
        {
            numSoftlandError.Checkvalue(true);
        }

        private void txtValue2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numFollowingerror.Checkvalue(true);
            }
            if (e.KeyCode == Keys.Enter)
            {
                numModeofop.Checkvalue(true);
            }

        }

        private void txtValue2_Leave(object sender, EventArgs e)
        {
            numFollowingerror.Checkvalue(true);
            numModeofop.Checkvalue(true);
        }

        /*private void txtValue2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numModeofop.Checkvalue(true);
            }

        }

        private void txtValue2_Leave(object sender, EventArgs e)
        {
            numModeofop.Checkvalue(true);
        }
        */
        private void txtForceSlopeDemo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numForceSlope.Checkvalue(true);
            }

        }

        private void txtForceSlopeDemo_Leave(object sender, EventArgs e)
        {
            numForceSlope.Checkvalue(true);
        }

        private void txtTarget2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosition2.Checkvalue(true);
            }
        }

        private void txtTarget2_Leave(object sender, EventArgs e)
        {
            numPosition2.Checkvalue(true);
        }

        private void txtTarget1Demo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosition1.Checkvalue(true);
            }

        }

        private void txtTarget1Demo_Leave(object sender, EventArgs e)
        {
            numPosition1.Checkvalue(true);
        }

        private void txtTarget2Demo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPosition2.Checkvalue(true);
            }
        }

        private void txtTarget2Demo_Leave(object sender, EventArgs e)
        {
            numPosition2.Checkvalue(true);
        }

       /* private void txtMeasureResult_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numMeasureResult.Checkvalue(true);  
            }
        }

        private void txtMeasureResult_Leave(object sender, EventArgs e)
        {
            numMeasureResult.Checkvalue(true);
        }*/

        private void btnSaveNonvolatile_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure you want to save the current configuartion in the controllers non-volatile memory ?", "Save in non-volatile memory", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {

                DownloadConfigobjects();
                port.WriteString(activeNode.ToString() + " W 0x11010 1702257011");  // Object 0x1010, sub1 - Store all parameters with signature "save" 

                // TODO: Maybe a check if all parameters are written correctly


                MessageBox.Show("Configuration successfully saved in the controllers non-volatile memory.");

            }


        }

        private void cmbDownloadConfigTuning_Click(object sender, EventArgs e)
        {
            cmbDownloadConfig_Click(sender, e);
            if (bConfigurationDownloaded)
            {
                btnUpload_Click(null, null);
            }
        }

        private void btnSaveconfig_Click(object sender, EventArgs e)
        {
            dlgSaveconfig dlgsaveConfig = new dlgSaveconfig();

            configurationFile configFile = new configurationFile(fullpath);

            string chosenInputfile = "";
            string chosenOutputfile = "";

            OpenFileDialog dlgInputfile = new OpenFileDialog();
            SaveFileDialog dlgOutputfile = new SaveFileDialog();

            dlgOutputfile.Filter = STR_DEFAULT_FILETYPE_MLC;
            dlgOutputfile.DefaultExt = "mlc";
            dlgOutputfile.Title = "Select output file";
            dlgOutputfile.OverwritePrompt = true;

            dlgInputfile.Filter = STR_DEFAULT_FILETYPE_MLC;
            dlgInputfile.DefaultExt = "mlc";
            dlgInputfile.Title = "Select input file";

            bool bOk = true;

            if (DialogResult.OK == dlgsaveConfig.ShowDialog())
            {
                if (dlgsaveConfig.MergeFileSelected()) // Then need an existing inputfile
                {
                    if (DialogResult.OK == dlgInputfile.ShowDialog())
                    {
                        chosenInputfile = dlgInputfile.FileName;
                        configFile.ReadfromFile(chosenInputfile);
                    }
                    else
                    {
                        bOk = false;
                    }
                }
                foreach (ConfigurationObject configurationObject in configurationObjects)
                {
                    if (!configurationObject.Isreadonly())
                    {
                        configFile.Add(configurationObject.Index(), configurationObject.Value());
                    }
                }
                if (bOk && (DialogResult.OK == dlgOutputfile.ShowDialog()))
                {
                    chosenOutputfile = dlgOutputfile.FileName;
                    if (chosenInputfile == chosenOutputfile)
                    {
                        MessageBox.Show("To merge the input file and output file must be different");
                    }
                    else
                    {
                        configFile.WritetoFile(chosenOutputfile);
                        MessageBox.Show("Configuration saved in " + chosenOutputfile);
                    }
                }
            }
        }

        private void btnSaveInController_Click(object sender, EventArgs e)
        {
            foreach (ConfigurationObject configurationObject in configurationObjects)
            {
                configurationObject.Write();
            }
            MessageBox.Show("Configuration successfully saved in the controller.");
        }

        private void EnableChartAxis(CheckBox chkPrimary, CheckBox chkSecondary, Series series, int objectIndex)
        {
            series.Enabled = (chkPrimary.Checked || chkSecondary.Checked) && (objectIndex != 0);
            if (chkPrimary.Checked)
            {
                series.YAxisType = AxisType.Primary;
                series.Legend = "LegendPrimary";
                // series.Legend = "LegendPrimary1";

            }
            if (chkSecondary.Checked)
            {
                series.YAxisType = AxisType.Secondary;
                series.Legend = "LegendSecondary";
                //  series.Legend = "LegendSecondary1";
            }
        }

        private void EnableChartAxis1(CheckBox chkPrimary1, CheckBox chkSecondary1, Series series, int objectIndex)
        {
            series.Enabled = (chkPrimary1.Checked || chkSecondary1.Checked) && (objectIndex != 0);
            if (chkPrimary1.Checked)
            {
                series.YAxisType = AxisType.Primary;
                // series.Legend = "LegendPrimary";
                series.Legend = "LegendPrimary1";

            }

            if (chkSecondary1.Checked)
            {
                series.YAxisType = AxisType.Secondary;
                //series.Legend = "LegendSecondary";
                series.Legend = "LegendSecondary1";
            }
        }



        private void EnableChartAxesRun()
        {
            EnableChartAxis(chkLogitem1primary, chkLogitem1secondary, chartRun.Series[0], logitems[0].GetCombinedIndex());
            EnableChartAxis(chkLogitem2primary, chkLogitem2secondary, chartRun.Series[1], logitems[1].GetCombinedIndex());


            //  EnableChartAxis(chkLogitem4primary, chkLogitem4secondary, chartDemo.Series[3], logitems[3].GetCombinedIndex());

            if ((chkLogitem1primary.Checked && logitems[0].IsObjectSelected()) ||
                (chkLogitem2primary.Checked && logitems[1].IsObjectSelected()))
            {
                chartRun.ChartAreas[0].AxisY.Enabled = AxisEnabled.True;

            }
            else
            {
                chartRun.ChartAreas[0].AxisY.Enabled = AxisEnabled.False;

            }
            if ((chkLogitem1secondary.Checked && logitems[0].IsObjectSelected()) ||
                (chkLogitem2secondary.Checked && logitems[1].IsObjectSelected()))
            {
                chartRun.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;


            }

            else
            {
                chartRun.ChartAreas[0].AxisY2.Enabled = AxisEnabled.False;
            }
            //            chartRun.Invalidate();

            //            MessageBox.Show(chartRun.Series[0].YAxisType.ToString() + " " + chartRun.Series[0].Enabled.ToString() + "\n" +
            //                            chartRun.Series[1].YAxisType.ToString() + " " + chartRun.Series[1].Enabled.ToString() + "\n" +
            //                            chartRun.Series[2].YAxisType.ToString() + " " + chartRun.Series[2].Enabled.ToString() + "\n" +
            //                            chartRun.Series[3].YAxisType.ToString() + " " + chartRun.Series[3].Enabled.ToString() );


        }

         private void EnableChartAxesRun1()
         {

             EnableChartAxis1(chkLP1Measure, chkLS1Measure, chartMeasure.Series[0], logitems1[0].GetCombinedIndexMeasure());
             EnableChartAxis1(chkLP2Measure, chkLS2Measure, chartMeasure.Series[1], logitems1[1].GetCombinedIndexMeasure());

             if ((chkLP1Measure.Checked && logitems1[0].IsObjectSelectedMeasure()) ||
                          (chkLP2Measure.Checked && logitems1[1].IsObjectSelectedMeasure()))
             {

                 chartMeasure.ChartAreas[0].AxisY.Enabled = AxisEnabled.True;
             }
             else
             {

                 chartMeasure.ChartAreas[0].AxisY.Enabled = AxisEnabled.False;
             }
             if ((chkLS1Measure.Checked && logitems1[0].IsObjectSelectedMeasure()) ||
                 (chkLS2Measure.Checked && logitems1[1].IsObjectSelectedMeasure()))
             {


                 chartMeasure.ChartAreas[0].AxisY2.Enabled = AxisEnabled.True;
             }

             else
             {


                 chartMeasure.ChartAreas[0].AxisY2.Enabled = AxisEnabled.False;
             }
          
             
         }

        private void txtMinPrimaryRun_Leave(object sender, EventArgs e)
        {
            numPrimaryaxisMinRun.Checkvalue(true);
            SetPrimaryRunAxis(true);
        }

        private void txtMaxPrimaryRun_Leave(object sender, EventArgs e)
        {
            numPrimaryaxisMaxRun.Checkvalue(true);
            SetPrimaryRunAxis(false);
        }

        private void txtMinPrimaryRun1_Leave(object sender, EventArgs e)
        {
            numPrimaryaxisMinRun1.Checkvalue(true);
               SetPrimaryRunAxis1(true);
        }

        private void txtMaxPrimaryRun1_Leave(object sender, EventArgs e)
        {
            numPrimaryaxisMaxRun1.Checkvalue(true);
            SetPrimaryRunAxis1(false);
        }

        private void txtMinSecondaryRun_Leave(object sender, EventArgs e)
        {
            numSecondaryaxisMinRun.Checkvalue(true);
            SetSecondaryRunAxis(true);
        }

        private void txtMaxSecondaryRun_Leave(object sender, EventArgs e)
        {
            numSecondaryaxisMaxRun.Checkvalue(true);
            SetSecondaryRunAxis(false);
        }

        private void txtMinSecondaryRun1_Leave(object sender, EventArgs e)
        {
            numSecondaryaxisMinRun1.Checkvalue(true);
            SetSecondaryRunAxis1(true);
        }

        private void txtMaxSecondaryRun1_Leave(object sender, EventArgs e)
        {
            numSecondaryaxisMaxRun1.Checkvalue(true);
            SetSecondaryRunAxis1(false);
        }

        private void txtMinPrimaryRun_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrimaryaxisMinRun.Checkvalue(true);
                SetPrimaryRunAxis(true);
            }
        }

        private void txtMaxPrimaryRun_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrimaryaxisMaxRun.Checkvalue(true);
                SetPrimaryRunAxis(false);
            }
        }

        private void txtMinPrimaryRun1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrimaryaxisMinRun1.Checkvalue(true);
                SetPrimaryRunAxis1(true);
            }
        }

        private void txtMaxPrimaryRun1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numPrimaryaxisMaxRun1.Checkvalue(true);
                SetPrimaryRunAxis1(false);
            }
        }

        private void txtMinSecondaryRun1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSecondaryaxisMinRun1.Checkvalue(true);
                SetSecondaryRunAxis1(true);
            }
        }

        private void txtMinSecondaryRun_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSecondaryaxisMinRun.Checkvalue(true);
                SetSecondaryRunAxis(true);
            }
        }

        private void txtMaxSecondaryRun_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSecondaryaxisMaxRun.Checkvalue(true);
                SetSecondaryRunAxis(false);
            }
        }

        private void txtMaxSecondaryRun1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numSecondaryaxisMaxRun1.Checkvalue(true);
                //  SetSecondaryRunAxis1(false);
            }
        }


        private void chkLogitem1primary_MouseClick(object sender, MouseEventArgs e)
        {
            chkLogitem1secondary.Checked = false;
            chkLogitem1primary.Checked = !chkLogitem1primary.Checked;
            EnableChartAxesRun();
        }

        private void chkLP1Measure_MouseClick(object sender, MouseEventArgs e)
        {
            chkLS1Measure.Checked = false;
            chkLP1Measure.Checked = !chkLP1Measure.Checked;
            //  EnableChartAxesRun1();
        }

        private void chkLogitem1secondary_MouseClick(object sender, MouseEventArgs e)
        {
            chkLogitem1primary.Checked = false;
            chkLogitem1secondary.Checked = !chkLogitem1secondary.Checked;
            EnableChartAxesRun();
        }

        private void chkLS1Measure_MouseClick(object sender, MouseEventArgs e)
        {
            chkLP1Measure.Checked = false;
            chkLS1Measure.Checked = !chkLS1Measure.Checked;
            //  EnableChartAxesRun1();
        }

        private void chkLogitem2primary_MouseClick(object sender, MouseEventArgs e)
        {
            chkLogitem2secondary.Checked = false;
            chkLogitem2primary.Checked = !chkLogitem2primary.Checked;
            EnableChartAxesRun();
        }

        private void chkLP2Measure_MouseClick(object sender, MouseEventArgs e)
        {
            chkLS2Measure.Checked = false;
            chkLP2Measure.Checked = !chkLP2Measure.Checked;
            //  EnableChartAxesRun1();
        }

        private void chkLogitem2secondary_MouseClick(object sender, MouseEventArgs e)
        {
            chkLogitem2primary.Checked = false;
            chkLogitem2secondary.Checked = !chkLogitem2secondary.Checked;
            EnableChartAxesRun();
        }

        private void chkLS2Measure_MouseClick(object sender, MouseEventArgs e)
        {
            chkLP2Measure.Checked = false;
            chkLS2Measure.Checked = !chkLS2Measure.Checked;
            //  EnableChartAxesRun1();
        }


        private void AutoScaleAxis1(Chart chart, Axis axis, AxisType axistype, TextBox txtMin, TextBox txtMax)
        {
            int min = 0;
            int max = 0;
            bool bFirst = true;

            foreach (Series series in chart.Series)
            {
                if (series.Enabled && (series.YAxisType == axistype))
                {
                    for (int j = 1; j < (series.Points.Count - 1); j++)     // Skip the first and last point to prevent error messages
                    {
                        if (series.Points[j].XValue > chart.ChartAreas[0].AxisX.Minimum)    // Then the point is visible on the x-axis
                        {

                            int value = (int)series.Points[j].YValues[0];
                            if (bFirst)
                            {
                                bFirst = false;
                                min = value;
                                max = value;
                            }
                            else
                            {
                                if (value < min)
                                {
                                    min = value;
                                }
                                if (value > max)
                                {
                                    max = value;
                                }
                            }
                        }
                    }
                }
            }


            int delta = (max - min) / 10;
            if (delta == 0)
            {
                delta = 1;
            }
            min = min - delta;
            max = max + delta;

            int grid = (max - min) / 10;

            if (grid == 0)
            {
                grid = 1;
            }

            //
            // the rounding algoritm for the grid:
            // 1 digit grid: do not round
            // 2 digits: round to the first multiple of 5
            // 3 digits: round to the first multiple of 50
            // 4 digits: round to the nearest multiple of 500
            // etc ...
            //
            int digits = grid.ToString().Length;
            int rounding = 10;
            if (digits > 1)
            {
                while (digits > 2)
                {
                    rounding = rounding * 10;
                    digits = digits - 1;
                }
                grid = grid + (rounding / 2);
                grid = grid / rounding;
                if (grid == 0)
                {
                    grid = 1;
                }
                grid = rounding * grid;
            }

            //            MessageBox.Show(" Min = " + min.ToString() + "\nMax = " + max.ToString() + "\nMax - min = " + (max - min).ToString() + "\nRounding = " + rounding.ToString() + "\nGrid = "  + grid.ToString());

            if ((max % grid) != 0)
            {
                max = (1 + (max / grid)) * grid;
            }

            min = max - 16 * grid;

            //  min = -2000;

            axis.Minimum = min;
            axis.Maximum = max;
            //            axis.Interval = grid;
            //            axis.MajorGrid.Interval = grid;
            //            axis.MajorTickMark.Interval = grid;
            axis.Interval = 2 * grid;
            axis.MajorGrid.Interval = 2 * grid;
            axis.MajorTickMark.Interval = double.NaN;

            txtMin.Text = min.ToString();
            txtMax.Text = max.ToString();

        }

        private void AutoScaleAxis(Chart chart, Axis axis, AxisType axistype, TextBox txtMin, TextBox txtMax)
        {
            int min = 0;
            int max = 0;
            bool bFirst = true;

            foreach (Series series in chart.Series)
            {
                if (series.Enabled && (series.YAxisType == axistype))
                {
                    for (int j = 1; j < (series.Points.Count - 1); j++)     // Skip the first and last point to prevent error messages
                    {
                        if (series.Points[j].XValue > chart.ChartAreas[0].AxisX.Minimum)    // Then the point is visible on the x-axis
                        {

                            int value = (int)series.Points[j].YValues[0];
                            if (bFirst)
                            {
                                bFirst = false;
                                min = value;
                                max = value;
                            }
                            else
                            {
                                if (value < min)
                                {
                                    min = value;
                                }
                                if (value > max)
                                {
                                    max = value;
                                }
                            }
                        }
                    }
                }
            }


            int delta = (max - min) / 10;
            if (delta == 0)
            {
                delta = 1;
            }
            min = min - delta;
            max = max + delta;

            int grid = (max - min) / 10;

            if (grid == 0)
            {
                grid = 1;
            }

            //
            // the rounding algoritm for the grid:
            // 1 digit grid: do not round
            // 2 digits: round to the first multiple of 5
            // 3 digits: round to the first multiple of 50
            // 4 digits: round to the nearest multiple of 500
            // etc ...
            //
            int digits = grid.ToString().Length;
            int rounding = 10;
            if (digits > 1)
            {
                while (digits > 2)
                {
                    rounding = rounding * 10;
                    digits = digits - 1;
                }
                grid = grid + (rounding / 2);
                grid = grid / rounding;
                if (grid == 0)
                {
                    grid = 1;
                }
                grid = rounding * grid;
            }

            //            MessageBox.Show(" Min = " + min.ToString() + "\nMax = " + max.ToString() + "\nMax - min = " + (max - min).ToString() + "\nRounding = " + rounding.ToString() + "\nGrid = "  + grid.ToString());

            if ((max % grid) != 0)
            {
                max = (1 + (max / grid)) * grid;
            }

            min = max - 16 * grid;

            //  min = -2000;

            axis.Minimum = min;
            axis.Maximum = max;
            //            axis.Interval = grid;
            //            axis.MajorGrid.Interval = grid;
            //            axis.MajorTickMark.Interval = grid;
            axis.Interval = 2 * grid;
            axis.MajorGrid.Interval = 2 * grid;
            axis.MajorTickMark.Interval = double.NaN;

            txtMin.Text = min.ToString();
            txtMax.Text = max.ToString();
        }

        private void btnAutoScale_Click(object sender, EventArgs e)
        {
            int min = 0;
            int max = 0;
            int value = 0;
            int minaxis = 0;
            int maxaxis = 0;

            for (int i = 0; i < chartRun.Series[0].Points.Count; i++)
            {
                value = (int)chartRun.Series[0].Points[i].YValues[0];
                if (i == 0)
                {
                    min = value;
                    max = value;
                }
                else
                {
                    if (value < min)
                    {
                        min = value;
                    }
                    if (value > max)
                    {
                        max = value;
                    }
                }
                int factor = 1;
                while (((max - min) / factor) != 0)
                {
                    factor *= 10;
                }
                factor = factor / 100;
                if (factor == 0)
                {
                    factor = 1;
                }
                maxaxis = ((max / factor) + 1) * factor;
                minaxis = ((min / factor) - 1) * factor;
                //                txtMax1.Text = maxaxis.ToString();
                //                txtMin1.Text = minaxis.ToString();
                //                SetChartAxes();
            }

            /*for (int i = 0; i < chartMeasure.Series[0].Points.Count; i++)
            {
                value = (int)chartMeasure.Series[0].Points[i].YValues[0];
                if (i == 0)
                {
                    min = value;
                    max = value;
                }
                else
                {
                    if (value < min)
                    {
                        min = value;
                    }
                    if (value > max)
                    {
                        max = value;
                    }
                }
                int factor = 1;
                while (((max - min) / factor) != 0)
                {
                    factor *= 10;
                }
                factor = factor / 100;
                if (factor == 0)
                {
                    factor = 1;
                }
                maxaxis = ((max / factor) + 1) * factor;
                minaxis = ((min / factor) - 1) * factor;
                //                txtMax1.Text = maxaxis.ToString();
                //                txtMin1.Text = minaxis.ToString();
                //                SetChartAxes();
            }*/

        }

        private void btnAutoscalePrimary1_Click(object sender, EventArgs e)
        {
            AutoScaleAxis(chartRun, chartRun.ChartAreas[0].AxisY, AxisType.Primary, txtMinPrimaryRun, txtMaxPrimaryRun);


        }


        /*private void btnAutoscaleSecondary1_Click(object sender, EventArgs e)
        {
            AutoScaleAxis(chartRun, chartRun.ChartAreas[0].AxisY2, AxisType.Secondary, txtMinSecondaryRun, txtMaxSecondaryRun);

            AutoScaleAxis(chartMeasure, chartMeasure.ChartAreas[0].AxisY2, AxisType.Secondary, txtMinSecondaryRun, txtMaxSecondaryRun);
        }*/

        private void btnEnable_Click(object sender, EventArgs e)
        {
            string newString = STR_COMMENTED_OUT;
            if (dgvProgram.Rows[SelectedRow()].Cells[COL_COMMENTED_OUT].Value.ToString() == STR_COMMENTED_OUT)
            {
                newString = "";
            }
            dgvProgram.Rows[SelectedRow()].Cells[COL_COMMENTED_OUT].Value = newString;
            ProcessMacroLines(true);
            SetEditButtons();
            bUnsavedChanges = true;
            bUndownloadedMacros = true;
        }

        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.X >= 600 && e.X <= 610 && e.Y >= 0 && e.Y <= 1)   // Hidden sweet spot
            {
                List<String> theUploadedProgram = new List<string>();
                //                MessageBox.Show("You clicked the mouse at " + e.X.ToString() + "," + e.Y.ToString());
                if (bConnected)
                {
                    dlgDownloadMacroProgress progress = new dlgDownloadMacroProgress();
                    progress.Text = "Upload progress";
                    progress.lblProgress.Text = "Halting program execution";
                    progress.Hide();
                    if (MessageBox.Show("Are you sure that you want to upload all macros from the controllers memory ?", "Confirm upload", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.OK)
                    {
                        progress.Refresh();
                        progress.Show();
                        bool bSave = bAsyncCommunicationEnabled;
                        bAsyncCommunicationEnabled = false;
                        Enabled = false;
                        Thread.Sleep(500);
                        port.WriteObject(activeNode, 0x32c02, 0);   // End program

                        bool[] macroList = new bool[64];

                        progress.lblProgress.Text = "Counting macros in controller";

                        progress.Refresh();

                        for (int i = 0; i < 64; i++)
                        {
                            long macrovalue = 0;
                            port.WriteObject(activeNode, 0x12c05, i);   // Select macro number
                            port.WriteObject(activeNode, 0x22c05, 0);   // Select first command of the macro
                            port.GetObjectValue(activeNode, 0x32c05, ref macrovalue);
                            //                    MessageBox.Show ("Macro " + i.ToString() + macrovalue.ToString("X16"));
                            macroList[i] = (((ulong)macrovalue != 0xFFFFFFF1FFFFFFFF) && ((ulong)macrovalue != 0x7FFFFFFFFFFFFFFF));
                            //                    MessageBox.Show(macroList[i].ToString(), i.ToString());

                        }

                        int totalMacros = 0;
                        for (int i = 0; i < 64; i++)
                        {
                            if (macroList[i])
                            {
                                totalMacros++;
                            }
                        }

                        if (totalMacros == 0)
                        {
                            progress.Hide();
                            MessageBox.Show("There are no macros in the controller", "No macros found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            int macrosUploaded = 1;
                            theUploadedProgram.Clear();

                            for (int macronumber = 0; macronumber < 64; macronumber++)
                            {
                                if (macroList[macronumber])
                                {
                                    theUploadedProgram.Add("0 W 0x12C05 " + macronumber.ToString());

                                    progress.lblProgress.Text = "Reading macro " + (macronumber).ToString() + " (" + macrosUploaded.ToString() + " of " + totalMacros.ToString() + ")";
                                    progress.BringToFront();
                                    progress.Refresh();
                                    bool endDetected = false;
                                    port.WriteObject(activeNode, 0x12c05, macronumber); // Select macro number
                                    for (int i = 0; (i < 64) && !endDetected; i++)
                                    {
                                        long value = 0;
                                        port.WriteObject(activeNode, 0x22c05, i); // Select macro line
                                        port.GetObjectValue(activeNode, 0x32c05, ref value);
                                        if (((ulong)value == 0xFFFFFFF1FFFFFFFF) || (ulong)value == 0x7FFFFFFFFFFFFFFF)
                                        {
                                            endDetected = true;
                                        }
                                        else
                                        {
                                            theUploadedProgram.Add("0 W 0x22C05 " + i.ToString());
                                            theUploadedProgram.Add("0 W 0x32C05 0x" + value.ToString("X16"));
                                        }
                                    }
                                    macrosUploaded++;
                                }
                            }
                            progress.Hide();
                            BringToFront();
                            string chosenFile;
                            StreamWriter file = null;
                            SaveFileDialog browseFile = new SaveFileDialog();

                            browseFile.Filter = STR_DEFAULT_FILETYPE_MLM;
                            browseFile.DefaultExt = "mlm";
                            if (browseFile.ShowDialog() == DialogResult.OK)
                            {
                                chosenFile = browseFile.FileName;
                                if (chosenFile != "")
                                {
                                    file = new System.IO.StreamWriter(chosenFile);
                                    file.WriteLine("// File uploaded from the controller by " + strName + " " + strVersion);
                                    file.WriteLine("//");
                                    foreach (string s in theUploadedProgram)
                                    {
                                        file.WriteLine(s);
                                    }
                                    file.Close();

                                }
                            }

                        }
                        Enabled = true;
                        BringToFront();
                        bAsyncCommunicationEnabled = bSave;
                    }
                    progress.Hide();


                }
            }
        }

        private void cmbNode_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetCurrentNode(int.Parse(cmbNode.SelectedItem.ToString()));
        }


        private void chkSetProtectedAccess_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSetProtectedAccess.Checked == false)
            {
                //MessageBox.Show("Disabling of copy protection will only take effect if there are no macros in the controller\n\n"+
                //                "Use the Erase all button to erase all macros", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void btnChangeNodeId_Click(object sender, EventArgs e)
        {
            dlgNewNodeID dialog = new dlgNewNodeID();

            dialog.Text = "Enter new node ID for node " + activeNode.ToString();

            for (int i = 1; i <= 127; i++)  // This is the valid range for node ID's
            {
                bool nodeUsed = false;
                foreach (Node n in Network)
                {
                    if (n.ID == i)
                    {
                        nodeUsed = true;
                    }
                }
                if (!nodeUsed)
                {
                    dialog.cmbNode.Items.Add(i);
                }
            }

            dialog.cmbNode.SelectedIndex = 0;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int oldNode = activeNode;
                int newNode = (int)dialog.cmbNode.SelectedItem;
                bool bAsyncCommunicationEnabledSave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;

                Thread.Sleep(500); // Just let us take some rest

                port.WriteObject(oldNode, 0x12000, newNode);    // write new node number
                port.WriteObject(newNode, 0x11010, 1702257011); // And save to non volatile (address the new node)

                foreach (NodeSelector ns in nodeselectors)
                {
                    if (ns.GetSelectedNode() == oldNode)
                    {
                        ns.SetInitialselection(newNode);
                    }
                    else
                    {
                        ns.SetInitialselection(ns.GetSelectedNode());
                    }
                }



                activeNode = newNode;

                bAsyncCommunicationEnabled = bAsyncCommunicationEnabledSave;

                bReconnect = true;

                btnConnect_Click(null, null);       // Disconnect

                btnConnect_Click(null, null);       // And connect again

                bReconnect = false;



                //                currentNode.ID = newNode;   // Set the new node
            }


        }

        private void chkAutoBackup_CheckedChanged(object sender, EventArgs eventArgs)
        {
            if (chkAutoBackup.Checked)
            {
                //                MessageBox.Show(backupFolderName);
                //                MessageBox.Show(DateTime.Now.ToString()+".lcc");
                if (!Directory.Exists(backupFolderName))
                {
                    if (DialogResult.OK == MessageBox.Show("The auto backup function will save backups in the folder:" + Environment.NewLine + Environment.NewLine +
                        backupFolderName + Environment.NewLine + Environment.NewLine +
                        "This folder does not exist" + Environment.NewLine +
                        "Hit OK to create this folder", "Confirm backup folder creation", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                    {
                        try
                        {
                            Directory.CreateDirectory(backupFolderName);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Can not create folder " + backupFolderName + Environment.NewLine +
                                            "Reason: " + e.ToString());
                            chkAutoBackup.Checked = false;
                            return;
                        }
                    }
                    else
                    {
                        chkAutoBackup.Checked = false;
                        return;
                    }
                }
            }
        }

        private void btnAutoBackupInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "The auto backup function, when enabled, will save a copy of the program" +
                " every time that the program (or part of it) is saved in the controller." + Environment.NewLine + Environment.NewLine +
                "The folder for the backups is " + backupFolderName + Environment.NewLine + Environment.NewLine +
                "The file name is the date and time of creation of the file with a .lcc suffix" + Environment.NewLine +
                "Note that backup files are not cleared by the program.", "Auto backup information",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void cbUST_EncRes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.Encoder_res_asigned = false;

        }

        private void cbUST_Pos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.pos_unit_assigned = false;
        }

        private void tbUST_FC_Enter(object sender, EventArgs e)
        {
            Globals.force_constant_asssigned = false;
        }

        private void tbUST_FC_TabIndexChanged(object sender, EventArgs e)
        {
            tbUST_FC_Enter(null, null);
        }

        private void tbUST_FC_TabStopChanged(object sender, EventArgs e)
        {
            tbUST_FC_Enter(null, null);
        }


        private void tbUST_MRC_Enter(object sender, EventArgs e)
        {
            Globals.mrc_assigned = false;
        }


        private void tbUST_FC_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                //MessageBox.Show("Tab");

            }
            if (e.KeyData == (Keys.Tab | Keys.Shift))
            {
                //
            }
            if (e.KeyData == Keys.Enter)
            {
                //e.IsInputKey = true;
            }
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2C04, 1, 2);
        }

        /*private void txtTargetPosition_TextChanged(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x607A, 0, cfgTargetPosition.Value());
            
        }*/



        private void cmbProgramType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

         private void btnASMeasure_Click(object sender, EventArgs e)
         {

             string strError = "";

             if (!loggingLiveMeasure)
             {
                 chartMeasure.Series[0].Points.Clear();
                 chartMeasure.Series[1].Points.Clear();


                 // chartDemo.Series[3].Points.Clear();
                 tracesamplesMeasure = int.Parse(cmbSamples.SelectedItem.ToString());
                 traceintervalMeasure = int.Parse(cmbILMeasure.SelectedItem.ToString());

                 chartMeasure.ChartAreas[0].AxisX.Minimum = 0;
                 chartMeasure.ChartAreas[0].AxisX.Maximum = traceintervalMeasure * tracesamplesMeasure;

                 SetPrimaryRunAxis1(true);
                 SetSecondaryRunAxis1(true);

                 EnableChartAxesRun1();
                 // btnAutoscalePrimary1.PerformClick();
                 btnASP1.PerformClick();

                 //
                 // Check if there is at least 1 object selected
                 //
                 bool bObjectselectedMeasure = false;

                 foreach (logitem1 item in logitems1)
                 {

                     item.GetValueMeasure();    // This is to reset de valueValid flag
                     if (item.IsObjectSelectedMeasure())
                     {
                         bObjectselectedMeasure = true;
                     }
                 }

                 if (!bObjectselectedMeasure)
                 {
                     strError = "No trace objects selected";
                 }

               /*  if (strError == "")
                 {
                     foreach (logitem1 item1 in logitems1)
                     {
                         foreach (logitem1 item2 in logitems1)
                         {
                             if ((item1 != item2) && item1.IsObjectSelectedMeasure() && item2.IsObjectSelectedMeasure() && ((item1.GetCombinedIndexMeasure() == item2.GetCombinedIndexMeasure()) && (item1.getLastNodeMeasure() == item2.getLastNodeMeasure())))
                             {
                                 strError = "Log items must be different";
                             }

                         }
                     }
                 }*/

                 if (strError != "")
                 {
                     MessageBox.Show(strError, "Can not start Logging", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 }

                 if (strError == "")
                 {
                     Globals.loglive.Start(logfileName);
                     foreach (logitem1 item in logitems1)
                     {
                         if (item.IsObjectSelectedMeasure())
                         {
                             string name = item.NameMeasure();
                             if (Network.Count > 1)
                             {
                                 name = "Node " + item.getLastNodeMeasure().ToString() + ": " + name;
                             }
                             Globals.loglive.AddWithoutTimestamp("Time(msec);" + name + ";");
                             item.seriesMeasure.LegendText = name;
                         }
                     }
                     Globals.loglive.AddWithoutTimestamp("\n");

                     stopWatch.Restart();

                     //              MessageBox.Show("Tracing object 0X" + liveObject.ToString("X") + " and 0X" + traceobject2.ToString("X"));
                     // btnStartTraceLive.Text = STR_STOP;
                     tmrLogging.Interval = traceinterval;
                     tmrLogging.Enabled = true;

                     EnableLogItemsMeasure(false);
                     tmrLogging.Start();
                 }

             }

             else
             {
                 tmrLogging.Stop();
                 tmrLogging.Enabled = false;
                 Globals.loglive.Stop();
                 btnStartTraceLive.Text = STR_START;
                 EnableLogItemsMeasure(true);
             }
             loggingLiveMeasure = !loggingLiveMeasure;    // Toggle the mode\
            string y = txtPosCloseToPart.ToString();

            int x = (Int32)Globals.utils.ConvertStringToInt64(y);

            // MessageBox.Show(txtPosCloseToPart.ToString());
            double d = Double.Parse(txtPosCloseToPart.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPosCloseToPart = (int)d * 1000;

            double f = Double.Parse(txtMeasureSpeed.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numMeasureSpeed = (int)f * 1000;



            if (cmbMeasureType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
             {



                 port.WriteObject(0, 0x607A, 0, (numPosCloseToPart));
                 port.WriteObject(0, 0x6081, 0, (numMeasureSpeed));
                 port.WriteObject(0, 0x2c02, 4, numMeasureSLError.Value());
                 port.WriteObject(0, 0x6071, 0, numMeasureTF.Value());
                 port.WriteObject(0, 0x222c00, 0, numMeasureDT.Value()); 

                 port.WriteObject(0, 0x2C04, 1, 35);                
             }
         }


         private void btnTeachDatum_Click(object sender, EventArgs e)
         {
             if (cmbMeasureType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
             {

                 chartMeasure.Series[0].Points.Clear();
                 chartMeasure.Series[1].Points.Clear();

                string y = txtPosCloseToPart.ToString();

                int x = (Int32)Globals.utils.ConvertStringToInt64(y);

                // MessageBox.Show(txtPosCloseToPart.ToString());
                double d = Double.Parse(txtPosCloseToPart.ToString());

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numPosCloseToPart = (int)d * 1000;

                double f = Double.Parse(txtMeasureSpeed.Text.ToString(), CultureInfo.InvariantCulture);

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numMeasureSpeed = (int)f * 1000;


                port.WriteObject(0, 0x607A, 0, numPosCloseToPart );
                 port.WriteObject(0, 0x6081, 0, numMeasureSpeed);
                 port.WriteObject(0, 0x2c02, 4, numMeasureSLError.Value());
                 port.WriteObject(0, 0x6071, 0, numMeasureTF.Value());
                 port.WriteObject(0, 0x222c00, 0, numMeasureDT.Value());

                 port.WriteObject(0, 0x2C04, 1, 30);
             }
         }

        // Actuator stop button for measurement tab
        private void btnMeasureStop_Click(object sender, EventArgs e)
        {
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            tmrLogging.Stop();
            tmrLogging.Enabled = false;
            Globals.loglive.Stop();
            // btnStartTraceLive.Text = STR_START;
            EnableLogItemsMeasure(true);

            loggingLiveMeasure = !loggingLiveMeasure;

            port.WriteObject(0, 0x6040, 0, 7); // Control word 7

            bAsyncCommunicationEnabled = bSave;

        }


        private void btnStartDemo_Click(object sender, EventArgs e)
        {

            string strError = "";
            chartRun.Series[0].Points.Clear();
            chartRun.Series[1].Points.Clear();

            // chartDemo.Series[3].Points.Clear();
            tracesamples = int.Parse(cmbSamples.SelectedItem.ToString());
            traceinterval = int.Parse(cmbIntervalLive.SelectedItem.ToString());

            chartRun.ChartAreas[0].AxisX.Minimum = 0;
            chartRun.ChartAreas[0].AxisX.Maximum = traceinterval * tracesamples;

            SetPrimaryRunAxis(true);
            SetSecondaryRunAxis(true);

            EnableChartAxesRun();


            // btnAutoscalePrimary1.PerformClick();
            btnAutoscalePrimary1.PerformClick();

            //
            // Check if there is at least 1 object selected
            //
            bool bObjectselected = false;

            foreach (logitem item in logitems)
            {

                item.GetValue();    // This is to reset de valueValid flag
                if (item.IsObjectSelected())
                {
                    bObjectselected = true;
                }
            }

            if (!bObjectselected)
            {
                strError = "No trace objects selected";
            }

            if (strError == "")
            {
                foreach (logitem item1 in logitems)
                {
                    foreach (logitem item2 in logitems)
                    {
                        if ((item1 != item2) && item1.IsObjectSelected() && item2.IsObjectSelected() && ((item1.GetCombinedIndex() == item2.GetCombinedIndex()) && (item1.getLastNode() == item2.getLastNode())))
                        {
                            strError = "Log items must be different";
                        }

                    }
                }
            }

            if (strError != "")
            {
                MessageBox.Show(strError, "Can not start Logging", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (strError == "")
            {
                Globals.loglive.Start(logfileName);
                foreach (logitem item in logitems)
                {
                    if (item.IsObjectSelected())
                    {
                        string name = item.Name();
                        if (Network.Count > 1)
                        {
                            name = "Node " + item.getLastNode().ToString() + ": " + name;
                        }
                        Globals.loglive.AddWithoutTimestamp("Time(msec);" + name + ";");
                        item.series.LegendText = name;
                    }
                }
                Globals.loglive.AddWithoutTimestamp("\n");

                stopWatch.Restart();

                //              MessageBox.Show("Tracing object 0X" + liveObject.ToString("X") + " and 0X" + traceobject2.ToString("X"));
                // btnStartTraceLive.Text = STR_STOP;
                tmrLogging.Interval = traceinterval;
                tmrLogging.Enabled = true;

                EnableLogItems(false);

                tmrLogging.Start();

              //  btnStartDemo.Text = "Actuator Stop";

            }



            else
            {
                tmrLogging.Stop();
                tmrLogging.Enabled = false;
                Globals.loglive.Stop();
                btnStartDemo.Text = "Actuator Start";
                EnableLogItems(true);
            }

          loggingLive = !loggingLive;    // Toggle the mode
                // Toggle the mode

            // This is the original part of sequence where the program will be loaded depending on selecting operation

            if (cmbProgramTypeDemo.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
            {
                port.WriteObject(0, 0x607A, 0, numTarget1.Value());
                port.WriteObject(0, 0x6081, 0, numVelocity.Value());
                port.WriteObject(0, 0x2C04, 1, 40);
                
            }

          
            if (cmbProgramTypeDemo.SelectedIndex == 1)//  05/04::VIGNESH:: Position move and Softland
            {
                port.WriteObject(0, 0x607A, 0, numTarget1.Value());
                port.WriteObject(0, 0x6081, 0, numVelocity.Value());
                port.WriteObject(0, 0x52c00, 0, numSoftlandError.Value());
              //  instructions.Add(new WriteInstruction(0x2c02, 4, (int)trackingerror, "Softland not yet detected"));   // If ACCUM is lower trackingerror

                port.WriteObject(0, 0x2C04, 1, 41);
                
            }

            if (cmbProgramTypeDemo.SelectedIndex == 2)// 05/04 ::VIGNESH:: Position move, softland and force move
            {
                port.WriteObject(0, 0x607A, 0, numTarget1.Value());
                port.WriteObject(0, 0x6081, 0, numVelocity.Value());
                port.WriteObject(0, 0x2c02, 4, numSoftlandError.Value());
                port.WriteObject(0, 0x6071, 0, numTargetForce.Value());
                port.WriteObject(0, 0x6087, 0, numForceSlope.Value());
                port.WriteObject(0, 0x2C04, 1, 42);
                btnStartDemo.Text = "Actuator Start";
                               
            }

        }

  

        private void cmbProgramTypeDemo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProgramTypeDemo.SelectedIndex == 0)
            {
                txtTargetForceDemo.Visible = false;
                lblForceDemo.Visible = false; // Target force label
                lblForceSlopeDemo.Visible = false; // Force slope label
                lblSoftlandSensitivityDemo.Visible = false; // Softland error label
                txtForceSlopeDemo.Visible = false;
                txtSoftlandErrorDemo.Visible = false;
                lbltargetPosition1.Visible = true;
                lblForce1.Visible = false;
                lblFS1.Visible = false;
                lblSS1.Visible = false;
                lblSpeed1.Visible = true;

                txtPosition.Visible = true;
                txtSpeedRange.Visible = true;
                txtSoftlandRange.Visible = false;
                txtForceRange.Visible = false;
                txtForceSlopeRange.Visible = false;

                txtProfileVelocityDemo.Visible = true;
                txtTargetPositionDemo.Visible = true;

                txtTargetPositionDemo.Text = "8000";
                txtProfileVelocityDemo.Text = "40000";
                
            }

            if (cmbProgramTypeDemo.SelectedIndex == 1)
            {
                txtTargetForceDemo.Visible = false;
                txtProfileVelocityDemo.Visible = true;
                txtTargetPositionDemo.Visible = true;
                lblSoftlandSensitivityDemo.Visible = true; // Softland error label
                lblForceDemo.Visible = false; // Target force label
                lblForceSlopeDemo.Visible = false; // Force slope label
                txtSoftlandErrorDemo.Visible = true;
                txtForceSlopeDemo.Visible = false;

                lbltargetPosition1.Visible = true;
                lblForce1.Visible = false;
                lblFS1.Visible = false;
                lblSS1.Visible = true;
                lblSpeed1.Visible = true;

                txtPosition.Visible = true;
                txtSpeedRange.Visible = true;
                txtSoftlandRange.Visible = true;
                txtForceRange.Visible = false;
                txtForceSlopeRange.Visible = false;

                txtTargetPositionDemo.Text = "8000";
                txtProfileVelocityDemo.Text = "40000";
                txtSoftlandErrorDemo.Text = "200";
            }

            if (cmbProgramTypeDemo.SelectedIndex == 2)
            {

                txtTargetForceDemo.Visible = true;
                txtProfileVelocityDemo.Visible = true;
                txtTargetPositionDemo.Visible = true;
                lblSoftlandSensitivityDemo.Visible = true; // Softland error label
                lblForceDemo.Visible = true; // Target force label
                lblForceSlopeDemo.Visible = true; // Force slope label
                txtSoftlandErrorDemo.Visible = true;
                txtForceSlopeDemo.Visible = true;
           
                lbltargetPosition1.Visible = true;
                lblForce1.Visible = true;
                lblFS1.Visible = true;
                lblSS1.Visible = true;
                lblSpeed1.Visible = true;

                txtPosition.Visible = true;
                txtSpeedRange.Visible = true;
                txtSoftlandRange.Visible = true;
                txtForceRange.Visible = true;
                txtForceSlopeRange.Visible = true;

                txtTargetPositionDemo.Text = "8000";
                txtProfileVelocityDemo.Text = "40000";
                txtSoftlandErrorDemo.Text = "200";
                txtForceSlopeDemo.Text = "14000";
                txtTargetForceDemo.Text = "6000";

            }

        }

        private void MeasureResult()
        {
            Int64 GPR31 = 1; //GPR for picking up the highest value on block
            Int64 GPR30 = 1; // GPR for picking up force value on switch
            Int64 x = 0;
            Int64 GPR32 = 1;
            Int64 GPR33 = 1;


            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;


            port.GetObjectValue(activeNode, 0x1E2C00, ref GPR30);
            port.GetObjectValue(activeNode, 0x1F2C00, ref GPR31);
            port.GetObjectValue(activeNode, 0x202C00, ref GPR32);

            port.GetObjectValue(activeNode, 0x212C00, ref GPR33);

            int value = 0;

            value = Convert.ToInt32(GPR32 / 1000); //Cnnverting to millimeters

            txtMeasureResult.Text = Convert.ToString(value);

            bAsyncCommunicationEnabled = bsave;

        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2c04, 1, 0);

            // For clearing the chart data
           
            tmrLogging.Stop();
            tmrLogging.Enabled = false;
            Globals.loglive.Stop();

            chartRun.Series[0].Points.Clear();
            chartRun.Series[1].Points.Clear();


        }

        //Home button for measurement tab
        private void btnMeasureHome_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2c04, 1, 0);
            chartMeasure.Series[0].Points.Clear();
            chartMeasure.Series[1].Points.Clear();
        }

        private void cmbNode1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void SetRange(object sender, EventArgs e)
        {

            txtPosition.Text = "2"; 
                            
        }

        


        private void btnASP1_Click(object sender, EventArgs e)
        {
            AutoScaleAxis1(chartMeasure, chartMeasure.ChartAreas[0].AxisY, AxisType.Primary, txtMinPrimaryRun1, txtMaxPrimaryRun1);
        }

        private void btnMeasureHome_Click_1(object sender, EventArgs e)
        {
            txtMeasureResult.Text = "";
            port.WriteObject(0, 0x2c04, 1, 0);
            chartMeasure.Series[0].Points.Clear();
            chartMeasure.Series[1].Points.Clear();
        }

        private void btnTeachDatum_Click_1(object sender, EventArgs e)
        {
            txtMeasureResult.Text = "";
            if (cmbMeasureType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
            {

                chartMeasure.Series[0].Points.Clear();
                chartMeasure.Series[1].Points.Clear();

                

                // MessageBox.Show(txtPosCloseToPart.ToString());
                double d = Double.Parse(txtPosCloseToPart.Text.ToString(), CultureInfo.InvariantCulture) * 1000;

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numPosCloseToPart = (int)d;

                double f = Double.Parse(txtMeasureSpeed.Text.ToString(), CultureInfo.InvariantCulture);

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numMeasureSpeed = (int)f * 1000;




                port.WriteObject(0, 0x607A, 0,  numPosCloseToPart);
                port.WriteObject(0, 0x6081, 0, numMeasureSpeed);
                port.WriteObject(0, 0x2c02, 4, numMeasureSLError.Value());
                port.WriteObject(0, 0x6071, 0, numMeasureTF.Value());
                port.WriteObject(0, 0x222c00, 0, numMeasureDT.Value());

                port.WriteObject(0, 0x2C04, 1, 30);
            }

        }

        private void btnASMeasure_Click_1(object sender, EventArgs e)
        {
            txtMeasureResult.Text = "";

            string strError = "";

            if (!loggingLiveMeasure)
            {
                chartMeasure.Series[0].Points.Clear();
                chartMeasure.Series[1].Points.Clear();


                // chartDemo.Series[3].Points.Clear();
                tracesamplesMeasure = int.Parse(cmbSamples.SelectedItem.ToString());
                traceintervalMeasure = int.Parse(cmbILMeasure.SelectedItem.ToString());

                chartMeasure.ChartAreas[0].AxisX.Minimum = 0;
                chartMeasure.ChartAreas[0].AxisX.Maximum = traceintervalMeasure * tracesamplesMeasure;

                SetPrimaryRunAxis1(true);
                SetSecondaryRunAxis1(true);

                EnableChartAxesRun1();
                // btnAutoscalePrimary1.PerformClick();
                btnASP1.PerformClick();

                //
                // Check if there is at least 1 object selected
                //
                bool bObjectselectedMeasure = false;

                foreach (logitem1 item in logitems1)
                {

                    item.GetValueMeasure();    // This is to reset de valueValid flag
                    if (item.IsObjectSelectedMeasure())
                    {
                        bObjectselectedMeasure = true;
                    }
                }

                if (!bObjectselectedMeasure)
                {
                    strError = "No trace objects selected";
                }

                /*  if (strError == "")
                  {
                      foreach (logitem1 item1 in logitems1)
                      {
                          foreach (logitem1 item2 in logitems1)
                          {
                              if ((item1 != item2) && item1.IsObjectSelectedMeasure() && item2.IsObjectSelectedMeasure() && ((item1.GetCombinedIndexMeasure() == item2.GetCombinedIndexMeasure()) && (item1.getLastNodeMeasure() == item2.getLastNodeMeasure())))
                              {
                                  strError = "Log items must be different";
                              }

                          }
                      }
                  }*/

                if (strError != "")
                {
                    MessageBox.Show(strError, "Can not start Logging", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (strError == "")
                {
                    Globals.loglive.Start(logfileName);
                    foreach (logitem1 item in logitems1)
                    {
                        if (item.IsObjectSelectedMeasure())
                        {
                            string name = item.NameMeasure();
                            if (Network.Count > 1)
                            {
                                name = "Node " + item.getLastNodeMeasure().ToString() + ": " + name;
                            }
                            Globals.loglive.AddWithoutTimestamp("Time(msec);" + name + ";");
                            item.seriesMeasure.LegendText = name;
                        }
                    }
                    Globals.loglive.AddWithoutTimestamp("\n");

                    stopWatch.Restart();

                    //              MessageBox.Show("Tracing object 0X" + liveObject.ToString("X") + " and 0X" + traceobject2.ToString("X"));
                    // btnStartTraceLive.Text = STR_STOP;
                    tmrLogging.Interval = traceintervalMeasure;
                    tmrLogging.Enabled = true;

                    EnableLogItemsMeasure(false);
                    tmrLogging.Start();
                }

            }

            else
            {
                tmrLogging.Stop();
                tmrLogging.Enabled = false;
                Globals.loglive.Stop();
                btnStartTraceLive.Text = STR_START;
                EnableLogItemsMeasure(true);
            }
            loggingLiveMeasure = !loggingLiveMeasure;    // Toggle the mode

           

            if (cmbMeasureType.SelectedIndex == 0)// 05/04::VIGNESH:: Position move 
            {
                double d = Double.Parse(txtPosCloseToPart.Text.ToString(), CultureInfo.InvariantCulture) * 1000;

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numPosCloseToPart = (int)d;

                double f = Double.Parse(txtMeasureSpeed.Text.ToString(), CultureInfo.InvariantCulture);

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numMeasureSpeed = (int)f * 1000;




                port.WriteObject(0, 0x607A, 0, numPosCloseToPart);
                port.WriteObject(0, 0x6081, 0, numMeasureSpeed);
                port.WriteObject(0, 0x2c02, 4, numMeasureSLError.Value());
                port.WriteObject(0, 0x6071, 0, numMeasureTF.Value());
                port.WriteObject(0, 0x222c00, 0, numMeasureDT.Value());
                port.WriteObject(0, 0x2C04, 1, 35);

            }

            Int64 GPR31 = 1; //GPR for picking up the highest value on block
            Int64 GPR30 = 1; // GPR for picking up force value on switch
            Int64 x = 0;
            Int64 GPR32 = 1;
            Int64 GPR33 = 1;

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(200);
            var sw = new Stopwatch();
            sw.Start();
            port.GetObjectValue(activeNode, 0x212C00, ref GPR33);

            while ((GPR33) != 45)
            {
                port.GetObjectValue(activeNode, 0x212C00, ref GPR33);
                Thread.Sleep(50);
                if (sw.ElapsedMilliseconds > 10000)
                {
                    // error message here
                    return;
                }
               
            }

            port.GetObjectValue(activeNode, 0x202C00, ref GPR32);

            double value = 0;

            value = Convert.ToDouble(GPR32)/1000; //Converting to millimeters

            txtMeasureResult.Text = Convert.ToString(value);



            port.GetObjectValue(activeNode, 0x1E2C00, ref GPR30);
            port.GetObjectValue(activeNode, 0x1F2C00, ref GPR31);
            //port.GetObjectValue(activeNode, 0x202C00, ref GPR32);

            port.GetObjectValue(activeNode, 0x212C00, ref GPR33);

            
            bAsyncCommunicationEnabled = bsave;


           
            //MeasureResult();
            
        }

        public void Measure_Customize(object sender, EventArgs e)
        {
            

        }

        //Switch testing Home Routine

        private void btnHomeSwitch_Click(object sender, EventArgs e)
        {
            txtForceResult.Text = "";
            txtLastPosResult.Text = "";
            txtIniPosResult.Text = "";

            port.WriteObject(0, 0x2c04, 1, 0);
        }

        //Swtitch Testing::Teach routine
        private void btnSwitchTeach_Click(object sender, EventArgs e)
        {

          


        }

        private void calculation()
        {
            Int64 GPR43 = 1; //GPR for picking up first landed value on switch
            Int64 GPR53 = 1; // GPR for picking up force value on switch
            Int64 GPR47 = 1; // GPR for picking up last landed value on switch
            Int64 GPR51 = 1; // GPR for picking up last landed value on switch

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(50);
            var sw = new Stopwatch();
            sw.Start();
            port.GetObjectValue(activeNode, 0x332C00, ref GPR51);
                 
            while ((GPR51) != 45)
            {
                Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                port.GetObjectValue(activeNode, 0x332C00, ref GPR51);
                Thread.Sleep(50);
                if (sw.ElapsedMilliseconds > 10000)
                {
                    // error message here
                    return;
                }

            }

            /*new Thread(() =>
            
            {
                Thread.CurrentThread.IsBackground = true;
                while ((GPR51) != 1)
                {
                    port.GetObjectValue(activeNode, 0x332C00, ref GPR51);
                    Thread.Sleep(50);
                    if (sw.ElapsedMilliseconds > 10000)
                    {
                        // error message here
                        return;
                    }

                }
            }).Start();*/

            port.GetObjectValue(activeNode, 0x2B2C00, ref GPR43);
            port.GetObjectValue(activeNode, 0x352C00, ref GPR53);
            port.GetObjectValue(activeNode, 0x2F2C00, ref GPR47);

            double value5 = 0;
            double value6 = 0;
            double value7 = 0;

            value5 = Convert.ToDouble(GPR47) / 1000; //Converting to millimeters
            value6 = Convert.ToDouble(GPR43) / 1000; //Converting to millimeters
            value7 = Convert.ToDouble(GPR53) * (0.0015); //Converting to Newtons user understandable value

            txtIniPosResult.Text = Convert.ToString(value5);
            txtForceResult.Text = Convert.ToString(value7);
            txtLastPosResult.Text = Convert.ToString(value6);

            port.GetObjectValue(activeNode, 0x332C00, ref GPR51);
            bAsyncCommunicationEnabled = bsave;

        }
        // Start Button:: For switch testing
        private void btnStartSwitch_Click(object sender, EventArgs e)
        {

            /* Thread.CurrentThread.Priority = ThreadPriority.Highest;
            txtForceResult.Text = txtLastPosResult.Text = txtIniPosResult.Text = "";
            txtForceResult.Clear();
            txtLastPosResult.Clear();
            txtIniPosResult.Clear();
            
            btnClearSwitch_Click(sender, e);

            
            if(txtForceResult.Text == String.Empty)
            {
                port.WriteObject(0, 0x607A, 0, numPositionSwitch.Value());
                port.WriteObject(0, 0x322c00, 0, numSwitchSpeed.Value());
                port.WriteObject(0, 0x2c02, 4, numSwitchSensitivity.Value());
                port.WriteObject(0, 0x312c00, 0, numSwitchSoftlandSpeed.Value());

                port.WriteObject(0, 0x2C04, 1, 20); //Macro 20  
                // error message here
                calculation();

            }*/

            double d = Double.Parse(txtPosCloseSwitch.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPositionSwitch = (int)d * 1000;


            double f = Double.Parse(txtSwitchSpeed.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numSwitchSpeed = (int)f * 1000;

            double g = Double.Parse(txtSoftlandSpeedSwitch.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numSwitchSoftlandSpeed = (int)g * 1000;


            if (cmbSwitchType.SelectedIndex == 0)
            {
                port.WriteObject(0, 0x607A, 0, numPositionSwitch);
                port.WriteObject(0, 0x322c00, 0, numSwitchSpeed);
                port.WriteObject(0, 0x2c02, 4, numSwitchSensitivity.Value());
                port.WriteObject(0, 0x312c00, 0, numSwitchSoftlandSpeed);
               

                port.WriteObject(0, 0x2C04, 1, 20); //Macro 20  
                                                    // error message here
                calculation();

            }
            



        }

        
           

        

        // Stop button for :: Switch testing
        private void btnStopSwitch_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x6040, 0, 7); // Control word 7
        }

        private void btnSequence_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2C04, 1, 15);
        }

        private void btnHomeLeak_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2c04, 1, 0);
        }
        private void Leakcalculation()
        {
            Int64 GPR70 = 1; //GPR for picking up first landed value on switch
            Int64 GPR71 = 1; // GPR for picking up force value on switch
            Int64 GPR72 = 1; // GPR for picking up last landed value on switch
            Int64 GPR73 = 1; // GPR for picking up last landed value on switch
            

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(50);
            var sw = new Stopwatch();
            sw.Start();
            port.GetObjectValue(activeNode, 0x492C00, ref GPR73);

            while ((GPR73) != 45)
            {
                Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                port.GetObjectValue(activeNode, 0x492C00, ref GPR73);
                Thread.Sleep(50);
                if (sw.ElapsedMilliseconds > 10000)
                {
                    // error message here
                    return;
                }

            }

            
            port.GetObjectValue(activeNode, 0x462C00, ref GPR70);
            port.GetObjectValue(activeNode, 0x472C00, ref GPR71);
            port.GetObjectValue(activeNode, 0x482C00, ref GPR72);

            double value5 = 0;
            double value6 = 0;
            double value7 = 0;

            value5 = Convert.ToDouble(GPR70) / 1000; //Converting to millimeters
            value6 = Convert.ToDouble(GPR71) / 1000; //Converting to millimeters
            value7 = Convert.ToDouble(GPR72) / 1000; //Converting to millimeters


            txtFinalLeak.Text = Convert.ToString(value6);
            txtSoftlandLeak.Text = Convert.ToString(value5);
            txtLeakResult.Text = Convert.ToString(value7);

            port.GetObjectValue(activeNode, 0x492C00, ref GPR73);
            bAsyncCommunicationEnabled = bsave;

        }

        private void btnActuatorStartLeak_Click(object sender, EventArgs e)
        {
                        
            if (cmbLeakTestType.SelectedIndex == 0)
            {

                double f = Double.Parse(txtSpeedLeak.Text.ToString(), CultureInfo.InvariantCulture);

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numLeakSpeed = (int)f * 1000;

                double g = Double.Parse(txtPositionLeak.Text.ToString(), CultureInfo.InvariantCulture);

                // Double.TryParse(txtPosCloseToPart.ToString(), out d);

                numPositionLeak = (int)g * 1000;


                port.WriteObject(0, 0x607A, 0, numPositionLeak);
                port.WriteObject(0, 0x6081, 0, numLeakSpeed);
                port.WriteObject(0, 0x2c02, 4, numLeakSensitivity.Value());
                port.WriteObject(0, 0x6071, 0, numLeakForce.Value());
                port.WriteObject(0, 0x4A2C00, 0, numLeakDwellTime.Value());
                

                port.WriteObject(0, 0x2C04, 1, 45); // Macro 24 
                // error message here
                Leakcalculation();
            }

        }

        private void btnActuatorStopLeak_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x6040, 0, 7); // Control word 7
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }



        public void produser()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            int id = Thread.CurrentThread.ManagedThreadId;
            //Note that, I have collected threadId just before calling 
            //*this.Invoke* 
            //method else it would be same as of UI thread inside the below code 
            //block 
            this.Invoke((MethodInvoker)delegate ()
            {

                Int64 GPR63 = 1;
                Int64 GPR64 = 1;
                Int64 GPR67 = 1;

                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(50);
                var sw = new Stopwatch();
                sw.Start();
                port.GetObjectValue(activeNode, 0x402C00, ref GPR64);

               

                while ((GPR64) != 45)
                {
                    Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                    port.GetObjectValue(activeNode, 0x402C00, ref GPR64);
                    Thread.Sleep(50);
                    if (sw.ElapsedMilliseconds > 10000)
                    {
                            // error message here
                            return;
                    }
                }

                port.GetObjectValue(activeNode, 0x3F2C00, ref GPR63);
                port.GetObjectValue(activeNode, 0x432C00, ref GPR67);
                txtHighSpeedCycleTime.Text = Convert.ToString(GPR63);
                txtHighSpeedCycleFreq.Text = Convert.ToString(100 / GPR67);
                bAsyncCommunicationEnabled = bsave;
            });
        }

        public void produser2()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            int id = Thread.CurrentThread.ManagedThreadId;
            //Note that, I have collected threadId just before calling 
            //*this.Invoke* 
            //method else it would be same as of UI thread inside the below code 
            //block 
            this.Invoke((MethodInvoker)delegate ()
            {

                Int64 GPR95 = 1;
                Int64 GPR96 = 1;
               

                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(50);
                var sw = new Stopwatch();
                sw.Start();
                port.GetObjectValue(activeNode, 0x602C00, ref GPR96);

                

                while ((GPR96) != 45)
                {
                    Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                    port.GetObjectValue(activeNode, 0x602C00, ref GPR96);
                    Thread.Sleep(50);
                    if (sw.ElapsedMilliseconds > 10000)
                    {
                        // error message here
                        return;
                    }
                }
                
                
                port.GetObjectValue(activeNode, 0x5F2C00, ref GPR95);
                port.GetObjectValue(activeNode, 0x602C00, ref GPR96);
                txtPreCycleRes.Text = Convert.ToString(GPR95);
                
                bAsyncCommunicationEnabled = bsave;
            });
        }

        private void btnHighSpeedStart_Click(object sender, EventArgs e)
        {
            double d = Double.Parse(txtHighSpeedTarget1.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPositionA = (int)d * 1000;


            double f = Double.Parse(txtHighSpeedTarget2.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPositionB = (int)f * 1000;

            double g = Double.Parse(txtHighSpeedVelocity.Text.ToString(), CultureInfo.InvariantCulture);

            numVelocityHighSpeed = (int)g * 1000;

            port.WriteObject(0, 0x3C2C00, 0, numPositionA);
            port.WriteObject(0, 0x3E2C00, 0, numPositionB);
            port.WriteObject(0, 0x3D2C00, 0, numVelocityHighSpeed);
            port.WriteObject(0, 0x2C04, 1, 36); // Macro 36

            UpdateHighSpeed();
        }

        private void btnHighSpeedStop_Click(object sender, EventArgs e)
        {
            


            btnHighSpeedStopWasClicked = true;
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            port.WriteObject(0, 0x2c04, 1, 39); 
            bAsyncCommunicationEnabled = bSave;

        }


        private void btnHighSpeedHome_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2c04, 1, 0);
        }

        private void btnClearSwitch_Click(object sender, EventArgs e)
        {
            txtForceResult.Clear();          
            txtIniPosResult.Clear();           
            txtLastPosResult.Clear();           
        }

        private void cmbHighSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtHighSpeedTarget1.Text = "5";
            txtHighSpeedTarget2.Text = "15";
            txtHighSpeedVelocity.Text = "400";
        }

        private void panel21_Paint(object sender, PaintEventArgs e)
        {

        }


        private void panel29_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DisplayStepForceResult()
        {

            Int64 GPR80 = 1; 
            Int64 GPR81 = 1; 
            Int64 GPR82 = 1; 
            Int64 GPR83 = 1; 
            Int64 GPR84 = 1; 


            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(50);
            var sw = new Stopwatch();
            sw.Start();
            port.GetObjectValue(activeNode, 0x542C00, ref GPR84);

            while ((GPR84) != 45)
            {
                Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                port.GetObjectValue(activeNode, 0x542C00, ref GPR84);
                Thread.Sleep(50);
                if (sw.ElapsedMilliseconds > 10000)
                {
                    // error message here
                    return;
                }

            }


            port.GetObjectValue(activeNode, 0x502C00, ref GPR80);
            port.GetObjectValue(activeNode, 0x512C00, ref GPR81);
            port.GetObjectValue(activeNode, 0x522C00, ref GPR82);
            port.GetObjectValue(activeNode, 0x532C00, ref GPR83);

            double value8 = 0;
            double value9 = 0;
            double value10 = 0;
            double value11 = 0;

            value8 = Convert.ToDouble(GPR80) / 1000; //Converting to millimeters
            value9 = Convert.ToDouble(GPR81) / 1000; //Converting to millimeters
            value10 = Convert.ToDouble(GPR82) / 1000; //Converting to millimeters
            value11 = Convert.ToDouble(GPR83) / 1000; //Converting to millimeters

            txtSoftlandStepForce.Text = Convert.ToString(value8);
            txtPosition1StepForce.Text = Convert.ToString(value9);
            txtPosition2StepForce.Text = Convert.ToString(value10);
            txtPosition3StepForce.Text = Convert.ToString(value11);

            port.GetObjectValue(activeNode, 0x542C00, ref GPR84);
            bAsyncCommunicationEnabled = bsave;
        }

        private void btnStartStepForce_Click(object sender, EventArgs e)
        {      
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(20);
            port.WriteObject(0, 0x2C04, 1, 10); // Macr 10 for step force move

            bAsyncCommunicationEnabled = bSave;

            DisplayStepForceResult();

        }

        private void btnStopStepForce_Click(object sender, EventArgs e)
        {
          
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            port.WriteObject(0, 0x2c04, 1, 39);
            bAsyncCommunicationEnabled = bSave;

        }

        private void btnHomeStepForce_Click(object sender, EventArgs e)
        {
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);

            port.WriteObject(0, 0x2c04, 1, 0);

            bAsyncCommunicationEnabled = bSave;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtSoftlandStepForce.Clear();
            txtPosition1StepForce.Clear();
            txtPosition2StepForce.Clear();
            txtPosition3StepForce.Clear();
        }

        private void btnHighSpeedClear_Click(object sender, EventArgs e)
        {
            /*
            var sw = new Stopwatch();
            sw.Start();

            if (sw.ElapsedMilliseconds > 1000)
            {
                // error message here
                sw.Reset();
            }
            */          
            txtHighSpeedCycleTime.Clear();
            txtHighSpeedCycleFreq.Clear();

            UpdateHighSpeed();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtFinalLeak.Clear();
            txtSoftlandLeak.Clear();
            txtLeakResult.Clear();
        }

        private void label91_Click(object sender, EventArgs e)
        {

        }

        private void label92_Click(object sender, EventArgs e)
        {

        }


        private void txtHighSpeedCycleTime_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtHighSpeedCycleFreq_TextChanged(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
                Int64 GPR63 = 1; //handshaking Register for Position move
                Int64 GPR64 = 1;

                bool bsave = bAsyncCommunicationEnabled;
                bAsyncCommunicationEnabled = false;
                Thread.Sleep(20);
                port.GetObjectValue(activeNode, 0x402C00, ref GPR64);
            
                // btnAutoscalePrimary1.PerformClick();
                

                if(GPR64 ==45)
                {
                    port.GetObjectValue(activeNode, 0x3F2C00, ref GPR63);
                    txtHighSpeedCycleTime.Text = Convert.ToString(GPR63);
                    txtHighSpeedCycleFreq.Text = Convert.ToString(GPR63);
                    
                }
                             
            bAsyncCommunicationEnabled = bsave;
            // Globals.mainform.port.GetObjectValue(activeNode, 0xB2C00, ref GPR11);
            //Globals.mainform.port.GetObjectValue(activeNode, 0xC2C00, ref GPR12);       
            
        }

        private void btnPreStart_Click(object sender, EventArgs e)
        {
            txtPreCycleRes.Text = "";
            txtPreStroke.Text = "";

            double d = Double.Parse(txtPrePositionA.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPrePositionA = (int)d * 1000;


            double f = Double.Parse(txtPrePositionB.Text.ToString(), CultureInfo.InvariantCulture);

            // Double.TryParse(txtPosCloseToPart.ToString(), out d);

            numPrePositionB = (int)f * 1000;

            double g = Double.Parse(txtPreSpeed.Text.ToString(), CultureInfo.InvariantCulture);

            numPreVelocity = (int)g * 1000;


            port.WriteObject(0, 0x5A2C00, 0, numPrePositionA);
            port.WriteObject(0, 0x5E2C00, 0, numPrePositionB);
            port.WriteObject(0, 0x5B2C00, 0, numPreVelocity);
            port.WriteObject(0, 0x5C2C00, 0, numPreAcceleration.Value());
            port.WriteObject(0, 0x5D2C00, 0, numPreForce.Value());
            port.WriteObject(0, 0x622C00, 0, numPreDecel.Value());
            port.WriteObject(0, 0x632C00, 0, numPreSettlingtime.Value());
            port.WriteObject(0, 0x2C04, 1, 13); // Macro 36

   

            Int64 GPR95 = 1;
            Int64 GPR96 = 1;
            Int64 GPR97 = 1;
            Int64 GPR98 = 1;
            Int64 GPR99 = 1;

            bool bsave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(50);
            var sw = new Stopwatch();
            sw.Start();
            port.GetObjectValue(activeNode, 0x602C00, ref GPR96);
            
            while ((GPR96) != 45)
            {
                Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                port.GetObjectValue(activeNode, 0x602C00, ref GPR96);
                Thread.Sleep(50);
                if (sw.ElapsedMilliseconds > 10000)
                {
                    // error message here
                    return;
                }
            }


            port.GetObjectValue(activeNode, 0x5F2C00, ref GPR95);
            port.GetObjectValue(activeNode, 0x602C00, ref GPR96);
            port.GetObjectValue(activeNode, 0x612C00, ref GPR97);
            txtPreCycleRes.Text = Convert.ToString(GPR95);
            double x = Convert.ToDouble((numPrePositionB - numPrePositionA)/1000);
            txtPreStroke.Text = Convert.ToString(x);

            bAsyncCommunicationEnabled = bsave;

        }

        private void btnPreStop_Click(object sender, EventArgs e)
        {

            btnHighSpeedStopWasClicked = true;
            bool bSave = bAsyncCommunicationEnabled;
            bAsyncCommunicationEnabled = false;
            Thread.Sleep(100);
            port.WriteObject(0, 0x2c04, 1, 39);
            bAsyncCommunicationEnabled = bSave;
        }

        private void btnPreHome_Click(object sender, EventArgs e)
        {
            port.WriteObject(0, 0x2c04, 1, 0);
        }

        private void btnPreUpdateRes_Click(object sender, EventArgs e)
        {
           
            txtPreStroke.Clear();
            txtPreCycleRes.Clear();

            
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox8.SelectedIndex == 0 )
            { 
            txtPrePositionA.Text = "3";
            txtPrePositionB.Text = "18";
            txtPreSpeed.Text = "4000";
            txtPreDecel.Text = txtPreAcceleration.Text = "40000000";
            txtPreForce.Text = "6000";
                
            }
        }

        private void panel33_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
