﻿namespace Salesman_GUI
{
    partial class dlgOutputWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLogwindow = new System.Windows.Forms.TextBox();
            this.chkAddtimestamp = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkAddname = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtLogwindow
            // 
            this.txtLogwindow.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogwindow.Location = new System.Drawing.Point(12, 87);
            this.txtLogwindow.Multiline = true;
            this.txtLogwindow.Name = "txtLogwindow";
            this.txtLogwindow.ReadOnly = true;
            this.txtLogwindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLogwindow.Size = new System.Drawing.Size(787, 431);
            this.txtLogwindow.TabIndex = 0;
            // 
            // chkAddtimestamp
            // 
            this.chkAddtimestamp.AutoSize = true;
            this.chkAddtimestamp.Location = new System.Drawing.Point(12, 31);
            this.chkAddtimestamp.Name = "chkAddtimestamp";
            this.chkAddtimestamp.Size = new System.Drawing.Size(95, 17);
            this.chkAddtimestamp.TabIndex = 1;
            this.chkAddtimestamp.Text = "Add timestamp";
            this.chkAddtimestamp.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(131, 53);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 25);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(131, 23);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(79, 25);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkAddname
            // 
            this.chkAddname.AutoSize = true;
            this.chkAddname.Location = new System.Drawing.Point(12, 58);
            this.chkAddname.Name = "chkAddname";
            this.chkAddname.Size = new System.Drawing.Size(103, 17);
            this.chkAddname.TabIndex = 4;
            this.chkAddname.Text = "Add objectname";
            this.chkAddname.UseVisualStyleBackColor = true;
            // 
            // dlgOutputWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 518);
            this.Controls.Add(this.chkAddname);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkAddtimestamp);
            this.Controls.Add(this.txtLogwindow);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(815, 556);
            this.MinimumSize = new System.Drawing.Size(320, 556);
            this.Name = "dlgOutputWindow";
            this.Text = "Controller output";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.dlgOutputWindow_FormClosing);
            this.SizeChanged += new System.EventHandler(this.dlgOutputWindow_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLogwindow;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.CheckBox chkAddtimestamp;
        public System.Windows.Forms.CheckBox chkAddname;
    }
}