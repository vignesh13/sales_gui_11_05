﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LCC_Control_Center
{
    //
    // The Template macro uses the following controls from the mainform:
    //
    // Combobox1: 
    // Combobox2: 
    // txtParam1: 
    // txtParam2: 
    // txtParam3: 
    // txtParam4: 
    // txtParam5: 
    //
    public class TemplateSequence : Sequence
    {

        // private enums go here

        // constant strings that go into comboboxes go here


        //        const string STR_ENDSTOP =                 "End stop";
        //        const string STR_ENDSTOP_AND_INDEX_PULSE = "End stop and index pulse";
        //        const string STR_INDEX_PULSE =             "Index pulse";
        //        const string STR_USE_CURRENT_POSITION =    "Use current position";

        // constant strings that are parameter prefixes go here 

        //        const string STR_PREFIX_VINDEX =       "Vindex";
        //        const string STR_PREFIX_VENDSTOP =     "Vendstop";
        //        const string STR_PREFIX_METHOD =       "Method";
        //        const string STR_PREFIX_ACCELERATION = "Acc";
        //        const string STR_PREFIX_FORCE =        "Force";
        //        const string STR_PREFIX_TIMEOUT =      "Timeout";


        // Strings that are used as labels:

        const string STR_METHOD = "Method";
        const string STR_DIRECTION = "Direction";
        const string STR_ACCELERATION = "Acceleration";
        const string STR_SPEED_ENDSTOP = "Velocity for end stop search";
        const string STR_SPEED_INDEX = "Velocity for index search";
        const string STR_FORCE = "Force for end stop search";
        const string STR_TIMEOUT = "Timeout";
        //
        // private variables
        //

        private string parms;   // The parameter string

        //        private bool methodDefined = false;
        //        private HOMING_METHOD method;

        //        private bool velocity1Defined = false;
        //        private UInt32 velocity1;
        //        private bool velocity2Defined = false;
        //        private UInt32 velocity2;
        //        private bool accelerationDefined = false;
        //        private UInt32 acceleration;
        //        private bool forceDefined = false;
        //        private UInt32 force;

        //        private bool timeoutDefined = false;
        //        private UInt32 timeout;

        // Strings that go into the combo boxes

        //        public string[] homingMethods =     {STR_ENDSTOP,
        //                                             STR_ENDSTOP_AND_INDEX_PULSE,
        //                                             STR_INDEX_PULSE,
        //                                             STR_USE_CURRENT_POSITION};

        //        private string[] homingDirections = {STR_POSITIVE,
        //                                             STR_NEGATIVE};

        public TemplateSequence()    // The constructor
        {
            name = "Template for a sequence";
            mnemonic = "Template";
        }

        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;


            //            methodDefined = false;
            //            velocity1Defined = false;
            //            velocity2Defined = false;
            //            accelerationDefined = false;
            //            forceDefined = false;
            //            timeoutDefined = false;

            if (parms == "")       // Then use the default set of parameters
            {
                //                method = HOMING_METHOD.NEG_ENDSTOP_AND_INDEX;
                //                methodDefined = true;    
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        //                      case STR_PREFIX_VINDEX:      // This is an optional parameter, may be empty
                        //                          if (parameterValue != "")
                        //                          {
                        //                              velocity2 = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                        //                              velocity2Defined = true;
                        //                          }
                        //                          break;
                        //                      case STR_PREFIX_VENDSTOP:      // This is an optional parameter, may be empty
                        //                          if (parameterValue != "")
                        //                          {
                        //                              velocity1 = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                        //                              velocity1Defined = true;
                        //                          }
                        //                          break;
                        default:
                            // TODO: Error message undefined parameter here
                            break;
                    }
                }
            }
        }


        public override void ShowParametersInGui()
        {
            Globals.utils.HideAll();
            //
            // Set the layout of the labels and comboboxes, textboxes
            // and display the values
        }

        public override void UserInputHasChanged(Object sender)
        {
            //
            //            if (sender == Globals.mainform.ComboBox1 || sender == Globals.mainform.ComboBox2)
            //            {
            //				    }
            UpdateParametersFromGui();
            CreateCode();
        }


        public override void UpdateParametersFromGui()
        {
            //            if (method == HOMING_METHOD.CURRENT_POSITION)
            //            {
            //                accelerationDefined = false;
            //                forceDefined = false;
            //                velocity1Defined = false;
            //                velocity2Defined = false;
            //                timeoutDefined = false;
            //            }
            //            else
            //            {
            //                // Then the optional parameters acceleration and timeout may be filled in
            //                accelerationDefined = Globals.utils.ControlHasValue(Globals.mainform.txtParm1);
            //                if (accelerationDefined)
            //                {
            //                    acceleration = (UInt32)Globals.utils.GetControlValue(Globals.mainform.txtParm1, 1, UInt32.MaxValue, STR_ACCELERATION);
            //                }
            //              }
        }

        private void CreateCode()
        {
            //            instructions.Clear();
            //            instructions.Add(new WriteInstruction(0x6060, 0, 6, ShowLineInGui()));                // Modes of operation 6 (homing)
            //            instructions.Add(new WriteInstruction(0x6098, 0, (int) method, ""));     // Homing method
            //
            //
            //            if (velocity1Defined)
            //            {
            //                instructions.Add(new WriteInstruction(0x6099, 1, (int) velocity1, ""));    // Speed during serach for endstop
            //            }

            //            instructions.Add(new WriteInstruction(0x6040, 0, 6, ""));                        // Control word 6
            //            instructions.Add(new WriteInstruction(0x6040, 0, 7, ""));                        // Control word 7
            //            instructions.Add(new WriteInstruction(0x6040, 0, 15, ""));                       // Control word 15
            //            instructions.Add(new WriteInstruction(0x6040, 0, 31, ""));                       // Control word 31
            //            instructions.Add(new WriteInstruction(0x2C01, 23, 0x6041, ""));                  // Write status register to accumulator
            //            instructions.Add(new WriteInstruction(0x2C02, 11, 12, ""));                      // Execute if bit 12 of accumulator bit is
            //            instructions.Add(new WriteInstruction(0x2C04, 6, -2, ""));                       // Jump relative -2
            //            instructions.Add(new WriteInstruction(0x6040, 0, 15, ""));                       // Control word 15

            //            for (int i = 0; i < instructions.Count; i++)
            //            {
            //                MessageBox.Show(instructions[i].GetTextString());
            //            }
            //            MessageBox.Show("Macro heeft " + GetSize().ToString() + " instructies");
        }


        public override string ShowLineInGui()
        {
            string temp;
            temp = name;
            //            temp = temp + " " + STR_PREFIX_METHOD + "=" + ((int)method).ToString();
            //            temp = temp + "," + STR_PREFIX_ACCELERATION + "=";
            //            if (accelerationDefined)
            //            {
            //                temp = temp + acceleration.ToString();
            //            }
            //
            //            temp = temp + "," + STR_PREFIX_FORCE + "=";
            //            if (forceDefined)
            //            {
            //                temp = temp + force.ToString();
            //            }
            //
            //            temp = temp + "," + STR_PREFIX_VENDSTOP + "=";
            //            if (velocity1Defined)
            //            {
            //                temp = temp + velocity1.ToString();
            //            }
            //
            //            temp = temp + "," + STR_PREFIX_VINDEX + "=";
            //            if (velocity2Defined)
            //            {
            //                temp = temp + velocity2.ToString();
            //            }
            //
            //            temp = temp + "," + STR_PREFIX_TIMEOUT + "=";
            //            if (timeoutDefined)
            //            {
            //                temp = temp + timeout.ToString();
            //            }
            return temp;
            //            
            //        }
        }
    }
}
