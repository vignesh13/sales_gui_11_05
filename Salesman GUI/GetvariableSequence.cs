﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class GetvariableSequence : Sequence
    {

        private string parms;   // The parameter string

        private string variable = "";

        private const string STR_VARIABLE_TO_GET = "Variable to get";


        private const string STR_PREFIX_VAR      = "Var";


        private UserObjectselector UserObjectselectorVariable;


        public GetvariableSequence()    // The constructor
        {
            name = "Get variable";
            mnemonic = "GetVariable";
        }

        public override void Init()
        {
            UserObjectselectorVariable =  new UserObjectselector(Globals.mainform.lblName3, STR_VARIABLE_TO_GET, Globals.mainform.btnSelect1, Globals.mainform.lblUnits3, "", true, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserObjectselectorVariable = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;


            if (parms == "")       // Then use the default set of parameters
            {
                variable = "";
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case STR_PREFIX_VAR:

                            variable = parameterValue;
                            
                            break;
                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            UserObjectselectorVariable.SetSelection(variable);
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserObjectselectorVariable.Hide();

            //
            // Now switch the position and visibility of all selections dependant on  mode
            //
            UserObjectselectorVariable.Show(0); // And show the affected variable

            UserObjectselectorVariable.SetSelection(variable);
            Globals.mainform.txtComment.Text = strComment;

        }

        public override void ButtonClick(Object sender)
        {
            if (sender == Globals.mainform.btnSelect1)
            {
                UserObjectselectorVariable.SelectObject();
            }
        }



        public override void UserInputHasChanged(Object sender)
        {

            UpdateParametersFromGui();
            ShowParametersInGui();
        }


        public override void UpdateParametersFromGui()
        {

            variable = UserObjectselectorVariable.selectedIndexString;
            strComment = Globals.mainform.txtComment.Text;
            UserObjectselectorVariable.ShowMessageIfNotSelected(true);
            CreateCode();
        }





        public override string GetParameterString()
        {

            string temp = STR_PREFIX_VAR + "=" + variable;
            return temp;            
        }





        private void CreateCode()
        {

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);


            instructions.Clear();
            int targetVariable = UserObjectselectorVariable.GetIndex(variable);
            instructions.Add(new ReadInstruction((targetVariable & 0xFFFF), ((targetVariable >> 16) & 0xff), strtemp));

             
        }
    }
}
