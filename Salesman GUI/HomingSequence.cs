﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class HomingSequence : Sequence
    {

        const string STR_ENDSTOP =                 "Endstop";
        const string STR_ENDSTOP_AND_INDEX_PULSE = "Endstop and indexpulse";
        const string STR_INDEX_PULSE =             "Indexpulse";
        const string STR_USE_CURRENT_POSITION =    "Use current position";


        const string STR_PREFIX_VINDEX =       "Vindex";
        const string STR_PREFIX_VENDSTOP =     "Vendstop";
        const string STR_PREFIX_FORCE =        "Force";
        const string STR_PREFIX_ACCELERATION = "Acc";
        const string STR_PREFIX_TIMEOUT =      "Timeout";
        const string STR_PREFIX_OFFSET =       "Offset";



        const string STR_METHOD        = "Method";
        const string STR_ACCELERATION  = "Acceleration";
        const string STR_SPEED_ENDSTOP = "Velocity for end stop search";
        const string STR_FORCE_ENDSTOP = "Force for endstop detection";
        const string STR_SPEED_INDEX   = "Velocity for index search";
        const string STR_TIMEOUT       = "Timeout";
        const string STR_HOME_OFFSET   = "Home offset";



        private string parms;   // The parameter string

        private string method = "";
        private string direction = "";

        private bool velocity1Defined = false;
        private UInt32 velocity1;
        private bool forceDefinded = false;
        private UInt16 force;
        private bool velocity2Defined = false;
        private UInt32 velocity2;
        private bool accelerationDefined = false;
        private UInt32 acceleration;

        private bool timeoutDefined = false;
        private UInt32 timeout;

        private bool offsetDefined = false;
        private Int32 offset;


        private string[] homingDirections = {Globals.STR_POSITIVE,
                                             Globals.STR_NEGATIVE};


        public string[] homingMethods =     {STR_ENDSTOP,
                                             STR_ENDSTOP_AND_INDEX_PULSE,
                                             STR_INDEX_PULSE,
                                             STR_USE_CURRENT_POSITION};



        private UserCombobox UserComboboxMethod;
        private UserCombobox UserComboboxDirection;

        private UserTextbox UserTextboxVelocity1;
        private UserTextbox UserTextboxVelocity2;
        private UserTextbox UserTextboxAcceleration;
        private UserTextbox UserTextboxTimeout;
        private UserTextbox UserTextboxOffset;
        private UserTextbox UserTextboxForce;



        public HomingSequence()    // The constructor
        {
            name = "Homing";
            mnemonic = "Homing";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, homingMethods);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, homingDirections);

            UserComboboxMethod = new UserCombobox(Globals.mainform.lblName1, STR_METHOD, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxDirection = new UserCombobox(Globals.mainform.lblName2, Globals.STR_DIRECTION, Globals.mainform.ComboBox2, Globals.mainform.lblUnits2, "");
            UserTextboxVelocity1 = new UserTextbox(Globals.mainform.lblName3, STR_SPEED_ENDSTOP, Globals.mainform.txtParm1, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits3, Globals.STR_COUNTS_PER_SEC, false);
            UserTextboxVelocity2 = new UserTextbox(Globals.mainform.lblName4, STR_SPEED_INDEX, Globals.mainform.txtParm2, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits4, Globals.STR_COUNTS_PER_SEC, false);
            UserTextboxAcceleration = new UserTextbox(Globals.mainform.lblName5, STR_ACCELERATION, Globals.mainform.txtParm3, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits5, Globals.STR_COUNTS_PER_SEC2, false);
            UserTextboxTimeout = new UserTextbox(Globals.mainform.lblName6, STR_TIMEOUT, Globals.mainform.txtParm4, 0, 0, UInt16.MaxValue, Globals.mainform.lblUnits6, Globals.STR_MSEC, false);
            UserTextboxOffset = new UserTextbox(Globals.mainform.lblName7, STR_HOME_OFFSET, Globals.mainform.txtParm5, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits7, Globals.STR_COUNTS, false);
            UserTextboxForce = new UserTextbox(Globals.mainform.lblName8, STR_FORCE_ENDSTOP, Globals.mainform.txtParm6, 0, UInt16.MinValue, UInt16.MaxValue, Globals.mainform.lblUnits8, Globals.STR_PROMILLE_OF_RATED_FORCE, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxMethod = null;
            UserComboboxDirection = null;
            UserTextboxVelocity1 = null;
            UserTextboxVelocity2 = null;
            UserTextboxAcceleration = null;
            UserTextboxTimeout = null;
            UserTextboxOffset = null;
            UserTextboxForce = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            velocity1Defined = false;
            velocity2Defined = false;
            accelerationDefined = false;
            timeoutDefined = false;
            offsetDefined = false;
            forceDefinded = false;

            if (parms == "")       // Then use the default set of parameters
            {
                method = STR_ENDSTOP_AND_INDEX_PULSE;
                direction = Globals.STR_NEGATIVE;
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case STR_ENDSTOP:
                        case STR_INDEX_PULSE:
                        case STR_ENDSTOP_AND_INDEX_PULSE:
                        case STR_USE_CURRENT_POSITION:
                            method = parameterName;
                            break;

                        case Globals.STR_NEGATIVE:
                        case Globals.STR_POSITIVE:
                            direction = parameterName;
                            break;

                        case STR_PREFIX_VINDEX:      // This is an optional parameter, may be empty
                            if (parameterValue != "")
                            {
                                velocity2 = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                velocity2Defined = true;
                            }
                            break;

                        case STR_PREFIX_VENDSTOP:      // This is an optional parameter, may be empty
                            if (parameterValue != "")
                            {
                                velocity1 = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                velocity1Defined = true;
                            }
                            break;

                        case STR_PREFIX_ACCELERATION:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                acceleration = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                accelerationDefined = true;
                            }
                            break;

                        case STR_PREFIX_TIMEOUT:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                timeout = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                timeoutDefined = true;
                            }
                            break;

                        case STR_PREFIX_OFFSET:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                offset = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                                offsetDefined = true;
                            }
                            break;

                        case STR_PREFIX_FORCE:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                force = (UInt16)Globals.utils.ConvertStringToInt64(parameterValue);
                                forceDefinded = true;
                            }
                            break;
                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserComboboxDirection.Hide();
            UserTextboxAcceleration.Hide();
            UserTextboxVelocity1.Hide();
            UserTextboxForce.Hide();
            UserTextboxVelocity2.Hide();
            UserTextboxTimeout.Hide();
            UserTextboxOffset.Hide();

            Globals.mainform.ComboBox1.SelectedItem = method;
            if (direction != "")
            {
                Globals.mainform.ComboBox2.SelectedItem = direction;

            }


            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //


            switch (method)
            {
                case STR_USE_CURRENT_POSITION:
                    UserComboboxMethod.Show(0);
                    UserTextboxOffset.Show(1);
                    break;

                case STR_ENDSTOP:
                    UserComboboxMethod.Show(0);
                    UserComboboxDirection.Show(1);
                    UserTextboxAcceleration.Show(2);
                    UserTextboxVelocity1.Show(3);
                    UserTextboxForce.Show(4);
                    UserTextboxTimeout.Show(5);
                    UserTextboxOffset.Show(6);
                    break;

                case STR_ENDSTOP_AND_INDEX_PULSE:
                    UserComboboxMethod.Show(0);
                    UserComboboxDirection.Show(1);
                    UserTextboxAcceleration.Show(2);
                    UserTextboxVelocity1.Show(3);
                    UserTextboxForce.Show(4);
                    UserTextboxVelocity2.Show(5);
                    UserTextboxTimeout.Show(6);
                    UserTextboxOffset.Show(7);
                    break;

                case STR_INDEX_PULSE:
                    UserComboboxMethod.Show(0);
                    UserComboboxDirection.Show(1);
                    UserTextboxAcceleration.Show(2);
                    UserTextboxVelocity2.Show(3);
                    UserTextboxTimeout.Show(4);
                    UserTextboxOffset.Show(5);
                    break;

                default:
                    break;
            }

            UserTextboxAcceleration.SetValue(accelerationDefined ? acceleration.ToString() : "");
            UserTextboxVelocity1.SetValue(velocity1Defined ? velocity1.ToString() : "");
            UserTextboxForce.SetValue(forceDefinded ? force.ToString() : "");
            UserTextboxVelocity2.SetValue(velocity2Defined ? velocity2.ToString() : "");
            UserTextboxTimeout.SetValue(timeoutDefined ? timeout.ToString() : "");
            UserTextboxOffset.SetValue(offsetDefined ? offset.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;

        }


        public override void UserInputHasChanged(Object sender)
        {
            method = Globals.mainform.ComboBox1.Text;
            direction = Globals.mainform.ComboBox2.Text;
            UpdateParametersFromGui();
            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {
            forceDefinded = false;
            velocity1Defined = false;
            velocity2Defined = false;
            accelerationDefined = false;
            timeoutDefined = false;

            offsetDefined = UserTextboxOffset.HasValue();
            if (offsetDefined)
            {
                offset = (Int32)UserTextboxOffset.Value();
            }

            if (method == STR_USE_CURRENT_POSITION)
            {
                accelerationDefined = false;
                velocity1Defined = false;
                velocity2Defined = false;
                timeoutDefined = false;
            }
            else
            {
                // Then the optional parameters acceleration and timeout may be filled in
                accelerationDefined = UserTextboxAcceleration.HasValue();
                if (accelerationDefined)
                {
                    acceleration = (UInt32)UserTextboxAcceleration.Value();
                }

                timeoutDefined = UserTextboxTimeout.HasValue();
                if (timeoutDefined)
                {
                    timeout = (UInt32)UserTextboxTimeout.Value();
                }
            }
            if ((method == STR_ENDSTOP) || (method == STR_ENDSTOP_AND_INDEX_PULSE))                  //Check all modes that use the endstop 
            {

                velocity1Defined = UserTextboxVelocity1.HasValue();
                if (velocity1Defined)
                {
                    velocity1 = (UInt32)UserTextboxVelocity1.Value();
                }

                forceDefinded = UserTextboxForce.HasValue();
                if (forceDefinded)
                {
                    force = (UInt16)UserTextboxForce.Value();
                }

            }
            if ((method == STR_INDEX_PULSE) || (method == STR_ENDSTOP_AND_INDEX_PULSE))             // Check all modes that use the index pulse
            {
                velocity2Defined = UserTextboxVelocity2.HasValue();
                if (velocity2Defined)
                {
                    velocity2 = (UInt32)UserTextboxVelocity2.Value();
                }
            }
            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {
            string temp = method.Replace(' ', '_');

            if (method != STR_USE_CURRENT_POSITION)
            {
                temp = temp + Globals.STR_COMMA + direction.Replace(' ', '_');

                temp = temp + Globals.STR_COMMA + STR_PREFIX_ACCELERATION + "=";
                if (accelerationDefined)
                {
                    temp = temp + acceleration.ToString();
                }

                switch (method)
                {
                    case STR_ENDSTOP:                       // The methods that use the endstop
                    case STR_ENDSTOP_AND_INDEX_PULSE:


                        temp = temp + Globals.STR_COMMA + STR_PREFIX_VENDSTOP + "=";
                        if (velocity1Defined)
                        {
                            temp = temp + velocity1.ToString();
                        }

                        temp = temp + Globals.STR_COMMA + STR_PREFIX_FORCE + "=";
                        if (forceDefinded)
                        {
                            temp = temp + force.ToString();
                        }
                        break;
                }

                switch (method)
                {
                    case STR_INDEX_PULSE:               // The methods that use the index pulse
                    case STR_ENDSTOP_AND_INDEX_PULSE:

                        temp = temp + Globals.STR_COMMA + STR_PREFIX_VINDEX + "=";
                        if (velocity2Defined)
                        {
                            temp = temp + velocity2.ToString();
                        }
                        break;
                }

                temp = temp + Globals.STR_COMMA + STR_PREFIX_TIMEOUT + "=";
                if (timeoutDefined)
                {
                    temp = temp + timeout.ToString();
                }

            }

            temp = temp + Globals.STR_COMMA + STR_PREFIX_OFFSET + "=";
            if (offsetDefined)
            {
                temp = temp + offset.ToString();
            }

            return temp;            
        }

        private void CreateCode()
        {

            int methodnumber = 0;
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            //
            // First determine the number of this method:
            // See emcl manual for explanation
            //
            switch (method)
            {
                case STR_USE_CURRENT_POSITION:
                    methodnumber = 35;
                    break;

                case STR_INDEX_PULSE:
                    methodnumber = (direction == Globals.STR_POSITIVE) ? 34 : 33;
                    break;

                case STR_ENDSTOP:
                    methodnumber = (direction == Globals.STR_POSITIVE) ? -4 : -3;
                    break;

                case STR_ENDSTOP_AND_INDEX_PULSE:
                    methodnumber = (direction == Globals.STR_POSITIVE) ? -2 : -1;
                    break;
            }

            instructions.Clear();

            instructions.Add(new WriteInstruction(0x6098, 0, methodnumber, strtemp)) ;     // Homing method

            if (velocity1Defined)
            {
                instructions.Add(new WriteInstruction(0x6099, 1, (int)velocity1));    // Speed during serach for endstop
            }

            if (forceDefinded)
            {
                instructions.Add(new WriteInstruction(0x2102, 2, (int)force));    // Force to detect endstop
            }

            if (velocity2Defined)
            {
                instructions.Add(new WriteInstruction(0x6099, 2, (int) velocity2));    // Speed during search for index
            }

            if (accelerationDefined)
            {
                instructions.Add(new WriteInstruction(0x609A, 0, (int)acceleration));   // Homing acceleration
            }

            if (timeoutDefined)
            {
                instructions.Add(new WriteInstruction(0x2102, 1, (int)timeout));          // Homing timeout limit
            }

            if (offsetDefined)
            {
                instructions.Add(new WriteInstruction(0x607c, 0, (int)offset));          // Homing offset
            }

            if (method == STR_USE_CURRENT_POSITION)
            {
                CreateCodeForSystemMacro(Bios.callnumber.HOMING_ON_CURRENT_POSITION);
            }
            else
            {
                CreateCodeForSystemMacro(Bios.callnumber.MOTOR_ON);
                CreateCodeForSystemMacro(Bios.callnumber.HOMING);
            }
        }
    }
}
