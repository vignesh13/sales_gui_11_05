﻿using System.Windows.Forms;

namespace Salesman_GUI
{
    partial class ContactInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactInfo));
            this.linkMail = new System.Windows.Forms.LinkLabel();
            this.linkSmacInternational = new System.Windows.Forms.LinkLabel();
            this.linkSmacDutch = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // linkMail
            // 
            this.linkMail.AutoSize = true;
            this.linkMail.Location = new System.Drawing.Point(181, 97);
            this.linkMail.Name = "linkMail";
            this.linkMail.Size = new System.Drawing.Size(112, 13);
            this.linkMail.TabIndex = 138;
            this.linkMail.TabStop = true;
            this.linkMail.Text = "support@smac-mca.nl";
            this.linkMail.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.linkMail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkSmacInternational
            // 
            this.linkSmacInternational.AutoSize = true;
            this.linkSmacInternational.Location = new System.Drawing.Point(181, 165);
            this.linkSmacInternational.Name = "linkSmacInternational";
            this.linkSmacInternational.Size = new System.Drawing.Size(105, 13);
            this.linkSmacInternational.TabIndex = 139;
            this.linkSmacInternational.TabStop = true;
            this.linkSmacInternational.Text = "www.smac-mca.com";
            this.linkSmacInternational.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSmacInternational_LinkClicked);
            // 
            // linkSmacDutch
            // 
            this.linkSmacDutch.AutoSize = true;
            this.linkSmacDutch.Location = new System.Drawing.Point(181, 131);
            this.linkSmacDutch.Name = "linkSmacDutch";
            this.linkSmacDutch.Size = new System.Drawing.Size(93, 13);
            this.linkSmacDutch.TabIndex = 140;
            this.linkSmacDutch.TabStop = true;
            this.linkSmacDutch.Text = "www.smac-mca.nl";
            this.linkSmacDutch.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkSmacDutch_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 141;
            this.label1.Text = "LCC Support email:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 142;
            this.label2.Text = "SMAC homepage";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 144;
            this.label4.Text = "LCC support homepage";
           
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(244, 198);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(49, 24);
            this.btnOK.TabIndex = 146;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // ContactInfo
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 291);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkSmacDutch);
            this.Controls.Add(this.linkSmacInternational);
            this.Controls.Add(this.linkMail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ContactInfo";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SMAC Contact information";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkMail;
        private System.Windows.Forms.LinkLabel linkSmacInternational;
        private System.Windows.Forms.LinkLabel linkSmacDutch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnOK;
      
        
    }
}