﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class MacroSequence : Sequence
    {

        private string parms;   // The parameter string

        private string macroType = "";

        private UInt32 macronumber;

        private UInt32 repeatcount;


        public string[] macroTypes = {Globals.STR_CALL,
                                      Globals.STR_JUMP,
                                      Globals.STR_RETURN,
                                      Globals.STR_REPEAT,
                                      Globals.STR_UNPUSH,
                                      Globals.STR_END_PROGRAM};


        private UserCombobox UserComboboxMacrotype;

        private UserTextbox UserTextboxMacronumber;
        private UserTextbox UserTextboxRepeatcount;


        public MacroSequence()    // The constructor
        {
            name = "Macro";
            mnemonic = "Macro";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, macroTypes);

            UserComboboxMacrotype = new UserCombobox(Globals.mainform.lblName1, Globals.STR_ACTION, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");

            UserTextboxMacronumber = new UserTextbox(Globals.mainform.lblName2, Globals.STR_MACRO_NUMBER, Globals.mainform.txtParm1, 0, 0, 59, Globals.mainform.lblUnits2, "", true);
            UserTextboxRepeatcount = new UserTextbox(Globals.mainform.lblName3, Globals.STR_REPEAT_COUNT, Globals.mainform.txtParm2, 0, Int32.MinValue, Int32.MaxValue, Globals.mainform.lblUnits3, "", true);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxMacrotype = null;

            UserTextboxMacronumber = null;
            UserTextboxRepeatcount = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            // Set the defaults first

            macronumber = 0;
            repeatcount = 0;
            macroType = Globals.STR_CALL;


            if (parms != "")       // Then use the default set of parameters
            {

                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_CALL:
                        case Globals.STR_JUMP:
                        case Globals.STR_RETURN:
                        case Globals.STR_REPEAT:
                        case Globals.STR_UNPUSH:
                        case Globals.STR_END_PROGRAM:
                            macroType = parameterName;
                            break;


                        case Globals.STR_PREFIX_MACRO_NUMBER:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                macronumber = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                            }
                            break;

                        case Globals.STR_PREFIX_REPEAT_COUNT:      // This is an optional parameter, may be empty
                            if (parameterValue != "")
                            {
                                repeatcount = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                            }
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserComboboxMacrotype.Hide();
            UserTextboxMacronumber.Hide();
            UserTextboxRepeatcount.Hide();


            Globals.mainform.ComboBox1.SelectedItem = macroType;
            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //
            UserComboboxMacrotype.Show(0);       // Allways show the selected wait type
            switch (macroType)
            {
                case Globals.STR_CALL:
                case Globals.STR_JUMP:
                    UserTextboxMacronumber.Show(1);
                    break;

                case Globals.STR_RETURN:
                case Globals.STR_UNPUSH:
                case Globals.STR_END_PROGRAM:
                    break;

                case Globals.STR_REPEAT:
                    UserTextboxRepeatcount.Show(1);
                    break;
 
                default:
                    break;
            }

            UserTextboxMacronumber.SetValue(macronumber.ToString());
            UserTextboxRepeatcount.SetValue(repeatcount.ToString());
            Globals.mainform.txtComment.Text = strComment;

        }


        public override void UserInputHasChanged(Object sender)
        {
            macroType = Globals.mainform.ComboBox1.Text;
            UpdateParametersFromGui();
            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {


            macroType = UserComboboxMacrotype.Text();

            switch (macroType)
            {
                case Globals.STR_CALL:
                case Globals.STR_JUMP:
                    macronumber = (UInt32)UserTextboxMacronumber.Value();
                    break;

                case Globals.STR_REPEAT:
                    repeatcount = (UInt32)UserTextboxRepeatcount.Value();
                    break;
            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }



        private void CreateCode()
        {


            string strtemp = "Sequence " + mnemonic + " " + GetParameterString() + " " + strComment;

            instructions.Clear();

            switch(macroType)
            {
                case Globals.STR_CALL:
                    instructions.Add(new WriteInstruction(0x2c04, 0x01, (int)macronumber, strtemp));
                    break;
                case Globals.STR_JUMP:
                    instructions.Add(new WriteInstruction(0x2c04, 0x03, (int) macronumber, strtemp));
                    break;
                case Globals.STR_RETURN:
                    instructions.Add(new WriteInstruction(0x2c04, 0x02, 0, strtemp));
                    break;
                case Globals.STR_REPEAT:
                    instructions.Add(new WriteInstruction(0x2c02, 0x0c, (int)repeatcount, strtemp));
                    break;
                case Globals.STR_UNPUSH:
                    instructions.Add(new WriteInstruction(0x2c04, 0x07, 0, strtemp));
                    break;
                case Globals.STR_END_PROGRAM:
                    instructions.Add(new WriteInstruction(0x2c02, 0x03, 0, strtemp));
                    break;

            }

//            for (int i = 0; i < instructions.Count; i++)
//            {
//                MessageBox.Show(instructions[i].GetTextString());
//            }
//            MessageBox.Show("Macro heeft " + GetSize().ToString() + " instructies");
        }


        public override string GetParameterString()
        {
            string temp = macroType.Replace(' ', '_');

            switch (macroType)
            {
                case Globals.STR_CALL:
                case Globals.STR_JUMP:
                    temp += Globals.STR_COMMA + Globals.STR_PREFIX_MACRO_NUMBER + "="+ macronumber.ToString();
                    break;
                case Globals.STR_REPEAT:
                    temp += Globals.STR_COMMA + Globals.STR_PREFIX_REPEAT_COUNT + "=" + repeatcount.ToString();
                    break;
            }
            return temp;            
        }
    }
}
