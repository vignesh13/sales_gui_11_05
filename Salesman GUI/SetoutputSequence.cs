﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class SetoutputSequence : Sequence
    {

        private string parms;   // The parameter string

        private string outputType = "";
        private string activeState = "";
        private string modeEnabled = "";
        private string sourceRegister = "";
        private string destinationOutput = "";
        

        private UInt32 analogvalue;
        private UInt32 maxRepresentedValue;
        private bool maxRepresentedValueDefined = false;


        public string[] outputTypes = {
                                        Globals.STR_DIGITAL_OUTPUT_0,
                                        Globals.STR_DIGITAL_OUTPUT_1,
                                        Globals.STR_DIGITAL_OUTPUT_2,
                                        Globals.STR_DIGITAL_OUTPUT_3,
                                        Globals.STR_ANALOG_OUTPUT_10_BITS,
                                        Globals.STR_ANALOG_OUTPUT_16_BITS,
                                        Globals.STR_ANALOG_OUTPUT_AUTOMATIC
                                      };


        private string[] activeStates = {Globals.STR_ON,
                                         Globals.STR_OFF};

        private string[] modesEnabled = {Globals.STR_ENABLE,
                                         Globals.STR_DISABLE};

        private string[] destinationOutputs = {Globals.STR_ANALOG_OUTPUT_10_BITS,
                                               Globals.STR_ANALOG_OUTPUT_16_BITS};

                          

        private UserCombobox UserComboboxOutputtype;
        private UserCombobox UserComboboxActivestates;
        private UserCombobox UserComboboxModeEnabled;
        private UserCombobox UserComboboxDestinationOutput;


        private UserTextbox UserTextboxAnalogvalue10bits;
        private UserTextbox UserTextboxAnalogvalue16bits;
        private UserTextbox UserTextboxMaxRepresentedValue;

        private UserObjectselector UserObjectselectorSourceRegister;

        private bool bCheck = true;


        public SetoutputSequence()    // The constructor
        {
            name = "Set output";
            mnemonic = "SetOutput";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, outputTypes);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, activeStates);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox3, modesEnabled);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox4, destinationOutputs);

            UserComboboxOutputtype = new UserCombobox(Globals.mainform.lblName1, Globals.STR_OUTPUT, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxActivestates = new UserCombobox(Globals.mainform.lblName2, "", Globals.mainform.ComboBox2, Globals.mainform.lblUnits2, "");
            UserComboboxModeEnabled = new UserCombobox(Globals.mainform.lblName6, "", Globals.mainform.ComboBox3, Globals.mainform.lblUnits6, "");
            UserComboboxDestinationOutput = new UserCombobox(Globals.mainform.lblName8, Globals.STR_DESTINATION_OUTPUT, Globals.mainform.ComboBox4, Globals.mainform.lblUnits8, "");

            UserTextboxAnalogvalue10bits = new UserTextbox(Globals.mainform.lblName3, Globals.STR_VALUE, Globals.mainform.txtParm1, 0, 0, 1023, Globals.mainform.lblUnits3, "", true);
            UserTextboxAnalogvalue16bits = new UserTextbox(Globals.mainform.lblName4, Globals.STR_VALUE, Globals.mainform.txtParm2, 0, 0, 65535, Globals.mainform.lblUnits4, "", true);
            UserTextboxMaxRepresentedValue = new UserTextbox(Globals.mainform.lblName7, Globals.STR_MAX_REPRESENTED_VALUE, Globals.mainform.txtParm3, 0, 0, 0x7fffffff, Globals.mainform.lblUnits8, "", false);

            UserObjectselectorSourceRegister = new UserObjectselector(Globals.mainform.lblName5, Globals.STR_SOURCE_REGISTER, Globals.mainform.btnSelect1, Globals.mainform.lblUnits5, "", true, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxOutputtype = null;
            UserComboboxActivestates = null;
            UserComboboxModeEnabled = null;
            UserComboboxDestinationOutput = null;

            UserTextboxAnalogvalue10bits = null;
            UserTextboxAnalogvalue16bits = null;
            UserTextboxMaxRepresentedValue = null;
            UserObjectselectorSourceRegister = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            //
            // First set some defaults
            //

            analogvalue = 0;
            outputType = Globals.STR_DIGITAL_OUTPUT_0;
            activeState = Globals.STR_ON;
            maxRepresentedValue = 0;
            maxRepresentedValueDefined = false;
            modeEnabled = Globals.STR_ENABLE;
            sourceRegister = "";
            destinationOutput = Globals.STR_ANALOG_OUTPUT_10_BITS;


            if (parms != "")       // Then parse the parameter string
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_DIGITAL_OUTPUT_0:
                        case Globals.STR_DIGITAL_OUTPUT_1:
                        case Globals.STR_DIGITAL_OUTPUT_2:
                        case Globals.STR_DIGITAL_OUTPUT_3:
                        case Globals.STR_ANALOG_OUTPUT_10_BITS:
                        case Globals.STR_ANALOG_OUTPUT_16_BITS:
                        case Globals.STR_ANALOG_OUTPUT_AUTOMATIC:
                            outputType = parameterName;
                            break;

                        case Globals.STR_ON:
                        case Globals.STR_OFF:
                            activeState = parameterName;
                            break;

                        case Globals.STR_ENABLE:
                        case Globals.STR_DISABLE:
                            modeEnabled = parameterName;
                            break;

                        case Globals.STR_PREFIX_DESTINATION_OUTPUT:
                            destinationOutput = parameterValue;
                            break;
                        
                        case Globals.STR_PREFIX_MAX_REPRESENTED_VALUE:
                            if (parameterValue != "")
                            {
                                maxRepresentedValue = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                maxRepresentedValueDefined = true;
                            }
                            break;

                        case Globals.STR_PREFIX_VALUE:      // This is an optional parameter, may be empty
                            analogvalue = (UInt32) Globals.utils.ConvertStringToInt64(parameterValue);
                            break;

                        case Globals.STR_PREFIX_SOURCE_REGISTER:
                            sourceRegister = parameterValue;
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();

            UserComboboxOutputtype.Hide();
            UserComboboxActivestates.Hide();
            UserComboboxModeEnabled.Hide();
            UserComboboxDestinationOutput.Hide();
            UserObjectselectorSourceRegister.Hide();


            UserTextboxAnalogvalue10bits.Hide();
            UserTextboxAnalogvalue16bits.Hide();
            UserTextboxMaxRepresentedValue.Hide();

            
            Globals.mainform.ComboBox1.SelectedItem = outputType;
            Globals.mainform.ComboBox2.SelectedItem = activeState;
            Globals.mainform.ComboBox3.SelectedItem = modeEnabled;
            Globals.mainform.ComboBox4.SelectedItem = destinationOutput;

            UserObjectselectorSourceRegister.SetSelection(sourceRegister);


            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //
            UserComboboxOutputtype.Show(0);       // Allways show the selected wait type
            switch (outputType)
            {
                case Globals.STR_DIGITAL_OUTPUT_0:
                case Globals.STR_DIGITAL_OUTPUT_1:
                case Globals.STR_DIGITAL_OUTPUT_2:
                case Globals.STR_DIGITAL_OUTPUT_3:
                    UserComboboxActivestates.Show(1);
                    break;

                case Globals.STR_ANALOG_OUTPUT_10_BITS:
                    UserTextboxAnalogvalue10bits.Show(1);
                    break;

                case Globals.STR_ANALOG_OUTPUT_16_BITS:
                    UserTextboxAnalogvalue16bits.Show(1);
                    break;

                case Globals.STR_ANALOG_OUTPUT_AUTOMATIC:
                    UserComboboxModeEnabled.Show(1);
                    if (modeEnabled == Globals.STR_ENABLE)
                    {
                        UserObjectselectorSourceRegister.Show(2);
                        UserComboboxDestinationOutput.Show(3);
                        UserTextboxMaxRepresentedValue.Show(4);
                    }
                    break;

                default:
                    break;
            }

            UserTextboxAnalogvalue10bits.SetValue(analogvalue.ToString());
            UserTextboxAnalogvalue16bits.SetValue(analogvalue.ToString());
            UserTextboxMaxRepresentedValue.SetValue(maxRepresentedValueDefined ? maxRepresentedValue.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;

        }

        public override void ButtonClick(Object sender)
        {
            if (sender == Globals.mainform.btnSelect1)
            {
                UserObjectselectorSourceRegister.SelectObject();
            }
        }


        public override void UserInputHasChanged(Object sender)
        {
            outputType = Globals.mainform.ComboBox1.Text;
            bCheck = false;
            UpdateParametersFromGui();
            bCheck = true;
            if (sender == Globals.mainform.ComboBox1 || (sender == Globals.mainform.ComboBox3))       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {

            outputType = UserComboboxOutputtype.Text();
            activeState = UserComboboxActivestates.Text();
//            MessageBox.Show(modeEnabled + " " + destinationOutput);


            switch (outputType)
            {
                case Globals.STR_DIGITAL_OUTPUT_0:
                case Globals.STR_DIGITAL_OUTPUT_1:
                case Globals.STR_DIGITAL_OUTPUT_2:
                case Globals.STR_DIGITAL_OUTPUT_3:
                    break;

                case Globals.STR_ANALOG_OUTPUT_10_BITS:
                    analogvalue = (UInt32)UserTextboxAnalogvalue10bits.Value();
                    break;

                case Globals.STR_ANALOG_OUTPUT_16_BITS:
                    analogvalue = (UInt32)UserTextboxAnalogvalue16bits.Value();
                    break;

                case Globals.STR_ANALOG_OUTPUT_AUTOMATIC:
                    modeEnabled = UserComboboxModeEnabled.Text();
                    destinationOutput = UserComboboxDestinationOutput.Text();
                    sourceRegister = UserObjectselectorSourceRegister.selectedIndexString;
                    if (modeEnabled == Globals.STR_ENABLE)
                    {
                        UserObjectselectorSourceRegister.ShowMessageIfNotSelected(bCheck);
                    }
                    maxRepresentedValueDefined = UserTextboxMaxRepresentedValue.HasValue();
                    if (maxRepresentedValueDefined)
                    {
                        maxRepresentedValue = (UInt32)UserTextboxMaxRepresentedValue.Value();
                    }
                    break;
            }

            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }



        private void CreateCode()
        {

            // TODO: Create the code here

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);


            int mask = 0;
            int subindex = 0;


            instructions.Clear();

//            MessageBox.Show(outputType);

            switch(outputType)
            {
                case Globals.STR_DIGITAL_OUTPUT_0:
                    mask = 1;
                    break;
                case Globals.STR_DIGITAL_OUTPUT_1:
                    mask = 2;
                    break;
                case Globals.STR_DIGITAL_OUTPUT_2:
                    mask = 4;
                    break;
                case Globals.STR_DIGITAL_OUTPUT_3:
                    mask = 8;
                    break;
                case Globals.STR_ANALOG_OUTPUT_10_BITS:
                    subindex = 1;
                    break;
                case Globals.STR_ANALOG_OUTPUT_16_BITS:
                    subindex = 2;
                    break;
            }

            switch (outputType)
            {
                case Globals.STR_ANALOG_OUTPUT_10_BITS:
                case Globals.STR_ANALOG_OUTPUT_16_BITS:
                    instructions.Add(new WriteInstruction(0x2a04, subindex, (int) analogvalue, strtemp));
                    break;

                case Globals.STR_DIGITAL_OUTPUT_0:
                case Globals.STR_DIGITAL_OUTPUT_1:
                case Globals.STR_DIGITAL_OUTPUT_2:
                case Globals.STR_DIGITAL_OUTPUT_3:
                    instructions.Add(new WriteInstruction(0x2c01, 23, 0x22a02, strtemp + "Get value of digital output in ACCUM"));
                    switch (activeState)
                    {
                        case Globals.STR_ON:
                            instructions.Add(new WriteInstruction(0x2c01, 6, mask, "Set output bit to 1"));
                            break;
                        case Globals.STR_OFF:
                            instructions.Add(new WriteInstruction(0x2c01, 5, (~mask & 0xffff) , "Set output bit to 0"));
                            break;
                    }
                    instructions.Add(new WriteInstruction(0x2c01, 21, 0x22a02, "Write ACCUM back to digital outputs"));
                    break;

                case Globals.STR_ANALOG_OUTPUT_AUTOMATIC:
                    if (modeEnabled == Globals.STR_DISABLE)
                    {
                        instructions.Add(new WriteInstruction(0x2a08, 1, 0, strtemp + "Disable automatic mode"));
                    }
                    else // Enable
                    {
                        instructions.Add(new WriteInstruction(0x2a08, 2, UserObjectselectorSourceRegister.GetIndex(sourceRegister), strtemp + "Set source object"));
                        int destination = (destinationOutput == Globals.STR_ANALOG_OUTPUT_10_BITS) ? 1 : 2;
                        instructions.Add(new WriteInstruction(0x2a08, 3, destination, "Set destination output"));
                        if (maxRepresentedValueDefined)
                        {
                            instructions.Add(new WriteInstruction(0x2a08, 4, (int) maxRepresentedValue, "Set max represented value"));
                        }
                        instructions.Add(new WriteInstruction(0x2a08, 1, 1, "And enable automatic mode"));
                    }
                    break;
            }
        }


        public override string GetParameterString()
        {
            string temp = outputType.Replace(' ', '_');

            switch (outputType)
            {
                case Globals.STR_ANALOG_OUTPUT_10_BITS:
                case Globals.STR_ANALOG_OUTPUT_16_BITS:
                    temp += Globals.STR_COMMA + Globals.STR_PREFIX_VALUE + "=" + analogvalue.ToString();
                    break;
                case Globals.STR_DIGITAL_OUTPUT_0:
                case Globals.STR_DIGITAL_OUTPUT_1:
                case Globals.STR_DIGITAL_OUTPUT_2:
                case Globals.STR_DIGITAL_OUTPUT_3:
                    {
                        temp += Globals.STR_COMMA + activeState;
                    }
                    break;
                case Globals.STR_ANALOG_OUTPUT_AUTOMATIC:
                    temp += Globals.STR_COMMA + modeEnabled;
                    if (modeEnabled == Globals.STR_ENABLE)
                    {
                        temp += Globals.STR_COMMA + Globals.STR_PREFIX_SOURCE_REGISTER + "=" + sourceRegister;
                        temp += Globals.STR_COMMA + Globals.STR_PREFIX_DESTINATION_OUTPUT + "=" + destinationOutput;
                        temp += Globals.STR_COMMA + Globals.STR_PREFIX_MAX_REPRESENTED_VALUE + "=";
                        if(maxRepresentedValueDefined)
                        {
                            temp += maxRepresentedValue.ToString();
                        }
                    }
                    break;
            }
            return temp;            
        }
    }
}
