﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class SoftlandSequence : Sequence
    {

        const string STR_PREFIX_VELOCITY = "Vel";
        const string STR_PREFIX_ACCELERATION = "Acc";
        const string STR_PREFIX_FORCE = "Force";
        const string STR_PREFIX_ERROR = "Error";

        const string STR_ACCELERATION = "Acceleration";
        const string STR_SPEED_IMPACT = "Velocity for object search";
        const string STR_FORCE = "Force";
        const string STR_POSITION_ERROR = "Position error";

        const string STR_ACTION_AFTER_LANDING = "Action after landing";

        const string STR_APPLY_FORCE = "Apply force";
        const string STR_HOLD_FORCE = "Hold force";
        const string STR_HOLD_POSITION = "Hold position";
        const string STR_HOLD_MOTION_STATUS = "Hold motion status";
        const string STR_MOTOR_OFF = "Motor off";


        private string parms;   // The parameter string

        private bool velocityDefined = false;
        private UInt32 velocity;
        private bool accelerationDefined = false;
        private UInt32 acceleration;
        private bool forceDefined = false;
        private UInt32 force;
        private bool trackingerrorDefined = false;// ::VIGNESH::05/05
        private UInt32 trackingerror;


        private string direction = "";
        private string afterimpactAction = "";


        private string[] softlandDirections = {Globals.STR_POSITIVE,
                                               Globals.STR_NEGATIVE};

        public string[] afterimpactActions = {STR_APPLY_FORCE,
                                              STR_HOLD_FORCE,
                                              STR_HOLD_POSITION,
                                              STR_HOLD_MOTION_STATUS,
                                              STR_MOTOR_OFF};


        private UserCombobox UserComboboxDirections;
        private UserCombobox UserComboboxAfterimpactactions;

        private UserTextbox UserTextboxVelocity;
        private UserTextbox UserTextboxAcceleration;
        private UserTextbox UserTextboxForce;
        private UserTextbox UserTextboxTrackingerror;
       // private UserTextbox UserTextboxTrackingerrorDefined;



        public SoftlandSequence()    // The constructor
        {
            name = "Softland";
            mnemonic = "Softland";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, afterimpactActions);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, softlandDirections);

            UserComboboxAfterimpactactions = new UserCombobox(Globals.mainform.lblName1, STR_ACTION_AFTER_LANDING, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserComboboxDirections = new UserCombobox(Globals.mainform.lblName7, Globals.STR_DIRECTION, Globals.mainform.ComboBox2, Globals.mainform.lblUnits7, "");

            UserTextboxVelocity = new UserTextbox(Globals.mainform.lblName2, STR_SPEED_IMPACT, Globals.mainform.txtParm1, 0, 1, UInt32.MaxValue, Globals.mainform.lblUnits2, Globals.STR_COUNTS_PER_SEC, false);
            UserTextboxAcceleration = new UserTextbox(Globals.mainform.lblName3, STR_ACCELERATION, Globals.mainform.txtParm2, 0, 1, UInt32.MaxValue, Globals.mainform.lblUnits3, Globals.STR_COUNTS_PER_SEC2, false);
            UserTextboxForce = new UserTextbox(Globals.mainform.lblName4, STR_FORCE, Globals.mainform.txtParm3, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits4, Globals.STR_PROMILLE_OF_RATED_FORCE, false);
            UserTextboxTrackingerror = new UserTextbox(Globals.mainform.lblName6, STR_POSITION_ERROR, Globals.mainform.txtParm5, 0, 1, UInt32.MaxValue, Globals.mainform.lblUnits6, Globals.STR_COUNTS, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxAfterimpactactions = null;
            UserComboboxDirections = null;
            UserTextboxVelocity = null;
            UserTextboxAcceleration = null;
            UserTextboxForce = null;
            UserTextboxTrackingerror = null;
           // UserTextboxTrackingerrorDefined = null;
        }

        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            // Set the defaults

            velocityDefined = false;
            accelerationDefined = false;
            forceDefined = false;
            
            trackingerrorDefined = false; // VIGNESH::05/05// This is neccessary to make sure no value is defined in the text box by default
            afterimpactAction = STR_APPLY_FORCE;
            direction = Globals.STR_POSITIVE;

            if (parms != "")       // Then parse the parameter string
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case Globals.STR_POSITIVE:
                        case Globals.STR_NEGATIVE:
                            direction = parameterName;
                            break;

                        case STR_APPLY_FORCE:
                        case STR_HOLD_FORCE:
                        case STR_HOLD_POSITION:
                        case STR_HOLD_MOTION_STATUS:
                        case STR_MOTOR_OFF:
                            afterimpactAction = parameterName;
                            break;


                        case STR_PREFIX_VELOCITY:      // This is an optional parameter, may be empty
                            if (parameterValue != "")
                            {
                                velocity = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                velocityDefined = true;
                            }
                            break;


                        case STR_PREFIX_ACCELERATION:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                acceleration = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                accelerationDefined = true;
                            }
                            break;
                        case STR_PREFIX_FORCE:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                force = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                forceDefined = true;
                            }
                            break;

                        case STR_PREFIX_ERROR:   // This is an mandatory parameter// This is an optional parameter ::VIGNESH::05/05
                            if (parameterValue != "")
                            {
                                trackingerror = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                trackingerrorDefined = true;
                            }

                            //trackingerror = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);

                            break;
                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserComboboxAfterimpactactions.Hide();
            UserTextboxAcceleration.Hide();
            UserTextboxVelocity.Hide();
            UserTextboxForce.Hide();
            UserTextboxTrackingerror.Hide();

            Globals.mainform.ComboBox1.SelectedItem = afterimpactAction;
            Globals.mainform.ComboBox2.SelectedItem = direction;

            UserComboboxDirections.Show(0);
            UserTextboxVelocity.Show(1);
            UserTextboxAcceleration.Show(2);
            UserTextboxTrackingerror.Show(3);
            UserComboboxAfterimpactactions.Show(4);
            if (afterimpactAction == STR_APPLY_FORCE)
            {
                UserTextboxForce.Show(5);
            }


            UserTextboxAcceleration.SetValue(accelerationDefined ? acceleration.ToString() : "");
            UserTextboxVelocity.SetValue(velocityDefined ? velocity.ToString() : "");
            UserTextboxForce.SetValue(forceDefined ? force.ToString() : "");
            UserTextboxTrackingerror.SetValue(trackingerrorDefined ? trackingerror.ToString() : "");// VIGNESH:: 05/05
            Globals.mainform.txtComment.Text = strComment;

        }


        public override void UserInputHasChanged(Object sender)
        {
            afterimpactAction = Globals.mainform.ComboBox1.Text;
            direction = Globals.mainform.ComboBox2.Text;

            UpdateParametersFromGui();
            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
            {
                ShowParametersInGui();
            }
        }


        public override void UpdateParametersFromGui()
        {
            velocityDefined = UserTextboxVelocity.HasValue();
            if (velocityDefined)
            {
                velocity = (UInt32)UserTextboxVelocity.Value();
            }
            
            accelerationDefined = UserTextboxAcceleration.HasValue();
            if (accelerationDefined)
            {
                acceleration = (UInt32)UserTextboxAcceleration.Value();
            }

            forceDefined = UserTextboxForce.HasValue();
            if (forceDefined)
            {
                force = (UInt32)UserTextboxForce.Value();
            }

           trackingerrorDefined = UserTextboxTrackingerror.HasValue();// VIGNESH::05/05
            if (trackingerrorDefined)
            {
                trackingerror = (UInt32)UserTextboxTrackingerror.Value();
            }


           // trackingerror = (UInt32)UserTextboxTrackingerror.Value();


            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {

            string temp = "";

            temp += direction.Replace(' ', '_');

            temp += Globals.STR_COMMA + STR_PREFIX_VELOCITY + "=";
            if (velocityDefined)
            {
                temp += velocity.ToString();
            }

            temp += Globals.STR_COMMA + STR_PREFIX_ACCELERATION + "=";
            if (accelerationDefined)
            {
                temp += acceleration.ToString();
            }

            temp += Globals.STR_COMMA + STR_PREFIX_FORCE + "=";
            if (forceDefined)
            {
                temp += force.ToString();
            }

            temp += Globals.STR_COMMA + STR_PREFIX_ERROR + "=";//+ trackingerror.ToString();// VIGNESH:: 05/05
            if (trackingerrorDefined)
            {
                temp += trackingerror.ToString();
            }

            temp += Globals.STR_COMMA + afterimpactAction.Replace(' ', '_');

            return temp;
        }


        //
        // Softland uses profile position mode with a targetposition of +∞ or -∞ dependant on direction
        // Impact is detected by the value of the tracking error.
        // Impact detection is suppressed during the acceleration time (+ 20%) but no longer than 1 sec.
        // After impact is detected the mode of operation is switched to profiled force mode
        // 
        
        private void CreateCode()
        {

            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);
            int targetposition = (direction == Globals.STR_POSITIVE) ? 1000000000 : -1000000000;
            double accelerationTimeMsec = 0;
      
            if (accelerationDefined && velocityDefined && (acceleration > 0) && (velocity > 0))
            {
                accelerationTimeMsec = 1.2 * (double) velocity / (double) acceleration * 1000.0;

                if (accelerationTimeMsec > 1000)
                {
                    accelerationTimeMsec = 1000;
                }

            }

            instructions.Clear();

            instructions.Add(new WriteInstruction(0x607a, 0, targetposition, strtemp + "Set target position to big value in the proper direction"));

            if (velocityDefined)
            {
                instructions.Add(new WriteInstruction(0x6081, 0, (int)velocity));    // Profile velocity
            }

            if (accelerationDefined)
            {
                instructions.Add(new WriteInstruction(0x6083, 0, (int)acceleration));   // Profile acceleration
                instructions.Add(new WriteInstruction(0x6084, 0, (int)acceleration));   // Profile decelleration
            }


            CreateCodeForSystemMacro(Bios.callnumber.ABSOLUTE_POSITION_MOVE_CHANGE_IMMEDIATE);   // Start the position move


            instructions.Add(new WriteInstruction(0x2c02, 13, (int) accelerationTimeMsec, "Wait until acceleration has finished"));

            //            instructions.Add(new WriteInstruction(0x2c02, 13, 100, "Wait 100 msec extra"));     // TODO: REMOVE THIS LINE
            
            instructions.Add(new WriteInstruction(0x2c01, 22, 0x60f4, "Get following error actual value in ACCUM"));// 05/04:: VIGNESH:: This is for softalnd routine
            instructions.Add(new WriteInstruction(0x2c01, 19, 0, "Absolute value of following error in ACCUM now"));
            instructions.Add(new WriteInstruction(0x2c02, 4, (int)trackingerror, "Softland not yet detected"));   // If ACCUM is lower trackingerror

            

            instructions.Add(new WriteInstruction(0x2c04, 6, -3, "Loop back until softland is detected"));
            
          
            switch(afterimpactAction)
            {
                case STR_APPLY_FORCE:
                    CreateCodeForSystemMacro(Bios.callnumber.FORCE_MOVE);
                    if (forceDefined)
                    {
                        instructions.Add(new WriteInstruction(0x6071, 0, direction == Globals.STR_POSITIVE ? ((int)force) : -1 * (int)force, "Set target force for profiled force mode")); // 
                    }
                    else
                    {
                        instructions.Add(new WriteInstruction(0x2c01, 23, 0x6071, "No target force specified: get actual target force in ACCUM"));
                        instructions.Add(new WriteInstruction(0x2c01, 19, 0, "Absolute value of target force in ACCUM now"));
                        if (direction == Globals.STR_NEGATIVE)
                        {
                            instructions.Add(new WriteInstruction(0x2c01, 4, -1, "Negative direction so make target force negative"));
                        }
                        instructions.Add(new WriteInstruction(0x2c01, 21, 0x6071, "Write target force to start profile force mode"));
                    }
                    break;

                case STR_HOLD_FORCE:
                    instructions.Add(new WriteInstruction(0x2c01, 23, 0x6077, "Hold force: Get actual force in ACCUM"));
                    instructions.Add(new WriteInstruction(0x2c01, 21, 0x6071, "Write target force for profile force mode"));
                    CreateCodeForSystemMacro(Bios.callnumber.FORCE_MOVE);
                    instructions.Add(new WriteInstruction(0x2c01, 23, 0x6071, "Get target force in accum"));
                    instructions.Add(new WriteInstruction(0x2c01, 21, 0x6071, "Write target force to start profile force mode"));
                    break;

                case STR_HOLD_POSITION:
                    instructions.Add(new WriteInstruction(0x2c01, 22, 0x6064, "Hold position: Get actual position in ACCUM"));
                    instructions.Add(new WriteInstruction(0x2c01, 21, 0x607A, "Write target position"));
                    CreateCodeForSystemMacro(Bios.callnumber.ABSOLUTE_POSITION_MOVE_CHANGE_IMMEDIATE);
                    break;

                case STR_HOLD_MOTION_STATUS:    // No extra instructions needed
                    break;

                case STR_MOTOR_OFF:
                    CreateCodeForSystemMacro(Bios.callnumber.MOTOR_OFF);
                    break;

                default:
                    break;
            }

        }
    }
}
