﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Salesman_GUI
{
    public class Bios
    {
        private const string systemMacroFilename = "System macros V2.mlm";
        List<string> theBios = new List<string>();
        public enum callnumber
        {
            ABSOLUTE_POSITION_MOVE = 0,
            ABSOLUTE_POSITION_MOVE_CHANGE_IMMEDIATE = 1,
            RELATIVE_POSITION_MOVE = 2,
            RELATIVE_POSITION_MOVE_CHANGE_IMMEDIATE = 3,
            WAIT_FOR_TARGET_REACHED = 4,
            MOTOR_ON = 5,
            MOTOR_OFF = 6,
            HOMING = 7,
            VELOCITY_MOVE = 8,
            FORCE_MOVE = 9,
            HOMING_ON_CURRENT_POSITION = 10

        } ;
          
        public Bios()
        {
            theBios.Clear();
            if (!File.Exists(systemMacroFilename))
            {
                MessageBox.Show("Fatal error: Can not open file " + systemMacroFilename + "\n\n" +
                "Try to re-install the application", "System error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            foreach (string s in File.ReadAllLines(systemMacroFilename))
            {
                string strtemp;
                strtemp = s.Trim();
                if ((strtemp != "") && (strtemp[0] != '['))
                {
                    theBios.Add(strtemp);
                }
            }
//            for (int i = 0; i < theBios.Count; i++)
//            {
//                MessageBox.Show(theBios[i]);
//            }
        }

        public int Lines()
        {
            return theBios.Count;
        }

        public string Line(int index)
        {
            return theBios[index];
        }

        private bool IsCommentline(string s)
        {
            s = s.Trim();
            return ((s == "") || (s.IndexOf("//") == 0));
        }

        private int countObjects(string index)
        {
            int count = 0;
            foreach (string s in theBios)
            {
                if (!IsCommentline(s))
                {
                    if (Globals.utils.ConvertStringToInt64(s.ToUpper().Split()[2]) == Globals.utils.ConvertStringToInt64(index))
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        public int NumberOfMacros()
        {
            return countObjects("0x012C05");
        }

        public int NumberOfInstructions()
        {
            return countObjects("0x022C05");
        }
    }
}
