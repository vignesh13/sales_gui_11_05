﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public partial class dlgSaveconfig : Form
    {
        public dlgSaveconfig()
        {
            InitializeComponent();
        }

        private void dlgSaveconfig_Load(object sender, EventArgs e)
        {
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public bool MergeFileSelected()
        {
            return radioButton1.Checked;
        }
    }
}
