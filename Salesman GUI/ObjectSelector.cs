﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;


namespace Salesman_GUI
{
    public partial class ObjectSelector : Form
    {
        public int selectedIndex = 0;   // Combined index iof the selected item. 0 means no item slecfted
        public string groupname = "";
        public string objectname = "";
        public string fullname = "";

        public ObjectSelector()
        {
            InitializeComponent();
        }

        public void SetOutputVariables()
        {
            TreeNode parent = treeView1.SelectedNode.Parent;
            groupname = (parent == null ? "" : (parent.Text + "-"));
            objectname = treeView1.SelectedNode.Text;
            fullname = ((groupname + objectname).Replace(' ', '_')).Replace(',', '_') + "(0x" + selectedIndex.ToString("X6") + ")";
        }

        public int getindex(string fullstring)
        {
            int index = 0;

            bool bOk = fullstring.Length >= 10;
            if (bOk)
            {
                fullstring = (fullstring.Substring(fullstring.Length - 10)).ToUpper(); // selection now last 10 characters of the string
                bOk = ((fullstring[0] == '(') && (fullstring[1] == '0') && (fullstring[2] == 'X') && (fullstring[9] == ')'));
            }
            if (bOk)
            {
                bOk = int.TryParse(fullstring.Substring(3, 6), NumberStyles.HexNumber, null, out index);
            }
            return (bOk ? index : 0);
        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            selectedIndex = 0;
            
            if (treeView1.SelectedNode != null)
            {
                selectedIndex = int.Parse(treeView1.SelectedNode.Name);
                if (selectedIndex == 0)
                {
                    MessageBox.Show("The group item \"" + treeView1.SelectedNode.Text + "\" can not be selected.\n\n" + "Expand the group by clicking the + sign and then select one of the sub-items.", "No selection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    SetOutputVariables();
                }
            }
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private bool isAllowed(int index, int subindex)
        {
            bool allowed = (index >= 0x2000);
            switch (index)
            {
                case 0x1001:
                case 0x1003:
                case 0x1010:
                case 0x1011:
                    allowed = true;
                    break;
                case 0x2c01:
                case 0x2c02:
//               case 0x2c04:
                case 0x2ff0:
                    allowed = false;
                    break;

                case 0x2c05:
                    allowed = (subindex == 4); // allow only the protected access from the macro group
                    break;
            }
            return allowed;
        }



        public void Init(bool Readable,bool Writable)
        {
            CanopenObject canobject;
            string currentNodename = "";
            int currentNodeIndex = 0;
            treeView1.Nodes.Clear();
            selectedIndex = 0;

            for (int i = 0; i < Globals.dictionary.Count(); i++)
            {
                canobject = Globals.dictionary.dictionary[i];
                if (isAllowed(canobject.index, canobject.subindex) && ((Readable && canobject.IsReadable()) || (Writable && canobject.IsWritable()))) // Then add this to the treeview control
                {
                    string key = canobject.combinedindex.ToString();

                    if (canobject.groupname == "") // Just add this object in line
                    {
                        treeView1.Nodes.Add(key , canobject.objectname);
                    }
                    else
                    {
                        if (canobject.groupname != currentNodename)
                        {
                            treeView1.Nodes.Add("0", canobject.groupname);       // This is a groupname so 0 key here
                            currentNodeIndex = treeView1.Nodes.Count - 1;
                            currentNodename = canobject.groupname;
                        }
                        treeView1.Nodes[currentNodeIndex].Nodes.Add(key, canobject.objectname); 
                    }
                }
            }
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            btnOk_Click(sender, e);
        }

        

        //
        // The selection string must be in the full format.
        // groupname-objectname(0xSSXXXX)
        // SS is the 2 hex digit subindex
        // XXXX is the 4 hex digit index
        //
        // This function reads the object subindex and index from the last part in the string
        // groupname and objectname are reconstructed by this function and the correct leaf of the treeview will be selected.
        //



        public void SetSelection(string selection)
        {
            int index = 0;
            selectedIndex = 0;
            if (selection != null)
            {

                index = getindex(selection);
                bool bOk = (index != 0);
                if (bOk)
                {
                    string searchName = index.ToString();

                    treeView1.SelectedNode = null;

                    foreach (TreeNode node in treeView1.Nodes)
                    {
                        if (node.Nodes.Count > 0)   // Then this node has children
                        {
                            foreach (TreeNode subnode in node.Nodes)
                            {
                                if (subnode.Name == searchName)
                                {
                                    treeView1.SelectedNode = subnode;
                                }
                            }
                        }

                        else
                        {
                            if (node.Name == searchName)
                            {
                                treeView1.SelectedNode = node;
                            }
                        }
                    }
                    if (treeView1.SelectedNode == null)
                    {
                        bOk = false;
                    }
                }
                if (bOk)
                {
                    selectedIndex = index;
                    SetOutputVariables();
                    treeView1.Select();     // This will highlight the node
                }

            }
        }

        public void SetSelection(int combinedindex)
        {
            SetSelection( "(0x" + combinedindex.ToString("X6") + ")");
        }

        public TreeNode nextNode(TreeNode node)
        {
            TreeNode parent = node.Parent;
            TreeNode nextnode = null;
            if (parent != null)    // Then the node has a parent
            {
                if (node.Index == (parent.Nodes.Count - 1)) // Then this node is the last child node
                {
                    nextnode = parent.NextNode;
                    if (nextnode == null)
                    {
                        nextnode = treeView1.Nodes[0];
                    }
                }
                else
                {
                    nextnode = node.NextNode;
                }
            }
            else    // The node is a parent node
            {
                if (node.Nodes.Count > 0)   // Then there are child nodes
                {
                    nextnode = node.Nodes[0];
                }
                else
                {
                    nextnode = node.NextNode;
                    if (nextnode == null)
                    {
                        nextnode = treeView1.Nodes[0];
                    }
                }

            }
            return nextnode;
        }


        private void btnFindtext_Click(object sender, EventArgs e)
        {
            bool bFound = false;
            TreeNode startnode = treeView1.SelectedNode;              // This is the index of the node to start a search
            TreeNode actualnode;
            if (startnode == null)
            {
                startnode = treeView1.Nodes[0];
                actualnode = startnode;
            }
            else
            {
                
                actualnode = nextNode(startnode);
                startnode = actualnode;
            }

            do
            {
                int objectindex = int.Parse(actualnode.Name);   // Actually this is the Tag
                string objectstring = actualnode.Text + " 0x" + (objectindex & 0xFFFF).ToString("X4");
                objectstring = objectstring.ToUpper();
                string searchstring = txtFind.Text.ToUpper();

                if (objectstring.IndexOf(searchstring) >= 0)
                {
                    bFound = true;
                    treeView1.SelectedNode = actualnode;
                }
                else
                {
                    actualnode = nextNode(actualnode);
                }

            } while (!bFound && (actualnode != startnode));

            if (!bFound)
            {
                MessageBox.Show("String " + txtFind.Text + " Not found");
            }
            treeView1.Focus();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

            TreeNode node = treeView1.SelectedNode;
            
            if (node != null)
            {
                int index = int.Parse(node.Name.ToString());
                int subindex = (index >> 16) & 0xff;
                string strDisplay = "0x" + (index & 0xffff).ToString("X4");
                if (subindex != 0)
                {
                    strDisplay += " sub " + subindex.ToString();
                }
                txtObject.Text = strDisplay;
            }
        }

        private void txtFind_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                btnFindtext_Click(null, null);
                e.Handled = true;
               
            }
        }
    }
}
