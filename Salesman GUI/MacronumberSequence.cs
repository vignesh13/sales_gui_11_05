﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{


    public class MacronumberSequence : Sequence
    {

        private string parms;   // The parameter string


        private Int32 macronumber;


        private UserTextbox UserTextboxMacronumber;



        public MacronumberSequence()    // The constructor
        {
            name = "Macro number";
            mnemonic = "MacroNumber";
        }

        public override void Init()
        {

            UserTextboxMacronumber = new UserTextbox(Globals.mainform.lblName1, Globals.STR_MACRO_NUMBER, Globals.mainform.txtParm1, 0, 0, 59, Globals.mainform.lblUnits1, "", true);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserTextboxMacronumber = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters


            macronumber = 0;        // Set a default value

            switch (Globals.utils.GetNumParameters(parms))
            {
                case 0:
                    break;

                case 1:
                    macronumber = (Int32)Globals.utils.ConvertStringToInt64(parms);
                    if ((macronumber < 0) || (macronumber > 59))
                    {
                        macronumber = 0;
                        strError = "Macro number [" + parms + "] is not a valid macro number"; 
                    }
                    break;

                default:
                    strError = "Macro number [" + parms + "] too many parameters";
                    break;
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();
            UserTextboxMacronumber.Show(0);
            UserTextboxMacronumber.SetValue(macronumber.ToString());
            Globals.mainform.txtComment.Text = strComment;
        }

        public override void UserInputHasChanged(Object sender)
        {

//
// This is not necessary for a macro number
//            if (sender == Globals.mainform.ComboBox1)       // Prevent a "flashing" user interface
//            {
//                ShowParametersInGui();
//            }
        }


        public override void UpdateParametersFromGui()
        {

            macronumber = (Int32)UserTextboxMacronumber.Value();
            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }



        private void CreateCode()
        {
            instructions.Clear();
            instructions.Add(new WriteInstruction(0x2c05, 1, macronumber, Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment)));
        }


        public override string GetParameterString()
        {
            return macronumber.ToString();

        }

        public override int GetSize()        // Returns the size of the sequence in instructions
        {
            return 0;
        }

    }
}
