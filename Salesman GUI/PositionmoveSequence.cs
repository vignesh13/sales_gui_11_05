﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class PositionmoveSequence : Sequence
    {

        const string STR_ABSOLUTE =  "Absolute";
        const string STR_RELATIVE =  "Relative";

        const string STR_CHANGE_IMMEDIATE = "Change immediate";
        const string STR_ADD_TO_QUE =    "Add to que";

        const string STR_SETPOINT_MODE = "Setpoint mode";


        const string STR_PREFIX_TARGET =       "Target";
        const string STR_PREFIX_VELOCITY =     "Vel";
        const string STR_PREFIX_ACCELERATION = "Acc";



        const string STR_MODE          = "Mode";
        const string STR_TARGET        = "Target position";
        const string STR_VELOCITY      = "Profile velocity";
        const string STR_ACCELERATION  = "Profile acceleration";


        private string parms;   // The parameter string

        private string mode;
        private string setpointMode;

        private Int32 targetposition;
        private bool targetpositionDefined;
        private bool velocityDefined = false;
        private UInt32 velocity;
        private bool accelerationDefined = false;
        private UInt32 acceleration;
        private bool forceDefined = false;
        private Int16 force;

        public string[] moveModes    =     {STR_ABSOLUTE,
                                           STR_RELATIVE };

        public string[] setpointModes = { STR_CHANGE_IMMEDIATE,
                                          STR_ADD_TO_QUE};


        private UserCombobox UserComboboxMode;
        private UserCombobox UserComboboxSetpointMode;


        private UserTextbox UserTextboxTargetposition;
        private UserTextbox UserTextboxVelocity;
        private UserTextbox UserTextboxAcceleration;
        private UserTextbox UserTextboxForce;


        public PositionmoveSequence()    // The constructor
        {
            name = "Position move";
            mnemonic = "PositionMove";
        }

        public override void Init()
        {
            Globals.utils.FillComboBox(Globals.mainform.ComboBox1, moveModes);
            Globals.utils.FillComboBox(Globals.mainform.ComboBox2, setpointModes);


            UserComboboxMode = new UserCombobox(Globals.mainform.lblName1, STR_MODE, Globals.mainform.ComboBox1, Globals.mainform.lblUnits1, "");
            UserTextboxTargetposition = new UserTextbox(Globals.mainform.lblName2, STR_TARGET, Globals.mainform.txtParm1, 0, Int32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits2, Globals.POS_UNITS, false);
            UserTextboxVelocity = new UserTextbox(Globals.mainform.lblName3, STR_VELOCITY, Globals.mainform.txtParm2, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits3, Globals.VEL_UNITS, false);
            UserTextboxAcceleration = new UserTextbox(Globals.mainform.lblName4, STR_ACCELERATION, Globals.mainform.txtParm3, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits4, Globals.ACC_UNITS, false);
            UserComboboxSetpointMode = new UserCombobox(Globals.mainform.lblName5, STR_SETPOINT_MODE, Globals.mainform.ComboBox2, Globals.mainform.lblUnits5, "");
            UserTextboxForce = new UserTextbox(Globals.mainform.lblName6, "Force", Globals.mainform.txtParm4, 0, Int32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits5, Globals.FORCE_UNITS, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserComboboxMode = null;
            UserTextboxTargetposition = null;
            UserTextboxVelocity = null;
            UserTextboxAcceleration = null;
            UserComboboxSetpointMode = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            targetpositionDefined = false;
            velocityDefined = false;
            accelerationDefined = false;

            if (parms == "")       // Then use the default set of parameters
            {
                mode = STR_ABSOLUTE; 
                setpointMode = STR_CHANGE_IMMEDIATE; // this is a good default value

                targetposition = 0;
                targetpositionDefined = false;
            }
            else
            {
                setpointMode = STR_ADD_TO_QUE;      // If not defined in prior 1.1 releases: this is the default
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {
                        case STR_ABSOLUTE:
                        case STR_RELATIVE:
                            mode = parameterName;
                            break;

                        case STR_ADD_TO_QUE:
                        case STR_CHANGE_IMMEDIATE:
                            setpointMode = parameterName;
                            break;

                        case STR_PREFIX_TARGET:
                            if (parameterValue != "")
                            {
                                targetposition = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                                targetpositionDefined = true;
                            }
                            break;

                        case STR_PREFIX_VELOCITY:      // This is an optional parameter, may be empty
                            if (parameterValue != "")
                            {
                                velocity = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                velocityDefined = true;
                            }
                            break;

                        case STR_PREFIX_ACCELERATION:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                acceleration = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                accelerationDefined = true;
                            }
                            break;


                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserComboboxMode.Show(0);
            UserTextboxTargetposition.Show(1);
            UserTextboxVelocity.Show(2);
            UserTextboxAcceleration.Show(3);
            UserComboboxSetpointMode.Show(4);
            UserTextboxForce.Show(5);

            Globals.mainform.ComboBox1.SelectedItem = mode;
            Globals.mainform.ComboBox2.SelectedItem = setpointMode;

            UserTextboxTargetposition.SetValue(targetpositionDefined ? targetposition.ToString() : "");
            UserTextboxVelocity.SetValue(velocityDefined ? velocity.ToString() : "");
            UserTextboxAcceleration.SetValue(accelerationDefined ? acceleration.ToString() : "");
            UserTextboxForce.SetValue(forceDefined ? force.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;
        }

        public override void UserInputHasChanged(Object sender)
        {
            mode = Globals.mainform.ComboBox1.Text;
            setpointMode = Globals.mainform.ComboBox2.Text;
            UpdateParametersFromGui();
        }


        public override void UpdateParametersFromGui()
        {

            targetpositionDefined = UserTextboxTargetposition.HasValue();
            if (targetpositionDefined)
            {
                targetposition = (Int32)UserTextboxTargetposition.Value();
            }
            
            velocityDefined = UserTextboxVelocity.HasValue();
            if (velocityDefined)
            {
                velocity = (UInt32)UserTextboxVelocity.Value();
            }

            accelerationDefined = UserTextboxAcceleration.HasValue();
            if (accelerationDefined)
            {
                acceleration = (UInt32)UserTextboxAcceleration.Value();
            }


            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {
            string temp = mode.Replace(" ", "_");

            temp = temp + Globals.STR_COMMA + STR_PREFIX_TARGET + "=";
            if (targetpositionDefined)
            {
                temp = temp + targetposition.ToString();
            }

            temp = temp + Globals.STR_COMMA + STR_PREFIX_VELOCITY + "=";
            if (velocityDefined)
            {
                temp = temp + velocity.ToString();
            }
            
            temp = temp + Globals.STR_COMMA + STR_PREFIX_ACCELERATION + "=";
            if (accelerationDefined)
            {
                temp = temp + acceleration.ToString();
            }
            temp = temp + Globals.STR_COMMA + setpointMode.Replace(" ", "_");

            
            return temp;            
        }

        private void CreateCode()
        {
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);
            
            instructions.Clear();

            if (targetpositionDefined)
            {
                instructions.Add(new WriteInstruction(0x607a, 0, (int)targetposition, strtemp));    // Set Target position
                strtemp = "";
            }

            if (velocityDefined)
            {
                instructions.Add(new WriteInstruction(0x6081, 0, (int)velocity, strtemp));      // Profile velocity
                strtemp = "";
            }


            if (accelerationDefined)
            {
                instructions.Add(new WriteInstruction(0x6083, 0, (int)acceleration,strtemp));      // Profile acceleration
                instructions.Add(new WriteInstruction(0x6084, 0, (int)acceleration));      // Profile deceleration
                strtemp = "";
            }

            switch (mode)
            {
                case STR_ABSOLUTE:
                    switch (setpointMode)
                    {
                        case STR_ADD_TO_QUE:
                            CreateCodeForSystemMacro(Bios.callnumber.ABSOLUTE_POSITION_MOVE, strtemp);
                            break;
                        case STR_CHANGE_IMMEDIATE:
                            CreateCodeForSystemMacro(Bios.callnumber.ABSOLUTE_POSITION_MOVE_CHANGE_IMMEDIATE, strtemp);
                            break;
                        default:
                            break;
                    }
                    break;
                case STR_RELATIVE:
                    switch (setpointMode)
                    {
                        case STR_ADD_TO_QUE:
                            CreateCodeForSystemMacro(Bios.callnumber.RELATIVE_POSITION_MOVE, strtemp);
                            break;
                        case STR_CHANGE_IMMEDIATE:
                            CreateCodeForSystemMacro(Bios.callnumber.RELATIVE_POSITION_MOVE_CHANGE_IMMEDIATE, strtemp);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

        }
    }
}
