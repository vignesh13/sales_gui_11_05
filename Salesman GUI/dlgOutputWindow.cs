﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace Salesman_GUI
{
    public partial class dlgOutputWindow : Form
    {
        private Stopwatch stopwatch = new Stopwatch();
        private Stopwatch stopwatchMeasure = new Stopwatch();
        
        public dlgOutputWindow()
        {
            InitializeComponent();
        }


        public void init()      // Is called by the main application after the buttons are set
        {
            btnClear_Click(null, null);
            Size = MinimumSize;
        }

        private void start()
        {
            stopwatch.Reset();
            stopwatch.Start();
            if (chkAddtimestamp.Checked)
            {
                txtLogwindow.AppendText("Log started " + string.Format("{0:dd/MM/yyy hh:mm:ss.fff} ", DateTime.Now) + "\r\n");
            }
        }


        public void AddMessage(string message, int index)      // The index of the message is parsed by the data received handler on the mainform
        {

            if (chkAddtimestamp.Checked)
            {
                txtLogwindow.AppendText(stopwatch.ElapsedMilliseconds.ToString() + " ");
            }
            txtLogwindow.AppendText(message);
            if (chkAddname.Checked)
            {
                txtLogwindow.AppendText(" " + Globals.dictionary.ObjectLongName(index));
            }
            txtLogwindow.AppendText(Environment.NewLine);
            btnSave.Enabled = true;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog browseFile = new SaveFileDialog();
            browseFile.Filter = "text files (*.txt)|*.txt";
            browseFile.DefaultExt = "txt";
            browseFile.ShowDialog();
            string chosenFile = browseFile.FileName;
            if (chosenFile != "")
            {

                System.IO.StreamWriter outputfile = new System.IO.StreamWriter(chosenFile);

                outputfile.Write(txtLogwindow.Text);
                outputfile.Close();
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtLogwindow.Text = "";
            start();
            btnSave.Enabled = false;
        }

        private void dlgOutputWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
           
            // Hide();
        }


        private void dlgOutputWindow_SizeChanged(object sender, EventArgs e)
        {
           

        }
    }
}
