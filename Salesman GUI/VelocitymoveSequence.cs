﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{

    public class VelocitymoveSequence : Sequence
    {

        const string STR_PREFIX_TARGET =       "Target";
        const string STR_PREFIX_ACCELERATION = "Acc";



        const string STR_TARGET        = "Target velocity";
        const string STR_ACCELERATION  = "Profile acceleration";


        private string parms;   // The parameter string


        private bool targetvelocityDefined;
        private Int32 targetvelocity;

        private bool accelerationDefined = false;
        private UInt32 acceleration;


        private UserTextbox UserTextboxTargetvelocity;
        private UserTextbox UserTextboxAcceleration;



        public VelocitymoveSequence()    // The constructor
        {
            name = "Velocity move";
            mnemonic = "VelocityMove";
        }

        public override void Init()
        {

            UserTextboxTargetvelocity = new UserTextbox(Globals.mainform.lblName1, STR_TARGET, Globals.mainform.txtParm1, 0, Int32.MinValue, UInt32.MaxValue, Globals.mainform.lblUnits1, Globals.STR_COUNTS_PER_SEC, false);
            UserTextboxAcceleration = new UserTextbox(Globals.mainform.lblName2, STR_ACCELERATION, Globals.mainform.txtParm2, 0, 0, UInt32.MaxValue, Globals.mainform.lblUnits2, Globals.STR_COUNTS_PER_SEC2, false);
            strComment = "";
            strError = "";
        }

        public override void Exit()
        {
            UserTextboxTargetvelocity = null;
            UserTextboxAcceleration = null;
        }


 
        public override void SetParameters(string parameters)
        {
            parms = parameters.Trim();  // Copy to local parameters
            string parameterName;
            string parameterValue;

            targetvelocityDefined = false;
            accelerationDefined = false;

            if (parms == "")       // Then use the default set of parameters
            {
                targetvelocity = 0;
                targetvelocityDefined = false;
            }
            else
            {
                for (int i = 0; i < Globals.utils.GetNumParameters(parms); i++)
                {
                    parameterName  = Globals.utils.GetParameterName(parameters, i);
                    parameterValue = Globals.utils.GetParameterValue(parameters, i);

                    switch (parameterName)
                    {

                        case STR_PREFIX_TARGET:
                            if (parameterValue != "")
                            {
                                targetvelocity = (Int32)Globals.utils.ConvertStringToInt64(parameterValue);
                                targetvelocityDefined = true;
                            }
                            break;


                        case STR_PREFIX_ACCELERATION:   // This is an optional parameter
                            if (parameterValue != "")
                            {
                                acceleration = (UInt32)Globals.utils.ConvertStringToInt64(parameterValue);
                                accelerationDefined = true;
                            }
                            break;

                        default:
                            strError = "Unknown parameter \"" + parameterName + "\".";
                            break;
                    }
                }
            }
            CreateCode();
        }


        public override void ShowParametersInGui()
        {

            Globals.utils.HideAll();


            UserTextboxTargetvelocity.Show(0);
            UserTextboxAcceleration.Show(1);

            //
            // Now switch the position and visibility of all selections dependant on homing mode
            //

            UserTextboxTargetvelocity.SetValue(targetvelocityDefined ? targetvelocity.ToString() : "");
            UserTextboxAcceleration.SetValue(accelerationDefined ? acceleration.ToString() : "");
            Globals.mainform.txtComment.Text = strComment;
        }

        public override void UserInputHasChanged(Object sender)
        {

        }


        public override void UpdateParametersFromGui()
        {

            targetvelocityDefined = UserTextboxTargetvelocity.HasValue();
            if (targetvelocityDefined)
            {
                targetvelocity = (Int32)UserTextboxTargetvelocity.Value();
            }
            
            accelerationDefined = UserTextboxAcceleration.HasValue();
            if (accelerationDefined)
            {
                acceleration = (UInt32)UserTextboxAcceleration.Value();
            }


            strComment = Globals.mainform.txtComment.Text;
            CreateCode();
        }





        public override string GetParameterString()
        {

            string temp = STR_PREFIX_TARGET + "=";

            if (targetvelocityDefined)
            {
                temp = temp + targetvelocity.ToString();
            }

            temp = temp + Globals.STR_COMMA + STR_PREFIX_ACCELERATION + "=";
            if (accelerationDefined)
            {
                temp = temp + acceleration.ToString();
            }
            
            return temp;            
        }

        private void CreateCode()
        {
            string strtemp = Globals.utils.GetSourceString(mnemonic, GetParameterString(), strComment);

            instructions.Clear();

            if (accelerationDefined)
            {
                instructions.Add(new WriteInstruction(0x6083, 0, (int)acceleration, strtemp));      // Profile acceleration
                instructions.Add(new WriteInstruction(0x6084, 0, (int)acceleration));      // Profile deceleration
                strtemp = "";
            }

            CreateCodeForSystemMacro(Bios.callnumber.VELOCITY_MOVE, strtemp + "Set profile velocity mode first. ");                        // Need to set Profile velocity mode first
            if (targetvelocityDefined)
            {
                instructions.Add(new WriteInstruction(0x60ff, 0, (int)targetvelocity, strtemp + "Apply velocity"));    // Set target velocity will activate profile velocity mode
            }
            else
            {
                instructions.Add(new WriteInstruction(0x2c01, 22, 0x60ff, "Velocity not defined: Get target velocity in ACCUM"));
                instructions.Add(new WriteInstruction(0x2c01, 21, 0x60ff, "Apply target velocity"));
            }
        }
    }
}
