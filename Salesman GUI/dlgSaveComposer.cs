﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public partial class dlgSaveComposer : Form
    {
        public string LastAnswer = "";
        private bool bDontaskagain = false;
        public dlgSaveComposer()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            LastAnswer = "Yes";
            DialogResult = DialogResult.Yes;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            LastAnswer = "No";
            DialogResult = DialogResult.No;
        }

        public bool DonotShowAgain()
        {
            return bDontaskagain;
        }

        private void btnNoAndDoNotAskAgain_Click(object sender, EventArgs e)
        {
            LastAnswer = "No";
            bDontaskagain = true;
            DialogResult = DialogResult.No;
        }
        
    }
}
