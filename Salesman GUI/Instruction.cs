﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Salesman_GUI
{
    public class Instruction
    {
      
        protected int m_target = 0;     // Not implemented yet, target = 0 means address all nodes on th bus
        protected int m_index = 0;
        protected int m_subindex = 0;
        protected char m_direction = 'W';
        protected int m_value = 0;
        protected string m_comment = "";

        public string GetString()
        {
            string temp;
            temp = m_target.ToString() + " " + m_direction.ToString() + " 0x" + m_subindex.ToString("X2") + m_index.ToString("X4");
            if (m_direction == 'W')
            {
                temp += " " + m_value.ToString();
            }
            if (m_comment != "")
            {
                temp += " // " + m_comment;
            }
            return temp;
        }

        public string GetMacroCommandString()
        {
            //
            // To prevent errors (during runtime) some checks are done to make sure that the objects exist
            // in the object dictionary and that the read/write permissions are ok
            //
            string macrostring;
            int size = 0;
            bool bOk = Globals.dictionary.ObjectExists(m_index, m_subindex);
            switch (m_direction)
            {
               case 'R':
                   bOk = bOk && Globals.dictionary.ObjectHasReadAccess(m_index, m_subindex);
                   break;
               case 'W':
                   bOk = bOk && Globals.dictionary.ObjectHasWriteAccess(m_index, m_subindex);
                   break;
               default:
                   // TODO Error message (can never happen)
                   bOk = false;
                   break;
            }
            size = Globals.dictionary.ObjectSize(m_index, m_subindex);
            bOk = bOk && (size >= 0);
            macrostring = m_target.ToString();
            macrostring += " W 0x032C05 ";
            if (bOk)
            {
                macrostring += "0x" + m_subindex.ToString("X2");
                macrostring += m_index.ToString("X4");
                macrostring += size.ToString("X1");
                if (m_direction == 'R')             // read command
                {
                    macrostring += "1";             // 1 means read
                    macrostring += "00000000";      // data is 0 on read
                }
                else                                // write command
                {
                    macrostring += "0";             // 0 means write
                    switch (size)
                    {
                        case 1:
                            m_value &= 0xFF;
                            break;
                        case 2:
                            m_value &= 0xFFFF;
                            break;
                        default:                    // 4 or more bytes, no masking necessary
                            break;
                    }
                    macrostring += m_value.ToString("X8");
                }
                if (m_comment != "")
                {
                    macrostring += " // " + m_comment;
                }
            }
            else // Then some check failed: Generate error message
            {
                MessageBox.Show("Object 0x" + m_index.ToString("X4") + "sub0x" + m_subindex.ToString("X2") + " Does not exist or does not have the requested access rights"); 
                macrostring += "0x0000000000000000";
            }
            return macrostring;
        }

        public string GetTextString()
        {

            return GetMacroCommandString() + ((m_comment == "") ? "" : "\t//" + m_comment); 
        }
     }



    class ReadInstruction : Instruction
    {
        public ReadInstruction(int index, int subindex, string comment)
        {
            m_index = index;
            m_subindex = subindex;
            m_comment = comment;
            m_direction = 'R';
        }
        public ReadInstruction(int index, int subindex)
        {
            m_index = index;
            m_subindex = subindex;
            m_comment = "";
            m_direction = 'R';
        }

    }

    class WriteInstruction : Instruction
    {
        public WriteInstruction(int index, int subindex, int value, string comment)
        {
            m_index = index;
            m_subindex = subindex;
            m_value = value;
            m_comment = comment;
            m_direction = 'W';
        }

        public WriteInstruction(int index, int subindex, int value)
        {
            m_index = index;
            m_subindex = subindex;
            m_value = value;
            m_comment = "";
            m_direction = 'W';
        }

    
    }
}
